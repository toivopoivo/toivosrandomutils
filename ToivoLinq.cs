using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ToivonRandomUtils;



using System.Linq;
using System.Text;

namespace ToivoLinq
{
	public static class ToivoLinq
	{
		public static List<T2> SelectImmediate<T1,T2> (this IEnumerable<T1> queryTarget, System.Func<T1,T2> query)
		{
		
			var list = new List<T2> ();
			foreach (var item in queryTarget) {
				try {
					list.Add (query (item));
				} catch (System.Exception ex) {
					throw ex;
				}
			}
			return list;
		}

		public static List<T2> SelectImmediateNonDefault<T1,T2> (this IEnumerable<T1> queryTarget, System.Func<T1,T2> query)
		{
			var list = new List<T2> ();
			foreach (var item in queryTarget) {
				try {
					var evaluated = query (item);
					if (!object.Equals (evaluated, default(T2))) {
						list.Add (evaluated);

					}
				} catch (System.Exception ex) {
					throw ex;
				}
			}
			return list;

		}
// use built in from now on		
#if  !UNITY_2022_1_OR_NEWER
		public static HashSet<T1> ToHashSet<T1>(this IEnumerable<T1> ienum)
		{
			var hs = new HashSet<T1>();
            foreach (var item in ienum)
            {
				hs.Add(item);
            }
			return hs;
		}
#endif
		/// <summary>
		/// throws error if duplicates
		/// </summary>
		public static HashSet<T1> CheckForDuplicates<T1>(this IEnumerable<T1> ienum)
		{
			var hs = new HashSet<T1>();
			foreach (var item in ienum)
			{
				if (hs.Contains(item))
				{
					throw new SystemException("duplicate check fail");
				}
				hs.Add(item);
			}
			return hs;
		}
		public static List<T1> WhereImmediate<T1> (this IEnumerable<T1> queryTarget, System.Func<T1,bool> query)
		{
			var list = new List<T1> ();
			foreach (var item in queryTarget) {
				try {
					if (query (item)) {
						list.Add (item);
					}
				} catch (System.Exception ex) {
					throw ex;
				}
			}
			return list;
		}

        public static T1 FirstImmediate<T1>(this IList<T1> queryTarget, System.Func<T1, bool> query)
        {
            for (int i = 0; i < queryTarget.Count; i++)
            {
                if (query(queryTarget[i])){
                    return queryTarget[i];
                }
            }
            throw new System.InvalidOperationException();
        }

		public static KeyValuePair<T1, float> SelectMin<T1>(this IEnumerable<T1> queryTarget, System.Func<T1, float> query)
		{
			var result = SelectMinOrDefault(queryTarget, query);
			if (result.Key == null) throw new System.Exception("no items");
			return result;
		}
		public static KeyValuePair<T1,float>  SelectMinOrDefault<T1> (this IEnumerable<T1> queryTarget, System.Func<T1,float> query)
		{
			return MinOrMaxItemOrDefaultKey (queryTarget, query, -1);
		}

		public static KeyValuePair<T1,float>  ImmediateMaxItemOrDefaultKey<T1> (this IList<T1> queryTarget, System.Func<T1,float> query)
		{
			return MinOrMaxItemOrDefaultKey (queryTarget, query, 1);
		}

		static KeyValuePair<T1,float>  MinOrMaxItemOrDefaultKey<T1> (this IEnumerable<T1> queryTarget, System.Func<T1,float> query, int dir)
		{
			KeyValuePair<T1,float> lastBiggest = new KeyValuePair<T1,float> (default(T1), float.MinValue);
            foreach (var item in queryTarget)
            {
				try {
					var evaluatedValue = query (item) * dir;
					if (evaluatedValue > lastBiggest.Value) {
						lastBiggest = new KeyValuePair<T1,float> (item,evaluatedValue);
					}
				} catch (System.Exception ex) {
					throw ex;
				}
			}
			return new KeyValuePair<T1,float> (lastBiggest.Key, lastBiggest.Value * dir);

		}

		static void SwapDown<T>(IList<T> list, int atIndex)
		{
			var temp = list[atIndex - 1];
			list[atIndex - 1] = list[atIndex];
			list[atIndex] = temp;
		}
		/// <summary>
		///  imagine this
		///  you want a sniper to target big monsters and not small ants
		///  and if there are no big monsters, then atleast pick the closest ant
		/// 
		///  so as the first scoring function you'd pass its size AS A NEGATIVE
		///  and then as second scoring function the distance
		///  
		///  yeah so to make this clear, pass scorers in ORDER of their importance
		///  
		/// video on how every scoring function is evaluated
		/// https://drive.google.com/file/d/1un7wil8AOX2KMkfH0_Mg13Y5nHg4hN7W/view?usp=sharing
		/// 
		/// also basically zero garbage... (except some anon func relating)
		/// x
		/// </summary>
		/// <typeparam name="T"> </typeparam>
		/// <param name="sortedList">the list that is sorted.  NOTE: this function will actually manipulate the list order in ARBITRARY WAY.</param>
		/// <param name="scoringFunctions">0 score is besst score (ascending order)</param>
		/// <returns>will always return something</returns>
		public static T BestOfBestest<T>(this IList<T> sortedList, params System.Func<T, int>[] scoringFunctions)
		{
			if (sortedList.Count == 0) return default;
			if (sortedList.Count == 1) return sortedList[0];


			int disqualified = 0;
			for (int scoringI = 0; scoringI < scoringFunctions.Length; scoringI++)
			{

				int smallestScoreForCurrentLevel = int.MaxValue;
				int bestCount = 0;
				for (int i = disqualified; i < sortedList.Count; i++)
				{
					int currentValue = scoringFunctions[scoringI].Invoke(sortedList[i]);
					if (smallestScoreForCurrentLevel > currentValue)
					{
						bestCount = 1;
						smallestScoreForCurrentLevel = currentValue;
					}
					else if (smallestScoreForCurrentLevel == currentValue)
					{
						bestCount++;
					}
					else
					{
						for (int x = 0; x < bestCount; x++)
						{
							SwapDown(sortedList, i - x);
						}
					}
				}

				disqualified = (sortedList.Count - bestCount);

			}

			return sortedList[sortedList.Count - 1];
		}
	}
}









