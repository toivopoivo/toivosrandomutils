﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
public class MenuItemActionHandler : EditorWindow
{
	public List<object> menuComds = new List<object> ();
	public System.Action<object> commandFunction;
	public bool actionDone;

	public static MenuItemActionHandler Init (System.Action<object> commandFunc)
	{

		var menuItemActionHandler = EditorWindow.GetWindow (typeof(MenuItemActionHandler)) as MenuItemActionHandler;
		menuItemActionHandler.commandFunction = commandFunc;
		menuItemActionHandler.currentRoutine = menuItemActionHandler.Routining ();
		return menuItemActionHandler;

	}

	IEnumerator currentRoutine;

	void Update ()
	{

		if (!currentRoutine.MoveNext ()) {
			Close ();
		}
//		Repaint ();
	}

	void OnGUI ()
	{
		GUILayout.Label (str);
	}

	string str;

	IEnumerator Routining ()
	{
		yield return null;
		for (int i = 0; i < menuComds.Count; i++) {
			str = ("item " + i / menuComds.Count);
			commandFunction (menuComds [i]);
			Repaint ();

		}
		actionDone = true;
	}
}
#endif