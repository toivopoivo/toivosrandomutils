﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using ToivonRandomUtils;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// based on this video
/// https://www.youtube.com/watch?v=z9b5aRfrz7M
/// </summary>
public class SendToForms : MonoBehaviour, IOnInspectorGUI {

    [Header("ctrl+f 'form action' in html for the form ")]
    public string formActionUrl;
    public List<SheetsFormField> formFields;
    /// <summary>
    /// </summary>
    /// <param name="data">dictionary (humanreadablename of field, value of field. all fields must be present</param>
    public void PostDataToForms(Dictionary<string, string> data)
    {
        StaticPostDataToForms(formFields.ToDictionary(x => "" + x.entryId, x => data[x.humanReadableName]), formActionUrl);
    }

    public static void StaticPostDataToForms(Dictionary<string, string> idToData, string formActionUrl)
    {
        if (!Application.isPlaying)
            throw new SystemException();
        InvokeAction.CustomCoroutine(PostingData(formActionUrl, idToData), true);
    }

    static IEnumerator PostingData(string formActionUrl, Dictionary<string, string> idToData)
    {
        WWWForm form = new WWWForm();

        foreach (var item in idToData)
        {
            var itemKey = item.Key;
            form.AddField("entry." + itemKey, item.Value ?? "");
        }

        using (UnityWebRequest www = UnityWebRequest.Post(formActionUrl, form))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.LogWarning("Web request Error: " + www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
            }
        }
    }
    


    [System.Serializable]
    public class SheetsFormField
    {
        public string humanReadableName;
        [Header("found inside the name attribute of forms input field html for field (entry.000000000)")]
        public int entryId;
    }

    public void OnInspectorGUI()
    {
        if (GUILayout.Button("test sending"))
        {
            PostDataToForms(formFields.Select(x => x.humanReadableName).ToDictionary(x => x, x => "testing"));
            Debug.Log("sendeded");  
        }
    }
}
