﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GetTypeInChildren : MonoBehaviour
{

	List<Component> comps = new List<Component> ();

	public bool permissiveContainsMode;
	public string search = "";


	[HideInInspector]
	public string result;
	[HideInInspector]
	public string selectedType;

	[HideInInspector]
	public List<string> manyTypes;



	System.Text.StringBuilder sb = new System.Text.StringBuilder ();
	IEnumerator<string> currentMatching;
	string lastSearch;
	ToivonRandomUtils.RandomUtils.MatchData matchData;

	#region IOnInspectorGUI implementation



	public void InpspectorMoveNext ()
	{
		if (currentMatching != null && search == lastSearch) {
			System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch ();
			stopwatch.Start ();
			while (true) {
				if (stopwatch.ElapsedMilliseconds > 100) {
					GUILayout.Button ("evaluating... press button to make go faster");
					return;
				}
				if (!currentMatching.MoveNext ()) {
					break;
				}
			}
			selectedType = currentMatching.Current;
			currentMatching = null;
			result = matchData.resultData.ToString();
		}
		GetComponentsInChildren<Component> (true, comps);
		if (permissiveContainsMode) {
			manyTypes = comps.Where(x=> x != null).Select (x =>x.GetType ().Name).Where (X => X.Contains (search)).Distinct ().ToList ();
			if (manyTypes.Any ()) {
				sb.Length = 0;
				for (int i = 0; i < (manyTypes.Count > 10 ? 10 : manyTypes.Count); i++) {
					sb.Append (string.Format ("{0}, ", manyTypes [i]));
				}
				GUILayout.Label (sb.ToString ());
			}

		} else {
			if (lastSearch != search) {
				lastSearch = search;
				matchData = new ToivonRandomUtils.RandomUtils.MatchData (comps.Select (x => x.GetType ().Name).ToList (), search, true);
				currentMatching = ToivonRandomUtils.RandomUtils.CheckMatching (matchData);
			}
			GUILayout.Label ("result: " + result);
			GUILayout.Label ("selectedType: " + selectedType);
		}
		if (GUILayout.Button ("Select and destroy this")) {
			#if UNITY_EDITOR
			GetComponentsInChildren<Component> (true, comps);
			var currentSelection = UnityEditor.Selection.objects.ToList ();
			currentSelection.Remove (gameObject);
			if (permissiveContainsMode) {
				currentSelection.AddRange (comps.Where (x => manyTypes.Contains (x.GetType ().Name)).Select (x => x.gameObject).Cast<Object> ());
			} else {
				currentSelection.AddRange (comps.Where (x => x.GetType ().Name == selectedType).Select (x => x.gameObject).Cast<Object> ());
			}
			UnityEditor.Selection.objects = currentSelection.ToArray ();
			DestroyImmediate (this);
			#endif
		}

	}

	#endregion
}
