﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class Polycount : MonoBehaviour, IOnInspectorGUI
{
	public void OnInspectorGUI()
	{
#if UNITY_EDITOR
		UnityEditor.EditorGUILayout.HelpBox("triangles: "+ GetComponentsInChildren<MeshFilter>().Sum(X => X.sharedMesh.triangles.Length / 3), MessageType.Info );
#endif
	}
}
