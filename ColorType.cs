using System.Linq;
using UnityEngine;

public enum ColorType {
	Red,
	Blue,
	Green,
	Yellow,
	Black,
	White,
	Grey,
	Brown,
	DarkBrown,
	Pink
}


public static class ColorTypeUtility
{
    public static Color NiceUIRandomColor()
    {
        return Color.Lerp(ColorType2Color((ColorType)UnityEngine.Random.Range(0, System.Enum.GetValues(typeof(ColorType)).Length)), Color.gray, 0.5F);
    }
	public static Color ColorType2Color (ColorType colorType)
	{
		Color color;
		var brown = color = Color.Lerp (Color.red, Color.green, 0.5f);
		switch (colorType) {
		case ColorType.Black:
			color = Color.black;
			break;
		case ColorType.Red:
			color = Color.red;
			break;
		case ColorType.Blue:
			color = Color.blue;
			break;
		case ColorType.Green:
			color = Color.green;
			break;
		case ColorType.Yellow:
			color = Color.yellow;
			break;
		case ColorType.White:
			color = Color.white;
			break;
		case ColorType.Grey:
			color = Color.grey;
			break;
		case ColorType.Brown:
			color = brown;
			break;
		case ColorType.DarkBrown:
			color = Color.Lerp (brown, Color.black, 0.5f);
			break;
		case ColorType.Pink:
			color = Color.Lerp (Color.white, Color.red, 0.5f);
			break;
		
		}
		return color;
	}

	public static Color NumberToColor(int number)
    {
		Color color = Color.white;
		var colortypes = System.Enum.GetValues(typeof(ColorType)) as ColorType[];
		int indexOffset = 0;
        for (int i = 0; i < (float)number; i+= colortypes.Length)
		{ 
            for (int j = 0; j < (number + indexOffset) % colortypes.Length; j++)
            {
				color = Color.Lerp(color, ColorType2Color((ColorType)j), 0.5f);
            }
			indexOffset++;
        }
		return color;
    }
}