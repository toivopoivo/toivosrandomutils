﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ToivonRandomUtils
{
    public class DelegateEqualityComparer<T> : IEqualityComparer<T>
    {
        System.Func<T,T, bool> comparisonDelegate;

        public DelegateEqualityComparer(Func<T, T, bool> comparisonDelegate)
        {
            this.comparisonDelegate = comparisonDelegate;
        }

        public bool Equals(T x, T y)
        {
            return comparisonDelegate(x, y);
        }

        public int GetHashCode(T obj)
        {
            return obj.GetHashCode();
        }
    }

    public class DelegateComparer<T> : IComparer<T>
    {
        System.Func<T, System.IComparable> comparisonDelegate;
        public int Compare(T x, T y)
        {
            return comparisonDelegate(x).CompareTo(comparisonDelegate(y));
        }
        public DelegateComparer(System.Func<T, System.IComparable> comparisonDelegate)
        {
            this.comparisonDelegate = comparisonDelegate;
        }

    }
    public static class QuickSortUtility
    {

       
        public static void Quicksort<T>(this List<T> elements, System.Func<T, System.IComparable> getComparable)
        {
            DelegateComparer<T> delegateComparer = new DelegateComparer<T>(getComparable);
            elements.Sort(delegateComparer);
        }

    }
}
