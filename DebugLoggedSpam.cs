﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DebugLoggedSpam : MonoBehaviour
{
	public static DebugLoggedSpam instance;
	public List<string> spam = new List<string> ();

	public bool letAllThrough = false;

	void Awake ()
	{
		instance = this;
	}
}

public static class LogFiltered
{
	//public static bool letAllThrough = false;

	public static void LogError (object spam, Object context = null)
	{
		LogPrivate (spam, true, context);
	}

	public static void Log (object spam, Object context = null)
	{
		LogPrivate (spam, false, context);
	}

	static void LogPrivate (object spam, bool error, Object context)
	{
		
		if (DebugLoggedSpam.instance == null) {
			var debugSpamListener = new GameObject ();
			debugSpamListener.name = "debugSpamListener";
			debugSpamListener.AddComponent<DebugLoggedSpam> ();
		}
		if (DebugLoggedSpam.instance.spam.Contains (spam.ToString ()) && !DebugLoggedSpam.instance.letAllThrough) {
			return;
		} else {
			if (error) Debug.LogError (spam, context);
			else Debug.Log (spam, context);
			DebugLoggedSpam.instance.spam.Add (spam.ToString ());
		}
	}
}