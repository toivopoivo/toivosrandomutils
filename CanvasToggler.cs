using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasToggler : MonoBehaviour
{

    private void OnGUI()
    {
        var findObjectsOfType = FindObjectsOfType<Canvas>();
        int yOffset = 30;
        Rect boxRect = new Rect(200, 200, 200, yOffset*findObjectsOfType.Length + 20);
        GUI.Box(boxRect, "close: ESC");

        foreach (Canvas canvas in findObjectsOfType)
        {
            canvas.enabled = GUI.Toggle(new Rect(boxRect.x + 10, boxRect.y + yOffset, 180, 20), canvas.enabled, canvas.name);
            yOffset += 25;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Destroy(gameObject);
        }
    }
}
