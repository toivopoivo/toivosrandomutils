using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

namespace ToivonRandomUtils
{
	public class Notification : MonoBehaviour
	{
		public static HashSet<Notification> notificationGraveyard = new HashSet<Notification>();
		public static int notificationsCount;

		public Text text;

		public RectTransform rectTransform;
		void Update ()
		{
			if (mainCam != null) {
				text.transform.rotation =  transform.rotation = mainCam.transform.rotation;
			}
		}
        static Font font;
        public Camera mainCam;

        private void Start()
        {
            if (font == null) LoadFont();
            GetComponentInChildren<Text>().font = font;
        }

        public static Font LoadFont()
        {
	        return font = Resources.Load<Font>("Asap-Medium");
        }

        private void OnEnable()
        {
            DebugVisualisationRecorder.TryRegisterVisualisation(gameObject);
            notificationsCount++;

        }

        void OnDisable ()
		{
		//	Destroy (gameObject);
			notificationsCount--;
		}
	}



	/// <summary>
	/// toivo random utils territory, plx no rape 
	/// </summary>
	public static class SceneNotification
	{
		
		public static GameObject ShowNotification (this Vector3 position, string text, float time, Color color, float scale = 0.002f, Notification moveold = null)
		{
			return CreateNotification (null, position, text, time, color, scale, moveold);
		}

		public static GameObject ShowNotification (this Transform parent, Vector3 localPosition, string text, float time, Color color, float scale = 0.002f, Notification moveold = null)
		{
			return CreateNotification (parent, localPosition, text, time, color, scale, moveold);
		}
		
		public static void ShowFullScreenNotification(string msg ,float time)
		{
			var canvas = new GameObject("debug canvas").AddComponent<Canvas>();
			canvas.renderMode = RenderMode.ScreenSpaceOverlay;
			var textgo = new GameObject("text");
			textgo.transform.SetParent(canvas.transform,false);
			var rect =textgo.gameObject.AddComponent<RectTransform>();
			rect.anchorMin = new Vector2();
			rect.anchorMax = new Vector2(1,1);
			rect.sizeDelta = new Vector2();
			var textComp = textgo.transform.gameObject.AddComponent<UnityEngine.UI.Text>();
			textComp.text = msg;
			textComp.resizeTextMinSize = 8;
			textComp.font = Notification.LoadFont();
			textComp.resizeTextMaxSize = 100;
			textComp.resizeTextForBestFit = true;
			canvas.sortingOrder = 9999;
			UnityEngine.GameObject.Destroy(canvas.gameObject, time);
		}

		public static IEnumerator VisualiseFuckOutOfMesh (this Mesh mesh, float time)
		{
			Debug.Log ("showing vertices");
			yield return null;
			for (int i = 0; i < mesh.vertices.Length; i++) {
				mesh.vertices [i].ShowNotification ("" + i, time, Color.yellow);
				yield return null;
			}
			Debug.Log ("showing triangles");
			yield return null;
			for (int i = 2; i < mesh.triangles.Length; i += 3) {
				mesh.vertices [i].ShowNotification ("" + i, time, Color.yellow);
				mesh.vertices [i - 1].ShowNotification ("" + (i - 1), time, Color.yellow);
				mesh.vertices [i - 2].ShowNotification ("" + (i - 2), time, Color.yellow);
				yield return null;

			}

		}

		//rape
		public static bool devtoolshudon = true;

		private static GameObject notificationParent;

		/// <summary>
		/// Creates the notification. if transform is specfied, position will be localspace
		/// </summary>
		static GameObject CreateNotification (Transform parent, Vector3 position, string text, float time, Color color, float scale, Notification createdNotification = null)
		{
			//rape
			if (!devtoolshudon) {
				Debug.Log ("Not showing scene notification, hud is off" + text);
				return null;
			}


			if (createdNotification == null && Notification.notificationGraveyard.Count > 0 )
			{
				Notification.notificationGraveyard.Remove(null);
				// most garbage free way ?
				foreach (var first in Notification.notificationGraveyard)
				{
					createdNotification = first;
					//	if(!createdNotification.gameObject.activeSelf) createdNotification.gameObject.SetActive(true);
					break;
				}

				Notification.notificationGraveyard.Remove(createdNotification);
			}


			if (createdNotification == null) {
				if (notificationParent == null) {
					notificationParent = new GameObject ("Notification parent");
				}
				var go =  new GameObject ();
				go.transform.SetParent (notificationParent.transform);
				go.name = $"notification {Notification.notificationsCount}";

				createdNotification = go.AddComponent<Notification> ();
				var canvas = go.AddComponent<Canvas> ();
				canvas.renderMode = RenderMode.WorldSpace;
				go.transform.localScale = Vector3.one * scale;
				//			go.transform.LookAt (Camera.main.transform);
				createdNotification.rectTransform = go.GetOrAddComponent<RectTransform> ();
				
				

				createdNotification.rectTransform.sizeDelta = new Vector2 (800f, 600f);
				var textGo = new GameObject ();
				textGo.name = "text";
				createdNotification.text  = textGo.AddComponent<Text> ();
			
				createdNotification.text.resizeTextMaxSize = 300;
				createdNotification.text.resizeTextForBestFit = true;
				RectTransform textRect = textGo.GetOrAddComponent<RectTransform> ();
				textRect.SetParent (createdNotification.rectTransform);
				textRect.ResetRectTrasform ();
				textRect.localScale = Vector3.one;

				var camera = Camera.main;
				if (camera != null)
				{
					createdNotification.mainCam = camera;
					textRect.localRotation = Quaternion.LookRotation ((position - camera.transform.position).MultiplyAxises (-1, 0, -1), Vector3.up);
				}
				else
				{
					Debug.LogError ("Tried to show notifications but no Main Camera", parent);
				}
			}
			
			createdNotification.text.text = text;
			createdNotification.text.color = color;
			
			createdNotification.StopAllCoroutines();
			createdNotification.StartCoroutine(Dispose (time, createdNotification) );
			
			if (parent == null) {
				createdNotification.rectTransform.position = position;
			} else {
				createdNotification.rectTransform.transform.SetParent (parent, false);
				createdNotification.rectTransform.localPosition = position;
			}
			if (time >= 2f)
			{
				var transformPosition = createdNotification.transform.position;
				createdNotification.StartCoroutine (TweenValue.TweenVector3 (transformPosition, transformPosition + Vector3.up, time, x => createdNotification.transform.position = x));
			}
			return createdNotification.gameObject;
		}
	
		static IEnumerator Dispose (float time, Notification n)
		{
			yield return new WaitForSeconds (time);
			//n.gameObject.SetActive(false);
			// enabling and disabling actually is performance overhead. thanks unity
			n.transform.position = Vector3.one * 9999f;
			n.text.color = n.text.color.SameDifferentAlpha(0);
			Notification.notificationGraveyard.Add(n);
		}
	}

}