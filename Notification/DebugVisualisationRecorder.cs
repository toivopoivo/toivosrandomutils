﻿using System.Collections.Generic;
using System.Linq;
using ToivoSyntaticSugars;
using UnityEngine;

namespace ToivonRandomUtils
{
    public class DebugVisualisationRecorder : MonoBehaviour, IOnInspectorGUI
    {
        public static DebugVisualisationRecorder instance;
        public Dictionary<int,Transform> recordedVisualisations = new Dictionary<int,Transform>();
        int updateTicks;
        public int scrollPos;
        
        [Range(0,1f)]
        public float slider;
        public Transform allFrames;
        void OnEnable()
        {
            instance = this;
            ClearAndReset();
        }

        [ToivonRandomUtils.InspectorButton]
        private void ClearAndReset()
        {
            if (allFrames != null) Destroy(allFrames.gameObject);
            recordedVisualisations.Clear();
            allFrames = new GameObject("all frames").FApply(x => x.transform.SetParent(transform)).transform;
            minFrame = int.MinValue;
            maxFrame = int.MaxValue;
        }

        int minFrame = int.MaxValue;
        int maxFrame = int.MinValue;
        public static void TryRegisterVisualisation(GameObject gameObject)
        {
            if (instance == null) return;
            instance.AddViz(gameObject);
        }
        public void AddViz(GameObject go)
        {
            if (go.GetComponentInParent<DebugVisualisationRecorder>()) return;
            if (!recordedVisualisations.ContainsKey(updateTicks))
            {
                recordedVisualisations.Add(updateTicks, new GameObject("frame " + updateTicks).transform.FApply(x => x.SetParent(allFrames)).FApply(x => x.gameObject.SetActive(false)));
            }
            if (updateTicks < minFrame) minFrame = updateTicks;
            if (updateTicks > maxFrame) maxFrame = updateTicks;
            Instantiate(go, recordedVisualisations[updateTicks]).FApply(x => x.GetComponentsInChildren<ActionTaskHandler>(true).ForEach(y => Destroy(y)));
        }
        public void Update()
        {
            updateTicks++;
        }
        float lastSlider;
        public void OnInspectorGUI()
        {
            if (!Application.isPlaying) return;
            if(slider != lastSlider)
            {
                var lerped = Mathf.Lerp(minFrame, maxFrame, slider);
                scrollPos = recordedVisualisations.OrderBy(x => Mathf.Abs(x.Key - lerped)).First().Key;
                lastSlider = slider;
            }
            allFrames.gameObject.SetActive(scrollPos >= 0);
            foreach (var item in recordedVisualisations)
            {
                item.Value.gameObject.SetActive(item.Key == scrollPos);
            }
        }

        [ToivonRandomUtils.InspectorButton]
        public void NextStep()
        {
            scrollPos++;
            lastSlider = slider = Mathf.InverseLerp(minFrame, maxFrame, scrollPos);
        }
        [ToivonRandomUtils.InspectorButton]
        public void PreviousStep()
        {
            scrollPos--;
            lastSlider = slider = Mathf.InverseLerp(minFrame, maxFrame, scrollPos);
        }
    }
}