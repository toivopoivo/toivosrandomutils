﻿namespace ToivoSyntaticSugars
{
    public static class ToivoSyntaticSugar
    {
        public static T FApply<T>(this T target, System.Action<T> action)
        {
            action(target);
            return target;
        }
    }
}