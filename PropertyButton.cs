﻿using UnityEngine;
using System.Collections;

public class PropertyButton : PropertyAttribute
{
	public string invokedMethodName;
	public System.Type type;
	/// <summary>
	/// NOTE: METHOD 1 PARAMETER can be UnityEngine.Object that gets you the parent fields owner Object
	/// 2 PARAMETER can be int that gets you the array index
	/// </summary>
	/// <param name="type">Type.</param>
	/// <param name="invokedMethodName">Invoked method name.</param>
	public PropertyButton (System.Type type, string invokedMethodName)
	{
		this.invokedMethodName = invokedMethodName;
		this.type = type;
	}
	
}
