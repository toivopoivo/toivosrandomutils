﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TierHierarchyItem : MonoBehaviour
{
	public Text label;
	public TierHierarchyItemData data;
	public RectTransform rectTrasform;
	public Image background;

	public void Refresh (TierHierarchyItemData data)
	{
		this.data = data;
		label.text = data.Name;
		background.color = data.Color;
	}

	void Reset ()
	{
		rectTrasform = GetComponent<RectTransform> ();
	}
}
