﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ToivonRandomUtils;

public class TierHierarchy : MonoBehaviour
{
	[ForceProperGoName]
	public TierHierarchyItem itemTemplate;
	[ForceProperGoName]
	public CanvasLineDrawer lineTemplate;
	public ITierHierarchyQueryData data;
	public List<TierHierarchyItem> instantiatedNodes;

	public Vector2 padding = Vector2.one * 0.1f;

	public List<CanvasLineDrawer> lineCloneList;

	void Start ()
	{
		editorDebugStepping &= Application.isEditor;
	}

	[ToivonRandomUtils.InspectorButton]
	void ClearAll ()
	{
		currentStep = 0;
		currentVisualising = null;
		instantiatedNodes.DestroyGameObjectsAndClear (true);
		lineCloneList.DestroyGameObjectsAndClear (true);
	}

	public bool editorDebugStepping;
	[NonEditableField (typeof(int))]
	public int currentStep;

	public void Visualise (ITierHierarchyQueryData data)
	{
		this.data = data;
		currentVisualising = Visualising (data);
		if (!editorDebugStepping)
			while (currentVisualising.MoveNext ()) {
			}

	}

	[ToivonRandomUtils.InspectorButton]
	void DebugStep ()
	{
		currentVisualising.MoveNext ();
		currentStep++;
	}


	public IEnumerator currentVisualising;

	IEnumerator Visualising (ITierHierarchyQueryData queryData)
	{
		if (editorDebugStepping)
			Debug.Log ("start");
		
		instantiatedNodes.DestroyGameObjectsAndClear (true);
		lineCloneList.DestroyGameObjectsAndClear (true);
		float cellHeight = 1f / (float)queryData.UniqueTiers.Count;
		var allItems = queryData.AllItems;
		List<List<TierHierarchyItemData>> tierGroups = new List<List<TierHierarchyItemData>> ();
		// changing this orderby you could probably make it go from up to down
		queryData.UniqueTiers = queryData.UniqueTiers.OrderBy (x => x).ToList ();
		yield return null;
		if (editorDebugStepping)
			Debug.Log (" items that ara dependended by items  2 levels or more above get promoted");
		int loops = 0;
		bool didSomething = true;
		while (didSomething) {
			loops++;
			if (editorDebugStepping)
				Debug.Log ("loop cycle" + loops);
			yield return null;
			if (loops > 1000) {
				throw new System.Exception ("infinite while detected");
			}
			didSomething = false;
			for (int i = 0; i < allItems.Count; i++) {
				var firstItemThatNeedsThis = allItems.Where (x => x.GetDependecies.SingleOrDefault (y => allItems [i].Item == y.Item) != null).OrderBy (x => x.visualTier).FirstOrDefault ();
				if (editorDebugStepping) {
					if (firstItemThatNeedsThis == null)
						Debug.Log (allItems [i].Name + " is not prequisity to anything");
					else {
						Debug.Log (allItems [i].Name + " is needed by " + firstItemThatNeedsThis.Name);
						Debug.Log (firstItemThatNeedsThis.Name + " visual tier is " + firstItemThatNeedsThis.visualTier);
						Debug.Log (allItems [i].Name + " is visual tier is " + allItems [i].visualTier);
					}
				}
				if (firstItemThatNeedsThis == null)
					continue;
				int uniqueTierIndex = queryData.UniqueTiers.IndexOf (firstItemThatNeedsThis.visualTier) - 1;
				if (editorDebugStepping)
					Debug.Log ("tier below " + firstItemThatNeedsThis.Name + " is " + queryData.UniqueTiers [uniqueTierIndex]);
				if (allItems [i].visualTier < queryData.UniqueTiers [uniqueTierIndex]) {
					if (editorDebugStepping)
						Debug.Log ("setting visual tier of " + allItems [i].Name + " from " + allItems [i].visualTier + " to " + queryData.UniqueTiers [uniqueTierIndex]);
					allItems [i].visualTier = queryData.UniqueTiers [uniqueTierIndex];
					if (editorDebugStepping)
						Debug.Log ("<color = green>promoted</color>");
					didSomething = true;
					break;
				} else {
					if (editorDebugStepping)
						Debug.Log ("<color = green>did nothing</color>");
				}

			}
		}

		yield return null;
		for (int i = 0; i < queryData.UniqueTiers.Count; i++) {
			tierGroups.Add (queryData.AllItems.Where (x => x.visualTier == queryData.UniqueTiers [i]).ToList ());
		}


		// create cells & set cell heights 

		int biggestTier = 0;
		Dictionary<TierHierarchyItemData, RectTransform> itemRectDic = new Dictionary<TierHierarchyItemData, RectTransform> ();
		for (int i = 0; i < tierGroups.Count; i++) {
			int j = 0;
			itemTemplate.gameObject.SetActive (true);
			foreach (var item in tierGroups[i]) {
				var tierHierarchyItem = itemTemplate.DoTheStandardCloningThing (instantiatedNodes, transform);
				tierHierarchyItem.Refresh (item);
				var goRectT = tierHierarchyItem.rectTrasform;
				item.cell = goRectT;
				item.cellX = i;
				item.cellY = j;
				goRectT.anchorMin = new Vector2 (0, (cellHeight * i));
				goRectT.anchorMax = new Vector2 (0, (cellHeight * (i + 1)));
				goRectT.name = item.Name;
				itemRectDic.Add (item, goRectT);
				j++;
			}
			itemTemplate.gameObject.SetActive (false);
			if (j > biggestTier) {
				biggestTier = j;
			}
		}

		// set cell widths

		float cellWidth = 1f / (biggestTier);

		for (int i = 0; i < tierGroups.Count; i++) {
			for (int j = 0; j < tierGroups [i].Count; j++) {
				RectTransform goRectT = itemRectDic [tierGroups [i] [j]];
				goRectT.anchorMin = new Vector2 (cellWidth * (j), goRectT.anchorMin.y);
				goRectT.anchorMax = new Vector2 (cellWidth * (j + 1), goRectT.anchorMax.y);
				goRectT.sizeDelta = Vector2.zero;
				goRectT.anchoredPosition = Vector2.zero;

			}
		}


		// create lines

		lineTemplate.gameObject.SetActive (true);
		for (int i = 0; i < instantiatedNodes.Count; i++) {
			List<TierHierarchyItemData> deps = instantiatedNodes [i].data.GetDependecies;
			for (int j = 0; j < deps.Count; j++) {
				var line = MakeLineBetweenToCells (
					           instantiatedNodes [i].data.cell,
					           deps [j].cell,
					           lineTemplate,
					           lineCloneList
				           );
				line.gameObject.name = "line: " + instantiatedNodes [i].data.Name + " -> " + deps [j].Name; 
				line.SetAsFirstSibling ();
				//				line.SetParent (instantiatedNodes [i].rectTrasform, false);

			}
		}
		lineTemplate.gameObject.SetActive (false);
		itemTemplate.gameObject.transform.SetAsFirstSibling ();
		lineTemplate.gameObject.transform.SetAsFirstSibling ();
	}

	public static RectTransform MakeLineBetweenToCells (RectTransform a, RectTransform b, CanvasLineDrawer lineTemplate, List<CanvasLineDrawer> linesList)
	{
		var createdLineDrawer = lineTemplate.DoTheStandardCloningThing (linesList, lineTemplate.transform.parent);
		var createdLine = createdLineDrawer.GetComponent<RectTransform> ();
		createdLine.anchorMax = AnchorMid (b);
		createdLine.anchorMin = AnchorMid (a);
		createdLine.sizeDelta = Vector2.zero;
		createdLine.anchoredPosition = Vector2.zero;
		createdLineDrawer.onUpdate = true;
		return createdLine;
	}

	static Vector2 AnchorMid (RectTransform rect)
	{
		return (rect.anchorMin + rect.anchorMax) * 0.5f;
	}

	public struct CellIndex
	{
		public int x;
		public int y;

		public CellIndex (int x, int y)
		{
			this.x = x;
			this.y = y;
		}
	}
}


public interface ITierHierarchyQueryData
{
	List<TierHierarchyItemData> AllItems{ get; }

	List<int> UniqueTiers{ get; set; }

}


public class TierHierarchyQueryData<T> : ITierHierarchyQueryData where T: class
{
	public List<TierHierarchyItemData<T>> allVisualisedItems = new List<TierHierarchyItemData<T>> ();
	public System.Func<T,IEnumerable<T>> getDependenciesFunc;
	public System.Func<T,string> getNameFunc;
	public System.Func<T, Color> getUiColorFunc;
	public System.Action<T> onLeftMouseClicked;
	public System.Action<T> onRightMouseClicked;
	public List<int> uniqueTiers = new List<int> ();

	public List<int> UniqueTiers { get { return uniqueTiers; } set { uniqueTiers = value; } }

	
	public TierHierarchyQueryData (List<T> allItems, System.Func<T, IEnumerable<T>> getDependenciesFunc, System.Func<T,string> getNameFunc, System.Func<T, Color> getUiColorFunc, System.Action<T> onLeftMouseClicked, System.Action<T> onRightMouseClicked)
	{

		#if UNITY_EDITOR
		try {
			allItems.ForEach (x => allItems.Single (y => y == x));
		} catch (System.Exception ex) {
			throw new System.ArgumentException ("all items must be unique " + ex);
		}
		#endif
		this.getUiColorFunc = getUiColorFunc;
		this.onLeftMouseClicked = onLeftMouseClicked;
		this.onRightMouseClicked = onRightMouseClicked;
		this.getNameFunc = getNameFunc;
		this.getDependenciesFunc = getDependenciesFunc;
		for (int i = 0; i < allItems.Count; i++) {
			var tierHierarchyItemData = new TierHierarchyItemData<T> (i, allItems [i], getDependenciesFunc (allItems [i]).ToList (), this);
			allVisualisedItems.Add (tierHierarchyItemData);
		}


		// get dependies recurse to calculate different tiers

		for (int i = 0; i < allVisualisedItems.Count; i++) {
			GetDependensies (allVisualisedItems, allVisualisedItems [i], ref allVisualisedItems [i].tier);
			allVisualisedItems [i].visualTier = allVisualisedItems [i].tier;
			if (!uniqueTiers.Contains (allVisualisedItems [i].tier)) {
				uniqueTiers.Add (allVisualisedItems [i].tier);
			}
		}



	}



	void GetDependensies (List<TierHierarchyItemData<T>>allitems, TierHierarchyItemData<T> currentItem, ref int tier)
	{
		tier += currentItem.dependencies.Count;
		for (int i = 0; i < currentItem.dependencies.Count; i++) {
			GetDependensies (allitems, allitems.Single (x => x.item == currentItem.dependencies [i]), ref tier);
		}
	}

	#region ITierHierarchyQueryData implementation

	public int ItemCount { get { return allVisualisedItems.Count; } }


	public List<TierHierarchyItemData> AllItems { get { return allVisualisedItems.Cast<TierHierarchyItemData> ().ToList (); } }

	#endregion
}

public abstract class TierHierarchyItemData
{
	public abstract	int Tier{ get; }

	public abstract string Name { get ; }

	public RectTransform cell;

	public int cellX, cellY;

	public abstract List<TierHierarchyItemData> GetDependecies{ get; }

	public int visualTier;

	public abstract object Item{ get ; }

	public abstract Color Color{ get; }

	public abstract void MouseLeftClickc ();

	public abstract void MouseRightClick ();

}

public class TierHierarchyItemData<T> : TierHierarchyItemData where T: class
{
	public int ID;
	public T item;
	public List<T> dependencies;
	public int tier;
	public TierHierarchyQueryData<T> parentData;


	public TierHierarchyItemData (int id, T item, List<T> dependencies, TierHierarchyQueryData<T> parentData)
	{
		this.parentData = parentData;
		this.ID = id;
		this.item = item;
		this.dependencies = dependencies;
	}

	#region implemented abstract members of TierHierarchyItemData

	public override int Tier { get { return tier; } }

	public override string Name { get { return parentData.getNameFunc (item); } }

	public override List<TierHierarchyItemData> GetDependecies { get { return parentData.allVisualisedItems.Where (x => dependencies.Contains (x.item)).Cast<TierHierarchyItemData> ().ToList (); } }


	public override object Item { get { return item as object; } }

	public override Color Color { get { return parentData.getUiColorFunc (item); } }

	public override void MouseLeftClickc ()
	{
		parentData.onLeftMouseClicked (item);
	}

	public override void MouseRightClick ()
	{

		parentData.onRightMouseClicked (item);
	}

	#endregion
}