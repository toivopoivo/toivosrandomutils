﻿using UnityEngine;
using System.Collections;
using System;
using ToivonRandomUtils;

public static class TweenValue
{
    
	public static IEnumerator Tween (float originalValue, float targetValue, float tweenTime, Action<float> setTargetValue, AnimationCurve curve = null, Action onFinish = null, bool unscaledTime = false)
	{
		float currentTime = 0;
		while (currentTime <= tweenTime) {
			currentTime += unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime;
			setTargetValue (Mathf.Lerp (originalValue, targetValue, (curve != null) ? curve.Evaluate (currentTime / tweenTime) : currentTime / tweenTime));
			yield return null;
		}
		if (onFinish != null) {
			onFinish ();
		}
	}

	public static IEnumerator TweenTransform2MatchPosNRot (Transform fromTrans, Transform toTrans, float time, Action onFinish = null, AnimationCurve curve = null)
	{
		float tweenProgress = 0;
		var tween = Tween (0f, 1f, time, x => tweenProgress = x, curve, onFinish);
		var ogFromTransPos = fromTrans.position;
		var ogFromTransRot = fromTrans.rotation;
		while (tween.MoveNext ()) {
			fromTrans.position = Vector3.Lerp (ogFromTransPos, toTrans.position, tweenProgress); 
			fromTrans.rotation = Quaternion.Slerp (ogFromTransRot, toTrans.rotation, tweenProgress);
			yield return null;
		}
		fromTrans.position = toTrans.position;
		fromTrans.rotation = toTrans.rotation;

	}

	public static IEnumerator Delay(float delay, IEnumerator executeAfterDelay)
	{
		yield return new WaitForSeconds(delay);
		yield return executeAfterDelay;
	}

	public static IEnumerator TweenVector3 (Vector3 from, Vector3 to, float time, Action<Vector3> setValue, Action onFinish = null, AnimationCurve curve = null)
	{
		if (curve == null)
			curve = AnimationCurve.EaseInOut (0f, 0f, 1f, 1f);
		return Tween (0f, 1f, time, (x) => setValue (Vector3.Lerp (from, to, x)), curve, onFinish);
	}

	public static IEnumerator TweenVector2 (Vector2 from, Vector2 to, float time, Action<Vector2> setValue, Action onFinish = null, AnimationCurve curve = null)
	{
		if (curve == null)
			curve = AnimationCurve.EaseInOut (0f, 0f, 1f, 1f);
		return Tween (0f, 1f, time, (x) => setValue (Vector2.Lerp (from, to, x)), curve, onFinish);
	}

	public static IEnumerator IntengerCumulator (int from, Func<int> getValue, Action<int> setValue, Action onFinish = null, bool infiniteCumulator = false)
	{
		int current = from;
		while (from != getValue () || infiniteCumulator) {
			int getVal = getValue ();
			if (current - getVal == 0)
				yield return new WaitForFixedUpdate ();
			else {
				int dir = -(current - getVal) / Mathf.Abs (current - getVal);
				current += dir;
				setValue (current);
				yield return new WaitForFixedUpdate ();
			}
		}
		if (onFinish != null) {
			onFinish ();
		}
	}

	public static IEnumerator TweenColor (Color from, Color to, float time, Action<Color> setValue, Action onFinish = null, AnimationCurve curve = null)
	{
		if (curve == null)
			curve = AnimationCurve.EaseInOut (0f, 0f, 1f, 1f);
		return Tween (0f, 1f, time, (x) => setValue (Color.Lerp (from, to, x)), curve, onFinish);
	}

	public static IEnumerator Wobbler (Vector3 origin, Vector3 limits, float frequencyTime, Action<Vector3> setValue, Func<Vector3>getCurrentPos, Func<bool> wobbleWhile, Action onCycleFinish = null, AnimationCurve curve = null)
	{
		while (wobbleWhile ()) {
			yield return TweenVector3 (getCurrentPos (), origin + limits.MultiplyAxises (UnityEngine.Random.insideUnitSphere), frequencyTime, setValue, onCycleFinish, curve);
		}
		yield return TweenVector3 (getCurrentPos (), origin, frequencyTime, setValue, onCycleFinish, curve);
	}

	public static IEnumerator PingPongFader (Action<float> setValue, float cycleTime, float minVal, float maxVal)
	{
		float time = cycleTime;
		int dir = -1;
		while (true) {
			if (time <= 0) {
				dir = 1;
			} else if (time >= cycleTime) {
				dir = -1;
			}
			time += Time.deltaTime * dir;
			setValue (Mathf.Lerp (minVal, maxVal, time / cycleTime));
			yield return new WaitForFixedUpdate ();
		}
	}
}
