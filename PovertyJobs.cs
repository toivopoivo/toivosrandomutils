﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace toivosrandomutils
{
    public class ThreadedLineProcessor<T>
    {
        private Action<int, T> jobFunc; // Pathfinding function
 
        private IProducerConsumerCollection<T> line;
        
        private bool debugSingleThread; // Debug flag
        private int workerCount; // Number of threads
        private volatile bool running = false;

        public ThreadedLineProcessor(IProducerConsumerCollection<T> line, Action<int, T> jobFunc, bool debugSingleThread = false)
        {
            this.line = line;
            this.jobFunc = jobFunc;
            this.debugSingleThread = debugSingleThread;
            ThreadPool.GetMaxThreads(out workerCount, out _); // Get system thread count
        }

        public void Run()
        {
            if (running) return;
            running = true;

            if (debugSingleThread)
            {
                WorkerLoop(0);
            }
            else
            {
                // Multi-threaded mode
                Parallel.For(0, workerCount, WorkerLoop);
            }
        }

        private void WorkerLoop(int threadIndex)
        {
            try
            {
                while (line.Count>0) // Keep running while there's work
                {
                    if(line.TryTake(out var t))
                        jobFunc(threadIndex, t);
                    else
                        // threading strangeness?
                        break;
                }
            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
           
        }
    }


    public class ThreadedLineProcessorTest
    {
        public static void Run()
        {
            Debug.Log("Single Thread");
            RunTest(true);
            Debug.Log("Multi Thread");
            RunTest(false);

        }

        private static void RunTest(bool singleThread)
        {
            int cycles = 0;
            ConcurrentQueue<string> strings = new ConcurrentQueue<string>(Enumerable.Range(0, 5).Select(x => x.ToString()).ToList());
            var processor = new ThreadedLineProcessor<string>(
                strings,
                (x, y) =>
                {
                    Debug.Log($"thread {x}: {y}");
                    cycles++;
                }, singleThread);
            processor.Run();
        }
    }
}