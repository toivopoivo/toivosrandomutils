﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
#if UNITY_EDITOR
using UnityEditor.Events;
#endif
using UnityEngine.UI;
public class SetSomethingActive : MonoBehaviour {
	public UnityEvent onSetActiveEvent;
	public GameObject something;
	public bool setItNotActiveOnStart;
	public void SetIt(){
		if(!something.activeSelf){
			something.gameObject.SetActive(true);
			if(onSetActiveEvent!=null)
				onSetActiveEvent.Invoke();
		}else 
			something.gameObject.SetActive(false);
	}
	void Start(){
		if(setItNotActiveOnStart)
			something.gameObject.SetActive(false);
	}
#if UNITY_EDITOR
    [ContextMenu("add on click ref")]
    public void AddOnClickRefernce() {
        var but = GetComponent<Button>();
        if (but != null)
        {
            UnityEventTools.AddPersistentListener(but.onClick, SetIt);
        }
    }
#endif       
}
