﻿using UnityEngine;
using System.Collections;

public interface IOnInspectorGUI 
{
	#if UNITY_EDITOR
	void OnInspectorGUI();
	#endif
}
public interface IOnInspectorGUIDragReceiver 
{
#if UNITY_EDITOR
	/// <summary>
	/// returns: acceptable 
	/// </summary>
	bool ContinueInspectorDrag(Object[] objects);
	void InspectorDragPerformed(Object[] objects);

	string GetDragMessage();
#endif
}


public interface IOnValidateSafe{
	#if UNITY_EDITOR
	void OnValidateSafe();
	#endif
}
