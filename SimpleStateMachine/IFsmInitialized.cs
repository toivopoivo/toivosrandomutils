﻿namespace SimpleFSMSpace
{
	public interface IFsmInitialized
	{
		void Initialize();
	}
}