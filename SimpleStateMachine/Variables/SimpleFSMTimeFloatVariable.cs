﻿using System;
using UnityEngine;

namespace SimpleFSMSpace
{
	public class SimpleFSMTimeFloatVariable : SimpleFSMNumericalVariable
	{
		public SimpleFSMUpdater.TimeType timeType;
		public override Type[] GetSuitableTypes()
		{
			return new[] {typeof(float)};
		}

		public override object GetValue()
		{
			return  timeType == SimpleFSMUpdater.TimeType.ScaledTime? Time.time : Time.unscaledTime;
		}

		public override void SetValue(object val)
		{
			throw new System.Exception();
		}


		public override float FloatValue { get => (float) GetValue(); set => SetValue(value); }
	}
}