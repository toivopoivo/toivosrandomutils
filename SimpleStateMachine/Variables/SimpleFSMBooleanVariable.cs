﻿using System;

namespace SimpleFSMSpace
{
	public class SimpleFSMBooleanVariable : SimpleFSMVariable
	{
		public bool isOn;


		public void ToggleOnAndTickStateMachine()
		{
			isOn = true;

			RippleUpdateFsm();
		}
		public void ToggleOffAndTickStateMachine()
		{
			isOn = false;
			RippleUpdateFsm();
		}

		public override Type[] GetSuitableTypes()
		{
			return new[] {typeof(bool)};
		}

		public override object GetValue()
		{
			return isOn;
		}

		public override void SetValue(object val)
		{
			isOn = (bool) val;
		}
	}
}
