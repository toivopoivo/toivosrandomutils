﻿using System;
using System.Collections;
using System.Collections.Generic;
using SimpleFSMSpace;
using UnityEngine;

public class SimpleFSMIntVariable : SimpleFSMNumericalVariable
{
   [SerializeField] int value;

   public override Type[] GetSuitableTypes()
   {
      return new[] {typeof(int)};
   }

   public override object GetValue()
   {
      return value;
   }

   public override void SetValue(object val)
   {
      value = (int) val;
   }

   public int Value
   {
      get => value;
      set => this.value = value;
   }

   public override float FloatValue { get => Value; set => Value = (int)value; }


  
}
