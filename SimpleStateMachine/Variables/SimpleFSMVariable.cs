﻿using UnityEngine;

namespace SimpleFSMSpace
{
	public abstract class SimpleFSMVariable : MonoBehaviour
	{
		public abstract System.Type[] GetSuitableTypes(); 
		public abstract object GetValue();
		public abstract void SetValue(object val);

		protected void RippleUpdateFsm()
		{
			
			var componentInParent = GetComponentInParent<SimpleFSM>();
			// no recursion pls
			if (!componentInParent.updating)
			{
				int iters = 0;
				while (componentInParent.UpdateStateMachine())
				{
					iters++;
					if(iters>100) throw new System.Exception("infinite state transition loop detected");
				}
			}
			
		}
	}

	public abstract class SimpleFSMNumericalVariable : SimpleFSMVariable
	{
		public abstract float FloatValue { get; set; }
	}
}