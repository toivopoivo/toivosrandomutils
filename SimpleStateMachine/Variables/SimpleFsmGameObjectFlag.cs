﻿using System;
using UnityEngine;

namespace SimpleFSMSpace
{
    public class SimpleFsmGameObjectFlag : SimpleFSMVariable
    {
        public GameObject targetGameObject;
        public override Type[] GetSuitableTypes()
        {
            return new[] {typeof(bool)};
        }

        public override object GetValue()
        {
            return targetGameObject.activeInHierarchy;
        }

        public override void SetValue(object val)
        {
            targetGameObject.gameObject.SetActive((bool)val);
        }
    }
}