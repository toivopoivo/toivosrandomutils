﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleFSMSpace
{
    public class SimpleFSMStringEnumVariable : SimpleFSMVariable
    {
        public string[] values;
        public int currentIndex = 0;
        public override Type[] GetSuitableTypes()
        {
            return new Type[]{typeof(int)};
        }

        public Dictionary<string, int> valueToIndex;
        private void OnEnable()
        {
            valueToIndex = new Dictionary<string, int>();
            for (int i = 0; i < values.Length; i++)
            {
                valueToIndex.Add(values[i], i);
            }
        }

        public override object GetValue()
        {
            return values[currentIndex];
        }

        public override void SetValue(object val)
        {
            currentIndex = (int)val;
        }

        public void ChangeToAndTickStateMachine(string value)
        {
            currentIndex = valueToIndex[value];
           RippleUpdateFsm();
        }
    }
}