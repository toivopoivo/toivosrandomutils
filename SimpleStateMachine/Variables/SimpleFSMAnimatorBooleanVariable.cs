﻿using System;
using UnityEngine;

namespace SimpleFSMSpace
{
	public class SimpleFSMAnimatorBooleanVariable : SimpleFSMVariable
	{
		public Animator animator;
		public string variableName;
	
		public override Type[] GetSuitableTypes()
		{
			return new[] {typeof(bool)};
		}

		public override object GetValue()
		{
			return animator.GetBool(variableName);
		}

		public override void SetValue(object val)
		{
			animator.SetBool(variableName, (bool) val);
		}

		public void SetTrue(){
			animator.SetBool(variableName,true);
		}
		public void SetFalse(){
			animator.SetBool(variableName,false);
		}
	}
}