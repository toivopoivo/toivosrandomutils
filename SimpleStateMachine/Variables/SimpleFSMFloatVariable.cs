﻿using System;
using UnityEngine;

namespace SimpleFSMSpace
{
	public class SimpleFSMFloatVariable : SimpleFSMNumericalVariable
	{
		[SerializeField]
		float value;


		public override Type[] GetSuitableTypes()
		{
			return new[] {typeof(float)};
		}

		public override object GetValue()
		{
			return value;
		}

		public override void SetValue(object val)
		{
			 value = (float)val;
		}

		public float Value
		{
			get
			{
				return value;
			}
			set
			{
				this.value = value;
			}
		}

		public override float FloatValue { get => Value; set => Value = value; }
	}
}