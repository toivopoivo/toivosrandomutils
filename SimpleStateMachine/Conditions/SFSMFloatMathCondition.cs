﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using SimpleFSMSpace;
using UnityEditor;
using UnityEngine;

public class SFSMFloatMathCondition : SimpleFSMCondition, IOnInspectorGUI
{
    public SimpleFSMNumericalVariable a;
    public SimpleFSMNumericalVariable b;
    public SimpleFSMNumericalVariable c;

    public OperandType operandType;
    public ComparisonType comparisonType;
    public enum OperandType
    {
        Minus
    }
    
    public enum  ComparisonType
    {
        IsLess,
        IsMore,
        IsEqual,
        IsLessOrEqual,
        IsMoreOrEqual,
        
    }
    protected override bool Eval()
    {
        float outcome = EvaluateOutcome(); 
        var value = c.FloatValue;
        return Evaluate(outcome, value, comparisonType);
    }

    public  static bool Evaluate(float outcome, float value, ComparisonType comparisonType)
    {
        switch (comparisonType)
        {
            case ComparisonType.IsLess:
                return outcome < value;
            case ComparisonType.IsMore:
                return outcome > value;
            case ComparisonType.IsEqual:
                // ReSharper disable once CompareOfFloatsByEqualityOperator
                return outcome == value;
            case ComparisonType.IsLessOrEqual:
                return outcome <= value;
            case ComparisonType.IsMoreOrEqual:
                return outcome >= value;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public float EvaluateOutcome()
    {
        switch (operandType)
        {
            case OperandType.Minus:
                return  a.FloatValue - b.FloatValue;  
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

 
    public new void OnInspectorGUI()
    {
#if UNITY_EDITOR
        base.OnInspectorGUI();
        UnityEditor.EditorGUILayout.HelpBox($"{a} {operandType} {b}  {comparisonType}  {c}", MessageType.Info);
#endif
    }

  
}
