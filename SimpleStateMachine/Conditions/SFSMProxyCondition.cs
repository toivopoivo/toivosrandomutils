﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using SimpleFSMSpace;
using UnityEditor;
using UnityEngine;

public class SFSMProxyCondition : SimpleFSMCondition
{
    public SimpleFSMConditionsGroup other;

    protected override bool Eval()
    {
        return other.Match();
    }


}
public class SFSMButtonClickedCondition : SimpleFSMCondition
{
    [NonEditableField(typeof(bool))]
    public bool clickedFlag;
    public UnityEngine.UI.Button button;

    private void OnEnable()
    {
        button.onClick.AddListener(OnClick);
    }

    private void OnClick()
    {
        clickedFlag = true;
    }

    private void OnDisable()
    {
        button.onClick.RemoveListener(OnClick);
    }

    protected override bool Eval()
    {
        if (clickedFlag)
        {
            clickedFlag = false;
            return true;
        }

        return false;

    }


}
