﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using SimpleFSMSpace;
using UnityEditor;
using UnityEngine;

public class SFSMStringEnumCondition : SimpleFSMCondition, IOnInspectorGUI
{
    public SimpleFSMStringEnumVariable stringEnumVariable;
    public string isValue;
    private string tempTextField;

    protected override bool Eval()
    {
        return stringEnumVariable.valueToIndex[isValue] == stringEnumVariable.currentIndex;
    }

    public override void OnInspectorGUI()
    {
#if UNITY_EDITOR
        ToivonRandomUtils.RandomUtils.EditorGUIFilterPopUp("pick value", stringEnumVariable.values, ref isValue, ref tempTextField, this);
#endif
        base.OnInspectorGUI();
    }
}
