using System;
using System.Collections;
using System.Collections.Generic;
using SimpleFSMSpace;
using UnityEngine;

public class SFSMActionFinishedCondition : SimpleFSMCondition
{
    public HandyComponentField actionComponent;
    private SimpleFSMAction action;

    private void Start()
    {
	   action =  (SimpleFSMAction) actionComponent.component;
    }


    protected override bool Eval()
    {
	    return action.IsCompleted();
    }
}
