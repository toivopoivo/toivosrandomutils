﻿using SimpleFSMSpace;
using UnityEngine;

public class SimpleFSMUpdateOnEnable : MonoBehaviour
{
	public SimpleFSM simpleFsm;
	private void OnEnable()
	{
		simpleFsm.UpdateStateMachine();

	}
}