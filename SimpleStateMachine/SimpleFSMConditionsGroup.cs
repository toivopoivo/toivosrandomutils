﻿using System.Collections.Generic;
using ToivoLinq;
using UnityEditor;
using UnityEngine;

namespace SimpleFSMSpace
{
	public  class SimpleFSMConditionsGroup : MonoBehaviour, IFsmInitialized, IOnInspectorGUI
	{
		public ConditionsEvaluationType evaluationType;
		public enum ConditionsEvaluationType
		{
			All,
			Any
		}
		private SimpleFSMCondition[] conditions;
		private KeyValuePair<bool, float> lastMatchResult;
		public bool Match()
		{
			bool result;
			if (evaluationType == ConditionsEvaluationType.All)
			{
				result = conditions.FastAll(x => x.Evaluate() == !x.invert);
			}
			else
			{
				result = conditions.FastAny(x => x.Evaluate() == !x.invert);
			}
			lastMatchResult = new KeyValuePair<bool, float>(result, Time.time);			
			return result;

		}

		public void Initialize()
		{
			conditions = GetComponents<SimpleFSMCondition>();
		}

		public void OnInspectorGUI()
		{
			if (Application.isPlaying)
			{
#if UNITY_EDITOR
				GUI.color = lastMatchResult.Key ? Color.green : Color.white;
				UnityEditor.EditorGUILayout.HelpBox("last match result: "+lastMatchResult.Key + " t: "+ lastMatchResult.Value + " delta "+ (Time.time - lastMatchResult.Value), MessageType.Info);
#endif
			}
		}
	}
}