﻿using ToivonRandomUtils;
using UnityEditor;
//using UnityEditor.Experimental.Networking.PlayerConnection;
using UnityEngine;

namespace SimpleFSMSpace
{
	public  class SimpleFSMTransition : MonoBehaviour, IFsmInitialized, IOnInspectorGUI, IVisualizableNodeTransition
	{
		public bool invertConditions;
		public SimpleFSMConditionsGroup conditions;
		public SimpleFSMState transitionTo;
	//	public GameObject transitionActions;
		
		public bool Evaluate()
		{
			if (conditions == null)
			{
				return true;
			}
			return conditions.Match()== !invertConditions;
		}

		public void Initialize()
		{
			if (transitionTo == null)
			{
				throw new System.Exception();
			}
		}

		public void OnInspectorGUI()
		{
			if (conditions == null)
			{
#if UNITY_EDITOR
				UnityEditor.EditorGUILayout.HelpBox("transition without condition is made always", MessageType.Warning);
#endif
			}
		}
		
#if UNITY_EDITOR
		[ContextMenu("CreateConditionGroup")]
		void CreateOnClickActions()
		{
			var go = new GameObject(gameObject.name + " to " + transitionTo.gameObject.name + " conditions");
			go.transform.SetParent(transform);
			var actGroup = go.AddComponent<SimpleFSMConditionsGroup>();
			conditions = actGroup;
			UnityEditor.Undo.RegisterCreatedObjectUndo(go, "asd");
			UnityEditor.Undo.RecordObject(this, "asd");
			UnityEditor.Selection.activeGameObject = go;
		}

#endif

		public string GetName()
		{
			return $"if {(invertConditions ? "NOT" : "")} ({conditions.NullSafeUnityObjName()}) transition to -> {GetTargetNode().GetName()}";
		}

		public IVisualizableNode GetTargetNode()
		{
			return transitionTo;
		}
	}
}