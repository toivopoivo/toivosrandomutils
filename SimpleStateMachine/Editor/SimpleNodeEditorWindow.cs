﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using SimpleFSMSpace;
using ToivonRandomUtils;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// based on tutorial: https://gram.gs/gramlog/creating-node-based-editor-unity/
/// </summary>
public class SimpleNodeEditorWindow : EditorWindow
{
//private SerializedObject serializedObject;
//private SerializedProperty settingsProp;
//public Settings settings = new Settings();

	private void OnEnable()
	{
		//	serializedObject = new SerializedObject(this);
		//	settingsProp = serializedObject.FindProperty("settings");
		nodeStyle = new GUIStyle();
		nodeStyle.normal.background = EditorGUIUtility.Load("builtin skins/darkskin/images/node1.png") as Texture2D;
		nodeStyle.border = new RectOffset(12, 12, 12, 12);
		
		
		textStyle = new GUIStyle();
		textStyle.normal.textColor = Color.yellow;
		textStyle.wordWrap = true;

		
		labelStyle = new GUIStyle();
		labelStyle.fontSize = 40;

		EditorApplication.hierarchyChanged += HierarchyChanged;
	}

	private void HierarchyChanged()
	{
		ClearState();
	}

	void ClearState()
	{
		stateToNode.Clear();
		Debug.Log("SimpleNodeEditorWindow.ClearState" + " t: " + Time.time);
	}

	private void OnDisable()
	{
		EditorApplication.hierarchyChanged -= HierarchyChanged;
	}

	[MenuItem("toivos best windows/SimpleNodeEditorWindow")]
	public static SimpleNodeEditorWindow Init()
	{
		var simpleNodeEditorWindow = EditorWindow.GetWindow<SimpleNodeEditorWindow>();
		simpleNodeEditorWindow.Show();
		return simpleNodeEditorWindow;
	}


	private Vector2 windowScrollPos;
	private GUIStyle nodeStyle;
	private GUIStyle textStyle;
	private GUIStyle labelStyle;




	public IVisualisableNodesProvider provider;
	private Dictionary<IVisualizableNode, Node> stateToNode = new Dictionary<IVisualizableNode, Node>();
	private bool analyseInputOutputFlag = false;
	private string curSel;
	private string textFieldStr;

	private void Update()
	{
		if (provider != null)
		{
			provider.GetNodesProviderPreferences().goFast = true;
			Repaint();
		}
	}

	public void OnGUI()
	{
		//	EditorGUI.BeginChangeCheck();
		//	EditorGUILayout.PropertyField(settingsProp);
		//	if (EditorGUI.EndChangeCheck())
		//	{
		//		serializedObject.ApplyModifiedProperties();
		//		serializedObject.Update();
		//	}
//
		//	if (settings.target == null)
		//	{
		//		return;
		//	}


		if (provider == null)
		{
			var allProviders = ToivonRandomUtils.RandomUtils.GetComponentInChildrenFromRootGOs<IVisualisableNodesProvider>(true);
			var displayedOptions = allProviders.Select(X => X.GetName() + X.GetHashCode()).ToArray();
			int index;
			if (ToivonRandomUtils.RandomUtils.EditorGUIFilterPopUp("", displayedOptions, ref curSel, ref textFieldStr, this, out index))
			{
				provider = allProviders[index];
			}

			return;
		}

		GUI.Box(new Rect(50,50,100,50), provider.GetName(), labelStyle );

		int allNodesCount = stateToNode.Count;
		float allNodesSquare = 1;
		if (allNodesCount != 0)
		{
			allNodesSquare = Mathf.Sqrt(allNodesCount);
		}

		
		ProcessNodeEvents(Event.current);

		var preferences = provider.GetNodesProviderPreferences();
		var allStates = provider.GetNodes();
		foreach (var state in allStates)
		{
			if (!stateToNode.ContainsKey(state))
			{
				var visualisableNodeData = state.GetVisualisationData();
				if (visualisableNodeData.resetPos)
				{
					visualisableNodeData.editorWindowPos = new Vector2(200, 200) + new Vector2(preferences.defaultOffset.x * (allNodesCount % allNodesSquare), preferences.defaultOffset.y * (allNodesCount / allNodesSquare));
					visualisableNodeData.color = Color.white;
					visualisableNodeData.resetPos = false;
				}

				stateToNode.Add(state, new Node(nodeStyle, state,textStyle));
				allNodesCount++;
				allNodesSquare = Mathf.Sqrt(allNodesCount);

				analyseInputOutputFlag = true;
			}

			var node = stateToNode[state];
			var visualisationData = state.GetVisualisationData();
			var nodeRect = EvaluateNodeRect(visualisationData, node);
			node.Draw(visualisationData.color, nodeRect);
		}

		if (analyseInputOutputFlag)
		{
			Debug.Log("SimpleNodeEditorWindow.OnGUI analyse inputoutput" + " t: " + Time.time);


			foreach (var statePair in stateToNode)
			{
				statePair.Value.inputOutputLayers.Clear();
				statePair.Value.inputOutputLayers.Add(new InputOutputLayer {name = statePair.Key.GetName()});
				var transitions = statePair.Key.GetTransitions();
				foreach (var simpleFsmTransition in transitions)
				{
					var visualizableNode = simpleFsmTransition.GetTargetNode();
					string transitionStr;
					try
					{
						transitionStr= simpleFsmTransition.GetName();
						if (visualizableNode != null) statePair.Value.inputOutputLayers.Add(new InputOutputLayer {name = transitionStr, outputTo = stateToNode[visualizableNode]});
					}
					catch (Exception e)
					{
						Debug.LogException(e);
						if (visualizableNode != null) statePair.Value.inputOutputLayers.Add(new InputOutputLayer {name = "ERROR", outputTo = null});
					}
				}

				//	statePair.Value.rect.height  = defaultNodeSize.y * statePair.Value.inputOutputLayers.Count;
			}

			analyseInputOutputFlag = false;
		}

		foreach (var state in allStates)
		{
			stateToNode[state].DrawLayerConnections();
		}
	}

	private Rect EvaluateNodeRect(VisualisableNodeData visualisationData, Node node)
	{
		var nodesProviderPreferences = provider.GetNodesProviderPreferences();
		return new Rect(visualisationData.editorWindowPos + windowScrollPos, new Vector2(nodesProviderPreferences.nodeSize.x, nodesProviderPreferences.nodeSize.y * node.inputOutputLayers.Count));
	}

	private void ProcessNodeEvents(Event e)
	{
		bool nodeInteracted = false;
		foreach (var nodePair in stateToNode)
		{
			bool thisNodeInteracted = false;
			var visualisableNodeData = nodePair.Key.GetVisualisationData();
			var nodeRect = EvaluateNodeRect(visualisableNodeData, nodePair.Value);
			nodeInteracted |= thisNodeInteracted = nodePair.Value.ProcessEvents(e, nodeRect, out Vector2 delta);
			visualisableNodeData.editorWindowPos += delta;
			if (thisNodeInteracted)
			{
				GUI.changed = true;
			}
		}

		if (!nodeInteracted)
		{
			if (e.button == 0 && e.type == EventType.MouseDrag)
			{
				windowScrollPos += e.delta;
				e.Use();
				GUI.changed = true;
			}
		}
	}


	public class Node
	{
		//public Rect rect;
		public GUIStyle style;
		public GUIStyle nodeTextStyle;

		public bool isDragged;
		public List<InputOutputLayer> inputOutputLayers = new List<InputOutputLayer>();
		public IVisualizableNode target;

		public Node(GUIStyle nodeStyle, IVisualizableNode target, GUIStyle nodeTextStyle)
		{
			this.target = target;
			//rect = new Rect(position.x, position.y, width, height);
			style = nodeStyle;
			this.nodeTextStyle = nodeTextStyle;
		}


		public void Draw(Color nodeColor, Rect rect)
		{
			GUI.color = nodeColor;
			
			var copyRect = rect;

			GUI.Box(copyRect, "", style);


			// draw layers
			copyRect.height *= 1f / inputOutputLayers.Count;
			copyRect.width -= 20;
			copyRect.x += 10;
			copyRect.y += 10;
			copyRect.height -= 10;

			bool first = true;
			foreach (var inputOutputLayer in inputOutputLayers)
			{
				var line = copyRect;
				line.height = 2;
				var prevbg = GUI.backgroundColor;
				GUI.backgroundColor = Color.black;
				GUI.Box(line,"");
				//GUI.Box(copyRect);

				if(first) GUI.backgroundColor = new Color(0, 0, 0,0);
				else GUI.backgroundColor = new Color(0, 0, 0, 0.5f);
				
				GUI.Box(copyRect,inputOutputLayer.name, nodeTextStyle);
				GUI.backgroundColor = prevbg ;
				
				inputOutputLayer.inputPoint = new Vector3(copyRect.x+20, copyRect.y + (copyRect.height * 0.5f));
				inputOutputLayer.outputPoint = new Vector3(copyRect.x + copyRect.width, copyRect.y + (copyRect.height * 0.5f));
				copyRect.y += copyRect.height;
				first = false;
			}
		}


		public void DrawLayerConnections()
		{
			foreach (var inputOutputLayer in inputOutputLayers)
			{
				if (inputOutputLayer.outputTo != null)
				{
					Handles.BeginGUI();
					var inputPoint = inputOutputLayer.outputTo.inputOutputLayers[0].inputPoint;
					Handles.DrawLine(inputOutputLayer.outputPoint, inputPoint);
					var deltaNormalised = (inputPoint - inputOutputLayer.outputPoint).normalized;
					Handles.DrawLine(inputPoint, inputPoint - (Quaternion.Euler(0,0,30)* (deltaNormalised*15)) );
					Handles.DrawLine(inputPoint, inputPoint - (Quaternion.Euler(0,0,-30)* (deltaNormalised*15)) );

					Handles.EndGUI();
				}
			}
		}


		private Vector2 mouseDownPos;

		public bool ProcessEvents(Event e, Rect rect, out Vector2 delta)
		{
			delta = Vector2.zero;
//			copyRect.center = scrollPos + copyRect.center;
			switch (e.type)
			{
				case EventType.MouseDown:
					if (e.button == 0)
					{
						mouseDownPos = e.mousePosition;
						if (rect.Contains(e.mousePosition))
						{
							isDragged = true;
							GUI.changed = true;
						}
						else
						{
							GUI.changed = true;
						}
					}

					break;

				case EventType.MouseUp:
					isDragged = false;
					if (rect.Contains(e.mousePosition) && mouseDownPos == e.mousePosition) target.OnNodeClicked();
					break;

				case EventType.MouseDrag:
					if (e.button == 0 && isDragged)
					{
						delta = e.delta;
						e.Use();
						return true;
					}

					break;
			}

			return false;
		}
	}

	public class InputOutputLayer
	{
		public Node inputTo;
		public Node outputTo;
		public Vector3 inputPoint;
		public Vector3 outputPoint;
		public string name;
	}
}