﻿using System.Collections;
using System.Collections.Generic;
using SimpleFSMSpace;
using ToivonRandomUtils;
using UnityEngine;
using  UnityEditor;
[CustomPropertyDrawer(typeof(NodesProviderPreferences))]
public class NodesProviderEditor : PropertyDrawer
{
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		float defaultHeight = UnityEditor.EditorGUI.GetPropertyHeight(property);
		defaultHeight += GetButtonHeight();
		return defaultHeight;
	}

	private static int GetButtonHeight()
	{
		return 20;
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		UnityEditor.EditorGUI.PropertyField(position, property, true);
		position.y += GetPropertyHeight(property, label) - GetButtonHeight();
		position.height = GetButtonHeight();
		if (GUI.Button(position, "Open Node Editor"))
		{
			Debug.Log("NodesProviderEditor.OnGUI" + " t: " + Time.time);
			var snew= SimpleNodeEditorWindow.Init();
			snew.provider = property.serializedObject.targetObject as IVisualisableNodesProvider;
		}
	}
}
