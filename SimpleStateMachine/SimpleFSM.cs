﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ToivonRandomUtils;
using UnityEngine;

namespace SimpleFSMSpace
{
    public class SimpleFSM : MonoBehaviour, IFsmInitialized, IVisualisableNodesProvider
    {
        [System.NonSerialized]
        public SimpleFSMState currentState;
        [System.NonSerialized]
        public List<SimpleFSMState> allFSMStates = new List<SimpleFSMState>();

        public bool debugLog = true;
        private bool initialized = false;
        [System.NonSerialized]
        public bool updating;

        public System.Action onUpdated;
        /// <summary>
        /// </summary>
        /// <returns>whether a state was changed or not. this can be used to make sure that the transition </returns>
        public bool UpdateStateMachine()
        {
            updating = true;
            bool stateChanged = false;
            if (!initialized)
            {
                Initialize();
            }
            var transitionThatShouldBeTaken =  currentState.EvaluateTransitions();
            if (transitionThatShouldBeTaken != null)
            {
                var prevState = currentState;
                if (prevState != null)
                {
                    if (prevState.onExitStateActions != null)
                    {
                        prevState.onExitStateActions.Execute();
                    }
                }
                if (transitionThatShouldBeTaken.transitionTo.onEnterStateActions != null)
                {
                    transitionThatShouldBeTaken.transitionTo.onEnterStateActions.Execute();
                }
                currentState = transitionThatShouldBeTaken.transitionTo;
                if (debugLog)   Debug.Log("SimpleFSM.UpdateStateMachine changed state to "+ currentState + " from " + prevState+ "t: " + Time.time);
                stateChanged = true;
            }

            updating = false;
            onUpdated?.Invoke();

            return stateChanged;
        }

        public void Initialize()
        {
            var allInit = GetComponentsInChildren<IFsmInitialized>(true).ToList();
            allInit.Remove(this);
            allInit.ForEach(x =>
            {
                try
                {
                    x.Initialize();
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    Debug.LogError("failed to initialize " + x, x as UnityEngine.Object);
                }
                

            });
            allFSMStates = allInit.Where(x => x is SimpleFSMState).Cast<SimpleFSMState>().ToList();
            currentState = allFSMStates[0];
            if (currentState.onEnterStateActions != null)
            {
                currentState.onEnterStateActions.Execute();
            }
            initialized = true;
        }

        public IEnumerable<IVisualizableNode> GetNodes()
        {
            return gameObject.GetComponentsInChildren<SimpleFSMState>();
        }

        public string GetName()
        {
            return gameObject.name;
        }

        public NodesProviderPreferences nodesProviderPreferences = new NodesProviderPreferences();
        public NodesProviderPreferences GetNodesProviderPreferences()
        {
            return nodesProviderPreferences;
        }
    }
    
    
    public interface IVisualisableNodesProvider
    {
        IEnumerable<IVisualizableNode> GetNodes();
        string GetName();

        NodesProviderPreferences GetNodesProviderPreferences();
    }

    [System.Serializable]
    public class NodesProviderPreferences
    {
        public bool goFast;
        public Vector2 nodeSize = new Vector2(200, 50);
        public Vector2 defaultOffset= new Vector2(200, 50);
    }

    public interface IVisualizableNode
    {
        
        IEnumerable<IVisualizableNodeTransition> GetTransitions();
        VisualisableNodeData GetVisualisationData();
        string GetName();
        void OnNodeClicked();
    }

    [System.Serializable]
    public class VisualisableNodeData
    {
        public bool resetPos = true;
        public Vector2 editorWindowPos;
        public Color color;
    }
    public struct PocoTransition : IVisualizableNodeTransition
    {
        public string name;
        public IVisualizableNode targetNode;
        public string GetName()
        {
            return name;
        }

        public IVisualizableNode GetTargetNode()
        {
            return targetNode;
        }
    }

    public interface IVisualizableNodeTransition
    {
        string GetName();
        IVisualizableNode GetTargetNode();
    }
}
