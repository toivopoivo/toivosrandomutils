﻿using System;
using System.Collections;
using System.Collections.Generic;
using SimpleFSMSpace;
using UnityEngine;

public class SimpleFSMUpdater : MonoBehaviour
{
    public float updateInterval = -1f;
    private float lastUpdate;
    public TimeType timeType;
    private SimpleFSM simpleFsm;
	
    public enum  TimeType
    {
	    ScaledTime,
	    UnscaledTime
    }
    private void OnEnable()
    {
	     simpleFsm = GetComponent<SimpleFSM>();
    }

    void Update()
    {
	    float time = timeType == TimeType.ScaledTime ? Time.time : Time.unscaledTime;
	    if (updateInterval <= 0 ||  time - lastUpdate >= updateInterval)
	    {
		    simpleFsm.UpdateStateMachine();
		    lastUpdate = time;
	    }
    }
    
    
}