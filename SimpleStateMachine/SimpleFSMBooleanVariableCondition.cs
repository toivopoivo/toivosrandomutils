﻿
using System;
using System.Linq;

namespace SimpleFSMSpace
{
	public class SimpleFSMBooleanVariableCondition : SimpleFSMCondition, IFsmInitialized
	{
		public SimpleFSMVariable variable;

		protected override bool Eval()
		{
			return (bool) variable.GetValue();
		}


		public void Initialize()
		{
			if (variable == null) throw new SystemException();
			if (!variable.GetSuitableTypes().Contains(typeof(bool)))
			{
				throw new SystemException();
			}
		}
	}
}