﻿using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace SimpleFSMSpace
{
	public class SimpleFSMState : MonoBehaviour, IFsmInitialized, IOnInspectorGUI, IVisualizableNode
	{
		private SimpleFSMTransition[] transitions;
		private SimpleFSM fsm;

		[CreateButton]
		public SimpleFSMActionGroup onEnterStateActions;
		[CreateButton]
		public SimpleFSMActionGroup onExitStateActions;


		public VisualisableNodeData visualisableNodeData;

		public void Initialize()
		{
			fsm = GetComponentInParent<SimpleFSM>();
			transitions = GetComponents<SimpleFSMTransition>();
		}


		public SimpleFSMTransition EvaluateTransitions()
		{
			foreach (var t in transitions)
			{
				if (t.Evaluate())
				{
					return t;
				}
			}

			return null;
		}


#if UNITY_EDITOR

		[ContextMenu("Create On Enter State Actions")]
		void CreateEnterActions()
		{
			CreateActions(gameObject.name + " enter state actions");
			
		}

		[ContextMenu("Create On Exit State Actions ")]
		void CreateExitActions()
		{
			CreateActions(gameObject.name + " exit state actions");
		}

		void CreateActions(string obname)
		{
			
			var go = new GameObject(obname);

			go.transform.SetParent(transform);
			var actGroup = go.AddComponent<SimpleFSMActionGroup>();
			onEnterStateActions = actGroup;
			UnityEditor.Undo.RegisterCreatedObjectUndo(go, "asd");
			UnityEditor.Undo.RecordObject(this, "asd");
			UnityEditor.Selection.activeGameObject = go;
		}

#endif

		public void OnInspectorGUI()
		{
			if (Application.isPlaying)
			{
				if (fsm != null && fsm.currentState == this)
				{
#if UNITY_EDITOR
					EditorGUILayout.HelpBox("current state ! ", MessageType.Info);
#endif
				}
			}
		}

		public string GetName()
		{
			return gameObject.name;
		}

		public void OnNodeClicked()
		{
#if UNITY_EDITOR
			UnityEditor.Selection.activeGameObject = gameObject;
#endif
		}

		public IEnumerable<IVisualizableNodeTransition> GetTransitions()
		{
			return gameObject.GetComponents<SimpleFSMTransition>();
		}

		public VisualisableNodeData GetVisualisationData()
		{
			var componentInParent = GetComponentInParent<SimpleFSM>();
			if (componentInParent != null && componentInParent.currentState == this)
			{
				visualisableNodeData.color = Color.yellow;
			}
			else
			{
				visualisableNodeData.color = Color.white;
			}

			return visualisableNodeData;
		}

		public void OnVisualisationDataReset()
		{
			visualisableNodeData = new VisualisableNodeData();
			visualisableNodeData.color = Color.white;
		}
	}
}