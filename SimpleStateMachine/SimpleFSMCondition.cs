﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SimpleFSMSpace
{
	public abstract class SimpleFSMCondition : MonoBehaviour, IOnInspectorGUI
	{
		public bool invert;

		private KeyValuePair<bool, float> prevResult;
		public bool Evaluate()
		{
			var result = Eval();
			prevResult = new KeyValuePair<bool, float>(result, Time.time);
			return result;
		}
		protected abstract bool Eval();

		public virtual void OnInspectorGUI()
		{
			if (Application.isPlaying)
			{
#if UNITY_EDITOR
				if(prevResult.Key) GUI.color = Color.green;
				EditorGUILayout.HelpBox("prev result"+prevResult, MessageType.Info);
#endif
			}
		}
	}
}