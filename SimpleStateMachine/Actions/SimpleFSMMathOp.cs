﻿using System;
using System.Collections.Generic;

namespace SimpleFSMSpace
{
	public class SimpleFSMMathOp : SimpleFSMAction
	{
		public SimpleFSMNumericalVariable a, b, target;
		public MathOp mathOp;
		public override void Execute()
		{
			switch (mathOp)
			{
				case MathOp.Sum:
					target.FloatValue = a.FloatValue + b.FloatValue;
					break;
				case MathOp.Subtract:
					target.FloatValue = a.FloatValue - b.FloatValue;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		
		public enum MathOp
		{
			Sum,
			Subtract
		}
	}
}