﻿using UnityEngine.UI;

namespace SimpleFSMSpace
{
	public class SimpleFSMSetButtonInteractableAction : SimpleFSMAction
	{
		public bool setTo;
		public Button target;
		public override void Execute()
		{
			target.interactable = setTo;
		}
	}
}