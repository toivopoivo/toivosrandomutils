﻿using System.Collections;
using System.Collections.Generic;
using SimpleFSMSpace;
using UnityEngine;

public class SimpleFSMExecuteProxyGroup : SimpleFSMAction
{
	public SimpleFSMActionGroup targetGroup;
	public override void Execute()
	{
		targetGroup.Execute();
	}
}
