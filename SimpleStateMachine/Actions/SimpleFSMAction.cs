﻿using UnityEngine;

namespace SimpleFSMSpace
{
	public abstract class SimpleFSMAction : MonoBehaviour
	{
		public abstract void Execute();

		public virtual bool IsCompleted()
		{
			return false;
		}
	}
}