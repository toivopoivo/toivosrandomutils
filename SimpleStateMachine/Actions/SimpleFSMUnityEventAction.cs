﻿namespace SimpleFSMSpace
{
	public class SimpleFSMUnityEventAction : SimpleFSMAction
	{
		public UnityEngine.Events.UnityEvent unityEvent;
		public override void Execute()
		{
			unityEvent.Invoke();
		}
	}
}