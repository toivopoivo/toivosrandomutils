﻿using UnityEngine;

namespace SimpleFSMSpace
{
	public class SimpleFSMSetGameObjectActiveStateAction : SimpleFSMAction
	{
		public bool setTo;
		public GameObject targetGameObject;
		public override void Execute()
		{
			targetGameObject.SetActive(setTo);
		}
	}
}