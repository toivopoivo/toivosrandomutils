﻿namespace SimpleFSMSpace
{
	public class SimpleFSMSetNumericalVariableAction : SimpleFSMAction
	{
		public SimpleFSMNumericalVariable target;
		public SimpleFSMNumericalVariable value;
		public override void Execute()
		{
			target.FloatValue = value.FloatValue;
		}
	}
}