﻿using UnityEngine;

namespace SimpleFSMSpace
{
	public class SimpleFSMSetText : SimpleFSMAction
	{
		public UnityEngine.UI.Text text;
		[TextArea]
		public string stringFormat;
		public SimpleFSMVariable[] formattedVariables;
		private object[] values;

		public override void Execute()
		{
			if (values == null)
			{
				values = new object[formattedVariables.Length];
			}
			for (int i = 0; i < formattedVariables.Length; i++)
			{
				values[i] = formattedVariables[i].GetValue();
			}
			
			text.text = string.Format(stringFormat, values);
		}
	}
}