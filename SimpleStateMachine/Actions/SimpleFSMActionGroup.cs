﻿using System;
using UnityEditor;
using UnityEngine;

namespace SimpleFSMSpace
{
	public class SimpleFSMActionGroup : MonoBehaviour, IFsmInitialized, IOnInspectorGUI
	{
		private SimpleFSMAction[] actions;

		private float lastExecuted = -1;
		[ContextMenu("Execute")]
		public void Execute()
		{
			try
			{
				lastExecuted = Time.time;
				foreach (var simpleFsmAction in actions)
				{
					simpleFsmAction.Execute();
				}
			}
			catch (Exception e)
			{
				Debug.LogError("ERROR EXECUTING ACTION GROUP " + gameObject, gameObject);
				throw e;
			}
		}

		public void Initialize()
		{
			actions = GetComponents<SimpleFSMAction>();
		}

		public void OnInspectorGUI()
		{
			if (Application.isPlaying)
			{
				if (GUILayout.Button("Run"))
				{
					Execute();
				}

#if UNITY_EDITOR
				if (lastExecuted < 0)
				{
					EditorGUILayout.HelpBox("never executed", MessageType.Info);
				}
				else
				{
					EditorGUILayout.HelpBox($"last executed {Time.time - lastExecuted } seconds ago " , MessageType.Info);
				}
#endif
			}
		}
	}
	
}