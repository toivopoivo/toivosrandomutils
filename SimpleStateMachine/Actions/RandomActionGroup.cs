﻿using System.Collections;
using System.Collections.Generic;
using SimpleFSMSpace;
using ToivonRandomUtils;
using UnityEditor;
using UnityEngine;


public class RandomActionGroup : MonoBehaviour, IFsmInitialized, IOnInspectorGUI
{
   private SimpleFSMAction[] sfsmActions;

   public void Initialize()
   {
      sfsmActions = GetComponents<SimpleFSMAction>();
   }

   public void Execute()
   {
	   sfsmActions.RandomItem().Execute();
   }

   public void OnInspectorGUI()
   {
#if UNITY_EDITOR
      UnityEditor.EditorGUILayout.HelpBox("random one of these is executed", MessageType.Info);
#endif
   }
}