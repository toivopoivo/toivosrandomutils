﻿using System;
using UnityEngine;

namespace SimpleFSMSpace
{
	public class SimpleFSMPlaySoundAction : SimpleFSMAction
	{
		public AudioClip audioClip;
		public bool loop = false;
		AudioSource clipPlayer;
		[Range(0,1)]
		public float volume = 1f;
		public override void Execute()
		{
			if (clipPlayer != null)
			{
				clipPlayer.Play();
			}
			else
			{
				clipPlayer = new GameObject("audioclip player " + audioClip).AddComponent<AudioSource>();
				clipPlayer.clip = audioClip;
				clipPlayer.loop = loop;
				clipPlayer.volume = volume;
				DontDestroyOnLoad(clipPlayer);
				clipPlayer.Play();
				if (!loop)
				{
					Destroy(clipPlayer, audioClip.length);
				}
			}
		}

		public void Stop()
		{
			clipPlayer.Stop();
		}
		

		private void OnDestroy()
		{
			if(clipPlayer != null) Destroy(clipPlayer);
		}

		public override bool IsCompleted()
		{
			if (clipPlayer == null || !clipPlayer.isPlaying) return true;
			return false;
		}
	}
}