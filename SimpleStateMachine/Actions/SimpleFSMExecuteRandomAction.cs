﻿using SimpleFSMSpace;

public class SimpleFSMExecuteRandomAction : SimpleFSMAction
{
	public RandomActionGroup randomActionGroup;


	public override void Execute()
	{
		randomActionGroup.Execute();
	}
}