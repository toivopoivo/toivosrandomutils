﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Comment : MonoBehaviour, IOnInspectorGUI
{
    [TextArea(2,10)]
    public string comment;
    
    [DateLong]
    public long date;

    
    public void OnInspectorGUI()
    {
        if (GUILayout.Button("update date"))
        {
#if UNITY_EDITOR
            UnityEditor.Undo.RecordObject(this, "updated date");
#endif
            date = System.DateTime.Now.ToFileTimeUtc();
        }
    }
}
