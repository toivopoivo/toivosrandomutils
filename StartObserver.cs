﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StartObserver : MonoBehaviour
{
	public static StartObserver instance;
	public List<Component> started = new List<Component> ();
	public List<ListenerData> listeners = new List<ListenerData> ();

	static void CreateNewInstanceIfNeeded ()
	{
		if (instance == null) {
			GameObject newInstance = new GameObject ();
			newInstance.name = "StartObserver";
			instance = newInstance.AddComponent<StartObserver> ();
		}
	}


	public static void NotifyStart (Component component)
	{
		CreateNewInstanceIfNeeded ();
		instance.started.Add (component);
		var toBeInvokedAndRemoved = instance.listeners.FindAll (x => instance.started.Contains (x.component));
		foreach (var item in toBeInvokedAndRemoved) {
			item.callback ();
			instance.listeners.Remove (item);
		}
	}

	public static void Listen (Component component, System.Action callback)
	{
		CreateNewInstanceIfNeeded ();
		if (instance.started.Contains (component))
			callback ();
		else
			instance.listeners.Add (new ListenerData (component, callback));
	}

	public class ListenerData
	{
		public Component component;
		public System.Action callback;

		public ListenerData (Component component, System.Action callback)
		{
			this.callback = callback;
			this.component = component;
		}
	}
}
