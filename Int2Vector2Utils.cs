﻿#define DEBUG_SPAM

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

namespace ToivonRandomUtils
{
    public static class Int2Vector2Utils
	{
        public static void SetPixelToTexture(this IntVector2 pix, Texture2D tex,Color color,bool apply)
        {
            tex.SetPixel(pix.x, pix.y, color);
            if (apply) tex.Apply();
        }
        public static void SetAllPixelsToTexture(this IEnumerable<IntVector2> pixels, Texture2D tex, Color color, bool apply)
        {
            foreach (var item in pixels)
            {
                SetPixelToTexture(item, tex, color, false);       
            }
            if(apply) tex.Apply();
        }
        public static IntVector2? DirectionTypeToVector (this ToivonRandomUtils.IntVector2.DirectionType dirType)
		{
			switch (dirType) {
			case IntVector2.DirectionType.Up:
				return new IntVector2 (0, 1);
			case IntVector2.DirectionType.Right:
				return new IntVector2 (1, 0);
			case IntVector2.DirectionType.Down:
				return new IntVector2 (0, -1);
			case IntVector2.DirectionType.Left:
				return new IntVector2 (-1, 0);
			case IntVector2.DirectionType.UpRight:
				return new IntVector2 (1, 1);
			case IntVector2.DirectionType.DownRight:
				return new IntVector2 (1, -1);
			case IntVector2.DirectionType.DownLeft:
				return new IntVector2 (-1, -1);
			case IntVector2.DirectionType.UpLeft:
				return new IntVector2 (-1, 1);
			}
			return null;

		}

		public static IEnumerator<IntVector2> CenteredGridFilling (IntVector2 startPosition, bool debugSpam = false, int maxCycles = -1)
		{
			int cycle = 0;
			int edgeWidth = 1;
			while (true) {
                if (maxCycles > -1 && cycle == maxCycles)
                {
                    break;
                }
                IntVector2 currentPosition = startPosition + new IntVector2 (-cycle, cycle);
				yield return currentPosition;
				if (debugSpam)
					Debug.Log (" up left to up right");

				var moving = MoveToDirectionNumTimes (currentPosition, new IntVector2 (1, 0), edgeWidth, debugSpam); 
				while (moving.MoveNext ())
					yield return currentPosition = moving.Current;

				if (debugSpam)
					Debug.Log ("up right to lower right");
				moving = MoveToDirectionNumTimes (currentPosition, new IntVector2 (0, -1), edgeWidth, debugSpam); 
				while (moving.MoveNext ())
					yield return currentPosition = moving.Current;

				if (debugSpam)
					Debug.Log ("lower right to lower left");
				moving = MoveToDirectionNumTimes (currentPosition, new IntVector2 (-1, 0), edgeWidth, debugSpam); 
				while (moving.MoveNext ())
					yield return currentPosition = moving.Current;

				if (debugSpam)
					Debug.Log ("lower left to up left");
				moving = MoveToDirectionNumTimes (currentPosition, new IntVector2 (0, 1), edgeWidth - 1, debugSpam); 
				while (moving.MoveNext ())
					yield return moving.Current;

				if (debugSpam)
					Debug.Log ("cycle done");
				edgeWidth += 2;
				cycle++;
                
			}
		}

		public static IEnumerator<IntVector2> MoveToDirectionNumTimes (IntVector2 start, IntVector2 moveDir, int moves, bool debugSpam)
		{
			for (int i = 1; i < moves; i++) {
				if (debugSpam)
					Debug.Log ("" + i + "/" + moves);
				yield return start + (moveDir * i);
			}
		}

		/// <summary>
		/// NOTE RESULT PATH IS BOTH START AND END INCLUSIVE!!!
		/// </summary>
		public class FloodPathFindJob
		{

			public FloodPathFindJob (IntVector2 start, System.Func<IntVector2, bool> checkIfReachedDestination, System.Func<IntVector2, bool> isObstacle, bool fixedAxis, bool possibleManyTargets = false)
			{
				this.start = start;
				this.checkIfReachedDestination = checkIfReachedDestination;
				this.isObstacle = isObstacle;
				if (fixedAxis) {
					neighbours = IntVector2.CardinalNeighbours;
				} else {
					neighbours = IntVector2.AllNeighbours;
				}
				this.possibleManyTargets = possibleManyTargets;
			}

			IntVector2[] neighbours;

			public enum StateType
			{
				Failed,
				PendingNotStarted,
                PendingSearching,
                PendingReversing,
				Success

			}

			public StateType state = StateType.PendingSearching;
			IntVector2 start;
			bool possibleManyTargets = false;
			HashSet<Node> open = new HashSet<Node>();
			HashSet<IntVector2> closed = new HashSet<IntVector2>();

			List<Node> foundTargets = new List<Node> ();
			System.Func<IntVector2, bool> checkIfReachedDestination;
			System.Func<IntVector2, bool> isObstacle;

			public List<Node> allVisitedNodes = new List<Node> ();

			Node current;
			/// <summary>
			/// positions, not directions
			/// </summary>
			public List<List<IntVector2>> foundPaths = new List<List<IntVector2>> ();

			public class Node
			{
				public IntVector2 position;
				public Node parent;

				public Node (IntVector2 position, Node parent)
				{
					this.position = position;
					this.parent = parent;
				}

                public override string ToString()
                {
                    return "Node "+ position.ToString();
                }

            }

			public bool IsEnoughFoundPaths{ get { return !possibleManyTargets && foundTargets.Count > 0; } }

			public bool CheckForFoundPath (Node node)
			{
				
				if (checkIfReachedDestination (node.position)) {
					foundTargets.Add (node);
					return true;
				}
				return false;
			}
            /// <summary>
            /// 20.5.2018 refactored the debugvizdata argument out due perf overhead. if you (most likely my former self) were using it instead call visualize progress
            /// </summary>
            /// <returns></returns>
			public IEnumerator Pathfinding ()
			{
                lock (open)
                {
                    open.Add(current = new Node(start, null));
                }
				CheckForFoundPath (current);
                state = StateType.PendingSearching;
				//bool success = checkIfReachedDestination (current.position);
				while (true) {

                    if (open.Count == 0)
                    {
                        break;
                    }
					//if (success) ;

					if (IsEnoughFoundPaths) {
						break;
					}
					current = open.First();
#if DEBUG_SPAM
                    Debug.Log("NEW CYCLE");
                    Debug.Log("current " + current);
#endif
                    lock (closed)
                    {
                       closed.Add(current.position);
                    }
					for (int i = 0; i < neighbours.Length; i++) {

                        IntVector2 newPos = current.position + neighbours [i];
#if DEBUG_SPAM
                        Debug.Log("eval neighbour new pos"+newPos);
#endif
                        lock (closed)
                        {
                            if (closed.Contains(newPos))
                            {
#if DEBUG_SPAM

                                Debug.Log("neighbour "+newPos +" is closed");
#endif
                                continue;
                            }
                        }
						var newNode = new Node (newPos, current);
						if (CheckForFoundPath (newNode)) {
#if DEBUG_SPAM
                            Debug.Log("is destination");
#endif
                            if (IsEnoughFoundPaths)
								break;
						} else if (!isObstacle (newPos)) {
                            // doing "all" manually for perf
                            //open.All(X => X.position != newPos)
#if DEBUG_SPAM
                            Debug.Log("adding new pos to opens");
#endif
                            lock (open)
                            {
                                open.Add(newNode);
                            }
						}
					}
                    /*toivo 11.6.2018 why does this exsits*/
					allVisitedNodes.Add (current);
                    lock (open)
                    {

#if DEBUG_SPAM
                        Debug.Log("removing current from open. open count: "+open.Count);
#endif

                        open.Remove(current);
#if DEBUG_SPAM
                        Debug.Log("done. open count: " + open.Count);
#endif

                    }
                    yield return null;
				}

                state = StateType.PendingReversing;

                foreach (var item in foundTargets) {
					var list = new List<IntVector2> ();
					foundPaths.Add (list);
					current = item;
					while (true) {
						if (current.parent != null) {
							list.Add (current.position);
						} else {
							list.Add (start);
							break;
						}
						current = current.parent;
					}
					list.Reverse ();
				/*	if (debugVizData != null) { 20.5.2018 this used to visualise found paths but didnt need it atm so commented out
						debugVizData.Add (new VisualisationData (boxes: list.Select (x => new Bounds (x.Vector3XZ, Vector3.one)).ToArray (), color: Color.blue));
					}*/
				}

				state = foundTargets.Any () ? StateType.Success : StateType.Failed;


			}

            int alreadyDrawnClosedNodes;
            public  IEnumerator<KeyValuePair<IntVector2,Color>> VisualizeProgress()
            {
                Debug.Log("FloodPathFindJob state: " + state);
                List<IntVector2> closedCopy;
               lock (closed)
                {
                    closedCopy = closed.ToList();
                }
                    
                for (int i = alreadyDrawnClosedNodes; i < closedCopy.Count; i++)
                {
                    yield return new KeyValuePair<IntVector2, Color>(closedCopy[i], Color.red);
                    alreadyDrawnClosedNodes++;
                }

                lock (open) { 
                    Debug.Log("open count: " + open.Count);
                    foreach (var item in open)
                    {
                        yield return new KeyValuePair<IntVector2, Color>(item.position, Color.green);
                    }
                }
                lock (foundTargets)
                {
                    for (int i = 0; i < foundTargets.Count; i++)
                    {
                        yield return new KeyValuePair<IntVector2, Color>(foundTargets[i].position, Color.yellow);
                    }
                }
                if(current != null) yield return new KeyValuePair<IntVector2, Color>(current.position, Color.black);
            }

		}
        


        /// <summary>
        /// created for comparing adjacent points to each other with speed
        /// </summary>
        public class ScanJob
        {
            public  enum State
            {
                Unstarted,
                Started,
                ScalingHitObstacle,
                SteppingHitObstacle,
                Done
            }
            public State state;
            public IntVector2.DirectionType scalingDirection, steppingDir;
            /// <summary>
            /// first int2v is current & second is last pos
            /// </summary>
            public System.Func<IntVector2, IntVector2, bool> isMatch;
            public System.Func<IntVector2, bool> isObstacle;
            public IntVector2 startingPoint;
            /// <summary>
            /// is arbitrary if first
            /// </summary>
            public IntVector2 previousPosition;
            public IntVector2 currentPosition;

            public List<IntVector2> matches = new List<IntVector2>();
            public ScanJob( IntVector2 startingPoint, IntVector2.DirectionType scalingDirection, IntVector2.DirectionType steppingDir, Func<IntVector2, IntVector2, bool> isMatch, Func<IntVector2, bool> isObstacle)
            {
                this.scalingDirection = scalingDirection;
                this.steppingDir = steppingDir;
                this.isMatch = isMatch;
                this.isObstacle = isObstacle;
                this.startingPoint = startingPoint;
            }
            /// <summary>
            /// doesnt evaluate the starting point in order because wanted to avoid special case. dont know if best practice
            /// </summary>
            public IEnumerator Executing()
            {
                state = State.Started;
                var scalingDirToIntV2 = DirectionTypeToVector(scalingDirection).Value;
                var steppingDirInt = DirectionTypeToVector(steppingDir).Value;
                IntVector2 scaledPos = startingPoint;
                while (true)
                {
                    previousPosition = scaledPos;
                    scaledPos = scaledPos + scalingDirToIntV2;
                    if (isObstacle(scaledPos))
                    {
                        state = State.ScalingHitObstacle;
                        yield return null;
                        break;
                    }
                    currentPosition = scaledPos;
                    while (true)
                    {
                        // see here i skip the first point of the scanline
                        // that is on purpose
                        // because i think currently the point of this algorithm is to compare adjacent points to eachother
                        // so that wouldnt make sense with the first point since there is nothing to compare it to
                        previousPosition = currentPosition;
                        currentPosition += steppingDirInt;

                        if (isObstacle(currentPosition))
                        {
                            state = State.SteppingHitObstacle;
                            yield return null;
                            break;
                        }
                        if (isMatch(currentPosition,previousPosition))
                        {
                            matches.Add(currentPosition);
                        }


                        yield return null;
                    }

                    yield return null;
                }
                state = State.Done;

            }

        }

    }


}