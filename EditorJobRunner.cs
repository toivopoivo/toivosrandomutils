﻿using System.Collections;
using System.Collections.Generic;
using ToivonRandomUtils;
using UnityEngine;

[ExecuteInEditMode]
public class EditorJobRunner : MonoBehaviour {
    [DrawnNonSerialized]
    public IEnumerator job;
    

    private void OnEnable()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.update += SpecialUpdate;
#endif
    }

    private void OnDisable()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.update -= SpecialUpdate;
#endif
    }

    private void SpecialUpdate()
    {
        if (job == null)
        {
            DestroyImmediate(this);
            return;
        }
        var done =  !job.MoveNext();
        if (done) DestroyImmediate(this);
    }
}
