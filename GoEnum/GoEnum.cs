﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoEnum : MonoBehaviour
{
    private void OnEnable()
    {
        transform.parent.GetComponent<GoEnumParent>().SetChildActive(gameObject); 
    }

    private void OnDisable()
    {
        transform.parent.GetComponent<GoEnumParent>().ChildDisabled(gameObject); 
    }
}
