﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoEnumParent : MonoBehaviour
{
    private bool disablingChildren = false;
    public GameObject activeChild;
    
    
    private void OnTransformChildrenChanged()
    {
        Debug.Log("children changed!!");
    }


    public void SetChildActive(GameObject child)
    {
        if (disablingChildren) return;
        if (child.transform.parent != transform) throw new InvalidOperationException();
        Debug.Log("GoEnumParent SetChildActive "+child + " time: "+Time.time );
        disablingChildren = true;
        activeChild = child;
        for (int i = 0; i < transform.childCount; i++)
        {
            var item = transform.GetChild(i);
            item.gameObject.SetActive(child == item.gameObject );
        }

        disablingChildren = false;

    }

    public void ChildDisabled(GameObject o)
    {
        if (o == activeChild)
        {
            var si = o.transform.GetSiblingIndex();
            si++;
            if (si >= transform.childCount) si = 0;
            SetChildActive(transform.GetChild(si).gameObject);
        }
    }
}
