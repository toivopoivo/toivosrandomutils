﻿#if UNITY_EDITOR
#endif

using System;
using System.Collections.Generic;
using ToivonRandomUtils;

namespace ToivoThreadSafe
{
    public class ThreadSafeList<T>
    {
        List<T> m_list = new List<T>();

        public void Add(T item)
        {
            lock (m_list)
            {
                m_list.Add(item);

            }
        }

        public void Remove(T item)
        {
            lock (m_list)
            {
                m_list.Remove(item);
            }
        }


        public T this[int key]
        {
            get
            {
                lock (m_list)
                {
                    return m_list[key];
                }
            }

            set
            {
                lock (m_list){
                    m_list[key] = value;
                }

            }
        }


        public int Count
        {
            get
            {
                lock(m_list){
                    return m_list.Count;
                }
            }
        }
    }
}
