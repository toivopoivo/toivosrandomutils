﻿//#define DEBUG_SPAM

using UnityEngine;
using System.Linq;
using System;

namespace ToivonRandomUtils
{
    [System.Serializable]
	public struct IntVector2: IEquatable<IntVector2>
	{
		public int x;
		public int y;

		public IntVector2 (int x, int y)
		{
			this.x = x;
			this.y = y;
		}

		public IntVector2 Direction { get { return new IntVector2 (x == 0 ? 0 : x / Mathf.Abs (x), y == 0 ? 0 : y / Mathf.Abs (y)); } }

		public IntVector2 Abs { get { return new IntVector2 (Mathf.Abs (x), Mathf.Abs (y)); } }


		[System.Obsolete ("use AllNeighboursInstead", true)]
		public static IntVector2[] EightNeighbours (IntVector2 queryTarget)
		{
			var neggihtns = new IntVector2[8];
			int indexer = 0;
			for (int i = -1; i < 2; i++) {
				for (int j = -1; j < 2; j++) {
					if (i == 0 && j == 0) {
						continue;
					}
					neggihtns [indexer] = queryTarget + new IntVector2 (i, j);
					indexer++;
				}
			}
			return neggihtns;
		}
        /// <summary>
        /// min inclusive & max exclusive
        /// </summary>
        public bool IsInsideOfRect(IntVector2 min, IntVector2 max)
        {
            if(x>=min.x && x<max.x && y >= min.y && y < max.y)
            {
                return true;
            }
            return false;
        }

        public static IntVector2 FromVector3FlooredXZAxis (Vector3 v3)
		{
			return new IntVector2 (Mathf.FloorToInt (v3.x), Mathf.FloorToInt (v3.z));
		}

        public static IntVector2 Cast(Vector3 v3)
        {
	        return new IntVector2((int)v3.x,(int) v3.y);
        }

		public static IntVector2 operator + (IntVector2 a, IntVector2 b)
		{
			return new IntVector2 (a.x + b.x, a.y + b.y);
		}

		public static IntVector2 operator - (IntVector2 a, IntVector2 b)
		{
			return new IntVector2 (a.x - b.x, a.y - b.y);
		}

		public static IntVector2 operator * (IntVector2 a, int scalar)
		{
			return new IntVector2 (a.x * scalar, a.y * scalar);
		}

		public static bool operator == (IntVector2 a, IntVector2 b)
		{
			return a.x == b.x && a.y == b.y;
		}

		public static bool operator != (IntVector2 a, IntVector2 b)
		{
			return (!(a == b));
		}

        public static explicit operator Vector2 (IntVector2 a)
        {
            return new Vector2(a.x, a.y);
        }
		public override bool Equals (object obj)
		{
           
            var intVector2 = ((IntVector2)obj);
			return intVector2.x == x && intVector2.y == y;
		}

        public bool Equals(IntVector2 other)
        {
            return other.x == x && other.y == y;
        }
        public override int GetHashCode()
        {
            return x +100000 + y;
        }

        public Vector3 Vector3XZ { get { return new Vector3 (x, 0, y); } }

		public static IntVector2[] CardinalNeighbours = {
			new IntVector2 (0, 1),
			new IntVector2 (1, 0),
			new IntVector2 (0, -1),
			new IntVector2 (-1, 0),
		};

		[Obsolete("Use 'CardinalNeighbours' instead.")]
		public static IntVector2[] FixedAxisNeighbours;
		
		public static IntVector2[] AllNeighbours = {
			new IntVector2 (-1, 1),
			new IntVector2 (0, 1),
			new IntVector2 (1, 1),
			new IntVector2 (1, 0),
			new IntVector2 (1, -1),
			new IntVector2 (0, -1),
			new IntVector2 (-1, -1),
			new IntVector2 (-1, 0)
		};

		public override string ToString ()
		{
			return string.Format ("X: {0} Y: {1}", x, y);
		}


		public enum DirectionType
		{
			Up,
			Right,
			Down,
			Left,
			UpRight,
			DownRight,
			DownLeft,
			UpLeft,
			None
		}


		public DirectionType GetDirection (bool roundDirtyDirections = false)
		{
			bool absXAndYAreSame = Mathf.Abs (x) == Mathf.Abs (y);
            if (absXAndYAreSame && x == 0)
            {
                return DirectionType.None;
            }
            if (roundDirtyDirections) {
				return new [] { 
					new IntVector2 (x / Mathf.Abs (x), 0),
					new IntVector2 (y / Mathf.Abs (y), 0),
					Direction,
				}.OrderBy (i => Vector2.Angle (Vector2.up, new Vector2 (i.x, i.y))).First ().GetDirection ();
			} 
			
			if (absXAndYAreSame) { // its diagonal
				if (x > 0 && y > 0) return DirectionType.UpRight;
				if (x > 0 && y < 0) return DirectionType.DownRight;
				if (x < 0 && y < 0) return DirectionType.DownLeft;
				if (x < 0 && y > 0) return DirectionType.UpLeft;
			} else {
				if (y > 0 && x == 0) return DirectionType.Up;
				if (x > 0 && y == 0) return DirectionType.Right;
				if (y < 0 && x == 0) return DirectionType.Down;
				if (x < 0 && y == 0) return DirectionType.Left;
			}

			return DirectionType.None;
		}
		
		// Implicit conversion from IntVector2 to Vector2Int
		public static implicit operator Vector2Int(IntVector2 intVector2)
		{
			return new Vector2Int(intVector2.x, intVector2.y);
		}

		// Implicit conversion from Vector2Int to IntVector2
		public static implicit operator IntVector2(Vector2Int vector2Int)
		{
			return new IntVector2(vector2Int.x, vector2Int.y);
		}

    }


}