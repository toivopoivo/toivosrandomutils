﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ToivoLinq;
using ToivonRandomUtils;
using UnityEngine;

public class ObjectReferenceLinkAnalyser:MonoBehaviour
{
    public class AnalysisResult
    {
        public string hierarchyPath;
        public string fieldPath;
        public string fieldName;
        public UnityEngine.Object unityObjectValue;
        public int componentIndex;
        public int listIndex = -1;
    }

    #if UNITY_EDITOR
    [ContextMenu("AnalyseNonNullInSceneObjectLinks")]
    public void AnalyseNonNullInSceneObjectLinks()
    {
        //var allMonos = GetComponentsInChildren<MonoBehaviour>();
        var allTransforms = GetComponentsInChildren<Transform>(true);
        bool removeChildren = UnityEditor.EditorUtility.DisplayDialog("remove children?", "do just want cross sibling links?", "yes", "no");
        bool humanReadable = UnityEditor.EditorUtility.DisplayDialog("human readable or machine readable?", "who will be reading the data?", "human", "machine");
        List<AnalysisResult> objectFieldRefences = new List<AnalysisResult>();
        for (int b = 0; b < allTransforms.Length; b++)
        {
            var mons = allTransforms[b].GetComponents<MonoBehaviour>();
            for (int n = 0; n < mons.Length; n++)
            {
                MonoBehaviour mono = (MonoBehaviour)mons[n];
                if (mono == null) continue;
                MonoBehaviour obj = mono;
                string hierarchyPath = obj.transform.GetRecurseWhileNotNull(x => x.transform.parent).Select(x => x.gameObject.name).Reverse().Aggregate("", (x, y) => x + "." + y) + "." + mono.GetType().Name;
                Debug.Log(hierarchyPath);
                UnityEditor.EditorUtility.DisplayProgressBar("Analyzing", $"Might take a while... {b}/{allTransforms.Length}", (float)b / allTransforms.Length);
                try
                {
                    List<AnalysisResult> current = new List<AnalysisResult>();
                    Analyze(mono, current, hierarchyPath, "", n);
                    if (removeChildren)
                    {
                        var children = mono.GetComponentsInChildren<Component>(true).ToHashSet();
                        var childrenGos = mono.GetComponentsInChildren<Transform>(true).Select(x => x.gameObject).ToHashSet();

                        current.RemoveAll(x => children.Contains(x.unityObjectValue));
                        // remove children gameobjects
                        current.RemoveAll(x => childrenGos.Contains(x.unityObjectValue));
                    }
                    objectFieldRefences.AddRange(current);
                }
                catch (System.Exception ex)
                {
                    Debug.LogError("failed to analyze " + hierarchyPath);
                    Debug.LogException(ex);
                }
            }

        }
        // removes all assets
        objectFieldRefences.RemoveAll(x => UnityEditor.AssetDatabase.Contains(x.unityObjectValue));
        UnityEditor.EditorUtility.ClearProgressBar();

        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (humanReadable)
        {
            IEnumerable<IGrouping<string, AnalysisResult>> groups = objectFieldRefences.GroupBy(x => x.fieldName);
            foreach (var g in groups)
            {
                sb.AppendLine(g.Key);
                foreach (var item in g)
                {
                    sb.AppendLine($"\t{item.unityObjectValue}\t{item.hierarchyPath}\t{item.fieldPath}");
                }
            }
        }
        else
        {
            foreach (var item in objectFieldRefences)
            {
                Component component = (item.unityObjectValue as Component);
                if (component != null) sb.AppendLine($"\t{component.gameObject.name}\t{item.fieldName}\t{item.hierarchyPath}\t{item.fieldPath}\t{item.componentIndex}\t{item.listIndex}");
                else sb.AppendLine($"\t{(item.unityObjectValue as GameObject).name}\t{item.fieldName}\t{item.hierarchyPath}\t{item.fieldPath}\t{item.componentIndex}\t{item.listIndex}");
            }
        }
        UnityEditor.EditorUtility.OpenWithDefaultApp(ToivonRandomUtils.RandomUtils.CreateFileInMyDocumentsThatContainsString(sb.ToString()));

    }
    #endif

    private static void Analyze(object obj, List<AnalysisResult> objectFieldRefences, string hierarchyPath, string fieldPath, int componentI)
    {

        var fields = obj.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
        foreach (var item in fields)
        {
            var val = item.GetValue(obj);
            Debug.Log("analysing field named:" + item.Name);
            if (val == null) continue;
            var unityObj = val as UnityEngine.Object;
            if (unityObj != null)
            {
                objectFieldRefences.Add(new AnalysisResult { hierarchyPath = hierarchyPath, fieldPath = fieldPath, fieldName = item.Name, unityObjectValue = unityObj, componentIndex = componentI });
            }
            else
            {
                var valType = val.GetType();
                if (valType == typeof(string)) continue;
                if (valType.IsValueType) continue;
                var list = val as IList;
                if (list != null)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        var unityObjInList = list[i] as UnityEngine.Object;
                        if (unityObjInList != null)
                        {
                            objectFieldRefences.Add(new AnalysisResult { hierarchyPath = hierarchyPath, fieldPath = fieldPath, fieldName = item.Name, unityObjectValue = unityObjInList, componentIndex = componentI, listIndex = i });
                        }
                        else
                        {
                            Analyze(list[i], objectFieldRefences, hierarchyPath, fieldPath + "." + item.Name + "[" + i + "]", componentI);
                        }
                    }
                }
                else
                {
                    if (valType.IsSerializable) Analyze(val, objectFieldRefences, hierarchyPath, fieldPath + "." + item.Name, componentI);
                }
            }
        }
    }
}