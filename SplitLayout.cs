﻿using UnityEngine;
using System.Collections;
using ToivonRandomUtils;
using System.Linq;
using System.Collections.Generic;

namespace WorkInProgress
{
	public class SplitLayout : MonoBehaviour
	{
		public void Split (SplitLayoutSplit splitLayoutSplit)
		{
			throw new System.NotImplementedException ();
		}
	}

	public class SplitLayoutSplit
	{
		public int rowsCount { get; private set; }

		public int columnsCount{ get; private set; }

		public SplitLayoutCoordinate[] coordinates;
		public Dictionary<int, List<SplitLayoutCoordinate >> splitLayoutAreas;

		public SplitLayoutSplit (int rowsCount, int columnsCount, int[][] layout)
		{
			coordinates = new SplitLayoutCoordinate[rowsCount];
			if (rowsCount != layout.Length)
				Debug.LogError ("rowsCount must be the length of layout array");
			for (int i = 0; i < layout.Length; i++) {
				if (layout [i].Length < columnsCount) {
					Debug.LogError (" all columns must have lenght specified in columnsCount, problem with colum " + i);
				} else {
					for (int j = 0; j < columnsCount; j++) {
						coordinates [i] = new SplitLayoutCoordinate (i, j);
						coordinates [i].groupID = layout [i] [j];
					}
				}
			}
			this.rowsCount = rowsCount;
			this.columnsCount = columnsCount;

		}

		int GetGroupAtCoordinate (SplitLayoutCoordinate coord)
		{
			var select = coordinates.First ((x) => {
				return (coord.x == x.x && coord.y == x.y);
			});
			return select.groupID;
        
		}

		bool IsSameGroupID (SplitLayoutCoordinate current, Direction dir)
		{
			SplitLayoutCoordinate coordinateAtDir = null;
			if (dir == Direction.Up) {
				coordinateAtDir = new SplitLayoutCoordinate (current, 0, 1);
			} else if (dir == Direction.Left) {
				coordinateAtDir = new SplitLayoutCoordinate (current, -1, 0);
			} else if (dir == Direction.Right) {
				coordinateAtDir = new SplitLayoutCoordinate (current, 1, 0);
			} else {
				coordinateAtDir = new SplitLayoutCoordinate (current, 0, -1);
			}

			if (IsCoordinateOverBounds (coordinateAtDir)) {
				return false;
			}
			if (GetGroupAtCoordinate (coordinateAtDir) == GetGroupAtCoordinate (current)) {
				return true;
			}
			return false;

		}

		bool IsCoordinateOverBounds (SplitLayoutCoordinate current)
		{
			if (current.x >= columnsCount || current.x < 0) {
				return true;
			} else if (current.y >= rowsCount || current.y < 0) {
				return true;
			}
			return false;
		}

		public class SplitLayoutCoordinate
		{
			public int groupID;
			public int x;
			public int y;

			public SplitLayoutCoordinate (int x, int y)
			{
				this.x = x;
				this.y = y;
			}

			public SplitLayoutCoordinate (SplitLayoutCoordinate current, int x, int y)
			{
				this.x = current.x + x;
				this.y = current.y + y;
			}
		}

		public enum Direction
		{
			Up,
			Left,
			Down,
			Right
		}
	}
}