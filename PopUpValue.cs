﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class PopUpString
{
	/// <summary>
	/// NOTE: YOU HAVE TO CLEAR THIS FLAG IF YOU INTEND TO USE IT
	/// </summary>
	public bool dirty = false;

	public string[] possibleValues = new string[]{ "" };
	public int selectedIndex = 0;

	public string Value {
		get {
			try {
				return possibleValues [selectedIndex]; 
			}
			catch (System.Exception ex) {
				//throw ex;
				Debug.LogWarning("Popupstring fail:"+ex.ToString());
				return "NOPOPUPSTRINGVALUE";
			}
			
		}
	}

	public override string ToString ()
	{
		return Value;
	}

	public PopUpString ()
	{
	}

	/// <summary>
	/// use this constructor at your own risk
	/// as it might not get called but only once ever
	/// </summary>
	public PopUpString (params string[] values)
	{
		possibleValues = values;
	}
}
