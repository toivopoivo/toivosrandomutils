﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ToivonRandomUtils;


[ExecuteInEditMode]
public class DestroyerOfTheImmortals : MonoBehaviour
{
	void OnEnable ()
	{
		#if UNITY_EDITOR
		UnityEditor.SceneManagement.EditorSceneManager.sceneSaving += DestroyInEdit;
		#endif
	}


	bool naturalDestroy = false;

	void DestroyInEdit (UnityEngine.SceneManagement.Scene scene, string path)
	{
		naturalDestroy = true;
		DestroyImmediate (gameObject);
	}

	void OnDisable ()
	{
		// if you are here because of the error:
		// Cannot destroy GameObject while it is being activated or deactivated
		//
		// tell toivo
		#if UNITY_EDITOR
		UnityEditor.SceneManagement.EditorSceneManager.sceneSaving -= DestroyInEdit;
		if (naturalDestroy)
			return;

		var children = gameObject.GetComponentsOnlyFromChildren<Transform> (true);
		foreach (var item in children) {
			var cache = item;
			UnityEditor.EditorApplication.delayCall += () => {
				if (item != null) {
					DestroyImmediate (cache.gameObject);
				}
			};
		}
		UnityEditor.EditorApplication.delayCall += () => {
			if (this != null && gameObject != null) {
				DestroyImmediate (gameObject);
			}
		};
		#endif
	}
	
}
