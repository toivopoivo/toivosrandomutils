﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

public class InstanceRegistry : MonoBehaviour
{
	public Dictionary<System.Type, List<object>> registry = new Dictionary<System.Type, List<object>> ();
	static InstanceRegistry instance;
	static bool appQuit = false;

	public static bool ApplicationQuitting {
		get {
			return appQuit;
		}
	}

	void OnApplicationQuit ()
	{
		appQuit = true;
	}

	void OnEnable ()
	{
		if (instance == null) {
			instance = this;
		}
	}

	public static void TryCreateInstance ()
	{
		if (instance == null) {
			instance = new GameObject ("Instance Registry").AddComponent<InstanceRegistry> ();
		}
	}

	public static void Register (object obj)
	{
		TryCreateInstance ();
		var type = obj.GetType ();
		if (!instance.registry.ContainsKey (type)) {
			instance.registry.Add (type, new List<object> ());
		}
		if (!instance.registry [type].Contains (obj)) {
			instance.registry [type].Add (obj);
		} else {
			Debug.LogError ("registy already contains " + obj);
		}
	}


	public static bool ApplicationQuittingOrInstanceNull {
		get {
			return instance == null || appQuit;
		}
	}

	public static void DeRegister (object obj)
	{
		if (instance == null) {
			return;
		}
		List<object> list;
		if (instance.registry.TryGetValue (obj.GetType (), out list)) {
			list.Remove (obj);
		}
	}

	public static InstanceCollection<T> GetInstanceCollection <T> (T type) where T:class
	{
		if (instance == null)
			return null;
//		return instance.registry [type.GetType ()].Cast<T> ();
		return new InstanceCollection<T>{ objectsList = instance.registry [type.GetType ()] };
	}


	public class InstanceCollection<T> : IEnumerable<T> where T: class
	{
		public List<object> objectsList;

		
		#region IEnumerable implementation

		public IEnumerator<T> GetEnumerator ()
		{

			for (int i = 0; i < objectsList.Count; i++) {
				yield return objectsList [i] as T;
			}
		}

		#endregion

		#region IEnumerable implementation

		IEnumerator IEnumerable.GetEnumerator ()
		{
			return this.GetEnumerator ();
		}

		#endregion
	}
}