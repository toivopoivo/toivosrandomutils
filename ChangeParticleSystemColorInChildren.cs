﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetParticleSystemColorInChildren : MonoBehaviour
{

    public void Set(Color toColor)
    {
        var particleSystems = GetComponentsInChildren<ParticleSystem>(true);
        foreach (var item in particleSystems)
        {
            ParticleSystem.MainModule main = item.main;
            main.startColor = toColor;
        }

    }

}
