﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ToivonRandomUtils;
using UnityEngine.UI;
public class ListSpawner : MonoBehaviour {

    public ListSpawnerSettings settings=new ListSpawnerSettings();
    [System.Serializable]
    public class ListSpawnerSettings
    {
        public Font font;
        public Color fontColor = Color.black;
        public float minimunHeight=25.0f;
        public int fontSize = 12;
        public bool scrollable=false;
    }
    public List<RectTransform> listItems=new List<RectTransform>();

    [ContextMenu("test")]
    void TestList()
    {
        List<string> lista = new List<string>();
        lista.AddMultiple("penis", "kyrpä", "vittu");
        MakeStaticList(lista);
    }

    public void MakeStaticList(List<string> data, ListSpawnerSettings listSettings = null ){
        listItems.DestroyGameObjectsAndClear();
        if (listSettings != null)
        {
            settings = listSettings;
        }
        if (settings.scrollable) throw new System.NotImplementedException();
        for (int i = 0; i < data.Count; i++)
        {
            var item = new GameObject();
            listItems.Add(item.AddComponent<RectTransform>());
            item.AddComponent<LayoutElement>().minHeight=settings.minimunHeight;
            var textGO= new GameObject();
            textGO.transform.SetParent(item.transform);
            ZeroRectTransform(textGO.AddComponent<RectTransform>());
            var text = textGO.AddComponent<Text>();
            text.text = data[i];
            text.font = settings.font;
            text.color = settings.fontColor;
            text.fontSize = settings.fontSize;

        }
        MakeGenericList(listItems);
    }


    public void MakeGenericList(List<RectTransform> listItems)
    {
        if (this.listItems != listItems)
        {
            this.listItems.DestroyGameObjectsAndClear();
            this.listItems = listItems;
        }
        var listContainer= new GameObject ();
        var listRect= listContainer.AddComponent<RectTransform>();
        listContainer.transform.SetParent(transform);
        ZeroRectTransform(listRect);
        listContainer.AddComponent<VerticalLayoutGroup>().childForceExpandHeight=false;
        
        foreach (var item in listItems)
        {
            item.SetParent(listContainer.transform);
        }
       
    }
    public static void ZeroRectTransform( RectTransform rect)
    {
        rect.anchorMax = Vector2.one;
        rect.anchorMin = Vector2.zero;
        rect.anchoredPosition=Vector2.zero;
        rect.sizeDelta = Vector2.zero;
    }
}
