﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ToivoMeshGeneration;
using ToivonRandomUtils;

//[ExecuteInEditMode]
public class CanvasLineDrawer : MonoBehaviour
{
	public CanvasRenderer canvasRenderer;
	public Vector3 normalDir = Vector3.forward;
	public float width = 10f;
	public Material material;
	public bool onUpdate;
	[ToivonRandomUtils.InspectorButton]
	void ClearRenderer ()
	{
		canvasRenderer.Clear ();
	}

	void Update(){
		if (onUpdate) {
			DoIt ();
		}
	}

	[ToivonRandomUtils.InspectorButton]
	public void DoIt ()
	{
		RectTransform rectT = GetComponent<RectTransform> ();
		canvasRenderer = GetComponent<CanvasRenderer> ();
		Vector3 extends = new Vector3 (rectT.GetWorldWidth (), rectT.GetWorldHeight (), 0) * 0.5f;
		MeshGenerationData quadLine = QuadLine.GetQuadLine (-extends, extends, width, normalDir);
		ClearRenderer ();
		canvasRenderer.SetMesh (quadLine.Mesh); 
		canvasRenderer.SetMaterial (material, null);
	}
}
