﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteAlways]
public class RelativePreferredSize : MonoBehaviour
{
    [Range(0, 1)] public float value;
    LayoutElement layoutElement;
    private Canvas canvas;

    public ControlType controlType;
    public enum ControlType
    {
        Width,
        Height
    }
    private void Start()
    {
        layoutElement = GetComponent<LayoutElement>();
        canvas = GetComponentInParent<Canvas>(); 
        Update();
    }

    void Update()
    {
        if (Application.isPlaying) return;

        try
        {
            switch (controlType)
            {
                case ControlType.Width:
                    layoutElement.preferredWidth = RectTransformUtility.PixelAdjustRect((RectTransform)transform.parent,canvas).width * value ;
                    break;
                case ControlType.Height:
                    layoutElement.preferredHeight = RectTransformUtility.PixelAdjustRect((RectTransform)transform.parent,canvas).height * value ;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

        }
        catch (Exception e)
        {
            Debug.LogError(e, gameObject);
        }
    }
}
