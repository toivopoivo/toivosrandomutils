using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using ToivonRandomUtils;
using System.Linq;

public class ObjectSelectionWindowViewer:MonoBehaviour
{
	public RectTransform rectTrans;
	public virtual void Init(){
		rectTrans = GetComponent<RectTransform> ();
		rectTrans.ResetRectTrasform ();
	}

    /// <summary>
    /// destroying is actually not necessary
    /// it is only a example of disposing viewer
    /// override this function change
    /// </summary>
    public virtual void EndViewing()
    {
        Debug.Log("ObjectSelectionWindowViewer destroying " + gameObject.name);
        Destroy(gameObject);
    }


	public virtual void ViewObject(object obj){

	}
}







