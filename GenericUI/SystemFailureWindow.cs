﻿using System;
using System.Collections;
using System.Collections.Generic;
using ToivonRandomUtils;
using UnityEngine;
using UnityEngine.UI;

public class SystemFailureWindow : MonoBehaviour
{
    public UnityEngine.UI.Text text;
    public UnityEngine.UI.Button extraOptionTemplate;
    public static void Show(string errorMessage, params NamedAction[] extraOptions)
    {
        try
        {
            var systemFailureWindow = Resources.Load<SystemFailureWindow>("SystemFailureCanvas");
            var failureWindow = Instantiate(systemFailureWindow);
            extraOptions.ForEach(x => x.HookButton(failureWindow.extraOptionTemplate.DoTheStandardCloningThing(new List<Button>())));
            failureWindow.GetComponent<SystemFailureWindow>().text.text = errorMessage;
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }
    public void Reload()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
    public void Exit()
    {
        Application.Quit();
    }
}
