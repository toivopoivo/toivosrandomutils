﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FittedGridLayout : MonoBehaviour
{
	[NonNullField]
	public RectTransform rectTransform;
	public List<RectTransform> fitted = new List<RectTransform> ();



	public bool doOnUpdate;
	[NonEditableField(typeof(float))]
	public float oneCellWidth;
	[NonEditableField(typeof(float))]
	public float oneCellHeigth;
	[NonEditableField(typeof(int))]
	public int rows;
	[NonEditableField(typeof(int))]
	public int rowSize;
	[Header ("warning: causes funny things")]
	public bool goTopToDown = false;

	void Reset ()
	{
		FetchChilds ();
	}

	[ToivonRandomUtils.InspectorButton]
	public void FetchChilds ()
	{
		rectTransform = GetComponent<RectTransform> ();
		fitted.Clear ();
		GetComponentsInChildren<RectTransform> (fitted);
		fitted.RemoveAll (x => x.parent != rectTransform);

	}

	void Update ()
	{
		if (doOnUpdate)
			Layout ();
	}

	[ToivonRandomUtils.InspectorButton]
	public void Layout ()
	{
		rows = Mathf.CeilToInt (Mathf.Sqrt (fitted.Count)); 
		rowSize = Mathf.CeilToInt (Mathf.Sqrt (fitted.Count));
		oneCellWidth = 1f / rows;
		oneCellHeigth = 1f / rowSize;
		int indexer = 0;
//		int halfCount = Mathf.FloorToInt (fitted.Count / 2f);
		int gSize = rows * rowSize;
		if (gSize > fitted.Count) {
			int delta = gSize - fitted.Count;
			if (delta >= rowSize)
				rows--;
		}

		if (goTopToDown) {
			Top2DownSortie (fitted, rowSize);
		}

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < rowSize; j++) {
				if (indexer == fitted.Count) {
					break;
				}
				fitted [indexer].anchorMin = new Vector2 (oneCellWidth * j, Height (i));
				fitted [indexer].anchorMax = new Vector2 (oneCellWidth * (j + 1), Height (i + 1));
				if (goTopToDown) {
					// swap min/max y 
					float temp = fitted [indexer].anchorMin.y;
					fitted [indexer].anchorMin = new Vector2 (fitted [indexer].anchorMin.x, fitted [indexer].anchorMax.y);
					fitted [indexer].anchorMax = new Vector2 (fitted [indexer].anchorMax.x, temp);
				}
				fitted [indexer].sizeDelta = Vector2.zero;
				fitted [indexer].anchoredPosition = Vector2.zero;
//				fitted[indexer]
				indexer++;
			}

		}
	}

	/// <summary>
	/// Layout the specified rectTransforms. <-nice autocomplete. anyways, doesnt take into account if theres differents size subarrays. idgaf
	/// </summary>
	public void Layout (RectTransform[][] rectTransforms)
	{
		oneCellHeigth = 1f / rectTransforms.Length;
		oneCellWidth = 1f / rectTransforms [0].Length;
		for (int i = 0; i < rectTransforms.Length; i++) {
			for (int j = 0; j < rectTransforms [i].Length; j++) {
				try {
					rectTransforms [i] [j].anchorMin = new Vector2 (oneCellWidth * j, Height (i));
				} catch (System.Exception ex) {
					throw ex;
				}
				rectTransforms [i] [j].anchorMax = new Vector2 (oneCellWidth * (j + 1), Height (i + 1));
				if (goTopToDown) {
					// swap min/max y 
					float temp = rectTransforms [i] [j].anchorMin.y;
					rectTransforms [i] [j].anchorMin = new Vector2 (rectTransforms [i] [j].anchorMin.x, rectTransforms [i] [j].anchorMax.y);
					rectTransforms [i] [j].anchorMax = new Vector2 (rectTransforms [i] [j].anchorMax.x, temp);
				}
				rectTransforms [i] [j].sizeDelta = Vector2.zero;
				rectTransforms [i] [j].anchoredPosition = Vector2.zero;
				//				fitted[indexer]
			}

		}
	}
	//	/// <summary>
	//	/// used to reorganize layouted items so that their flow direction changes.
	//	/// </summary>
	//	public static void Top2DownSortie<T> (List<T> sortedList, int rowSize)
	//	{
	//		if (rowSize < 2)
	//			return;
	//		List<T> unsorted = sortedList.ToList ();
	//		List<int> indexesToBeRemoved = new List<int> ();
	//		sortedList.Clear ();
	//		int indexer = 0;
	//		while (unsorted.Count > 0) {
	//			if ((indexer) % rowSize == 0) {
	//				if (indexer == unsorted.Count)
	//					return;
	//				sortedList.Add (unsorted [indexer]);
	//				indexesToBeRemoved.Add (indexer);
	//			}
	//			indexer++;
	//			if (indexer == unsorted.Count) {
	//				indexer = 0;
	//				for (int i = 0; i < indexesToBeRemoved.Count; i++) {
	//					unsorted.RemoveAt (indexesToBeRemoved [i] - i);
	//				}
	//				indexesToBeRemoved.Clear ();
	////				sortedList.Add (unsorted [0]);
	//			}
	//		}
	//	}

	/// <summary>
	/// used to reorganize layouted items so that their flow direction changes.
	/// </summary>
	public static void Top2DownSortie<T> (List<T> sortedList, int rowSize)
	{
		List<T> unsortedList = sortedList.ToList ();
		sortedList.Clear ();
		for (int i = 0; i < rowSize; i++) {
			for (int j = i; j < unsortedList.Count; j += rowSize) {
				sortedList.Add (unsortedList [j]);
			}
		}
	}

	float Height (int i)
	{
		if (!goTopToDown)
			return oneCellHeigth * i;
		else
			return 1f - oneCellHeigth * i;

	}
}

