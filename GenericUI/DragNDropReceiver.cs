﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragNDropReceiver : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{

    private Func<DragNDropReceiver, DragNDroppable, bool> validateDrop;
    private Action<DragNDropReceiver, DragNDroppable> onDropPerformed;

    public System.Action onPointerEnterWhenNotDragging;
    public System.Action onPointerExitWhenNotDragging;

    // Setup method to configure validation and drop performed actions
    public void Setup(Func<DragNDropReceiver, DragNDroppable, bool> validateDropFunc, Action<DragNDropReceiver, DragNDroppable> onDropPerformedAction)
    {
        validateDrop = validateDropFunc;
        onDropPerformed = onDropPerformedAction;
    }

    // Called when the cursor enters the receiver
    public void OnPointerEnter(PointerEventData eventData)
    {

        if (DragNDroppable.currentDragged != null)
        {
            DragNDroppable.currentDragged.SetDropTarget(this);
        }
        else
        {
            onPointerEnterWhenNotDragging?.Invoke();
        }
    }

    // Called when the cursor exits the receiver
    public void OnPointerExit(PointerEventData eventData)
    {
        if (DragNDroppable.currentDragged != null && DragNDroppable.currentDragged.dropTarget == this)
        {
            DragNDroppable.currentDragged.SetDropTarget(null);
        }
        else
        {
            onPointerExitWhenNotDragging?.Invoke();
        }
    }

    // Handle the drop event
    public void HandleDrop(DragNDroppable droppable)
    {
        // Validate the drop using the provided function
        if (validateDrop != null && validateDrop.Invoke(this, droppable))
        {
            // If the drop is valid, invoke the action
            onDropPerformed?.Invoke(this, droppable);
        }
        else
        {
            Debug.Log("invalid drop");
        }
    }
}