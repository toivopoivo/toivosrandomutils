﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using ToivonRandomUtils;
using System;
using System.Reflection;
using System.Linq;

public class GenericSettingsPanel : MonoBehaviour
{
	public static GenericSettingsPanel instance;
	public Text settingLabel;
	public Setting settingTemplate;
	private static List<GenericSetting> genericSettings = new List<GenericSetting> ();
	public List<Setting> clones = new List<Setting> ();
	public bool useKeyToggle = true;
	public KeyCode key = KeyCode.F10;
	public bool closeOnStart = true;
	public bool autoSaveToPlayerPref = false;
	public bool createStandardButtons = false;
	void Awake ()
	{
		instance = this;
	}

	void Start ()
	{
		if(createStandardButtons){
			AddSetting (new GenericSetting ("Tehdas asetukset", ClearPrefs));
			AddSetting (new GenericSetting ("Peruuta muutokset", ClearPrefs));
			AddSetting (new GenericSetting ("Tallenna", SaveSettings ));
		}
		Refresh ();
		if (closeOnStart)
			gameObject.SetActive (false);

		CheckKeyLogger ();

	}
	public void RemoveSettings(){
		genericSettings.Clear ();
	}

	void CheckKeyLogger ()
	{
		if (keyLogger == null && useKeyToggle) {
			Debug.Log ("olen täällä");
			keyLogger = InvokeAction.Invoke (
				() => {
					return (Input.GetKeyUp (key));
				},
					() => {
						Refresh();
						gameObject.SetActive (!gameObject.activeSelf);
						Debug.Log ("toimin");
					},
					true,
					true

			);
		}
	}

	public string SaveSettings ()
	{
		System.Text.StringBuilder savedSettings = new System.Text.StringBuilder ();
		foreach (var item in genericSettings) {
			if (item.onSettingChange == null)
				continue;
			var mySlider = clones.Find ((x) => {
				return x.mySetting == item;
			}).slider;
			PlayerPrefs.SetFloat (item.name, mySlider.value);
			savedSettings.Append (item.name);
			savedSettings.Append (";");
			Debug.Log ("tallennettiin " + item.name);
		}
		PlayerPrefs.SetString ("genericSettingsPanelSettings", savedSettings.ToString ());
		return "tallennettu";
	}

	ActionTaskHandler keyLogger;

	public void Refresh ()
	{
		CheckKeyLogger ();
		clones.DestroyGameObjectsAndClear ();
		settingTemplate.gameObject.SetActive (true);
		foreach (var item in genericSettings) {
            
			var clone = settingTemplate.DoTheStandardCloningThing (clones, settingTemplate.transform.parent);
			clone.SetUp (item);

		}
		settingTemplate.gameObject.SetActive (false);
        
	}

	void OnDisable ()
	{
		if (autoSaveToPlayerPref)
			SaveSettings ();
		foreach (var item in clones) {
			item.mySetting.startValue = item.slider.value;
		}
	}

	void OnDestroy ()
	{
		if (autoSaveToPlayerPref)
			SaveSettings ();

	}

	[ContextMenu ("clear prefs")]
	public string ClearPrefs ()
	{
		foreach (var item in PlayerPrefs.GetString("genericSettingsPanelSettings").Split(';')) {
			if (PlayerPrefs.HasKey (item))
				PlayerPrefs.DeleteKey (item);
			else
				Debug.Log ("no saved settings for " + item);
		}
		return "tyhjennetty";

	}

	public string ResetSettings ()
	{
		foreach (var item in clones) {
			item.slider.value = item.mySetting.startValue;
			item.SliderChange ();
		}
		return "muutokset peruttu";
	}

	public static void AddSetting (GenericSetting s, bool redraw = true)
	{
        
		if (PlayerPrefs.HasKey (s.name) && s.onSettingChange != null) {
			Debug.Log ("löyty asetus " + s.name);
			float f = PlayerPrefs.GetFloat (s.name);
			s.onSettingChange (f);
			s.startValue = f;
		}
		genericSettings.Add (s);
		if (instance != null)
		if (redraw)
			instance.Refresh ();
	}

	public static void AddSettings (bool doNotRedraw = false, params GenericSetting[] s)
	{
		foreach (var item in s) {
			AddSetting (item, (doNotRedraw) ? false : item == s [s.Length - 1]);
		}
	}

	public static void MakeClassAdjustableFromSettings<T> (T adjustable)where T: ISettingsClass
	{
//		adjustable.GetType().GetFields
		var gs = adjustable as ISettingsClass;
		var fields = gs.GetType ().GetFields ( );
		var methods = gs.GetType ().GetMethods ();
		List<GenericSetting> setts = new List<GenericSetting> ();
		List<string> usedNames = new List<string> ();
		foreach (var item in methods) {
//			if (item.Name == "ToggleFreeformCamera") {
//				Debug.Log ("oikee");
//			}
			MethodInfo current = item;
			var attributes = item.GetCustomAttributes (typeof(SettingMethod), false);
			if (attributes.Length > 0) {
				if (item.ReturnType != typeof(string)) {
					throw new System.Exception ("generic setting panel methods must return string output. I.E \"on / off\" type of  thing. also no parameters");
				}
				usedNames.Add (item.Name);
				setts.Add (
									
					new GenericSetting (
						(attributes [0] as SettingMethod).name, () => {
						string returnVal = current.Invoke (adjustable, null) as string;
						Debug.Log (item.Name);
						return returnVal;
					}) 
				);
				
			}
		}
//
		foreach (var item in fields) {
			var attributes = item.GetCustomAttributes (typeof(SettingRange), false);

			if (attributes.Length > 0) {
				FieldInfo current = item;
				SettingRange attribute= attributes [0] as SettingRange;
				setts.Add (
					new GenericSetting (
						current.Name,
						(x) => {
							current.SetValue (adjustable, Mathf.Lerp (attribute.minAmount, attribute.maxAmount, x));
							return current.GetValue (adjustable).ToString();
						},
						Mathf.InverseLerp(attribute.minAmount, attribute.maxAmount, (float)current.GetValue (adjustable))
					)
				);
			}
//
		}


		AddSettings (true, setts.ToArray ());
	}
}

/// <summary>
/// use only on floats
/// </summary>

[AttributeUsage (AttributeTargets.Field)]
public class SettingRange : Attribute
{
	public float maxAmount, minAmount;
	public string settingName;

	public SettingRange (float minAmount, float maxAmount, string settingName)
	{
		this.minAmount = minAmount;
		this.maxAmount = maxAmount;
		this.settingName = settingName;
	}

}

/// <summary>
/// Setting method must return string
/// </summary>
[AttributeUsage (AttributeTargets.Method)]
public class SettingMethod : Attribute
{
	public string name;

	public SettingMethod (string name)
	{
		this.name = name;
	}
}

public interface ISettingsClass
{
	string SettingsID{ get; }
}

public class GenericSetting
{
	public Func<float, string> onSettingChange;
	public Func<string> onToggle;
	public string name;
	public Action onSetUp;
	public float startValue;

	public GenericSetting (string name, System.Func<float, string> onSettingChange, float startVal)
	{
		this.name = name;
		this.onSettingChange = onSettingChange;
		startValue = startVal;
	}

	public GenericSetting (string name, System.Func<string> onToggle)
	{
		this.name = name;
		this.onToggle = onToggle;
	}
}