﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ToivonRandomUtils;

public class Setting : MonoBehaviour
{
	public Text text, buttonText;
	public Slider slider;
	public Button button;
	string settingName;
	System.Func<float, string> onSettingChange;
	System.Func<string> onToggle;
	public GenericSetting mySetting;

	public void SetUp (GenericSetting s)
	{
		mySetting = s;
		if (s.onToggle != null) {
			SetUp (s.name, s.onToggle);
		} else {
			slider.value = s.startValue;
			SetUp (s.name, s.onSettingChange);
		}
	}

	public void SetUp (string settingName, System.Func<string> onToggle)
	{
		this.onToggle = onToggle;
		this.settingName = settingName;
		button.gameObject.SetActive (true);
		slider.gameObject.SetActive (false);
//		var but = button.GetComponentInChildren<Text> ();
		buttonText.text = settingName; 
		text.text = "";
	}

	public void SetUp (string settingName, System.Func<float, string> onSettingChange)
	{
		this.onSettingChange = onSettingChange;
		this.settingName = settingName;
		button.gameObject.SetActive (false);
		slider.gameObject.SetActive (true);
		slider.value = mySetting.startValue;
		SliderChange ();
	}

	public void SliderChange ()
	{
		if (onSettingChange != null) {
			text.text = settingName + ": " + onSettingChange (slider.value);
		}
	}

	public void OnToggle ()
	{
		Debug.Log ("click");
		text.text = onToggle ();
		InvokeAction.Invoke (() => {
			if (text != null)
				text.text = "";
		}, 2f);
	}
}
