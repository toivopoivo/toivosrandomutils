using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageBlinker : MonoBehaviour
{
    Image image;
    public Color startColor = Color.white;
    public Color blinkColor = Color.yellow;
    public float speed = 3;
    void Start()
    {
        image = GetComponent<Image>();
    }

    void Update()
    {
        image.color = Color.Lerp(startColor,blinkColor, Mathf.Sin( Time.unscaledTime  * speed ) );
    }

    private void OnDisable()
    {
        image.color = startColor;
    }
}
