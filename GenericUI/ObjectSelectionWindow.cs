﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;
using ToivonRandomUtils;
using System.Linq;

public class ObjectSelectionWindow : MonoBehaviour
{
	public Text label, dataText;
	public Transform buttonHolder;
	public ButtonAndText buttonTemplate;
	public Image image;
	public Button bigButtonTemplateLeft, bigButtonTemplateRight;


    public RectTransform normalViewer;
	ObjectSelectionWindowViewer customViewer;


	public List<ButtonAndText> clones = new List<ButtonAndText> ();
	List<Button> bigButtonLeftClones = new List<Button> ();
	List<Button> bigButtonRightClones = new List<Button> ();

	public MeshRenderer boundingBoxMeshRenderer;
	//Action<System.Object> onSelect;

	//Action onExit;
	public	ISelectionWindowItem selected;

	Button confirmation;

	public Dropdown quicktoggleDropdown;
	public void QuickToggleDropdownChanged(int value)
	{
		if (value == 1)
		{
			clones.ForEach(x=>x.GetComponentInChildren<Toggle>().isOn = true);
		}

		if (value == 2)
		{
			clones.ForEach(x=>x.GetComponentInChildren<Toggle>().isOn = false);
		}

		quicktoggleDropdown.value = 0;

	}

	/// <summary>
	/// Sets everything up
	interface IExtraButton
	{
		Button Button { get; set; }
		
		bool OnlyAccessibleWhenItemSelected { get; set; }
		
		bool Interactable (object item);
	}
	
	
	public class ExtraButton<T> :IExtraButton
	{
		public string name;
		public Action<T> action;
		public Func<T, bool> comfirmationPresquisities;
		
		public ExtraButton (string name, Action<T> action, Func<T, bool> comfirmationPresquisities, bool onlyAccessibleWhenItemSelected)
		{
			this.name = name;
			this.action = action;
			this.comfirmationPresquisities = comfirmationPresquisities;
			this.OnlyAccessibleWhenItemSelected = onlyAccessibleWhenItemSelected;
		}
		
		public Button Button { get; set; }
		
		public bool OnlyAccessibleWhenItemSelected { get ; set; }
		
		
		public bool Interactable (object obj)
		{
			var cast = (T)obj;
			return comfirmationPresquisities (cast);
		}
	}

	

    public bool twoClickConfirm = true;

	List<IExtraButton> iExtraButtons;
	private System.Action<object> onSelect;
	

	public Dictionary<object, ISelectionWindowItem> windowListItems =new Dictionary<object, ISelectionWindowItem>();

	/// <summary>>
	/// sets up the object selection window
	/// </summary>
	/// <param name="title">Title of the window.</param>
	/// <param name="selectable">The selectable data</param>
	/// <param name="comfirmationPresquisities">should return if user is able to "buy" the data.</param>
	/// <param name="onConfirm">what to do after succesful purchase and handle closing the window.</param>
	/// <param name="nameOfTheObject">Should return name of the object.</param>
	/// <param name="onExit">Should handle exiting the window, do you want to destroy or set nonactive?</param>
	/// <param name="description">Should give description</param>
	/// <param name="newCustomViewer">custom visualisator ?</param>
	/// <param name="extraButtons">perhaps you want some extra functionality?</param>
	/// <param name="onSelect">do you want some special thing to happen when a thingi s selected</param>
	public void SetUp<T> (string title, List<T> selectable, Func<T, bool> comfirmationPresquisities,
	                      Action<T> onConfirm, Func<T, string> nameOfTheObject, Action onExit, Func<T, string> description,
						ObjectSelectionWindowViewer newCustomViewer = null,System.Action<T> onSelect = null,
	                      System.Func<T, bool> quickToggleGet = null, System.Action<T,bool> quickToggleSet = null, string nothingSelectedText = "", params ExtraButton<T>[] extraButtons)
	{

		if (this.customViewer != null) customViewer.EndViewing();
		normalViewer.gameObject.SetActive (newCustomViewer == null);
		if (newCustomViewer != null) {
			newCustomViewer.transform.SetParent (normalViewer.parent);
			newCustomViewer.Init ();
			customViewer = newCustomViewer;
//			createdViewer.getc
		}

		if (quickToggleGet == null)
		{
			quicktoggleDropdown.gameObject.SetActive(false);
		}
		else
		{
			quicktoggleDropdown.gameObject.SetActive(true);
			quicktoggleDropdown.onValueChanged.RemoveListener(QuickToggleDropdownChanged);
			quicktoggleDropdown.onValueChanged.AddListener(QuickToggleDropdownChanged);
		}
	

		if (onSelect != null) this.onSelect = (x) => onSelect((T) x);
		else this.onSelect = null;

		dataText.text = nothingSelectedText;
		iExtraButtons = extraButtons.Cast<IExtraButton> ().ToList ();
		//	this.extraButtons = extraButtons;
		bigButtonLeftClones.DestroyGameObjectsAndClear ();
		bigButtonRightClones.DestroyGameObjectsAndClear ();
//		var get = GetComponent<DisableOnStart> ();
//		if (get) {
//			Destroy(get);
//		}
		gameObject.SetActive (true);
		//this.onSelect =  onSelect;
		//this.comfirmationPresquisities = (x) => { return comfirmationPresquisities((T)x); };
		//this.onConfirm = onConfirm;

		//this.nameOfTheObject = (x) => { return nameOfTheObject((T)x); };

		//this.onExit = onExit;

		AddButton (false, "Exit", onExit);
		if (onConfirm != null) {
			confirmation = AddButton (true, "Confirm", () => {
				onConfirm ((T)selected.value);
			});
			confirmation.interactable = false;
		}

		foreach (var item in extraButtons) {
			item.Button = AddButton (true, item.name, () => {
				item.action ((T)selected.value);
			});
			if (item.OnlyAccessibleWhenItemSelected)
				confirmation.interactable = false;
		}

		label.text = title;
		foreach (var item in clones) { 
			Destroy (item.gameObject);
		}
		clones.Clear ();
		buttonTemplate.gameObject.SetActive (true);
		windowListItems.Clear();
		for (int i = 0; i < selectable.Count; i++) {
			ISelectionWindowItem item = new SelectionWindowItem<T> (selectable [i], comfirmationPresquisities, nameOfTheObject, description);
			windowListItems.Add(selectable[i],item);
			var clone = buttonTemplate.DoTheStandardCloningThing (clones, buttonHolder);
			clone.text.text = nameOfTheObject (selectable [i]);
			if (comfirmationPresquisities != null && !comfirmationPresquisities (selectable [i]))
				clone.text.color = Color.gray;
			clone.onSendData = (x) => {
				Select (item);
			};

			var toggle = clone.GetComponentInChildren<Toggle>();
			if (quickToggleGet != null)
			{
				toggle.isOn = quickToggleGet(selectable[i]);
				var cache = selectable[i];
				toggle.onValueChanged.AddListener(x => quickToggleSet(cache, x));
			}
			else
			{
				toggle.gameObject.SetActive(false);
			}
			clone.someData = item;
		}
		buttonTemplate.gameObject.SetActive (false);
        Debug.Log("ObjectSelectionWindow " + gameObject.name +" setup complete. selectable count: "+selectable.Count);
		//this.description = (x) => {return description((T)x) ; };
	}
	

	Button AddButton (bool leftSide, string buttonName, Action onClick)
	{
		var but = MakeBasicButton (leftSide, buttonName); //h a c k
		but.noArgsSendData = onClick;
		return but.button;
	}
	//    Button AddButton<T>(bool leftSide, string name, Action<T> onClickSelectedCallBack)
	//    {
	//		var BAT = MakeBasicButton (leftSide, name);
	//		Debug.Log ("tpye t:" + typeof(T));
	//		if(selected != null ){
	//			BAT.onSendData = (x)=>{ onClickSelectedCallBack( (T)selected.value); }; // x does nothing
	//		}
	//		return BAT.button;
	//    }

	ButtonAndText MakeBasicButton (bool leftSide, string buttonName)
	{
		Button clone;
		if (leftSide) {
			bigButtonTemplateLeft.gameObject.SetActive (true);
			clone = bigButtonTemplateLeft.DoTheStandardCloningThing (bigButtonLeftClones);
			bigButtonTemplateLeft.gameObject.SetActive (false);
		} else {
			bigButtonTemplateRight.gameObject.SetActive (true);
			clone = bigButtonTemplateRight.DoTheStandardCloningThing (bigButtonRightClones);
			bigButtonTemplateRight.gameObject.SetActive (false);

		}

		ButtonAndText BAT = clone.gameObject.GetOrAddComponent<ButtonAndText> ();
		BAT.GetButtonAndText ();
		BAT.text.text = buttonName;
		return BAT;
	}


	public void Select (ISelectionWindowItem windowItem)
	{
		selected = windowItem;
		UpdateSelected();
		//if(onSelect!=null1
		//onSelect (someData);
		
		onSelect?.Invoke(windowItem.value);
	}

	public void UpdateSelected()
	{
		if (selected == null)
		{
			return;
		}
		dataText.text = selected.Description;
		confirmation.interactable = selected.ConfirmationPrequisities;
		foreach (var item in iExtraButtons) {
			if (item.OnlyAccessibleWhenItemSelected) {
				item.Button.interactable = item.Interactable (selected.value);
			}
		}
		if (customViewer != null) {
			customViewer.ViewObject (selected.value);
		}

	}

	//public void Confirm ()
	//{
	//    onConfirm (selected);
	//}
	//public void Exit(){
	//    onExit ();
	//}
}

public interface ISelectionWindowItem
{
	bool ConfirmationPrequisities { get; }

	string Name { get; }

	string Description { get; }

	object value { get; }
}

public class SelectionWindowItem<T>:ISelectionWindowItem
{
	public T item;
	Func<T, bool> comfirmationPresquisities;
	Func<T, string> nameOfTheObject;
	Func<T, string> description;

	public SelectionWindowItem (T item, Func<T, bool> comfirmationPresquisities, Func<T, string> nameOfTheObject, Func<T, string> description)
	{
		this.item = item;
		this.comfirmationPresquisities = comfirmationPresquisities;
		this.nameOfTheObject = nameOfTheObject;
		this.description = description;
	}

	public bool ConfirmationPrequisities {
		get {
			if (comfirmationPresquisities == null)
				return true;
			else
				return comfirmationPresquisities (item);
		}
	}

	public string Name { get { return nameOfTheObject (item); } }

	public string Description { get { return description (item); } }

	public object value { get { return item; } }
}

public class NamedAction
{
	public Action action;
	public string name;

	public NamedAction ()
	{
	}

	public NamedAction (string buttonName, Action action)
	{
		this.action = action;
		name = buttonName;
	}

	public NamedAction (Action action, string buttonName)
	{
		this.action = action;
		name = buttonName;
	}

	public void HookButton(Button button)
	{
		button.onClick.AddListener(()=>action());
		var text = button.GetComponentInChildren<Text>();
		text.text = name;
	}
}
