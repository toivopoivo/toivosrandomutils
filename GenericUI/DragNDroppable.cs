using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;

public class DragNDroppable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    private Func<RectTransform> createMovedGraphicFunc;
    private RectTransform movedGraphic;
    private Canvas canvas;
    private CanvasGroup canvasGroup;
    private Vector3 originalPosition;
    public DragNDropReceiver dropTarget;
    public static DragNDroppable currentDragged;

    // Setup method that takes a Func<RectTransform> to instantiate the movedGraphic dynamically
    public void Setup(Func<RectTransform> createGraphicFunc)
    {
        createMovedGraphicFunc = createGraphicFunc;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        originalPosition = transform.position;
        currentDragged = this;
        CreateDragCanvasAndSpawnGraphic();
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (movedGraphic != null)
        {
            // Apply the cursor offset to keep the graphic aligned with the cursor correctly
            movedGraphic.position = Input.mousePosition;
        }
    }

    private void OnDisable()
    {
        OnEndDrag(default);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (movedGraphic != null)
        {
            Destroy(movedGraphic.gameObject);
        }

        // Notify the receiver about the end of the drag if the target is valid
        if (dropTarget != null)
        {
            try
            {
                dropTarget.HandleDrop(this);

            }
            catch (Exception e)
            {
                Debug.LogException(e);
            }
        }
        else
        {
            // Revert to the original position if no valid drop target
            transform.position = originalPosition;
        }

        // Clear the drop target on end drag
        dropTarget = null;
        currentDragged = null;
        // Clean up the temporary canvas
        if (canvas != null)
        {
            Destroy(canvas.gameObject);
        }
    }

    private void CreateDragCanvasAndSpawnGraphic()
    {
        // Create a new canvas for the dragged graphic
        canvas = new GameObject("DragCanvas").AddComponent<Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        canvas.sortingOrder = 1000;

        var canvasScaler = canvas.gameObject.AddComponent<CanvasScaler>();
        canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
        canvasScaler.referenceResolution = new Vector2(1920, 1080);

        // Use the Func to spawn the movedGraphic
        movedGraphic = createMovedGraphicFunc.Invoke();
        movedGraphic.SetParent(canvas.transform, false);
        movedGraphic.sizeDelta = GetComponent<RectTransform>().sizeDelta;

        // Add a CanvasGroup to control transparency
        canvasGroup = movedGraphic.gameObject.AddComponent<CanvasGroup>();
        canvasGroup.alpha = 0.5f;
        canvasGroup.blocksRaycasts = false;
    }

    // Method for setting the drop target, which is set by the DragNDropReceiver
    public void SetDropTarget(DragNDropReceiver target)
    {
        dropTarget = target;
    }
}
