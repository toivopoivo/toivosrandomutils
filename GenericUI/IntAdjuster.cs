﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.SymbolStore;
using UnityEngine;

public class IntAdjuster : MonoBehaviour
{
	public UnityEngine.UI.Text fieldTitle;
	public UnityEngine.UI.Text currentValue;
	public UnityEngine.UI.Button increaseButton;
	public UnityEngine.UI.Button decreaseButton;

	private System.Action<int> onChanged;
	private System.Func<int,bool>  isValidValue;
	private int current;
	public void Configure(string fieldName, int value, System.Action<int> onChanged, System.Func<int,bool> isValidValue)
	{
		this.isValidValue = isValidValue; 
		current = value;
		fieldTitle.text = fieldName;
		currentValue.text = value.ToString();
		this.onChanged = onChanged;
		
		
		
		increaseButton.onClick.RemoveAllListeners();
		increaseButton.onClick.AddListener(()=>Add(1));
		
		decreaseButton.onClick.RemoveAllListeners();
		decreaseButton.onClick.AddListener(()=>Add(-1));
		
		EnableIfCanBeAddedOrOpposite(1,increaseButton);
		EnableIfCanBeAddedOrOpposite(-1,decreaseButton);

	}

	void Add(int val)
	{
		current += val;
		onChanged(current);
		currentValue.text = current.ToString();
		EnableIfCanBeAddedOrOpposite(1,increaseButton);
		EnableIfCanBeAddedOrOpposite(-1,decreaseButton);
	}

	void EnableIfCanBeAddedOrOpposite(int value, UnityEngine.UI.Button button)
	{
		button.interactable = isValidValue(current + value);
	}
	

}
