﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ToivonRandomUtils;
using System;
using System.Linq;
using System.Reflection;
using UnityEngine.UI;
public class ReflectionAdjuster : ObjectSelectionWindowViewer
{
    public ReflectionAdjusterControl template;
    public List<ReflectionAdjusterControl> createdControls = new List<ReflectionAdjusterControl>();
    public List<Button> createdBackButtons = new List<Button>();
    public RectTransform contentRect;
    [UnityEngine.Serialization.FormerlySerializedAs("backButton")]
    public Button backButtonTemplate;

    public LogType logType;

    public override void ViewObject(object obj)
    {
     //   lastTarget = null;
        gameObject.SetActive(true);
        StartAdjusting(obj);
    }

    bool init = false;

    void TryInit()
    {
        if (init) return;
        init = true;
        template.GetParsers();
    }

    public bool disableGameObjectOnEndViewing = true;

    public override void EndViewing()
    {
        if(disableGameObjectOnEndViewing) gameObject.SetActive(false);
        targetHistory.Clear();
        onEndViewing?.Invoke();
    }

    public System.Action onEndViewing;

    List<object> targetHistory = new List<object>();
    private bool customBindingFlags = false;
    private BindingFlags _bindingFlags;
    public void StartAdjusting<T>(T targetObject, bool addHistory= true, bool showHistory=true, BindingFlags? bindingFlags = null) where T:class
    {
        if (bindingFlags != null)
        {
            customBindingFlags = true;
            _bindingFlags = bindingFlags.Value;
        }
        if (targetObject == null)
        {
            throw new System.Exception("what are you doing?");
        }
        TryInit();
        if(addHistory) targetHistory.Add(targetObject);
        createdControls.DestroyGameObjectsAndClear();
        createdBackButtons.DestroyGameObjectsAndClear();
        if (showHistory)
        {
            for (int i = 0; i < targetHistory.Count; i++)
            {
                var createdBackButton = backButtonTemplate.DoTheStandardCloningThing(createdBackButtons);
                var component = createdBackButton.transform.GetChild(0).GetComponent<Text>();
                if (component != null)
                {
                    component.text = targetHistory[i].ToString();
                }
                else
                {
                    Debug.LogError("text component was null");
                }
                var cachedI = i;
                createdBackButton.onClick.AddListener(() =>
                {
                    var temp = targetHistory[cachedI];
                    for (int j = targetHistory.Count - 1; j >= cachedI + 1; j--)
                    {
                        targetHistory.RemoveAt(j);
                    }
                    StartAdjusting(temp, false);
                });
            }
        }


        IList rootList = targetObject as IList;
        if (rootList != null)
        {
            AdjustList(rootList);
        }
        else
        {
            AdjustDataContainer(targetObject);

        }
        Debug.Log("adjusting " + targetObject + ". createdControls count: " + createdControls.Count);
    }

    private void AdjustList(IList targetObject)
    {
        Type elementType = targetObject.GetElementType();
        var parserIndex = template.parsers.FindIndex(x => x.IsParserForType(elementType));
        Debug.Log("element type: " + elementType + " parser index: " + parserIndex);
        for (int i = 0; i < targetObject.Count; i++)
        {

            ReflectionAdjusterControl createdControl = template.DoTheStandardCloningThing(createdControls, contentRect.transform, true);
            if (parserIndex != -1)
            {
                var parser = createdControl.parsers[parserIndex];
                createdControl.activeParser = parser;
                createdControl.activeParser.gameObject.SetActive(true);
                createdControl.activeParser.InitListControl(createdControl, i, targetObject);
            }
            createdControl.label.text = i+". ";
            var item = targetObject[i];

            var removeButton = Instantiate(createdControl.listButton, createdControl.listButton.transform.parent);
            removeButton.gameObject.SetActive(true);
            removeButton.GetComponentInChildren<Text>().text = "remove";
            removeButton.onClick.AddListener(() => { targetObject.Remove(item); StartAdjusting(targetObject, false); });
            

            if (item != null)
            {
                createdControl.listButton.gameObject.SetActive(true);
                createdControl.listButton.onClick.AddListener(() => StartAdjusting(item));
                System.Reflection.FieldInfo listItemNameField = item.GetType().GetFields().FirstOrDefault(x => x.GetCustomAttributes(typeof(ReflectionAdjusterListItemName), true).Length != 0);
                if (listItemNameField != null )
                {
                    createdControl.listButton.GetComponentInChildren<Text>().text = listItemNameField.GetValue(item) as string;
                }
                else
                {
                    createdControl.listButton.GetComponentInChildren<Text>().text = targetObject[i].NullSafeToString();
                }
            }

        }
        var addEmpty = template.DoTheStandardCloningThing(createdControls, contentRect.transform, true);
        addEmpty.listButton.gameObject.SetActive(true);
        addEmpty.listButton.GetComponentInChildren<Text>().text = "add new instance";
        addEmpty.listButton.onClick.AddListener(() => { targetObject.Add(Activator.CreateInstance(targetObject.GetElementType())); StartAdjusting(targetObject, false); });
        addEmpty.label.gameObject.SetActive(false);
        
        var addEmitted = template.DoTheStandardCloningThing(createdControls, contentRect.transform, true);
        addEmitted.listButton.gameObject.SetActive(true);
        addEmitted.listButton.GetComponentInChildren<Text>().text = "add instance";
        addEmitted.listButton.onClick.AddListener(() => { targetObject.Add(Activator.CreateInstance(elementType)); StartAdjusting(targetObject, false); });
        addEmitted.label.gameObject.SetActive(false);
    }


    private void AdjustDataContainer<T>(T targetObject) where T : class
    {
        FieldInfo[] fields;
        if(customBindingFlags) fields = targetObject.GetType().GetFields(_bindingFlags);
        else  fields = targetObject.GetType().GetFields();
        
        foreach (var fieldInfo in fields)
        {
            var parserIndex = template.parsers.FindIndex(x => x.IsParserForType(fieldInfo.FieldType));
            var ilist = fieldInfo.GetValue(targetObject) as IList;

            if (ilist != null)
            {
                CreateButtonForNewAdjustmentLayer(fieldInfo, ilist);
            }
            else
            {
                if (parserIndex != -1)
                {
                    ReflectionAdjusterControl createdControl = template.DoTheStandardCloningThing(createdControls, contentRect.transform, true);
                    var parser = createdControl.parsers[parserIndex];
                    parser.gameObject.SetActive(true);
                    createdControl.label.text = fieldInfo.Name;
                    createdControl.activeParser = parser;
                    createdControl.activeParser.InitControl(createdControl, fieldInfo, targetObject);
                    createdControl.name = "adjuster for " + targetObject.GetType().Name + "." + fieldInfo.Name;
                } else if (fieldInfo.FieldType.IsClass)
                {
                    CreateButtonForNewAdjustmentLayer(fieldInfo, fieldInfo.GetValue(targetObject));
                }
            }
        }
    }

    private void CreateButtonForNewAdjustmentLayer(System.Reflection.FieldInfo fieldInfo, object target)
    {
    
        ReflectionAdjusterControl createdControl = template.DoTheStandardCloningThing(createdControls, contentRect.transform, true);
        createdControl.label.text = fieldInfo.Name +" (null: "+(target==null)+")";
        createdControl.listButton.interactable = target != null;
        createdControl.listButton.gameObject.SetActive(true);
        createdControl.listButton.onClick.AddListener(() => StartAdjusting(target));
    }
}
public abstract class AdjusterSettingParser : MonoBehaviour
{
    public abstract void InitControl(ReflectionAdjusterControl adjusterControl, System.Reflection.FieldInfo fieldInfo, object targetObject);
    public abstract object Get(ReflectionAdjusterControl adjusterControl);
    public abstract bool IsParserForType(System.Type type);
    public virtual void InitListControl(ReflectionAdjusterControl reflectionAdjusterControl ,int listIndex, IList targetList)
    {
        return;
    }
}
