﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ReflectionAdjusterControl : MonoBehaviour
{
    /*  public InputField inputField;
      public Toggle toggle;
      public Dropdown dropdown;
      */
    public Text label;
    public AdjusterSettingParser activeParser;
    public List<AdjusterSettingParser> parsers;

    public Button listButton;

    public void GetParsers()
    {
        parsers = GetComponentsInChildren<AdjusterSettingParser>(true).ToList();

    }
}


