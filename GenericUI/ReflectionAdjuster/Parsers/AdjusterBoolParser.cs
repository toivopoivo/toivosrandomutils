﻿using System;
using System.Reflection;
using UnityEngine.UI;

public class AdjusterBoolParser : AdjusterSettingParser
{
    public override bool IsParserForType(Type type)
    {
        return type == typeof(bool);
    }

    public override void InitControl(ReflectionAdjusterControl adjusterControl,  FieldInfo fieldInfo, object targetObject)
    {

        Toggle toggle = GetComponent<Toggle>();
        toggle.onValueChanged.AddListener((x) => fieldInfo.SetValue(targetObject, x));
        toggle.isOn = (bool)fieldInfo.GetValue(targetObject);
    }

    public override object Get(ReflectionAdjusterControl adjusterControl)
    {
        return GetComponent<Toggle>().isOn;
    }
}
