﻿using System;
using System.Reflection;
using UnityEngine.UI;

public class AdjusterIntParser : AdjusterSettingParser
{
 
    public override object Get(ReflectionAdjusterControl adjusterControl)
    {
        return int.Parse(GetComponent<InputField>().text);
    }

    public override void InitControl(ReflectionAdjusterControl adjusterControl, FieldInfo fieldInfo, object targetObject)
    {

        InputField inputField = GetComponent<InputField>();
        inputField.text = fieldInfo.GetValue(targetObject).ToString();
        if(fieldInfo.FieldType == typeof(int)) inputField.onValueChanged.AddListener((x) => fieldInfo.SetValue(targetObject, int.Parse(x)));
        // ushort
        else inputField.onValueChanged.AddListener((x) => fieldInfo.SetValue(targetObject, ushort.Parse(x)));

    }

    public override bool IsParserForType(Type type)
    {
        return type == typeof(int) || type == typeof(ushort);
    }
}
