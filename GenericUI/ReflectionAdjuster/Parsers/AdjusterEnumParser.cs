﻿using System.Collections.Generic;
using System;
using UnityEngine.UI;
using System.Reflection;

public class AdjusterEnumParser : AdjusterSettingParser
{
    public override bool IsParserForType(System.Type type){return typeof(System.Enum) == type; } 

    public override void InitControl(ReflectionAdjusterControl adjusterControl, FieldInfo fieldInfo, object targetobject)
    {
        var dropdown = GetComponent<Dropdown>();
        dropdown.gameObject.SetActive(true);
        dropdown.ClearOptions();
        object currentValue = fieldInfo.GetValue(targetobject);
        var values = System.Enum.GetValues(currentValue.GetType()) ;
        List<string> options = new List<string>();
        foreach (var item in values)
        {
            options.Add(item.ToString());
        }
        dropdown.AddOptions(options);
        dropdown.value = (int)currentValue;

        dropdown.onValueChanged.AddListener((x) => fieldInfo.SetValue(targetobject, x));

    }

    public override object Get(ReflectionAdjusterControl adjusterControl)
    {
        return GetComponent<Dropdown>().value;
    }
}
