﻿using System;
using System.Reflection;
using UnityEngine.UI;

public class AdjusterFloatParser : AdjusterSettingParser
{


    public override object Get(ReflectionAdjusterControl adjusterControl)
    {
        return float.Parse(GetComponent<InputField>().text);
    }

    public override void InitControl(ReflectionAdjusterControl adjusterControl, FieldInfo fieldInfo, object targetObject)
    {
        InputField inputField = GetComponent<InputField>();
        inputField.text = fieldInfo.GetValue(targetObject).ToString();
        inputField.onValueChanged.AddListener((x) => fieldInfo.SetValue(targetObject, float.Parse(x)));
    }

    public override bool IsParserForType(Type type)
    {
        return type == typeof(float);
    }
}
