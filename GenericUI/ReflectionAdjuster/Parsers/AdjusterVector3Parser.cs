﻿using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.UI;

public class AdjusterVector3Parser : AdjusterSettingParser
{

    public Text label;
    public InputField[] inputFields;

    public override object Get(ReflectionAdjusterControl adjusterControl)
    {
        return new Vector3(float.Parse( inputFields[0].text ) , float.Parse(inputFields[1].text), float.Parse(inputFields[2].text )  );
    }

    public override void InitControl(ReflectionAdjusterControl adjusterControl, FieldInfo fieldInfo, object targetObject)
    {
        for (int i = 0; i < inputFields.Length; i++)
        {
            var inputField = inputFields[i];
            label.text = fieldInfo.Name;
            Vector3 v3 = (Vector3)fieldInfo.GetValue(targetObject);
            inputFields[0].text = v3[0].ToString();
            inputFields[1].text = v3[1].ToString();
            inputFields[2].text = v3[2].ToString();
            inputField.onValueChanged.AddListener((x) => fieldInfo.SetValue(targetObject, Get(adjusterControl)));

        }
        
    }

    public override bool IsParserForType(Type type)
    {
        return type == typeof(Vector3);
    }
}
