﻿using System;
using System.Reflection;
using UnityEngine.UI;

public class AdjusterDateTimeParser : AdjusterSettingParser
{


	public override object Get(ReflectionAdjusterControl adjusterControl)
	{
		return GetComponent<InputField>().text;
	}

	public override void InitControl(ReflectionAdjusterControl adjusterControl, FieldInfo fieldInfo, object targetObject)
	{
		InputField inputField = GetComponent<InputField>();
		inputField.onValueChanged.AddListener((x) => fieldInfo.SetValue(targetObject,   System.DateTime.Parse(x)));
		inputField.text = fieldInfo.GetValue(targetObject) == null ? "" : fieldInfo.GetValue(targetObject).ToString();
	}

	public override bool IsParserForType(Type type)
	{
		return type == typeof(DateTime);
	}
}