﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ToivoLinq
{
    /// <summary>
    /// these functions should go very fast and not littering all over the place
    /// </summary>
    public static class FastTLinq
    {
        public static T FastSingle<T>(this IList<T> queryTarget, System.Func<T, bool> query, bool singleOrDefault = false)
        {
            int foundIndex = -1;
            for (int i = 0; i < queryTarget.Count; i++)
            {
                if (query(queryTarget[i]))
                {
                    if(foundIndex < 0)
                    {
                        foundIndex = i;
                    }else
                    {
                        throw new System.InvalidOperationException("single failed because multiple found");
                    }
                }
            }
            if (foundIndex < 0)
            {
                if(singleOrDefault) {
                    return default(T);
                }
                throw new System.InvalidOperationException("single failed because none found");
            }
            else
            {
                return queryTarget[foundIndex];
            }
        }

        public static T FastSingleOrDefault<T>(this IList<T> queryTarget, System.Func<T, bool> query) {
            return queryTarget.FastSingle(query,true);
        }

        public static bool FastAny<T>(this IEnumerable<T> queryTarget, System.Func<T, bool> query = null)
        {
            foreach (var item in queryTarget)
            {
                if (query == null || query(item))
                {
                    return true;
                }
            }
            return false;
        }
        public static void ExecuteWhere<T>(this IList<T> queryTarget, System.Func<T, bool> query, System.Action<T> action)
        {
            for (int i = 0; i < queryTarget.Count; i++)
            {
                if (query(queryTarget[i]))
                {
                    action(queryTarget[i]);
                }
            }
        }

        public static bool FastAll<T>(this IEnumerable<T> queryTarget, System.Func<T, bool> query = null)
        {
            foreach (var item in queryTarget)
            {
                if (!query(item))
                {
                    return false;
                }
            }
            return true;
        }

        public static int FastCount<T>(this IList<T> queryTarget, System.Func<T, bool> query = null)
        {
            int val = 0;
            for (int i = 0; i < queryTarget.Count; i++)
            {
                if (query == null || query(queryTarget[i]))
                {
                    val++;
                }
            }
            return val;
        }

        public static T FastLastOrDefault<T>(this IList<T> queryTarget, System.Func<T, bool> query = null)
        {
            return FastLast(queryTarget, query, true);
        }

        public static T FastLast<T>(this IList<T> queryTarget, System.Func<T, bool> query = null,bool orDefault=false) {
            int count = queryTarget.Count;
            if(orDefault && count == 0)
            {
                return default(T);
            }
            if (query == null && count > 0) {
                return queryTarget[count - 1];
            }
            for (int i = count - 1; i >= 0; i--) {
                if (query(queryTarget[i])) {
                    return queryTarget[i];
                }
            }
            throw new System.InvalidOperationException();
        }

        public static T FastFirstOrDefault<T>(this IList<T> queryTarget) {
            return FastFirstOrDefault(queryTarget, null);
        }

        public static T FastFirstOrDefault<T>(this IList<T> queryTarget, System.Func<T, bool> query)
        {
            for (int i = 0; i < queryTarget.Count; i++)
            {
                if (query == null || query(queryTarget[i]))
                {
                    return queryTarget[i];
                }
            }
            return default(T);
        }


        static T FastSelectBest<T>(this IList<T> queryTarget, System.Func<T, System.IComparable> query, bool lowest, bool orDefault = true)
        {
            if (orDefault)
            {
                if (queryTarget.Count == 0) return default(T);
            }
            System.IComparable best = query(queryTarget[0]);
            T returned = queryTarget[0];
            for (int i = 0; i < queryTarget.Count; i++)
            {
                System.IComparable current = query(queryTarget[i]);
                if (lowest ? current.CompareTo(best) < 0 : current.CompareTo(best) > 0)
                {
                    best = current;
                    returned = queryTarget[i];
                }
            }
            return returned;
        }

        public static T FastSelectLowestOrDefault<T>(this IList<T> queryTarget, System.Func<T, System.IComparable> query)
        {
            return FastSelectBest(queryTarget, query, true, true);
        }

        public static T FastSelectHighestOrDefault<T>(this IList<T> queryTarget, System.Func<T, System.IComparable> query)
        {
            return FastSelectBest(queryTarget, query, false, true);
        }

        //Dictionary<object,object> funcsToIenumerators
        //static bool inputListIsNormalListOutside = true;

        /*public static void FastWhere<T>(this IList<T> inputList, List<T> userProvidedResultsList, System.Func<T, bool> query) {

            //bool inputListIsNormalListLocal = inputList is List<T>;
            //inputListIsNormalListOutside = inputList is List<T>;
            //outsideval = inputList as List<T>;
            //outsideval = true;

            //if(inputList != userProvidedResultsList) { 
            //if(inputList != (userProvidedResultsList as IList<T>)) {
            if (inputList is List<T> &&  inputList != userProvidedResultsList) {
                userProvidedResultsList.Clear();
                //foreach (var item in inputList) {
                //   if(query(item)) {
                //       userProvidedResultsList.Add(item);
                //   }
                //}
                for (int i = 0; i < inputList.Count; i++) {
                    var item = inputList[i];
                    if(query(item)) {
                        userProvidedResultsList.Add(item);
                    }
                }
            }
            else {
                userProvidedResultsList.RemoveAll(x => !query(x));
            }
        }*/


        /// <summary>
        /// NOT TESTED - TEST BEFORE USAGE
        /// </summary>
        public static T FastNext<T>(this IList<T> inputList, System.Func<T, bool> query, int startIndex, bool loopAround,bool canReturnStartIndex, bool orDefault)
        {
            if (startIndex + 1 >= inputList.Count) {
                if (orDefault) return default(T);
                else throw new System.InvalidOperationException("no next, index last");
            }
            bool found = false;
            T foundItem = default(T);
            for (int i = 0; i < inputList.Count; i++)
            {
                if (i == inputList.Count - 1 && !loopAround) break;
                int index = startIndex + i % inputList.Count;
                if (!canReturnStartIndex && index == startIndex) continue;

                T item = inputList[index];
                if (found = query(item))
                {
                    foundItem = item;
                    break;
                }
            }

            if (!found && !orDefault)
            {
                throw new System.InvalidCastException("no next found");
            }

            return foundItem;

        }

        public static void SuperFastWhere<T>(this IList<T> inputList, T[] cacheArray, out int resultsCount, System.Func<T, bool> query)
        {
            resultsCount = 0;
            for (int i = 0; i < inputList.Count; i++)
            {
                if (query(inputList[i]))
                {
                    cacheArray[resultsCount] = inputList[i];
                    resultsCount++;
                }
            }
        }

        public static void FastWhere<T>(this IList<T> inputList, List<T> userProvidedResultsList, out int resultsCount, System.Func<T,bool> query) {

            //inputListIsNormalListOutside = inputList is List<T>;
            /*
            if (!inputListIsNormalListLocal || inputList != userProvidedResultsList) {
                //ignoreval = UnityEngine.Random.value + UnityEngine.Random.value;

                userProvidedResultsList.Clear();

                for (int i = 0; i < inputList.Count; i++) {
                    var item = inputList[i];
                    if (query(item)) {
                        userProvidedResultsList.Add(item);
                    }
                }
            }
            else {
                //ignoreval = UnityEngine.Random.value;

                for (int n = userProvidedResultsList.Count-1; n >= 0; n--) {
                    if(!query(userProvidedResultsList[n])) {
                        userProvidedResultsList.RemoveAt(n);
                    }
                }
            }*/

            resultsCount = 0;
            for (int i = 0; i < inputList.Count; i++)
            {
                if (query(inputList[i]))
                {
                    if (resultsCount == userProvidedResultsList.Count) userProvidedResultsList.Add(inputList[i]);
                    else userProvidedResultsList[resultsCount] = inputList[i];
                }
            }


        }

        //static float ignoreval;

#if UNITY_EDITOR
    //   [UnityEditor.MenuItem("Helper/GCTests/TestFastWhere")]
    //   public static void TestFastWhere() {
    //       //var objs = UnityEngine.Object.FindObjectsOfType<UnityEngine.Transform>().ToList();
    //       var objs = UnityEngine.Object.FindObjectsOfType<UnityEngine.Transform>().Where(x => x.name.Contains("fargo")).ToList();
    //
    //       var newList = new List<UnityEngine.Transform>(10000);
    //       newList.Add(objs[0]);
    //
    //
    //       /*UnityEngine.Profiling.Profiler.BeginSample("TestFastWhere SingleOrDefault");
    //       objs.FastSingleOrDefault(x => x.position.x > 30000);
    //       UnityEngine.Profiling.Profiler.EndSample();
    //
    //       UnityEngine.Profiling.Profiler.BeginSample("TestFastWhere SingleOrDefault x100");
    //       for (int i = 0; i < 100; i++) {
    //           objs.FastSingleOrDefault(x => x.position.x > 30000);
    //       }            
    //       UnityEngine.Profiling.Profiler.EndSample();*/
    //
    //       //return;
    //
    //       UnityEngine.Profiling.Profiler.BeginSample("TestFastWhere otherlist");
    //       FastWhere(objs, newList, x => x.position.x > 0);
    //       UnityEngine.Profiling.Profiler.EndSample();
    //
    //       objs = UnityEngine.Object.FindObjectsOfType<UnityEngine.Transform>().Where(x => x.name.Contains("fargo")).ToList();
    //       newList = new List<UnityEngine.Transform>(10000);
    //       newList.Add(objs[0]);
    //
    //       UnityEngine.Profiling.Profiler.BeginSample("TestFastWhere inplace removeall");
    //       FastWhere(objs, objs, x => x.position.x > 0);
    //       UnityEngine.Profiling.Profiler.EndSample();
    //
    //       UnityEngine.Profiling.Profiler.BeginSample("TestFastWhere otherlist x100");
    //       for (int i = 0; i < 100; i++) {
    //           //FastWhere(objs, newList, x => x.position.y > 0);
    //           FastWhere(objs, newList, x => x.position.y > 0);
    //       }
    //       UnityEngine.Profiling.Profiler.EndSample();
    //
    //       UnityEngine.Profiling.Profiler.BeginSample("TestFastWhere inplace removeall x100");
    //       for (int i = 0; i < 100; i++) {
    //           FastWhere(objs, objs, x => x.position.y < 0);
    //       }            
    //       UnityEngine.Profiling.Profiler.EndSample();
    //   }
    //

        [UnityEditor.MenuItem("Helper/GCTests/TestNormalSort")]
        public static void TestNormalSort() {

            var listt = new List<int>();

            listt.Add(3);
            listt.Add(2);
            listt.Add(4);
            listt.Add(60);
            listt.Add(0);

            listt.Sort((x, y) => {
                if (x < y) return -1;
                else return 1;
            });

            UnityEngine.Debug.Log("tested");

            /*tempApproaches0.Sort((x, y) => {
                if (x.GetValidity() == y.GetValidity()) {
                    if (x.CanClimbNow() && y.CanClimbNow()) {
                        if (x.GetLikeability() - y.GetLikeability() > 0) return 1;
                        else return -1;
                    }
                    else return x.CanClimbNow() ? 0 : 1;
                }
                else return x.GetValidity() > y.GetValidity() ? 1 : -1;
            });
            best = tempApproaches0.FastFirstOrDefault();*/
        }
        #endif
    }
}