﻿

namespace ToivonRandomUtils
{
    public class IntervalUpdate
    {
        public float lastUpdateStamp;
        public float interval;
        public void Tick(float time)
        {
            if (time - lastUpdateStamp >= interval)
            {
                action();
                lastUpdateStamp = time;
            }
        }

        public System.Action action;
    }
}
