﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.EventSystems;
using ToivonRandomUtils;

namespace ToivoDebug
{
	public class CheatInput : MonoBehaviour
	{
		public InputField input;
		public Text result;
		public bool setNotActiveOnStart;
		Dictionary<string,Action<string>> returningCheatDictionary = new Dictionary<string, Action<string> > ();
		Dictionary<string,Action> nonReturningCheatDictionary = new Dictionary<string, Action > ();
        public Dropdown dropdown;
        public Button templateFvoriteCheatsButton;
		List<string> AllKeys {
			get {
				List<string> keys = returningCheatDictionary.Keys.ToList ();
				keys.AddRange (nonReturningCheatDictionary.Keys.ToList ());
				return (keys);
			}
		}

		void Awake ()
		{
			CurrentCheatInput.Set (this);

        }

        public bool open;
		void ToggleDebugConsoles (string crap)
		{
			CurrentCheatInput.ToggleActive ();
			CurrentCustomLog.ToggleActive ();
		}
        [UnityEngine.Serialization.FormerlySerializedAs("toggleWithF8Key")]
        public bool toggleWithKey = false;
        public KeyCode toggleKey = KeyCode.F8;

        public bool toggleWith5FingerTouchInput = false;


        public void ClearCheats ()
		{
			nonReturningCheatDictionary.Clear ();
			returningCheatDictionary.Clear ();
		}
        public void RemoveCheat(string cheat)
        {
            nonReturningCheatDictionary.Remove(cheat);
            returningCheatDictionary.Remove(cheat);
            TryUpdateDropdown();
        }
        /* toivo 13.3.2020 calling this onenable caused some weird bug in 2019.3.0f6 so moved it to update
 		void OnEnable(){
			EventSystem.current.SetSelectedGameObject (gameObject, null);
			input.OnPointerClick (new UnityEngine.EventSystems.PointerEventData (EventSystem.current));
            if (toggleWithF8Key)
            {
                if (runner == null)
                {
                    runner = InvokeAction.Invoke(() => Input.GetKeyUp(KeyCode.F8), SpecialUpdate, true);
                }
            }
            TryUpdateDropdown();
            if(dropdown!=null) dropdown.onValueChanged.AddListener(DrowdownChanged);
            RefreshFavCheatsButtons(GetFavoriteCheats());
        }
        */

        private float held5FingersTime = -1; 
        bool init = false;
         bool cheatAdded = false;
        private void Update()
        {
            if (!init)
            {
	            if(EventSystem.current==null) return;
                EventSystem.current.SetSelectedGameObject(gameObject, null);
                input.OnPointerClick(new UnityEngine.EventSystems.PointerEventData(EventSystem.current));
                if (toggleWithKey)
                {
                    if (runner == null)
                    {
                        runner = InvokeAction.Invoke(() => Input.GetKeyUp(toggleKey) && enabled, SpecialUpdate, true, scope: gameObject);
                        if (setNotActiveOnStart) gameObject.SetActive(false);
                    }
                }
                if (toggleWith5FingerTouchInput)
                {
	                if (touchInputRunner == null)
	                {
		                touchInputRunner = InvokeAction.Invoke(() =>
		                {
			                if (Input.touchCount == 5 )
			                {
				                if (held5FingersTime < 0)
				                {
					                held5FingersTime = Time.unscaledTime;
				                }
			                }
			                else
			                {
				                held5FingersTime = -1;
			                }
			                return held5FingersTime > 0 && Time.unscaledTime - held5FingersTime > 5 && enabled;
		                }, SpecialUpdate, true, scope: gameObject);
		              
		                if (setNotActiveOnStart) gameObject.SetActive(false);
	                }
                }
                if (dropdown != null) dropdown.onValueChanged.AddListener(DrowdownChanged);
                RefreshFavCheatsButtons(GetFavoriteCheats());
                init = true;

                cheatAdded = true;
            }

            if (cheatAdded)
            {
	            TryUpdateDropdown();
            }

        }


        void Start ()
		{
			returningCheatDictionary.AddOrChangeDictionaryValue ("toggleDebugConsoles", ToggleDebugConsoles);
            

        }

        private void OnDisable()
        {
            init = false;
            if (dropdown != null) dropdown.onValueChanged.RemoveListener( DrowdownChanged);

                Debug.Log("cheatinput disabled", gameObject);
        }

        //        public void AddCheat(string key,Action<string> cheat){
        //            key = key.ToLower ();
        //            if (!cheatDictionary.ContainsKey (key)) {
        //                cheatDictionary.Add(key,cheat);
        //            }
        //            else {
        //                cheatDictionary[key]+=cheat;
        //            }
        //        }
        public ActionTaskHandler runner;
        public ActionTaskHandler touchInputRunner;

         public void SpecialUpdate()
         {
	         held5FingersTime = -1f;
            open = !open;
            gameObject.SetActive(open);
            if (open)
            {
               //Time.timeScale = 0;
                EventSystem.current.SetSelectedGameObject(gameObject, null);
                input.OnPointerClick(new UnityEngine.EventSystems.PointerEventData(EventSystem.current));
            }
            else
            {
             //   Time.timeScale = 1f;
            }
        }


		//        public void AddCheat(string key,Action<string> cheat){
		//            key = key.ToLower ();
		//            if (!cheatDictionary.ContainsKey (key)) {
		//                cheatDictionary.Add(key,cheat);
		//            }
		//            else {
		//                cheatDictionary[key]+=cheat;
		//            }
		//        }
		/// <summary>
		/// 
		/// </summary>
		/// <param name="key"></param>
		/// <param name="cheat"></param>
		public void AddCheat (string key, Action cheat)
		{
			cheatAdded = true;
			key = key.ToLower ();
			nonReturningCheatDictionary.AddOrChangeDictionaryValue (key, cheat);
		}

        void TryUpdateDropdown()
        {
            if (!dropdown) return;
            dropdown.ClearOptions();
            List<Dropdown.OptionData> options = new List<Dropdown.OptionData>();
            options.Add(new Dropdown.OptionData("Pick a cheat"));
            options.AddRange(nonReturningCheatDictionary.Select(x => new Dropdown.OptionData(x.Key)));
            dropdown.AddOptions(options);
        }

        bool breakLoop = false;
        private List<Button> createdFavCheatButtons = new List<Button>();
        public Action<string> onCheated;

        private void DrowdownChanged(int arg0)
        {
            if (breakLoop) return;
            input.text = dropdown.options[arg0].text;
            breakLoop = true;
            dropdown.value = 0;
            breakLoop = false;
            Run();
        }


		public void AddReturningCheat (string key, Action<string> cheat)
		{
			key = key.ToLower ();
			returningCheatDictionary.AddOrChangeDictionaryValue (key, cheat);
		}

		public void Run (string cheat = null)
		{
			if (cheat != null)
			{
				input.text = cheat;
			}
            bool ranCheat = false;
            Debug.Log("running cheat <" + input.text + ">");
            onCheated?.Invoke(input.text);
            

            if (returningCheatDictionary.ContainsKey (input.text.ToLower ())) {
                ranCheat = true;
				result.text = "OK!";
				returningCheatDictionary [input.text.ToLower ()] (input.text.ToLower ());
			} else if (nonReturningCheatDictionary.ContainsKey (input.text.ToLower ())) {
				result.text = "OK!";
                ranCheat = true;
                nonReturningCheatDictionary[input.text.ToLower ()] ();
			} else {
				
				RandomUtils.MatchData.MatchResultType resultType;
				string tempResultStr = ToivonRandomUtils.RandomUtils.CheckMatch (AllKeys, input.text, out resultType, true);
				bool fallback = false;
				if (resultType == RandomUtils.MatchData.MatchResultType.NotFound) {
					var str = AllKeys.FirstOrDefault (x => x.ToLower ().Contains (input.text.ToLower ()));
					if (!string.IsNullOrEmpty (str)) {
						input.text = str;
						fallback = true;
					}
				}
				if (!fallback) {
					input.text = tempResultStr;
					result.text = resultType.ToString ();
				}
				
			}

            if (ranCheat)
            {
                FavoriteCheats favoriteCheats = GetFavoriteCheats();
                if (favoriteCheats.usedCheats.Contains(input.text))
                {
                    favoriteCheats.usedCheats.Remove(input.text);
                }
                favoriteCheats.usedCheats.Add(input.text);
                int index = 0;
                while (favoriteCheats.usedCheats.Count > 12)
                {
                    favoriteCheats.usedCheats.RemoveAt(0);
                    index++;
                    if (index > 100) throw new Exception("if this is ever a case i dont know how to progams");
                }
                favoriteCheats.usedCheats = favoriteCheats.usedCheats.Distinct().ToList();
                RefreshFavCheatsButtons(favoriteCheats);
                PlayerPrefs.SetString(GetPrefKey(), JsonUtility.ToJson(favoriteCheats));
            }


        }

        public string GetPrefKey()
        {
            return "favCheats_" + gameObject.scene.name;
        }

        private void RefreshFavCheatsButtons(FavoriteCheats favoriteCheats)
        {
            createdFavCheatButtons.DestroyGameObjectsAndClear();
            int buttonsCount = Mathf.Min( Mathf.RoundToInt(6f * (Screen.width / 1920f)), favoriteCheats.usedCheats.Count) * 2;
            for (int i = favoriteCheats.usedCheats.Count - 1; i >= 0; i--)
            {
                if (Mathf.Abs(i - favoriteCheats.usedCheats.Count) > buttonsCount)
                {
                    break;
                }
                var item = favoriteCheats.usedCheats[i];
                var createdButton = templateFvoriteCheatsButton.DoTheStandardCloningThing(createdFavCheatButtons);
                createdButton.onClick.AddListener(() => { input.text = item; Run(); });
                createdButton.GetComponentInChildren<Text>().text = item;
            }
        }

        private FavoriteCheats GetFavoriteCheats()
        {
            FavoriteCheats favoriteCheats = null;
            try
            {
                favoriteCheats = JsonUtility.FromJson<FavoriteCheats>(PlayerPrefs.GetString(GetPrefKey(), ""));
            }
            catch (Exception ex)
            {
                Debug.Log("failed to parse favorite cheats " + ex);
            }
            if (favoriteCheats == null) favoriteCheats = new FavoriteCheats();
            return favoriteCheats;
        }

    }
    public class FavoriteCheats
    {
        public List<string> usedCheats = new List<string>();
    }

	public static class CurrentCheatInput
	{
		private static CheatInput current;

		public static bool ToggleActive ()
		{
			if (current != null) {
				var activeSelf = current.transform.gameObject.activeSelf;
				current.transform.gameObject.SetActive (!activeSelf);
				if (activeSelf) {
					EventSystem.current.SetSelectedGameObject (current.gameObject, null);
					current.input.OnPointerClick (new UnityEngine.EventSystems.PointerEventData (EventSystem.current));
				}
				return activeSelf;
			} else
				return false;


		}

		public static void Set (CheatInput cheatInput)
		{
			current = cheatInput;
		}

		public static void AddCheat (string key, Action cheat)
		{
			if (current != null) {
				current.AddCheat (key, cheat);
			}
		}

		public static void AddReturningCheat (string key, Action<string> cheat)
		{
			if (current != null) {
				current.AddReturningCheat (key, cheat);
			}
		}

		public static void SetResultText (string s)
		{
			if (current != null) {
				current.result.text = s;
			}
		}
	}

	public class DebugListDrawer<T>
	{
		private Vector2 scrollPosition;
		private List<T> items;
		private Func<T, string> getName;
		private Action<T> onButtonClick;
		private string buttonLabel;
    
		public DebugListDrawer(List<T> items, Func<T, string> getName, Action<T> onButtonClick, string buttonLabel = "Play")
		{
			this.items = items;
			this.getName = getName;
			this.onButtonClick = onButtonClick;
			this.buttonLabel = buttonLabel;
		}

		public void OnGUI()
		{
			if (items == null || getName == null || onButtonClick == null) return;

			int itemHeight = 30;
			int width = 200;
			int height = Mathf.Min(items.Count * itemHeight + 10, Screen.height / 2);
			int x = Screen.width - width; // Right edge
			int y = (Screen.height - height) / 2; // Centered vertically

			GUI.Box(new Rect(x - 10, y - 10, width + 20, height + 20), "Debug Menu");

			scrollPosition = GUI.BeginScrollView(
				new Rect(x, y, width, height),
				scrollPosition,
				new Rect(0, 0, width - 20, items.Count * itemHeight)
			);

			for (int i = 0; i < items.Count; i++)
			{
				T item = items[i];
				string name = getName(item);
				Rect itemRect = new Rect(0, i * itemHeight, width - 20, itemHeight);

				GUI.Label(new Rect(itemRect.x, itemRect.y, itemRect.width / 2, itemHeight), name);
				if (GUI.Button(new Rect(itemRect.x + itemRect.width / 2, itemRect.y, itemRect.width / 2, itemHeight), buttonLabel))
				{
					onButtonClick(item);
				}
			}

			GUI.EndScrollView();
		}
	}

}