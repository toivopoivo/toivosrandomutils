using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;


public class ComponentHolder : MonoBehaviour {
    public enum ComponentType
    {
        Unspecified,
        Image,
        Text
    }
   

    public List<Component> components;
//    [HideInInspector]
    public List<ComponentWithStringKey> componentsWithKeys;
    [System.Serializable]
    public class ComponentWithStringKey{
        public ComponentType componentType;
#pragma warning disable 
        [SerializeField]
        private Component component;
        [SerializeField]
        private Text text;
        [SerializeField]
        private Image image;
#pragma warning restore 

        public string key;
        public Component GetMyValue(){
            if (componentType == ComponentType.Image)
            {
                return image;
            } else if(componentType==ComponentType.Text)
            {
                return text;
            } else {
                return component;
            }
        }
    }
    public object data;
    public Action<object> onDataSend;
    void Start(){
    }
    /// <summary>
    /// Gets the first cached component of the specified type.
    /// </summary>
    public T GetCachedComponent<T>() where T:class{
        T returnValue=null;
        foreach (var item in components)
        {
            if(item as T !=null) returnValue=item as T;
        }
        return returnValue;
    }
    /// <summary>
    /// Gets the first cached component with the specified key and type.
    /// </summary>
    public T GetCachedComponent<T>(string key) where T:class{
        T returnValue=null;
        foreach (var item in componentsWithKeys)
        {
            if(item.key==key) returnValue=item.GetMyValue() as T;
        }
        return returnValue;
    }
//    public T
    public void SendData(){
        if (onDataSend != null)
        {
            onDataSend(data);
        }
    }
}
