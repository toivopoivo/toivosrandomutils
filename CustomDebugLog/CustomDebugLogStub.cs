﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomDebugLogStub : MonoBehaviour
{
    public GameObject createdCstomDebugLogPrefab;
    GameObject instance;
    public void OnEnable()
    {
        instance =  Instantiate(createdCstomDebugLogPrefab);
    }

    public void OnDisable()
    {
        if (instance != null) {
            Debug.Log("CustomDebugLogStub -  destroying created custom debuglog");
            Destroy(createdCstomDebugLogPrefab);
        }
    }
}
