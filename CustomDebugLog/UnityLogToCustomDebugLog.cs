﻿using System;
using System.Collections;
using System.Collections.Generic;
using ToivoDebug;
using UnityEngine;
using ToivonRandomUtils;

public class UnityLogToCustomDebugLog : MonoBehaviour {

    CustomDebugLog customDebugLog;
    private void OnEnable()
    {

        customDebugLog = GetComponent<CustomDebugLog>();
        UnityEngine.Application.logMessageReceived += LogMessageReceived;
    }
    Dictionary<int, int> logCounts = new Dictionary<int, int>();
    private void LogMessageReceived(string condition, string stackTrace, LogType type)
    {

        string format = "{0}";
        if(type == LogType.Exception || type == LogType.Error)
        {
            format = "<color=red>ERROR {0}</color>";
        }

        if (type == LogType.Warning)
        {
            format = "<color=yellow>WARNING {0}</color>";
        }
        customDebugLog.Log(string.Format(format, condition));
        logCounts.AddNewOrAddToOld((int)type, 1);
        customDebugLog.LogDynamic(type.ToString(),""+logCounts[(int)type]);
    }

    private void OnDisable()
    {
        UnityEngine.Application.logMessageReceived -= LogMessageReceived;
    }
}
