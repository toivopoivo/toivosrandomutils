﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Diagnostics;
using ToivonRandomUtils;
using System;
using System.Linq;

namespace ToivoDebug
{
	public class CustomDebugLog : MonoBehaviour
	{
		int lines;
		public Text logText, dynamic, biggerDynamic;
		bool biggerEnabled;
		public Dictionary<string,Dictionary<string, string>> channels = new Dictionary<string,Dictionary<string, string>> ();
		public static CustomDebugLog customDebugLog;
		List<string> newKeys = new List<string> ();
		string normalLogs = "";
		// Use this for initialization
		public bool setNotActiveOnStart;
		public bool toggleWithKey;
		public KeyCode toggleKey;
		public bool dontDestroyOnLoad;
        public bool setAsCurrentCustomLog = true;
        public Dropdown channelsDropdown;

        public KeyCode changeChannelWithKey = KeyCode.PageDown;

		void Awake ()
		{
            //		logText.text = "";
            CurrentCustomLog.Set (this);
			if (dontDestroyOnLoad)
				DontDestroyOnLoad (gameObject);
		
		}

        private void Update()
        {
            if (Input.GetKeyUp(changeChannelWithKey))
            {
                int tempVal = channelsDropdown.value + 1;
                if (tempVal >= channelsDropdown.options.Count)
                {
                    channelsDropdown.value = 0;
                }else
                {
                    channelsDropdown.value = tempVal;
                }
            }
        }


        public void SwitchToBiggerDynamicPanel ()
		{
			biggerDynamic = dynamic;
			dynamic = biggerDynamic;
			biggerEnabled = !biggerEnabled;

			// notice show i swapped them above
			dynamic.gameObject.SetActive (true);
			biggerDynamic.gameObject.SetActive (false);
			if (biggerEnabled) {
				logText.gameObject.SetActive (false);
			}

		}

		void Start ()
		{
            channelsDropdown.onValueChanged.AddListener(DropdownValChanged);
    /*        LogTimeStamp (this, "start");*/
			if (toggleWithKey) {
				InvokeAction.Invoke (
					() => {
						return (Input.GetKeyUp (toggleKey));
					},
					() => gameObject.SetActive (!gameObject.activeSelf)
                  , true);
			}
			if (setNotActiveOnStart) {
				gameObject.SetActive (false);
				return;
			}
			StartCoroutine (LogStartUpTime ());
		}

        private void DropdownValChanged(int arg0)
        {
            currentChannel = channelsDropdown.options[arg0].text;
        }

        IEnumerator LogStartUpTime ()
		{
			yield return new WaitForFixedUpdate ();
			/*LogTimeStamp (this, "Fixed update");*/
		}

		public void Refresh (string newValue)
		{
			if (!newKeys.Contains (newValue))
				newKeys.Add (newValue);
			Refresh ();  
			if (!clearing && gameObject.activeSelf)
				StartCoroutine (ClearNew ());
		}

		bool clearing;

		IEnumerator ClearNew ()
		{
			clearing = true;
			yield return new WaitForSeconds (1f);
			newKeys.Clear ();
			clearing = false;
		}

		public void Refresh ()
		{
			logText.text = normalLogs + "\n";
			dynamic.text = "";
            if (channels.Count == 0) return;
            if(currentChannel!="" && channels.ContainsKey(""))
                foreach (var item in channels[""])
                {
                    if (newKeys.Contains(item.Key))
                        dynamic.text += "\n<color=yellow>" + item.Key + "\t" + item.Value + "</color>";
                    else
                        dynamic.text += "\n" + item.Key + "\t" + item.Value;
                }
            dynamic.text += "\n\n---- " + currentChannel + " ----\n";
            foreach (var item in channels[currentChannel]) {
				if (newKeys.Contains (item.Key))
					dynamic.text += "\n<color=yellow>" + item.Key + "\t" + item.Value + "</color>";
				else
					dynamic.text += "\n" + item.Key + "\t" + item.Value;
			}
		}

		public void Log (string log)
		{
			lines++;
			if (lines == 200) {
				lines = 0;
				normalLogs = "";
			}
			normalLogs = normalLogs + "\n" + log;
			Refresh ();
		}


        public void LogDynamic(string key, string logData, string channel = "")
        {
            Dictionary<string, string> listenerSlots = GetOrCreateChannel(channel);
            bool different = (!listenerSlots.ContainsKey(key) || logData != listenerSlots[key]);
            listenerSlots.AddOrChangeDictionaryValue(key, logData);
            if (different)
                Refresh(key);
            else
                Refresh();
        }

        private Dictionary<string, string> GetOrCreateChannel(string channel)
        {
            if (!channels.ContainsKey(channel))
            {
                channels.Add(channel, new Dictionary<string, string>());
                channelsDropdown.ClearOptions();
                foreach (var item in channels)
                {
                    channelsDropdown.AddOptions(new List<string>(new string[]{ item.Key } ) );
                }
                channelsDropdown.value = channelsDropdown.options.FindIndex(x => x.text == currentChannel);
            }
            var listenerSlots = channels[channel];
            return listenerSlots;
        }

        public void LogTimeStamp(object caller, string message = "")
        {
            if (!stopWatch.IsRunning)
            {
                stopWatch.Start();
            }
            Log("Time stamp: " + stopWatch.ElapsedMilliseconds + ", caller " + caller.ToString() + " " + message);
        }

        public  Stopwatch stopWatch = new Stopwatch();
        private string currentChannel = "";
    }

	public static class CurrentCustomLog
	{
		private static List<string> toBeDebugLogged = new List<string> ();
		public static CustomDebugLog current;

		public static void ToggleActive ()
		{
			if (current != null)
				current.transform.gameObject.SetActive (!current.transform.gameObject.activeSelf);
		}

		public static void Set (CustomDebugLog logger)
		{
			current = logger;
		}

		public static bool Exists {
			get{ return current != null; }
		}

		public static void Log (object log)
		{
			Log (log.ToString ());
		}

		public static void Log (string log)
		{
			if (current != null) {
				if (toBeDebugLogged.Count > 0) {
					foreach (var item in toBeDebugLogged) {
						current.Log (item);
					}
				}
				toBeDebugLogged.Clear ();
				current.Log (log);
			} else {
				toBeDebugLogged.Add (log);
			}
		}

		public static void LogDynamic (string key, object logData, string channel ="")
		{
			LogDynamic (key, logData.ToString (),channel);
		}

		public static void LogDynamic (string key, string logData, string channel = "")
        {
            if (current == null)
                return;
            current.LogDynamic(key, logData,channel) ;
        }

       

        public static long StopWatchEllapsedMilliseconds {
			get {
				return current.stopWatch.ElapsedMilliseconds;
			}
		}
	}

}