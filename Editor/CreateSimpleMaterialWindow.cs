﻿using UnityEditor;
using UnityEngine;
using System.Collections;

public class CreateSimpleMaterialWindow : EditorWindow
{
	ColorType colorType;

	[MenuItem ("Assets/Create Simple Material")]
	static void Init ()
	{
//		ColorType
		
		CreateSimpleMaterialWindow window = (CreateSimpleMaterialWindow)EditorWindow.GetWindow (typeof(CreateSimpleMaterialWindow));
		window.Show ();
	}

	void OnGUI ()
	{
		colorType = (ColorType)EditorGUILayout.EnumPopup (colorType);
		if (GUILayout.Button ("Create")) {
			Color color = ColorTypeUtility.ColorType2Color (colorType);
			CreateMaterial (color, colorType.ToString ());

		}
	}

	public static Material CreateMaterial (Color color, string materialName)
	{
		var newMaterial = new Material (Resources.Load<Material> ("SecretMaterial"));
		newMaterial.color = color;
		string localFolderPath = "Assets/Materials/";
		System.IO.Directory.CreateDirectory(Application.dataPath.Replace("Assets", localFolderPath));
        AssetDatabase.CreateAsset (newMaterial, AssetDatabase.GenerateUniqueAssetPath (localFolderPath + materialName + ".mat"));
		EditorGUIUtility.PingObject (newMaterial);
		return newMaterial;
	}
}

public static class MaterialUtility
{
	static MenuItemActionHandler actionHandler;

	[MenuItem ("CONTEXT/Renderer/Replace material with new")]
	public static void RepMat (MenuCommand comds)
	{
		if (actionHandler != null && !actionHandler.actionDone) {
			actionHandler.menuComds.Add (comds.context);
		} else {
			var renderer = (comds.context as Renderer);
			var newMat = CreateSimpleMaterialWindow.CreateMaterial (Color.white, renderer.gameObject.name);
			if (UnityEditor.Selection.gameObjects.Length == 1) {
				ChangeMaterial (comds, newMat);
			} else {
				actionHandler = MenuItemActionHandler.Init (
					(x) => ChangeMaterial (x, newMat)
				);
				actionHandler.menuComds.Add (comds);
			}
		}

	}

	static void ChangeMaterial (object comds, Material newMat)
	{
		var renderer = (comds as Renderer);
		Undo.RecordObject (comds as Renderer, "created & set new material");
		renderer.material = newMat;
	}
}

