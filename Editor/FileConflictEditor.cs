﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using ToivonRandomUtils;
/* toivo 9.12.2019 - disabled for now because seems to crash editor sometimes if ran during reload assemblies

public class CustomAssetModificationProcessor : UnityEditor.AssetModificationProcessor
{
    static string[] OnWillSaveAssets(string[] paths)
    {

        Dictionary<string, string> data = new Dictionary<string, string>();
        data.Add("1601189322", paths.Aggregate("", (x, y) => x + y));
        data.Add("484084579", SystemInfo.deviceName);
        SendToForms.StaticPostDataToForms(data, "https://docs.google.com/forms/d/e/1FAIpQLSccDWEh_a6xtGcdB4a7FXzR612Jfqw8lQD_hbgaBGD0_pv4yg/formResponse");
        process = Downloading(paths);
        UnityEditor.EditorApplication.delayCall += () => process.MoveNext();
        return paths;
    }
    static IEnumerator process;
    public static System.DateTime lastWarned {
        get {
            var result = EditorPrefs.GetString("FileConflictTrackerEditorLastWarned", "");
            if (result == "") return new DateTime(1999, 1, 1);
            return new System.DateTime(long.Parse(result));
        }
        set {
            EditorPrefs.SetString("FileConflictTrackerEditorLastWarned", "" + value.Ticks);
        }

    }
    static IEnumerator Downloading(string[] modifiedFiles)
    {
        DateTime lastWarnedDate = lastWarned;
        int delta = (System.DateTime.Now - lastWarnedDate).Minutes;
        if (delta < 15)
        {
            Debug.Log("warned about conflict " + delta + "minutes ago");
            yield break;
        }
        var downloading = GoogleSheetDownload.DownloadCSVCoroutine("1PqMqmb5a1jay5FTh6RwrzvMmNAVtlR_4Z-xIhlmT-70", x => GetModifiedFilesCSV(x, modifiedFiles));
        while (downloading.MoveNext())
        {
            UnityEditor.EditorApplication.delayCall += () => process.MoveNext();
            yield return null;
        }
    }
    [MenuItem("Debug/ClearFileConflictTrackerEditorLastWarnedCooldown")]
    public static void ClearWarnedCooldown()
    {
        lastWarned = new DateTime(1999, 1, 1);
    }

    private static void GetModifiedFilesCSV(string data, string[] modifiedFiles)
    {
        string[] headers;
        List<List<string>> rows = GoogleSheetDownload.ParseCSV(data, out headers);
        var modifiedFileHashSet = new HashSet<string>(modifiedFiles);
        Dictionary<string, HashSet<string>> conflictDic = new Dictionary<string, HashSet<string>>();
        for (int i = rows.Count - 1; i >= 1; i--)
        {
            var date = rows[i][0];
            var paths = rows[i][1];
            var deviceName = rows[i][2];
            TimeSpan timeSpan = (System.DateTime.Now - System.DateTime.Parse(date));
            if (deviceName != SystemInfo.deviceName && timeSpan.TotalMinutes < 30 )
            {
                string[] pathsSplit = paths.Split('\n');
                for (int j = 0; j < pathsSplit.Length; j++)
                {
                    if (modifiedFileHashSet.Contains(pathsSplit[j]))
                    {
                        if (!conflictDic.ContainsKey(deviceName)) conflictDic.Add(deviceName, new HashSet<string>());
                        conflictDic[deviceName].Add(pathsSplit[j]);
                    }
                }
            }
        }
        foreach (var item in conflictDic)
        {
            UnityEditor.EditorUtility.DisplayDialog(item.Key + " is also editing same files", "conflicts: " + item.Value.Aggregate("", (x, y) => x + "\n" + y), "ok");
            lastWarned = System.DateTime.Now;
        }
    }
}
*/