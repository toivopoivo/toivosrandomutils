﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Linq;

[CustomPropertyDrawer (typeof(EnumFilter))]
public class EnumFilterDrawer  : PropertyDrawer
{
	

	public string textFieldVal = "";


	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
//		if (fieldInfo.GetType ().IsEnum) {
		// was going to make support for lists but fuck it
//		}

		var thirdOfWidth = position.width / 3f;
		EditorGUI.LabelField (new Rect (position.x, position.y, thirdOfWidth, 17f), property.displayName);

		System.Type enumFilterType = (attribute as EnumFilter).type;
		EditorGUI.BeginChangeCheck ();
		/*textFieldVal = EditorGUI.TextField (new Rect (position.x + thirdOfWidth, position.y, thirdOfWidth, 17f), EditorPrefs.GetString ("editedEnumFilterValue", textFieldVal)).ToLower ();*/
		textFieldVal = EditorGUI.TextField (new Rect (position.x + thirdOfWidth, position.y, thirdOfWidth, 17f), textFieldVal).ToLower ();
		if (EditorGUI.EndChangeCheck ()) {
			EditorPrefs.SetString ("editedEnumFilterValue", textFieldVal);
			var enumVals = System.Enum.GetValues (enumFilterType).Cast<object> ().ToArray ();
//			int index = System.Array.FindIndex (enumVals, x => x.ToString ().ToLower ().Contains (textFieldVal));
//			if (index != -1) {
//				property.enumValueIndex = index;
//			}
			var filteredEnums = enumVals.Select (x => x.ToString ()).Where (x => x.ToLower ().Contains (textFieldVal));
//			int index = System.Array.FindIndex (enumVals, x => x.ToString ().ToLower ().Contains (textFieldVal));
			if (filteredEnums.Any ()) {
				var orderedEnumerable = filteredEnums.OrderByDescending (x => Score (x, textFieldVal)).ToList();
				var best = orderedEnumerable.First ();
				property.enumValueIndex = System.Array.FindIndex (enumVals, x => x.ToString () == best);
			} else {
				UnityEditor.EditorApplication.Beep();
			}
		}
		EditorGUI.PropertyField (new Rect (position.x + thirdOfWidth + thirdOfWidth, position.y, thirdOfWidth, 17f), property, GUIContent.none);

	}

	int Score (string comparedA, string comparedB)
	{
		comparedA = comparedA.ToLower ();
		comparedB = comparedB.ToLower ();
		int cycles = Mathf.Min (comparedA.Length, comparedB.Length);
		int score = 0;
		for (int i = 0; i < cycles; i++) {
			if(comparedA[i] == comparedB[i]){
				score +=2;
			} else {
				score += comparedB.Contains(  comparedA [i] ) ? 1 : -1;
			}
		}
		score-=Mathf.Abs(comparedA.Length-comparedB.Length);

		return score;
	}
}
