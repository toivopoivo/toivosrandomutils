﻿using UnityEngine;
using System.Collections;
using ToivonRandomUtils;
using System.Collections.Generic;
using UnityEditor;
using System.Linq;
using System.Reflection;

public class GrepSceneWindow : EditorWindow
{
    [MenuItem("toivos best windows/Experimental/GrepSceneWindow Window")]
    static void Init()
    {
        EditorWindow.GetWindow<GrepSceneWindow>().Show();
    }


    List<Component> found = new List<Component>();
    string state = "not started";
    string typeCriteria = "";
    string regexInput = "";
    private void OnGUI()
    {
        EditorGUILayout.LabelField("state: "+state);
        typeCriteria = EditorGUILayout.TextField("type criteria (exact) : ", typeCriteria);
        regexInput = EditorGUILayout.TextField("regexInput: ", regexInput);
        if (GUILayout.Button("start grepping")){
            found.Clear();
            currentGrepping = Grepping(typeCriteria, regexInput);
        }
        if (GUILayout.Button("stop"))
        {
            currentGrepping = null;
        }

        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
        foreach (var item in found)
        {
            EditorGUILayout.ObjectField(item,typeof(Component),true);
        }
        EditorGUILayout.EndScrollView();
    }
    Vector2 scrollPos;
    private void Update()
    {
        if (currentGrepping != null)
        {
            currentGrepping.MoveNext();
            Repaint();
        }
    }

    IEnumerator currentGrepping;
    IEnumerator Grepping(string typeCriteria, string regexInput)
    {

        state = "GetComponentInChildrenFromRootGOs";
        yield return null;
        var allCompontents =     ToivonRandomUtils.RandomUtils.GetComponentInChildrenFromRootGOs<Component>();
        yield return null;
        System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
        for (int i = 0; i < allCompontents.Count; i++)
        {
            if (allCompontents[i] == null) continue;
            state = "matching progress " + ((float)i / allCompontents.Count) * 100f;
            if (string.IsNullOrEmpty(typeCriteria) || allCompontents[i].GetType().Name.ToLower() == typeCriteria.ToLower())
            {
                string editorJson = EditorJsonUtility.ToJson(allCompontents[i],true);
                var newlinesplit = editorJson.Split('\n');
                foreach (var line in newlinesplit)
                {
                    if (new System.Text.RegularExpressions.Regex(regexInput, System.Text.RegularExpressions.RegexOptions.IgnoreCase).IsMatch(line)) {
                        found.Add(allCompontents[i]);
                        break;
                    }
                }
            }
            if (sw.ElapsedMilliseconds > 100)
            {
                yield return null;
                sw = System.Diagnostics.Stopwatch.StartNew();
            }
        }
         state = "ready";

    }



}