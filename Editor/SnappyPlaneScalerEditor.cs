﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(SnappyPlaneScaler))]
public class SnappyPlaneScalerEditor : Editor{

    private void OnSceneGUI()
    {

        SnappyPlaneScaler snappyPlaneScaler = target as SnappyPlaneScaler;
        Bounds bounds = snappyPlaneScaler.GetComponent<MeshRenderer>().bounds;


        
        UnityEditor.EditorGUI.BeginChangeCheck();
        var newMax = Handles.DoPositionHandle(bounds.max, Quaternion.identity);
        if (EditorGUI.EndChangeCheck())
        {
            for (int i = 0; i < 3; i++)
            {
                newMax[i] = Mathf.Round(newMax[i]);
            }
            Vector3 min = bounds.min;
            Vector3 delta = newMax - min;
            Undo.RecordObject(snappyPlaneScaler.transform, "moved plane");
            snappyPlaneScaler.transform.position =( min + newMax) * 0.5f;
            snappyPlaneScaler.transform.localScale = new Vector3(delta.x,delta.z,1);
        }

        /*warning dry from upper block */
        UnityEditor.EditorGUI.BeginChangeCheck();
        var newMin = Handles.DoPositionHandle(bounds.min, Quaternion.identity);
        if (EditorGUI.EndChangeCheck())
        {
            for (int i = 0; i < 3; i++)
            {
                newMin[i] = Mathf.Round(newMin[i]);
            }
            Vector3 max = bounds.max;
            Vector3 delta = newMin - max;
            Undo.RecordObject(snappyPlaneScaler.transform, "moved plane");
            snappyPlaneScaler.transform.position = (max + newMin) * 0.5f;
            snappyPlaneScaler.transform.localScale = new Vector3(delta.x, delta.z, 1);
        }
    }

    void SceneGUIRepaintEvent() {
        
    }

}
