﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Diagnostics;

public class GitMoverWindow : EditorWindow
{

    public List<UnityEngine.Object> quickList = new List<Object>();
    public List<MoveOperation> objectsToMove;
    public UnityEngine.Object moveToFolder;
    private SerializedObject serializedObject;
    private SerializedProperty prop;
    private SerializedProperty moveToFolderProp;

    private void OnEnable()
    {
        serializedObject = new SerializedObject(this);
        prop = serializedObject.FindProperty("objectsToMove");
        moveToFolderProp = serializedObject.FindProperty("moveToFolder");
        quickListProp = serializedObject.FindProperty("quickList");

    }

    [MenuItem("toivos best windows/GitMoverWindow")]
    public static void Init() {
        EditorWindow.GetWindow<GitMoverWindow>().Show();
    }
    public string gitPath = "C:\\Program Files\\Git\\bin\\git.exe";
    private SerializedProperty quickListProp;

    private Vector2 scrlopos;
    private void OnGUI()
    {
        scrlopos = EditorGUILayout.BeginScrollView(scrlopos);
        EditorGUILayout.HelpBox("Git in my experience doesn't detect moved files very easily. This tool moves files in git and unity friendly way (takes care of meta files also)", MessageType.Info);
        UnityEditor.EditorGUILayout.PropertyField(quickListProp,true);
        if (quickList.Count > 0)
        {
            foreach (var q in quickList)
            {
                objectsToMove.Add(new MoveOperation{objectToMove = q});
            }
            quickList.Clear();
        }
        UnityEditor.EditorGUILayout.PropertyField(prop,true);
        UnityEditor.EditorGUILayout.PropertyField(moveToFolderProp, true);
        serializedObject.ApplyModifiedProperties();
        serializedObject.Update();

        gitPath = UnityEditor.EditorGUILayout.TextField("GitPath",gitPath);

        bool rename = false;
        string toPathInsideAssets = null;
        if (moveToFolder == null)
        {
            EditorGUILayout.HelpBox("Move to folder is not assigned so rename mode enabled ", MessageType.Warning);
            rename = true;
            
        }
        else
        {
            toPathInsideAssets = AssetDatabase.GetAssetPath(moveToFolder);
        }

            bool move = GUILayout.Button("Move");
            if (move)
            {
                move = EditorUtility.DisplayDialog("Are you sure you want to move the files.", "", "yes", "no");
            }
            // goes inside asset folder, which we don't want
            var datapath = new DirectoryInfo(Application.dataPath).Parent;
            List<KeyValuePair<string,string>> operations = new List<KeyValuePair<string,string>>();
            foreach (var item in objectsToMove)
            {

                if (rename)
                {
                    toPathInsideAssets = AssetDatabase.GetAssetPath(item.objectToMove);
                    toPathInsideAssets = new FileInfo(datapath + toPathInsideAssets).Directory.ToString().Replace(datapath.ToString(), "");
                }


                string originalFilePath = datapath + "/" + AssetDatabase.GetAssetPath(item.objectToMove);
                string fileName = new FileInfo(originalFilePath).Name;
                if (!string.IsNullOrEmpty(item.renameTo)) fileName = item.renameTo;
                string newPath = datapath + "/" + toPathInsideAssets + "/" + fileName;


                operations.Add(new KeyValuePair<string, string>(originalFilePath, newPath));
                operations.Add(new KeyValuePair<string, string>(originalFilePath + ".meta", newPath + ".meta"));

            }

            int ranOps = 0;
            foreach (var op in operations)
            {

                // EditorGUILayout.LabelField(op.Key + "\t -> \t" + op.Value);
                if(new System.IO.FileInfo(op.Key).Extension != new System.IO.FileInfo(op.Value).Extension)
                {
                    EditorGUILayout.HelpBox("extension will be changed", MessageType.Warning);
                }
                string command = "mv \""+ op.Key + "\" \"" + op.Value + "\"";
                EditorGUILayout.TextField(command);
                if (move)
                {
                    // First, create a ProcessStartInfo for some configuration. 

                    ProcessStartInfo gitInfo = new ProcessStartInfo();
                    gitInfo.CreateNoWindow = true;
                   gitInfo.RedirectStandardError = true;
                   gitInfo.RedirectStandardOutput = true;
                   gitInfo.FileName = gitPath;
                   
                     gitInfo.UseShellExecute = false;
                    // Then create a Process to actually run the command.

                    Process gitProcess = new Process();
                    gitInfo.Arguments = command; // such as "fetch orign"
                    gitInfo.WorkingDirectory = datapath.ToString();

                    gitProcess.StartInfo = gitInfo;
                    gitProcess.Start();

                    string stderr_str = gitProcess.StandardError.ReadToEnd();  // pick up STDERR
                    string stdout_str = gitProcess.StandardOutput.ReadToEnd(); // pick up STDOUT
                     UnityEngine.Debug.Log(stderr_str);
                       UnityEngine.Debug.Log(stdout_str);
                //
                 //   gitProcess.WaitForExit();
                 //   gitProcess.Close();
                    ranOps++;

                }
            }

            if (ranOps > 0)
            {
                AssetDatabase.Refresh();
                UnityEngine.Debug.Log("ops done "+ranOps);
            }

        EditorGUILayout.EndScrollView();
    }
    [System.Serializable]
    public class MoveOperation
    {
        public UnityEngine.Object objectToMove;
        public string renameTo;
    }

}
