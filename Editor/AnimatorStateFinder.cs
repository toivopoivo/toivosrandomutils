using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ToivonRandomUtils;

using UnityEditor;
using UnityEditor.Animations;

using System.Linq;
using System.Text;



public class AnimatorStateFinder : EditorWindow
{
	public  AnimatorController inspectedAnimator;

	[MenuItem ("toivos best windows/anim state finder")]
	static void Init ()
	{
		EditorWindow.GetWindow<AnimatorStateFinder> ().Show ();
	}

	public List<string> allstates = new List<string> ();
	public string filter;
	Vector2 scrollpos;
	Dictionary<AnimatorStateMachine, AnimatorStateMachine> found;

	void GetStatePaths ()
	{
//			closed.Clear ();

		List<AnimatorStateMachine> open = new List<AnimatorStateMachine> ();
		found = new Dictionary<AnimatorStateMachine, AnimatorStateMachine> ();
		foreach (var layer in inspectedAnimator.layers) {
			string currentPath = layer.name;	
			open.Add (layer.stateMachine);
			found.Add (layer.stateMachine, null);
			while (true) {
				if (open.Count == 0)
					break;
				AnimatorStateMachine current = open.First ();
				open.RemoveAt (0);
				foreach (var item in current.stateMachines) {
					if (found.ContainsKey (item.stateMachine)) {
						continue;
					} else {
						found.Add (item.stateMachine, current);
						open.Add (item.stateMachine);
					}
				}
		
			}
			
		}

		foreach (var item in found) {
			var path = item.Value.GetRecurseWhileNotNull (x => found [x]);
			path.Reverse ();
			var pathStr = path.Aggregate ("", (x, y) => x + "/" + y.name + " pos(" + y.parentStateMachinePosition + ")");
			pathStr += "/" + item.Key.name + " pos "+ item.Key.entryPosition;
			foreach (var state in item.Key.states) {
				allstates.Add (pathStr + "/" + state.state.name + " (pos " + state.position + ")");
			}
		}

//		allstates.AddRange( found.Select(x=>found[x.Key]));
	}

	public string wiggled;
	bool posHasBeenStored;
	Vector3 actualStoredPos;
	void OnGUI ()
	{
		
		inspectedAnimator = EditorGUILayout.ObjectField (inspectedAnimator, typeof(AnimatorController), true) as AnimatorController;
		if (GUILayout.Button ("get states")) {
			allstates.Clear ();
			GetStatePaths ();
		}

		filter = UnityEditor.EditorGUILayout.TextArea (filter);

		scrollpos = EditorGUILayout.BeginScrollView (scrollpos);
		foreach (var item in allstates) {
			if (!string.IsNullOrEmpty (filter)) {
				if (!item.ToLower ().Contains (filter)) {
					continue;
				}
			}
//			found.Keys.FirstOrDefault (x => x.name == item);
			UnityEditor.EditorGUILayout.TextArea (item);
		}
		/**if (found != null) {			WIGGLING EXPERIMENT FAILED :(
			EditorGUI.BeginChangeCheck ();
			wiggled =	UnityEditor.EditorGUILayout.TextField ("wiggle", wiggled);
			if(EditorGUI.EndChangeCheck()) {
				posHasBeenStored = false;
			}
			if (!string.IsNullOrEmpty (wiggled)) {
//				found.SelectMany (x => x.Key.states).SingleOrDefault (x => x.state.name == wiggled).position = ;
				ChildAnimatorState wiggleTarget = found.SelectMany (x => x.Key.states).FirstOrDefault (x => x.state.name == wiggled);
				if (wiggleTarget.state != null) {
					EditorGUILayout.HelpBox ("wiggling " + wiggleTarget.state.name, MessageType.Info);
					if (!posHasBeenStored) {
						actualStoredPos = wiggleTarget.position;
						posHasBeenStored = true;
					}
					var vector3 = Random.insideUnitSphere * 10f;
					wiggleTarget.position += vector3 - (vector3 * 0.5f);
					wiggleTarget.n
				}
				if (GUILayout.Button ("revert")) {
//					wiggleTarget.
					wiggleTarget.position = actualStoredPos;
					wiggled = "";
				}
			
			}
		}*/

		EditorGUILayout.EndScrollView ();

	}

	void Update(){
		if(!string.IsNullOrEmpty(wiggled)){
			Repaint ();
		}
	}
}

