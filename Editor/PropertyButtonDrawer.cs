﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Linq;
using System.Collections.Generic;
using System;
using System.Reflection;

[CustomPropertyDrawer (typeof(PropertyButton))]
public class PropertyButtonDrawer : PropertyDrawer
{
	public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
	{
		return property.isExpanded ? 17f + EditorGUI.GetPropertyHeight (property) : EditorGUI.GetPropertyHeight (property);
	}

	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{

		var propButton = (attribute as PropertyButton);
	

		var propHeight = EditorGUI.GetPropertyHeight (property);
		EditorGUI.PropertyField (new Rect (position.x, position.y, position.width, propHeight), property, true);
		if (property.isExpanded && GUI.Button (new Rect (position.x, position.y + propHeight, position.width, 17f), propButton.invokedMethodName)) {

			var firstSuitableField = 
				property.serializedObject.targetObject.GetType ().GetFields (System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic).First (
					x => x.GetCustomAttributes (typeof(PropertyButton), false).Count (
						y => (y as PropertyButton).type == propButton.type
					) > 0 
				);
			var arrayIndex = ParsePropertyPathForListIndex(property, firstSuitableField, out var rightObj);

			var methodInfo = propButton.type.GetMethod (propButton.invokedMethodName);
			var methodParams = methodInfo.GetParameters ();
			if (methodParams.Length == 2 && methodParams [0].ParameterType.IsSubclassOf (typeof(UnityEngine.Object)) && methodParams [1].ParameterType == typeof(int))
				methodInfo.Invoke (rightObj, new[]{ property.serializedObject.targetObject, (object)arrayIndex });
			else if (methodParams.Length == 1 && methodParams [0].ParameterType == typeof(UnityEngine.Object))
				methodInfo.Invoke (rightObj, new[]{ property.serializedObject.targetObject });
			else
				methodInfo.Invoke (rightObj, null); 

		}
	}

	public static int ParsePropertyPathForListIndex(SerializedProperty property, FieldInfo firstSuitableField, out object rightObj)
	{
		var nonCastedArray = firstSuitableField.GetValue(property.serializedObject.targetObject);
		var array = nonCastedArray as IList;
		var splitIndex = property.propertyPath.Split('[', ']')[1];
		var arrayIndex = int.Parse(splitIndex);
		rightObj = array[arrayIndex];
		return arrayIndex;
	}
}
