﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(GetTypeInChildren))]
public class GetTypeInChildrenEditor : Editor
{
	public override void OnInspectorGUI ()
	{
		DrawDefaultInspector ();
		var GTICTarget = target as GetTypeInChildren;

		GTICTarget.InpspectorMoveNext ();
	}
}
