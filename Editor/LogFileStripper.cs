﻿
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.IO;
using ToivonRandomUtils;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Specialized;
using System;
using System.Text.RegularExpressions;
using System.Collections;
using ToivoLinq;

public class LogFileStripper : EditorWindow
{
    [SerializeField]
    public List<MatchedPattern> patterns = new List<MatchedPattern>();
    [SerializeField]
    public List<MatchCriteria> alwaysSkippedLines = new List<MatchCriteria>(
            new MatchCriteria[] {
                new MatchCriteria { matchmode = MatchMode.Exact, input = "" },
                new MatchCriteria { matchmode = MatchMode.Exact, input = " " },
                new MatchCriteria { matchmode = MatchMode.Exact, input = "\n" },
                new MatchCriteria { matchmode = MatchMode.Contains, input = "filename: c" }
            }
        );
    public string openedLogFilesParentFolder;
    List<string> openedLogFilePaths = new List<string>();

    public LogFileStripper logFileStripper;
    public bool testMode = false;
    //public bool  = false;

    SerializedProperty patternsProp;
    SerializedProperty alwaysSkippedLinesProp;
    SerializedProperty openedLogFilesParentFolderProp;

    [MenuItem("toivos best windows/LogFileStripper")]
    public static void Init()
    {
        EditorWindow.GetWindow<LogFileStripper>().Show();
    }

    private void OnEnable()
    {
        serializedObject = new SerializedObject(this);
        patternsProp = serializedObject.FindProperty("patterns");
        alwaysSkippedLinesProp = serializedObject.FindProperty("alwaysSkippedLines");
        openedLogFilesParentFolderProp = serializedObject.FindProperty("openedLogFilesParentFolder");
    }

    private void Update()
    {
        Repaint();
    }
    System.Diagnostics.Stopwatch sw;
    private void OnGUI()
    {
        
        EditorGUILayout.PropertyField(patternsProp,true);
        EditorGUILayout.PropertyField(alwaysSkippedLinesProp, true);
        EditorGUILayout.PropertyField(openedLogFilesParentFolderProp, true);
        testMode = UnityEditor.EditorGUILayout.Toggle("testmode: " ,testMode);
        serializedObject.ApplyModifiedProperties();
        if(!string.IsNullOrEmpty(openedLogFilesParentFolder))openedLogFilePaths = System.IO.Directory.GetDirectories(openedLogFilesParentFolder).SelectMany(x=>Directory.GetFiles(x)).Where(x=>Path.GetExtension(x)==".txt").ToList();
        foreach (var openedLogFilePath in openedLogFilePaths)
        {
            if (!File.Exists(openedLogFilePath))
            {
                EditorGUILayout.HelpBox("invalid path! "+openedLogFilePath, MessageType.Error);
            } else
            {
                EditorGUILayout.HelpBox(openedLogFilePath, MessageType.None);
            }
        }
        EditorGUILayout.LabelField("prosess index" + processIndex);
        EditorGUILayout.LabelField("process state:" + processState);
        if (GUILayout.Button("start"))
        {
            currentProcess = AllPathsProcessing();
        }
        if (GUILayout.Button("stop"))
        {
            currentProcess = null;
            TryDisposeIO();
        }
        sw = System.Diagnostics.Stopwatch.StartNew();

        if (currentProcess != null)
        {
            while (true)
            {
                if (!currentProcess.MoveNext())
                    break;
           //   if (testMode)
            //       break;
                if (sw.ElapsedMilliseconds > 100)
                {
                    sw.Reset();
                    sw.Start();
                    break;
                }
            }
        }
    }
    IEnumerator AllPathsProcessing()
    {
        for (int i = 0; i < openedLogFilePaths.Count; i++)
        {
            var cur = Processing(openedLogFilePaths[i]);
            processIndex = i;
            while(cur.MoveNext()) yield return null;
        }
    }

    string processState="NaN";
    int processIndex = -1;
    IEnumerator currentProcess;
    private SerializedObject serializedObject;
    System.IO.StreamWriter streamWriter;
    System.IO.StreamReader streamReader;
   IEnumerator Processing(string path)
    {
        processState = "reading text file";
        yield return null;
        //var logFileLines = System.IO.File.ReadAllLines(path);
        streamReader = new StreamReader(path);
        processState = "matching";
        yield return null;

        Dictionary<int, GroupedLines> groups = new Dictionary<int, GroupedLines>();
        GroupedLines lastGroup = null;
        List<string> logFileLines = new List<string>();
        int index = 0;
        streamWriter = null;
        if (testMode)
        {
            streamWriter = new StreamWriter("E:\\testlog\\logfilestripper_debugdata.txt");
            streamWriter.AutoFlush = true;
        }
        while (true)
        {
            string line = streamReader.ReadLine();
            if (line == null) break;
            MatchLine(groups,ref lastGroup, logFileLines, index, line);
            index++;
            processState = "matching, lastupdate: " + UnityEditor.EditorApplication.timeSinceStartup;
            yield return null;
        }
        TryDisposeIO();

        if (lastGroup != null)
        {
            lastGroup.groupEndLine = index - 1;
        }

        System.Text.StringBuilder newFileSb = new System.Text.StringBuilder();

        GroupedLines currentGroup = null;
        for (int i = 0; i < logFileLines.Count; i++)
        {
            if (currentGroup == null)
            {
                GroupedLines beginnigOfGroup;
                groups.TryGetValue(i, out beginnigOfGroup);
                if (beginnigOfGroup != null && beginnigOfGroup.groupEndLine - beginnigOfGroup.groupStartLine > 3)
                {
                    currentGroup = beginnigOfGroup;
                    newFileSb.AppendLine("LOGFILESTRIPPER begin stripped");
                    // let amount the of matching patterns lines through. in case they some unique information 
                    for (int j = 0; j < beginnigOfGroup.groupingPattern.lines.Count; j++)
                    {
                        if (i + j > logFileLines.Count) break;
                        newFileSb.AppendLine(logFileLines[i + j]);
                    }
                    newFileSb.AppendLine("LOGFILESTRIPPER end strippped groupsize, " + (beginnigOfGroup.groupEndLine - beginnigOfGroup.groupStartLine) + " lines");

                }
                else
                {
                    newFileSb.AppendLine(logFileLines[i]);

                }
            }
            else if (i == currentGroup.groupEndLine)
            {
                currentGroup = null;
            }
            processState = "prossecing matches " + ((float)i / logFileLines.Count);
            yield return null;
        }

        processState = "writing file";
        yield return null;
        if (!testMode) System.IO.File.WriteAllText(path, newFileSb.ToString());
        processState = "done";
        yield return null;
    }

    private void TryDisposeIO()
    {
        if (streamWriter != null)
        {
            streamWriter.Close();
            streamWriter = null;
        }
        if (streamReader != null)
        {
            streamReader.Close();
            streamReader = null;
        }
    }

    void MatchLine(Dictionary<int, GroupedLines> groups,  ref GroupedLines lastGroup, List<string> logFileLines, int index, string line)
    {
        logFileLines.Add(line);
        if (alwaysSkippedLines.FastAny(x => x.Matches(line)))
        {
            return;
        }
        if (testMode) streamWriter.WriteLine(line);
        bool anyMatch = false;
        if (testMode) streamWriter.WriteLine(line);
        foreach (var pattern in patterns)
        {
            bool isMatch = false;
            for (int i = 0; i < pattern.lines.Count; i++)
            {
                if (line.Contains(pattern.lines[i]))
                {
                    isMatch = true;
                    if (testMode) streamWriter.WriteLine("is match to "+ pattern.lines[i]);
                    break;
                }else
                {
                    if (testMode) streamWriter.WriteLine("is NOT match to " + pattern.lines[i]);
                }
            }

            if (isMatch)
            {
                anyMatch = true;
                if (lastGroup != null && lastGroup.groupingPattern == pattern)
                {
                    if (testMode) streamWriter.WriteLine("is part of the same group as last");
                }
                else
                {
                    if (lastGroup != null)
                    {
                        lastGroup.groupEndLine = index - 1;
                    }
                    if (testMode) streamWriter.WriteLine(" start new group");
                    var newGroup = new GroupedLines();
                    newGroup.groupingPattern = pattern;
                    newGroup.groupStartLine = index;
                    groups.Add(index, newGroup);
                    lastGroup = newGroup;
                }
                break;
            }
        }
        if (!anyMatch)
        {
            if (lastGroup != null)
            {
                lastGroup.groupEndLine = index - 1;
                if (testMode) streamWriter.WriteLine("end last group");
                lastGroup = null;
            }
        }
    }

    public class GroupedLines
    {
        public MatchedPattern groupingPattern;
        public int groupStartLine;
        public int groupEndLine = -1;
    }
    [System.Serializable]
    public class MatchedPattern
    {
        public string patternName;
        public List<string> lines = new List<string>();
    }
    [System.Serializable]
    public class MatchCriteria
    {
        [TextArea]
        public string input;
        public MatchMode matchmode;
        public bool Matches(string line)
        {
            if (matchmode == MatchMode.Exact)
                return line == input;
            else
                //return line.Contains(input, StringComparison.CurrentCultureIgnoreCase);
                return line.IndexOf(input, System.StringComparison.CurrentCultureIgnoreCase) >= 0;
        }


    }
    public enum MatchMode
    {
        Contains,
        Exact
    }
}
