﻿using UnityEngine;
using System.Collections;
using UnityEditor;
//using System;
//    [CustomEditor(typeof(ComponentHolder))]
//    public class ComponentHolderEditor : Editor {
//        SerializedProperty prop;
////        void Enable(){
////        }
//
//        public override void OnInspectorGUI () {
//            serializedObject.Update();
////            DrawDefaultInspector();
//            EditorGUILayout.PropertyField(serializedObject.FindProperty("components"),true);
//            EditorGUILayout.PropertyField(serializedObject.FindProperty("componentsWithKeys") , true );
//            serializedObject.ApplyModifiedProperties();
//        }
//    }
    [CustomPropertyDrawer(typeof(ComponentHolder.ComponentWithStringKey))]
    public class ComponentWithKeyDrawer: PropertyDrawer{
//        ComponentHolder.ComponentType what= ComponentHolder.ComponentType.Image;
        public override float GetPropertyHeight (SerializedProperty property, GUIContent label) {
//            return Screen.width < 333 ? (16f + 18f) : 16f;
            return 70f;
        }
       public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            //begin property
            label = EditorGUI.BeginProperty(position, label, property);

            //draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            //push indentlevel
            int oldIndentLevel = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            //calculateRects

            var typeRect = new Rect (position.x, position.y, 160, 20);
            var componentRect = new Rect (position.x, position.y+25, 160, 20);
            var keyRect = new Rect (position.x, position.y+50, 160, 20);

            SerializedProperty componentType= property.FindPropertyRelative("componentType");
            SerializedProperty key= property.FindPropertyRelative("key");
            SerializedProperty component= property.FindPropertyRelative("component");
            SerializedProperty image= property.FindPropertyRelative("image");
            SerializedProperty text= property.FindPropertyRelative("text");

            //draw fields 

//            Rect contentPosition = EditorGUI.PrefixLabel(position, label);
//            position.y += 30f;
//            if (position.height > 16f) {
//                position.height = 16f;
//                EditorGUI.indentLevel += 1;
//                contentPosition = EditorGUI.IndentedRect(position);
//                contentPosition.y += 18f;
//            }
            EditorGUI.PropertyField(typeRect ,componentType,GUIContent.none );
//            EditorGUI.indentLevel += 1;
//            contentPosition = EditorGUI.IndentedRect(position);
//            position.y += 30f;
            if (componentType.enumNames [componentType.enumValueIndex] == "Image")
            {
                EditorGUI.PropertyField(componentRect,image,GUIContent.none );
            } else if (componentType.enumNames [componentType.enumValueIndex] == "Text")
            {
                EditorGUI.PropertyField(componentRect,text,GUIContent.none );
            } else if (componentType.enumNames [componentType.enumValueIndex] == "Unspecified")
            {
                EditorGUI.PropertyField(componentRect,component,GUIContent.none );
            }
            EditorGUI.PropertyField(keyRect, key, GUIContent.none);
            EditorGUI.EndProperty();
            //pop indent level
            EditorGUI.indentLevel = oldIndentLevel;
//            EditorGUI.EnumPopup(position,  ) ;
           
        }
        
    }

//public ComponentType componentType;
//public Component component;
//public string key;

//public enum ComponentType
//{
//    Image,
//    Text,
//    Unspecified
//}