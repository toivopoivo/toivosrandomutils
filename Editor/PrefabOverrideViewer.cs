﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PrefabOverrideViewer : EditorWindow
{
    [MenuItem("toivos best windows/PrefabOverrideViewer")]
    public static void Init()
    {
        EditorWindow.GetWindow<PrefabOverrideViewer>().Show();
    }

    private Object target;
    private Vector2 scrollpos;

    void OnGUI()
    {
        target = EditorGUILayout.ObjectField(target, typeof(Object), true) as Component;
        if (target == null)
        {
            return;
        }

        var mods = PrefabUtility.GetPropertyModifications(target);
        EditorGUILayout.BeginScrollView(scrollpos);

        EditorGUILayout.BeginHorizontal();
        UnityEditor.EditorGUILayout.LabelField("target");
        UnityEditor.EditorGUILayout.LabelField("value");
        UnityEditor.EditorGUILayout.LabelField("propertyPath");
        UnityEditor.EditorGUILayout.LabelField("object reference");
        UnityEditor.EditorGUILayout.LabelField("prefab");
        EditorGUILayout.EndHorizontal();

        foreach (var item in mods)
        {
            if (target.GetType() != item.target.GetType())
            {
                continue;
            }

            EditorGUILayout.BeginHorizontal();
            //EditorGUILayout.HelpBox(Newtonsoft.Json.JsonConvert.SerializeObject(item), MessageType.None);
//            EditorGUILayout.HelpBox(""+item.target, MessageType.None);
            EditorGUILayout.ObjectField(item.target, typeof(Object), true);
            EditorGUILayout.HelpBox(item.value, MessageType.None);
            EditorGUILayout.HelpBox(item.propertyPath, MessageType.None);
            EditorGUILayout.ObjectField(item.objectReference, typeof(Object), true);
            var prefabAssetPath = UnityEditor.PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(target);
            var prefab = EditorGUILayout.ObjectField(AssetDatabase.LoadAssetAtPath<Component>(prefabAssetPath),
                typeof(Component), false);
            if (GUILayout.Button("apply"))
            {
                //UnityEditor.PrefabUtility.ApplyPropertyOverride(new SerializedObject(target).FindProperty(item.propertyPath), );
                var serializedProperty = new SerializedObject(target).FindProperty(item.propertyPath);
                UnityEditor.PrefabUtility.ApplyPropertyOverride(serializedProperty, prefabAssetPath,
                    InteractionMode.UserAction);
            }

            EditorGUILayout.EndHorizontal();
        }

        EditorGUILayout.EndScrollView();
    }
}