﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class CustomEditorDialogRunner : EditorWindow{
	public IEnumerator dialog;

	public static void Init(IEnumerator dialoging){
		var dialogRunner = EditorWindow.GetWindow<CustomEditorDialogRunner> ();
		dialogRunner.Show ();
		dialogRunner.dialog = dialoging;
	}

	void Update(){
		Repaint ();
	}
	void OnGUI(){
		if (dialog.MoveNext ()) {
			return;
		} 
		Close ();
	}

}
