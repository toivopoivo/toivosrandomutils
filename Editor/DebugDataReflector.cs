﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;


public class DebugDataReflector : EditorWindow
{
	[MenuItem ("toivos best windows/experimental/debug data reflector")]
	public static void Init ()
	{
		GetWindow<DebugDataReflector> ().Show ();
	}

	[SerializeField]
	public Object reflectedObject;

	const System.Reflection.BindingFlags fuckingEverythingBindingFlags = System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic;
	Vector2 scrollPos;
	void OnGUI ()
	{
		EditorGUILayout.BeginHorizontal();
		reflectedObject = EditorGUILayout.ObjectField ("reflected object", reflectedObject, typeof(MonoBehaviour), true);
		GUILayout.Label ("time: " + Time.time);
		EditorGUILayout.EndHorizontal();
		if (reflectedObject == null)
			return;
		scrollPos = EditorGUILayout.BeginScrollView (scrollPos);
		var fields = reflectedObject.GetType ().GetFields (fuckingEverythingBindingFlags);
		var reflectedData = fields.Where (x => x.GetCustomAttributes (typeof(ReflectedDebugData), true).Any ());
		foreach (var item in reflectedData) {
			var getValue = item.GetValue (reflectedObject);
			DrawReflectedGUIRecurse (item, getValue, 0);
		}

		EditorGUILayout.EndScrollView();
	}

	static void Indent (int indentLevel)
	{
		for (int i = 0; i < indentLevel; i++) {
			GUILayout.Space (10f);
		}
	}
	static Dictionary<object, int> showLastItemsCountForLists = new Dictionary<object, int> ();
	static void DrawReflectedGUIRecurse (System.Reflection.FieldInfo item, object getValue, int indentLevel)
	{
/*	EditorGUILayout.BeginHorizontal ();
		Indent (indentLevel);
		EditorGUILayout.HelpBox (string.Format ("{0} {1}", item.Name, getValue), MessageType.None);
		EditorGUILayout.EndHorizontal ();

*/
//		EditorGUILayout.HelpBox (string.Format ("{0} {1}", item.Name, getValue), MessageType.None);

		var list = getValue as IList;
		if (list != null) {
			if (!showLastItemsCountForLists.ContainsKey (list)) {
				showLastItemsCountForLists.Add (list, 100);
			}
			if(list.Count > 0 ){
				var invokedMethods = GetDebugDataReflectorInvokedMethodsOnType (list [0].GetType ());
				EditorGUILayout.BeginHorizontal();	
				foreach (var method in invokedMethods) {
					if(GUILayout.Button(method.Name+ " ALL")){
						for (int i = 0; i < list.Count; i++) {
							method.Invoke (list [i], null);
						}
							
					}
				}
				EditorGUILayout.EndHorizontal();
			}
			showLastItemsCountForLists [list] = EditorGUILayout.IntField ("shown LAST items count", showLastItemsCountForLists [list]);
			for (int i = 0; i < list.Count; i++) {
				if (list.Count - i > showLastItemsCountForLists [list]) {
					continue;
				}
				var listItem = list [i];
				DrawReflectedGUIRecurse (item, listItem, indentLevel + 1);
			}
			return;
		}

		if (getValue is UnityEngine.Object) {
			EditorGUILayout.BeginHorizontal ();
			Indent (indentLevel);
			EditorGUILayout.LabelField (item.Name);
			EditorGUILayout.ObjectField (getValue as Object, typeof(Object), true);
			EditorGUILayout.EndHorizontal ();
			return;
		}
		if (getValue == null) {
			EditorGUILayout.BeginHorizontal ();
			Indent (indentLevel);
			EditorGUILayout.LabelField (item.Name + ": null");
			EditorGUILayout.EndHorizontal ();
			return;
		}
		var subFieldsType = getValue.GetType ();
		if (subFieldsType.IsClass) {
			var allFields = subFieldsType.GetFields (fuckingEverythingBindingFlags);
			foreach (var subSubField in allFields) {
				DrawReflectedGUIRecurse (subSubField, subSubField.GetValue (getValue), indentLevel + 1);
			}
			var invokedMethods = GetDebugDataReflectorInvokedMethodsOnType (subFieldsType);
			EditorGUILayout.BeginHorizontal();
			foreach (var method in invokedMethods) {
				if(GUILayout.Button(method.Name)){
					method.Invoke (getValue, null);
				}
			}
			EditorGUILayout.EndHorizontal();
		} else {
			EditorGUILayout.BeginHorizontal ();
			Indent (indentLevel);
			EditorGUILayout.LabelField (string.Format ("{0}: {1}", item.Name, getValue));
			EditorGUILayout.EndHorizontal ();
		}
		
	}

	static IEnumerable<System.Reflection.MethodInfo> GetDebugDataReflectorInvokedMethodsOnType (System.Type subFieldsType)
	{
		return subFieldsType.GetMethods (fuckingEverythingBindingFlags).Where (x => x.GetCustomAttributes (typeof(DebugDataReflectorInvokedMethod), true).Any ());
	}
}


