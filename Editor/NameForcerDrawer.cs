﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer (typeof(ForceProperGoName))]
public class NameForcerDrawer : PropertyDrawer
{
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
		if (property.objectReferenceValue != null)
			property.objectReferenceValue.name = property.name;
		EditorGUI.PropertyField (new Rect (position.x, position.y, position.width, EditorGUI.GetPropertyHeight (property)), property);
	}
}
