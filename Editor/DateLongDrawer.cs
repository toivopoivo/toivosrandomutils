﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(DateLong))]
public class DateLongDrawer :  PropertyDrawer {

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) + 17f;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        position.height = 17f;
        EditorGUI.PropertyField(position, property);
        position.y += 17f;
        position.x += 17f;
        //EditorGUI.LabelField(position,new System.DateTime(property.longValue));
        EditorGUI.LabelField(position,DateTime.FromFileTimeUtc(property.longValue).ToLocalTime().ToString());

    }
}
