﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using ToivonRandomUtils;
using Debug = UnityEngine.Debug;
using Object = System.Object;

public class JsonComparison : EditorWindow
{
	[MenuItem("toivos best windows/ComponentJsonComparison")]
	public static void Init()
	{
		EditorWindow.GetWindow<JsonComparison>().Show();
	}

	private Component a, b, c;
	public void OnGUI()
	{
		a = (Component) UnityEditor.EditorGUILayout.ObjectField(a, typeof(Component), true);
		b = (Component) UnityEditor.EditorGUILayout.ObjectField(b, typeof(Component), true);
		c = (Component) UnityEditor.EditorGUILayout.ObjectField(c, typeof(Component), true);


		if (GUILayout.Button("compare"))
		{
			var apath=  ToivonRandomUtils.RandomUtils.CreateFileInMyDocumentsThatContainsString(EditorJsonUtility.ToJson(a, true), "json comparisons", "a_");
			var bpath = ToivonRandomUtils.RandomUtils.CreateFileInMyDocumentsThatContainsString(EditorJsonUtility.ToJson(b, true), "json comparisons", "b_");
			string cpath  ="";
			if (c != null)
			{
				cpath= ToivonRandomUtils.RandomUtils.CreateFileInMyDocumentsThatContainsString(EditorJsonUtility.ToJson(c, true), "json comparisons", "c_");
				System.Diagnostics.Process.Start("C:/Program Files/KDiff3/kdiff3.exe", $"\"{apath}\" \"{bpath}\" \"{cpath}\"");
			}
			else
			{
			   System.Diagnostics.Process.Start("C:/Program Files/KDiff3/kdiff3.exe", $"\"{apath}\" \"{bpath}\"");
				
			}
			
			
		}

	}
}