﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace ToivonRandomUtils
{
	[CanEditMultipleObjects] // Don't ruin everyone's day
	[CustomEditor (typeof(ScriptableObject), true)] // Target all MonoBehaviours and descendants
	public class ScritableObjectCustomEditor : Editor
	{
		public override void OnInspectorGUI ()
		{
			MonoBehaviourCustomEditor.InspectorButtonGUI (this);
		}
	}

}