﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer (typeof(PopUpString))]

public class PopUpStringPropDrawer : PropertyDrawer
{
	public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
	{
		return 20f;
	}

	ToivonRandomUtils.RandomUtils.PropDrawMethods guiFilterMethod = new ToivonRandomUtils.RandomUtils.PropDrawMethods(); 
	
	string cur;
	string temp;
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
		var possibleVals = property.FindPropertyRelative ("possibleValues");
		string[] possibleValsAr = new string[ possibleVals.arraySize];
		for (int i = 0; i < possibleValsAr.Length; i++) {
			try {
				if (possibleVals.GetArrayElementAtIndex (i).propertyType == SerializedPropertyType.String)
					possibleValsAr [i] = possibleVals.GetArrayElementAtIndex (i).stringValue;
			} catch (System.Exception ex) {
                Debug.Log(ex);
			}
		}
		var sIndexProp = property.FindPropertyRelative ("selectedIndex");
		var lastVal = sIndexProp.intValue;

		Rect popupRect = new Rect (position.x, position.y, position.width, 20f);
//		sIndexProp.intValue = EditorGUI.Popup (popupRect, property.name, lastVal, possibleValsAr);
//		 = EditorGUI.Popup (popupRect, property.name, lastVal, possibleValsAr);
		popupRect.width *= 0.5f;
		guiFilterMethod.popupPos=popupRect;
		popupRect.x += popupRect.width;
		guiFilterMethod.textfieldPos = popupRect;
		int intValue;
		ToivonRandomUtils.RandomUtils.EditorGUIFilterPopUp (property.name, possibleValsAr, ref cur, ref temp, property.serializedObject.context, out intValue, overrideDrawing: guiFilterMethod);
		sIndexProp.intValue = intValue;
		Event currentEvent = Event.current;
		if (currentEvent.type == EventType.ContextClick) {
			Vector2 mousePos = currentEvent.mousePosition;
			if (popupRect.Contains (mousePos)) {
				// Now create the menu, add items and show it
				GenericMenu menu = new GenericMenu ();
				menu.AddItem (new GUIContent ("SetDirty"), false, SetPropertyDirty, property);
				menu.AddSeparator ("");
				menu.ShowAsContext ();
				currentEvent.Use ();
			}
		}


		property.FindPropertyRelative ("dirty").boolValue |= (lastVal != sIndexProp.intValue || EditorPrefs.GetBool ("PopupStringDirtyFlagged"));
		EditorPrefs.SetBool ("PopupStringDirtyFlagged", false);
	}

	static void SetPropertyDirty (object x)
	{
		EditorPrefs.SetBool ("PopupStringDirtyFlagged", true);
	}
}
