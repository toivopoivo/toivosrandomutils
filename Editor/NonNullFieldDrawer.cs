﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer (typeof(NonNullField))]
public class NonNullFieldDrawer : PropertyDrawer
{
	public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
	{
		if (property.propertyType == SerializedPropertyType.ObjectReference) {
			return EditorGUI.GetPropertyHeight (property) + ((property.objectReferenceValue == null) ? fieldHeight : 0);
		} else {
			return 17f;
		}
	}

	const float fieldHeight = 30f;

	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
		int isNullMul = 0;
		if (property.propertyType == SerializedPropertyType.ObjectReference) {
			if (property.objectReferenceValue== null) {
				EditorGUI.HelpBox (new Rect (position.x, position.y, position.width, fieldHeight), "Note the field <" + property.name + "> should not be null", MessageType.Warning);
				isNullMul = 1;
			}
			EditorGUI.PropertyField (new Rect (position.x, position.y + (fieldHeight * isNullMul), position.width, EditorGUI.GetPropertyHeight (property)), property);
		} else {
			EditorGUI.HelpBox (new Rect (position.x, position.y, position.width, fieldHeight), "The field <" + property.name + "> is not a nullable field, remove the attribute", MessageType.Error);
			isNullMul = 1;
		}
	}


}
