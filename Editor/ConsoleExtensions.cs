﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using System.Text.RegularExpressions;
using ToivonRandomUtils;
using Debug = UnityEngine.Debug;
using Object = System.Object;

public class ConsoleExtensions : EditorWindow
{


    [MenuItem("toivos best windows/ConsoleExtensions")]
    public static void Init()
    {
        EditorWindow.GetWindow<ConsoleExtensions>().Show();
    }

    private Type logEntrIEStype;

    private void OnEnable()
    {
        logEntrIEStype = System.Type.GetType("UnityEditor.LogEntries, UnityEditor.dll");
    }

    private int analysisRow = -1;
    private string regexString;
    private string mesasge;
      public void OnGUI()
    {
         EditorGUI.BeginChangeCheck();
         var  turboFileLoggerExe =   EditorGUILayout.TextField( "turboFileLoggerExe",EditorPrefs.GetString("turboFileLoggerExe"));
        var logsFolder =   EditorGUILayout.TextField("logsFolder", EditorPrefs.GetString("logsFolder")) ;
        if (EditorGUI.EndChangeCheck())
        {
            EditorPrefs.SetString("logsFolder", logsFolder);
            EditorPrefs.SetString("turboFileLoggerExe", turboFileLoggerExe);
        }

        int specialActions = EditorGUILayout.Popup(-1,new string[]{ "test log", "test log spam", "open log with toivo turbo consle logger", "test set row to 800" } );
        
        if (specialActions == 0) 
        {
            Debug.Log("testing");
        }
        if (specialActions == 1)
        {
            for (int i = 0; i < 1000; i++)
            {
                Debug.Log("testing");
            }
        }
        if (specialActions == 2)
        {
            var filePath = logsFolder+ "\\log_"+System.IO.Path.GetRandomFileName()+".txt";
            System.IO.File.WriteAllText(filePath,GetLogData());
            
            ProcessStartInfo process = new ProcessStartInfo();
            process.CreateNoWindow = true;
            process.RedirectStandardError = true;
            process.RedirectStandardOutput = true;
            process.FileName = turboFileLoggerExe;
                   
            process.UseShellExecute = false;

            Process gitProcess = new Process();
            process.Arguments = filePath; 

            gitProcess.StartInfo = process;
            gitProcess.Start();

            string stderr_str = gitProcess.StandardError.ReadToEnd();  // pick up STDERR
            string stdout_str = gitProcess.StandardOutput.ReadToEnd(); // pick up STDOUT
            UnityEngine.Debug.Log(stderr_str);
            UnityEngine.Debug.Log(stdout_str);
        }
        
        if (specialActions == 3)
        {
            var row = 800;
            ShowRow(row);
        }
        EditorGUILayout.TextField("message",mesasge);
        regexString = EditorGUILayout.TextField("regex",regexString);
        analysisRow = EditorGUILayout.IntField("row",analysisRow);


        if (GUILayout.Button($"Highlight Next Match (total logs ({GetTotalLogCount(logEntrIEStype)}))"))
        {
            mesasge = "";
            HighlightNextMatch();
        }
        /*
        // private void SetActiveEntry(LogEntry entry)
        var setActiveEntryMethod = type.GetMethod("SetActiveEntry", BindingFlags.Instance | BindingFlags.NonPublic);
        // public static extern bool GetEntryInternal(int row, [Out] LogEntry outputEntry);
        var getEntry = logEntrIEStype.GetMethod("GetEntryInternal");
        object logEntry;
        var logEntryType = System.Type.GetType("UnityEditor.LogEntry, UnityEditor.dll");
        
        var parameters = new object[]{0, Activator.CreateInstance(logEntryType)  };
        //public static extern int StartGettingEntries();
        logEntrIEStype.GetMethod("StartGettingEntries").Invoke(null, null);
        
        getEntry.Invoke(null, parameters);
        //public static extern void EndGettingEntries();
        logEntrIEStype.GetMethod("EndGettingEntries").Invoke(null, null);
        
        setActiveEntryMethod.Invoke(console, new object[]{ parameters[1]} );
        */


    }

      private void HighlightNextMatch()
      {
          var allEntries = GetLogEntriesTillEnd(analysisRow + 1);
          System.Text.RegularExpressions.Regex regexClass = new Regex(regexString, RegexOptions.IgnoreCase);

          // reset row and try again
          if (allEntries.Length == 0 && analysisRow != -1)
          {
              mesasge += "row reset -";
              analysisRow = -1;
              HighlightNextMatch();
          }

          bool match = false;
          for (int i = 0; i < allEntries.Length; i++)
          {
              if (regexClass.IsMatch(allEntries[i]))
              {

                  analysisRow = i + analysisRow + 1;
                  mesasge = "match row "+analysisRow + " - ";
                  ShowRow(analysisRow);
                  match = true;
                  break;
              }
          }

          if (!match)
          {
              mesasge += "no matches - resetting row";
              analysisRow = -1;
          }
      }

      public string[] GetLogEntriesTillEnd(int startIndex)
      {
          
          int totalLogCount = GetTotalLogCount(logEntrIEStype);
          if ( startIndex >= totalLogCount)
          {
              return new string[0];
          } 
          var getEntry = logEntrIEStype.GetMethod("GetEntryInternal");
          var logEntryType = System.Type.GetType("UnityEditor.LogEntry, UnityEditor.dll");


          
          logEntrIEStype.GetMethod("StartGettingEntries").Invoke(null, null);


          string[] logs = new string[totalLogCount-startIndex];
          var fieldMessage = logEntryType.GetField("message");
          var fieldMode = logEntryType.GetField("mode");
          
          
          for (int i = startIndex; i < totalLogCount; i++)
          {
              
           
              var logEntryInstance = Activator.CreateInstance(logEntryType);
              var parameters = new object[]{i, logEntryInstance  };
              getEntry.Invoke(null, parameters);
              var mode =(Mode) fieldMode.GetValue(parameters[1]);
              string prefix = "";
              if (mode.HasFlag(Mode.Error))
              {
                  prefix += "Error ";
              }
              logs[i-startIndex]=prefix  + " " + fieldMessage.GetValue(parameters[1]) as string;
              
          }
          
          logEntrIEStype.GetMethod("EndGettingEntries").Invoke(null, null);

          return logs;
      }

      private static int GetTotalLogCount(Type logEntrIEStype)
      {
          return (int) logEntrIEStype.GetMethod("GetCount").Invoke(null,null);
      }

      private static void ShowRow(int row)
    {
        var type = System.Type.GetType("UnityEditor.ConsoleWindow, UnityEditor.dll");
        var logEntrIEStype = System.Type.GetType("UnityEditor.LogEntries, UnityEditor.dll");

        // m_ListView.scrollPos.y = (float) (LogEntries.GetCount() * rowHeight);
        var listViewState = type.GetField("m_ListView", BindingFlags.Instance | BindingFlags.NonPublic)
            .GetValue(EditorWindow.GetWindow(type));
        var fieldScrollPos = listViewState.GetType().GetField("scrollPos");

        int rowIndex = row;
        fieldScrollPos.SetValue(listViewState, new Vector2(0, 21 * rowIndex));

        type.GetMethod("ShowConsoleRow", BindingFlags.Static | BindingFlags.NonPublic)
            .Invoke(null, new object[] {rowIndex});
    }

      
      [System.Flags]
      internal enum Mode
      {
          Error = 1,
          Assert = 2,
          Log = 4,
          Fatal = 16, // 0x00000010
          DontPreprocessCondition = 32, // 0x00000020
          AssetImportError = 64, // 0x00000040
          AssetImportWarning = 128, // 0x00000080
          ScriptingError = 256, // 0x00000100
          ScriptingWarning = 512, // 0x00000200
          ScriptingLog = 1024, // 0x00000400
          ScriptCompileError = 2048, // 0x00000800
          ScriptCompileWarning = 4096, // 0x00001000
          StickyError = 8192, // 0x00002000
          MayIgnoreLineNumber = 16384, // 0x00004000
          ReportBug = 32768, // 0x00008000
          DisplayPreviousErrorInStatusBar = 65536, // 0x00010000
          ScriptingException = 131072, // 0x00020000
          DontExtractStacktrace = 262144, // 0x00040000
          ShouldClearOnPlay = 524288, // 0x00080000
          GraphCompileError = 1048576, // 0x00100000
          ScriptingAssertion = 2097152, // 0x00200000
          VisualScriptingError = 4194304, // 0x00400000
      }

   //public void OnGUI()
   //{
   //     EditorGUI.BeginChangeCheck();
   //     var  turboFileLoggerExe =   EditorGUILayout.TextField( "turboFileLoggerExe",EditorPrefs.GetString("turboFileLoggerExe"));
   //    var logsFolder =   EditorGUILayout.TextField("logsFolder", EditorPrefs.GetString("logsFolder")) ;
   //    if (EditorGUI.EndChangeCheck())
   //    {
   //        EditorPrefs.SetString("logsFolder", logsFolder);
   //        EditorPrefs.SetString("turboFileLoggerExe", turboFileLoggerExe);
   //    }

   //    if (GUILayout.Button("test log "))
   //    {
   //        Debug.Log("testing");
   //    }
   //    if (GUILayout.Button("test log spam"))
   //    {
   //        for (int i = 0; i < 1000; i++)
   //        {
   //            Debug.Log("testing");
   //        }
   //    }

   //    if (GUILayout.Button("test set active entry"))
   //    {
   //        var type = System.Type.GetType ("UnityEditor.ConsoleWindow, UnityEditor.dll");
   //        var logEntrIEStype = System.Type.GetType("UnityEditor.LogEntries, UnityEditor.dll");

   //        // m_ListView.scrollPos.y = (float) (LogEntries.GetCount() * rowHeight);
   //        var listViewState = type.GetField("m_ListView", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(EditorWindow.GetWindow(type));
   //        var fieldScrollPos = listViewState.GetType().GetField("scrollPos");

   //        var  = 800;
   //        int rowIndex = ;
   //         fieldScrollPos.SetValue(listViewState,new Vector2(0, 21*rowIndex) );
   //        
   //        type.GetMethod("ShowConsoleRow", BindingFlags.Static|BindingFlags.NonPublic).Invoke(null, new object[] {rowIndex});
   //        
   //        //private void UpdateListView()
   //        
   //     //   var methodUpdateListView = type.GetMethod("UpdateListView", BindingFlags.Instance | BindingFlags.NonPublic);
   //     //   methodUpdateListView.Invoke(EditorWindow.GetWindow(type),null);
   //        
   //        /*
   //        // private void SetActiveEntry(LogEntry entry)
   //        var setActiveEntryMethod = type.GetMethod("SetActiveEntry", BindingFlags.Instance | BindingFlags.NonPublic);
   //        // public static extern bool GetEntryInternal(int row, [Out] LogEntry outputEntry);
   //        var getEntry = logEntrIEStype.GetMethod("GetEntryInternal");
   //        object logEntry;
   //        var logEntryType = System.Type.GetType("UnityEditor.LogEntry, UnityEditor.dll");
   //        
   //        var parameters = new object[]{0, Activator.CreateInstance(logEntryType)  };
   //        //public static extern int StartGettingEntries();
   //        logEntrIEStype.GetMethod("StartGettingEntries").Invoke(null, null);
   //        
   //        getEntry.Invoke(null, parameters);
   //        //public static extern void EndGettingEntries();
   //        logEntrIEStype.GetMethod("EndGettingEntries").Invoke(null, null);
   //        
   //        setActiveEntryMethod.Invoke(console, new object[]{ parameters[1]} );
   //        */
   //    }

   //    if (GUILayout.Button("open log with toivo turbo consle logger"))
   //    {
   //        var filePath = logsFolder+ "\\log_"+System.IO.Path.GetRandomFileName()+".txt";
   //        System.IO.File.WriteAllText(filePath,GetLogData());
   //        
   //        ProcessStartInfo process = new ProcessStartInfo();
   //        process.CreateNoWindow = true;
   //        process.RedirectStandardError = true;
   //        process.RedirectStandardOutput = true;
   //        process.FileName = turboFileLoggerExe;
   //               
   //        process.UseShellExecute = false;

   //        Process gitProcess = new Process();
   //        process.Arguments = filePath; 

   //        gitProcess.StartInfo = process;
   //        gitProcess.Start();

   //        string stderr_str = gitProcess.StandardError.ReadToEnd();  // pick up STDERR
   //        string stdout_str = gitProcess.StandardOutput.ReadToEnd(); // pick up STDOUT
   //        UnityEngine.Debug.Log(stderr_str);
   //        UnityEngine.Debug.Log(stdout_str);
   //    }
   //}

    public static string GetLogData()
    {
        string logPathh;
        if (Application.isEditor && !Input.GetKey(KeyCode.LeftControl))
        {
            logPathh = string.Format("{0}\\Unity\\Editor\\Editor.log", Environment.GetEnvironmentVariable("LocalAppData"));
        }else
        {
            logPathh = string.Format("{0}\\..\\LocalLow\\Ablizmo\\IMC\\Player.log", Environment.GetEnvironmentVariable("LocalAppData"));
        }

        var fileStream = new FileStream(logPathh, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);


        int refLineLen = 125;
        var sReader = new StreamReader(fileStream);
        long pos = fileStream.Length - (20000 * refLineLen);
        if (pos < 0) pos = 0;
        fileStream.Position = pos;
        sReader.ReadLine();

        return sReader.ReadToEnd();
    }

    

}
