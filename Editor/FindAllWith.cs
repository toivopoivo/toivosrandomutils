﻿using UnityEngine;
using System.Collections;
using ToivonRandomUtils;
using System.Collections.Generic;
using UnityEditor;
using System.Linq;
using System.Reflection;

public class FindAllWith : EditorWindow
{
    
	public string methodName;
	public string findWithAssetGUID;
	public string findWithGuidBankGUid;
	public int findWithFileLocalIdentifier;
	//public List<Component> all = new List<Component>();
	/// <summary>
	/// key: referencer, value: refenced object
	/// </summary>
	public Dictionary<Component,UnityEngine.Object> all = new Dictionary<Component,Object> ();



	public List<Object> allObjects = new List<Object>();
	public bool dontListMultipleOfSameType;
	IEnumerator currentFinding;

	List<Component> components;
	int index;

	[MenuItem ("Debug/FindAllWith Window")]
	[MenuItem ("toivos best windows/FindAllWith Window")]
	static void Init ()
	{
		EditorWindow.GetWindow<FindAllWith> ().Show ();
	}

    
	IEnumerator FindingWithMethodName ()
	{
		if (all == null) all = new Dictionary<Component, Object> ();
		all.Clear ();
		components = RandomUtils.GetComponentInChildrenFromRootGOs<Component>(true).ToList ();
		for (index = 0; index < components.Count; index++) {
			var type = components [index].GetType ();
			if (dontListMultipleOfSameType && all.Any (x => x.GetType () == type)) {
				continue;
			}
			if (type.GetMethod (methodName, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public) != null) {
				all.Add (components [index], null);
			}
			yield return null;
		}
	}

	void Update ()
	{
		Repaint ();
	}

	public Object findRefObj;
	StaticObjectSelectionMode staticObjectSelectionMode;

	void SelectStatic (StaticObjectSelectionMode mode)
	{
		if (mode== StaticObjectSelectionMode.SelectNonStaticNonTriggerColliders) {
			selectedStatic = RandomUtils.GetComponentInChildrenFromRootGOs<Collider> (false).Where (x => !x.isTrigger && !GameObjectUtility.AreStaticEditorFlagsSet (x.gameObject, UnityEditor.StaticEditorFlags.NavigationStatic)).Where (x => x.attachedRigidbody == null || x.attachedRigidbody.isKinematic).Select (x => x.gameObject).Where (x => x.GetComponent<UnityEngine.AI.NavMeshObstacle> () == null).ToArray ();
		} else {
			selectedStatic = RandomUtils.GetComponentInChildrenFromRootGOs<MeshRenderer> (false).Where (x => GameObjectUtility.AreStaticEditorFlagsSet (x.gameObject, UnityEditor.StaticEditorFlags.NavigationStatic) && x.GetComponent<Collider> () == null).Select (x => x.gameObject).ToArray ();
		}
	}

	void FixStaticItem (StaticObjectSelectionMode mode, GameObject item	)
	{
		if (mode == StaticObjectSelectionMode.SelectNonStaticNonTriggerColliders) {	
			Undo.RecordObject (item, "set nav static");
			UnityEditor.GameObjectUtility.SetStaticEditorFlags (item, StaticEditorFlags.NavigationStatic);
			SelectStatic (mode);
			Selection.activeObject = selectedStatic.FirstOrDefault ();
			UnityEditor.EditorWindow.GetWindow<SceneView> ().AlignViewToObject ((Selection.activeObject as GameObject).transform);
		
		} else {
			Undo.RecordObject (item, "removed nav static");
			UnityEditor.GameObjectUtility.SetStaticEditorFlags (item, (StaticEditorFlags)0);
			SelectStatic (mode);
			Selection.activeObject = selectedStatic.FirstOrDefault ();
			UnityEditor.EditorWindow.GetWindow<SceneView> ().AlignViewToObject ((Selection.activeObject as GameObject).transform);
		}
	}

	public enum StaticObjectSelectionMode{SelectNonStaticNonTriggerColliders, SelectNonColliderStaticMeshRenderers}

	Vector2 scrollPos;


	PropertyModification[] GetChangesFromFromPrefab(Object inObj) {
		var mods = PrefabUtility.GetPropertyModifications (inObj);
		if (mods == null) return new PropertyModification[0];
		mods = mods.Where (x => !RandomUtils.IsPropertyModificationIgnorable (x)).ToArray ();

		return mods;
	}

	//public System.Func<Dictionary<Component,UnityEngine.Object>> onDrawExtraSelectionButtons;
	//public static System.Action<Dictionary<Component,UnityEngine.Object>> onDrawExtraSelectionButtons;


	enum FoundItemsGroupingMode{NoGrouping, PrefabModsCount, Materials, Type, WorldScale, StaticFlags}

	FoundItemsGroupingMode groupingMode = FoundItemsGroupingMode.Type;

	void OnGUI ()
	{
		dontListMultipleOfSameType = EditorGUILayout.Toggle ("dontListMultipleOfSameType", dontListMultipleOfSameType);

		EditorGUILayout.BeginHorizontal ();

		methodName = EditorGUILayout.TextField ("Method name", methodName);
		if (GUILayout.Button ("Find All",GUILayout.Width(150f))) {
			currentFinding = FindingWithMethodName ();	
		}

		EditorGUILayout.EndHorizontal ();


		EditorGUILayout.BeginHorizontal ();

		findWithAssetGUID = EditorGUILayout.TextField ("asset guid", findWithAssetGUID);
		if (GUILayout.Button ("Find with asset guid",GUILayout.Width(150f))) {
			Object obj = AssetDatabase.LoadAssetAtPath (AssetDatabase.GUIDToAssetPath (findWithAssetGUID), typeof(Object));
			Debug.Assert (obj != null, "fail");
			Selection.activeObject = obj;
		}

		EditorGUILayout.EndHorizontal ();

//		
		EditorGUILayout.BeginHorizontal ();

		findWithFileLocalIdentifier = EditorGUILayout.IntField ("findWithFileLocalIdentifier", findWithFileLocalIdentifier);
		if (GUILayout.Button ("Find with findWithFileLocalIdentifier", GUILayout.Width (150f))) {
			var comps = ToivonRandomUtils.RandomUtils.GetComponentInChildrenFromRootGOs<Component> (true);
			
			bool foundPropertyOnAny = false;
			foreach (var item in comps) {
				if(item==null)
					continue;
				;


				UnityEditor.SerializedObject serializedObject = new UnityEditor.SerializedObject(item);
				PropertyInfo inspectorModeInfo =
					typeof(SerializedObject).GetProperty("inspectorMode", BindingFlags.NonPublic | BindingFlags.Instance);
				inspectorModeInfo.SetValue(serializedObject, InspectorMode.Debug, null);
				UnityEditor.SerializedProperty localIdProp = serializedObject.FindProperty("m_LocalIdentfierInFile");
				if(localIdProp==null){
					continue;
				} else {
					foundPropertyOnAny = true;
				}
				if (localIdProp.intValue == findWithFileLocalIdentifier) {
					UnityEditor.EditorGUIUtility.PingObject(item);
					Debug.Log("success");
					break;
				}
			}
			Debug.Log("finished");
			Debug.Assert (foundPropertyOnAny, "misspelled field");
			

		}

		EditorGUILayout.EndHorizontal ();


		EditorGUILayout.BeginHorizontal ();

		findWithGuidBankGUid = EditorGUILayout.TextField ("guidbank guid", findWithGuidBankGUid);
		if (GUILayout.Button ("Find with guidbank",GUILayout.Width(150f))) {

			//bah wrong assembly
			//Object obj = RuntimeGUIDReferenceBanksManager.ResolveGUIDStatic (findWithGuidBankGUid);

			//do it the hard way
			var type = System.Type.GetType ("RuntimeGUIDReferenceBanksManager, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
			var methInfo = type.GetMethod ("ResolveGUIDStatic",new[]{typeof(string),typeof(bool)});
			var obj = methInfo.Invoke (null, new[]{findWithGuidBankGUid,null}) as UnityEngine.Object;


			Debug.Assert (obj != null, "fail");
			Selection.activeObject = obj;
		}

		EditorGUILayout.EndHorizontal ();

//		if (onDrawExtraSelectionButtons != null) {
//			onDrawExtraSelectionButtons (all);
//		}



		GUILayout.Space(20f);

		if (GUILayout.Button ("Select all gos in scene")) {
			var gos =FindObjectsOfType<GameObject> ();

			all.Clear ();
			foreach (var item in gos) {
				all.Add (item.transform, item);
			}
		}

		findRefObj = EditorGUILayout.ObjectField ("Find references to this object", findRefObj, typeof(Object), true);
		if (findRefObj != null) {
			var objList = new List<Object> ();
			objList.Add (findRefObj);
			var go = findRefObj as GameObject;
			if(go != null)	objList.AddRange (go.GetComponents<Component> ());
			currentFinding =	ReferenceFinding (objList);
		}
		findRefObj = null;

		if (currentFinding != null) {
			if (currentFinding.MoveNext ()) {
				GUILayout.Label (string.Format ("Found {0} monobehaviours", components.Count));
				GUILayout.Label (string.Format ("{0}/{1}", index, components.Count));
				GUILayout.Label (string.Format ("found implementor: {0}", Mathf.Max (all.Count, allObjects.Count)));
				return;
			}
		}

		if (all != null) {
			var list = all.Keys.ToList ();
			foreach (var item in list) {
				if (item == null) all.Remove (item);
			}

			groupingMode = (FoundItemsGroupingMode)EditorGUILayout.EnumPopup ("grouping mode:", groupingMode);

			GUILayout.BeginHorizontal ();
            if ( GUILayout.Button("select all") )
            {
				Selection.objects = all.Select (x => x.Key.gameObject).ToArray ();
            }
			var scaleGroups = all.GroupBy (x => x.Key.transform.lossyScale.ToString());

			foreach (var item in scaleGroups) {
				if ( GUILayout.Button("scale"+item.Key) ) {
					Selection.objects = item.Select (x => x.Key.gameObject).ToArray ();
				}
			}
			GUILayout.EndHorizontal ();



			IEnumerable<IGrouping<string, KeyValuePair<Component, Object>>> grouped = null;

			if (groupingMode == FoundItemsGroupingMode.NoGrouping) {
				grouped = all.GroupBy (x => "all");
			}

			if (groupingMode == FoundItemsGroupingMode.PrefabModsCount) {
				grouped = all.GroupBy (x => GetChangesFromFromPrefab (x.Key).Length.ToString());
			}

			if (groupingMode == FoundItemsGroupingMode.Materials) {
				grouped = all.GroupBy (x => GetRendsString (x));
			}

			if (groupingMode == FoundItemsGroupingMode.Type) {
				grouped = all.GroupBy (x => x.Key.GetType()+"");
			}
			if (groupingMode == FoundItemsGroupingMode.WorldScale) {
				grouped = all.GroupBy (x => x.Key.transform.lossyScale+"");
			}
			if (groupingMode == FoundItemsGroupingMode.StaticFlags) {
				grouped = all.GroupBy (x => GameObjectUtility.GetStaticEditorFlags(x.Key.gameObject)+"");
			}

			//results
			GUILayout.Space(5f);

			scrollPos = EditorGUILayout.BeginScrollView (scrollPos);

			foreach (var grouping in grouped) {

				EditorGUILayout.Space ();
				EditorGUILayout.LabelField ("Grouping:" + grouping.Key);


				foreach (var item in grouping) {
					//				GUIContent.			   
					GUILayout.BeginHorizontal ();
					var typeName = item.Key.GetType ().Name;
					var targetStr = typeName + " " + item.Key.name;

//					if ((item.Key.transform.lossyScale - Vector3.one).magnitude > 0.001f) {
//						targetStr += "worlscale:" + item.Key.transform.lossyScale;
//					}
					var prefabtype = PrefabUtility.GetPrefabType(item.Key);
					if (prefabtype == PrefabType.ModelPrefabInstance || prefabtype == PrefabType.DisconnectedModelPrefabInstance) targetStr += "(" + prefabtype + ")";

					if (GUILayout.Button (targetStr)) {

						//all.RemoveAll (x => x == null);
						bool cleared = false;
						if (!Selection.objects.All (x => x == null || all.Select(k => k.Key.gameObject).Contains ( x as GameObject ))) {
							//Selection.activeObject = null;
							cleared = true;
						}

						var sel = Selection.objects.ToList ();
						if (cleared) sel.Clear ();
						sel.Add (item.Key.gameObject);
						Selection.objects = sel.ToArray ();

						if(!cleared)EditorWindow.GetWindow<SceneView> ().ShowNotification(new GUIContent("Added "+item.Key.gameObject.name+" to selection"));
						//Selection.activeGameObject = item.gameObject;
					}

					if (GUILayout.Button ("locate")) {
						Selection.activeGameObject = item.Key.gameObject;

						//					var windowtype = typeof(EditorWindow).Assembly.GetType("UnityEditor.SceneView");
						//					var scenewindow = EditorWindow.GetWindow (windowtype);
						//
						//					scenewindow.Focus ();
						//
						//					EditorApplication.delayCall += () => {
						//						
						//						EditorApplication.delayCall += () => {
						//							var haxEvent = new Event();
						//							haxEvent.keyCode = KeyCode.F;
						//							haxEvent.type = EventType.KeyDown;
						//							scenewindow.SendEvent(haxEvent);
						//
						//							EditorApplication.delayCall += () => {
						//								var haxEvent2 = new Event();
						//								haxEvent2.keyCode = KeyCode.F;
						//								haxEvent2.type = EventType.KeyDown;
						//								scenewindow.SendEvent(haxEvent);
						//							};
						//						};
						//					};

						UnityEditor.EditorWindow.GetWindow<SceneView> ().FrameSelected (/*(Selection.activeObject as GameObject).transform*/);

					}

					GUILayout.FlexibleSpace();
					if (EditorGUILayout.Popup (-1, new []{ "show reference: "+item.Value }) == 0) {
						Selection.activeObject = item.Value;
					}

					if (GUILayout.Button ("FindObjectsOfTypePerfLogged " + typeName)) {
						Selection.objects =FindObjectsOfType (item.GetType ()).Cast<MonoBehaviour> ().Select (x => x.gameObject).ToArray ();
					}
					GUILayout.EndHorizontal ();
				}

			}

		}



		GUILayout.FlexibleSpace ();


		GUI.color = Color.yellow;
		if (GUILayout.Button ("select static nontrigger colliders that are not marked navmeshstatic")) {	
			staticObjectSelectionMode = StaticObjectSelectionMode.SelectNonStaticNonTriggerColliders;
			SelectStatic (staticObjectSelectionMode);		
			Selection.objects = selectedStatic;
		}
		if (GUILayout.Button ("select noncollider static meshrenderers")) {	
			staticObjectSelectionMode = StaticObjectSelectionMode.SelectNonColliderStaticMeshRenderers;
			SelectStatic (staticObjectSelectionMode);		
			Selection.objects = selectedStatic;
		}
		if (selectedStatic != null) {
			bool fixAll = false;
			if (GUILayout.Button ("fix all")) {
				fixAll = true;
			}
			if (GUILayout.Button ("clear")) {
				selectedStatic = null;
			}

			foreach (var item in selectedStatic) {
				EditorGUILayout.BeginHorizontal ();
				EditorGUILayout.LabelField(item.transform.GetRecurseWhileNotNull (x => x.transform.parent).Aggregate ("", (x, y) => y.name+"."+x ));
				EditorGUILayout.ObjectField (item, typeof(GameObject), true); 
				if (GUILayout.Button ("fix") || fixAll	) {
					FixStaticItem (staticObjectSelectionMode, item);

				}
				EditorGUILayout.EndHorizontal ();
			}
		}

		EditorGUILayout.EndScrollView ();

	}

	string GetRendsString (KeyValuePair<Component, Object> x)
	{
		if (!(x.Key is MeshFilter)) return "norend";

		var rend = x.Key.GetComponent<MeshRenderer>();

//		var jsonlol = EditorJsonUtility.ToJson (rend.sharedMaterials,true);
//		return jsonlol;

		string str = "mats:";
		foreach (var item in rend.sharedMaterials) {
			str += item.ToString () + "\n";
		}
		return str;

	}

	GameObject[] selectedStatic;

	bool TryBingo (object rawValue, Object unityObject)
	{
		Object objectValue = rawValue as Object;
		if (objectValue != null) {
			if (objectValue == unityObject) {
				if(all.ContainsKey (components[index]))
                {
                    Debug.Log("multiple references from the same object in " + components[index].gameObject + " " + components[index]+ " to "+objectValue);
                }else
                {
                    all.Add(components[index], objectValue);
                }
                return true;
			}
		}

		return false;
	}

	IEnumerator ReferenceFinding (List<Object> objects)
	{
		if (all == null) all = new Dictionary<Component, Object>();
		all.Clear ();

		components = ToivonRandomUtils.RandomUtils.GetComponentInChildrenFromRootGOs<Component> (true).ToList ();
		System.Diagnostics.Stopwatch watch = new System.Diagnostics.Stopwatch ();
		watch.Start ();
		for (index = 0; index < components.Count; index++) {
			var bindingFlags = BindingFlags.FlattenHierarchy | BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
			if (components [index] == null) {
				continue;
			}
			var meshFilter = components [index] as MeshFilter;
			if (meshFilter != null) { // mesh filters and materials are a special case NOTE: materials not implemented yet
				if (objects.Any (x => x == meshFilter.sharedMesh))
					all.Add (meshFilter,meshFilter.sharedMesh);
			}

			foreach (var item in components[index].GetType ().GetFields (bindingFlags)) {
				foreach (var unityObject in objects) {
					var rawValue = item.GetValue (components [index]);
					if (rawValue == null)
						continue;
					var type = rawValue.GetType ();
					if (!TryBingo(rawValue, unityObject))
                    {
                        List<IEnumerator> jobs = new List<IEnumerator>();
                        if (type.IsSerializable)
                        {
                            jobs.Add(CheckFieldsOfSerializableType(type, bindingFlags, rawValue, unityObject, jobs));
                        }
                        jobs.Add(CheckIfIsListAndContainsNewJobs(bindingFlags, unityObject, rawValue, jobs));

                        while (true)
                        {
                            if (jobs.Count == 0) break;
                            var currentJob = jobs.Last();
                            while (currentJob.MoveNext())
                            {
                                if (watch.ElapsedMilliseconds > 100f)
                                {
                                    yield return null;
                                    watch.Reset();
                                    watch.Start();
                                }
                            }
                            jobs.Remove(currentJob);
                        }

                    }
				}
			}
		}
		Debug.Log ("done");
	}

    private IEnumerator CheckIfIsListAndContainsNewJobs(BindingFlags bindingFlags, Object unityObject, object rawValue, List<IEnumerator> jobs)
    {
        /*toivo 27.6.2018 tried to implement some list support*/
        var someKindOfList = rawValue as IList;
        if (someKindOfList != null)
        {
            for (int i = 0; i < someKindOfList.Count; i++)
            {
                if (someKindOfList[i] == null) continue;
                if (someKindOfList[i].GetType().IsSerializable)
                {
                    CheckFieldsOfSerializableType(someKindOfList[i].GetType(), bindingFlags, someKindOfList[i], unityObject,jobs);
                }
                else
                {
                    TryBingo(someKindOfList[i], unityObject);
                }
                yield return null;
            }
        }
    }

    IEnumerator CheckFieldsOfSerializableType(System.Type type, BindingFlags bindingFlags, object rawValue, UnityEngine.Object unityObject,List<IEnumerator> jobs)
    {

            foreach (var serializableTypeField in type.GetFields(bindingFlags))
            {

                object otherValue = null;

                try
                {
                    if (serializableTypeField.FieldType.ToString() == "System.Void*") continue; //why the hell do I need to do this (doesnt crash on toivos machine :DD)
                                                                                                //Debug.Log("serializableTypeField type:" + serializableTypeField.FieldType);
                                                                                                //Debug.Log("serializableTypeField:"+serializableTypeField);
                                                                                                //Debug.Log("rawValue:" + rawValue);
                    otherValue = serializableTypeField.GetValue(rawValue);
                }
                catch (System.Exception e)
                {
                    Debug.LogError("findallwith fail:" + e);
                }

                if (otherValue != null)
                {
                    TryBingo(otherValue, unityObject);
                }
                 jobs.Add(CheckIfIsListAndContainsNewJobs(bindingFlags, unityObject, otherValue, jobs));
                yield return null;
            }
    }

}

