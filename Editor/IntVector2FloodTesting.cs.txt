﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using ToivonRandomUtils;
using System.Linq;


public class IntVector2FloodTesting
{
	[Test]
	public void IntVector2EqualityWorks ()
	{
		Assert.AreEqual (new IntVector2 (), new IntVector2 ());
	}



	[Test]
	public void IsPossibeToFindPathSingle ()
	{
		TestPathFindingJob (false, x => false);
	}

	[Test]
	public void IsPossibeToFindPathMany ()
	{
		TestPathFindingJob (true, x => x.Abs.x >5 || x.Abs.y > 5);
	}

	static void TestPathFindingJob (bool manyTargets, System.Func<IntVector2, bool> obstacles)
	{
		
		Int2Vector2Utils.FloodPathFindJob pathfindJob = new Int2Vector2Utils.FloodPathFindJob (new IntVector2 (), x => x == new IntVector2 (2, 2), obstacles, false, manyTargets);
		var pathFinding = pathfindJob.Pathfinding ();
		int cycles = 0;
		while (pathFinding.MoveNext ()) {
			cycles++;
			if (cycles > 10000) {
				Assert.Fail ();
			}
		}
		Assert.Pass ("cycles: " + cycles +" paths "+pathfindJob.foundPaths.Count);
	}

	[Test]
	public void DoObstaclesWork ()
	{
		Int2Vector2Utils.FloodPathFindJob pathfindJob = new Int2Vector2Utils.FloodPathFindJob (
			                                                new IntVector2 (), 
			                                                x => x == new IntVector2 (2, 2), 
			                                                x => IntVector2.EightNeighbours (new IntVector2 ()).Any (y => y == x),
			                                                false
		                                                );
		var pathFinding = pathfindJob.Pathfinding ();
		int cycles = 0;
		while (pathFinding.MoveNext ()) {
			cycles++;
			if (cycles > 1000) {
			}
		}

		Assert.AreEqual (pathfindJob.state, Int2Vector2Utils.FloodPathFindJob.StateType.Failed, "cycles " + cycles);

	}
}
