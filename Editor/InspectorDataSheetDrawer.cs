﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

[CustomPropertyDrawer (typeof(InspectorDataSheet))]
public class InspectorDataSheetDrawer : PropertyDrawer
{


	public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
	{
		return property.FindPropertyRelative ("editorHeight").floatValue;
	}

	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
		
		DrawProperty (position, property);
	
	}

	void DrawProperty (Rect position, SerializedProperty property)
	{

		//		types.Single (x => x.Name == property.type);
		var origPropName = property.name;
		var type = (attribute as InspectorDataSheet).type;
		var fields = type.GetFields ();

		var datacells = new List<string> ();
		var rowIndexes = new List<int> ();
		var showName = new List<bool> ();
		for (int i = 0; i < fields.Length; i++) {
			var attrs = fields [i].GetCustomAttributes (typeof(InspectorDataCell), false) as InspectorDataCell[];
			if (attrs.Length > 0) {
				datacells.Add (fields [i].Name);
				rowIndexes.Add (attrs [0].rowIndex);
				showName.Add (attrs [0].showName);
			}
		}
		float height = 0f;
		EditorGUI.LabelField (new Rect (position.x, position.y + height, position.width, 17f), property.displayName);
		height += 17f;
		// draw cells
		int currentRow = rowIndexes [0];
		int rowSize = rowIndexes.FindAll (x => x == currentRow).Count;
		int subIndexer = 0;
		bool showedName = false;
		for (int i = 0; i < datacells.Count; i++) {
			if (rowIndexes [i] != currentRow) {
				height += 17f;
				if (showedName)
					height += 17f;
				showedName = false;
				currentRow = rowIndexes [i];
				rowSize = rowIndexes.FindAll (x => x == currentRow).Count;
				subIndexer = 0;
			}
			var splittedWidth = position.width / (float)rowSize;
			if (showName [i] || showedName) {
				showedName = true;
				EditorGUI.LabelField (new Rect (position.x + (splittedWidth * subIndexer), position.y + height, splittedWidth, 17f), property.FindPropertyRelative (datacells [i]).displayName);
				EditorGUI.PropertyField (new Rect (position.x + (splittedWidth * subIndexer), position.y + height + 17f, splittedWidth, 17f), property.FindPropertyRelative (datacells [i]), GUIContent.none);
			} else {
				EditorGUI.PropertyField (new Rect (position.x + (splittedWidth * subIndexer), position.y + height, splittedWidth, 17f), property.FindPropertyRelative (datacells [i]), GUIContent.none);

			}
			subIndexer++;
		}
		height += 17f;
		//		height += rowIndexes.Distinct ().ToList ().Count * 17f;
		// draw the normal fields
		var editorHeightProp = property.FindPropertyRelative ("editorHeight");
		//		property.Reset ();
		bool continued = false;
		bool first = true;
		int lastDepth = -1;
		//		var subProp = property.FindPropertyRelative (origPropName);
		int origPropDepth = property.depth;
		while (property.NextVisible (true)) {
			//			if (!origPropReached) {
			//				origPropReached |= property.name == origPropName;
			//				continue;
			//			}	
			var current = property;
			if (current.depth == origPropDepth+1 && !first)
				break;
			first = false;
			if (continued) {
				if (lastDepth < current.depth) {
					continue;
				} else {
					continued = false;
				}
			}
			var isEditorHeightProperty = current.name == "editorHeight";
			if (datacells.Contains (current.name) || isEditorHeightProperty) {
				continued = true;
				lastDepth = current.depth;
				continue;
			}
			var currentHeight = EditorGUI.GetPropertyHeight (current, null, true);
			EditorGUI.PropertyField (new Rect (position.x + 25f, position.y + height, position.width, currentHeight), current, false);
			height += currentHeight;
		}
		if (editorHeightProp == null)
			throw new System.Exception ("WRONG USAGE EXCEPTION. YOU SUCK. ADD FIELD CALLED EDITOR HEIGHT");
		editorHeightProp.floatValue = height+17f;
	}
}
