using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;

public class SpriteSheetCreatorWindow : EditorWindow
{
    // Variables for sprites, size, and saving path
    private List<Sprite> sprites = new List<Sprite>();
    private Vector2 sheetSize = new Vector2(1024, 1024);
    private string savePath = "Assets/spriteSheet.png";
    private Vector2 scrollPos;
    private Texture2D transparentTexture;

    [MenuItem("Tools/Sprite Sheet Creator")]
    public static void ShowWindow()
    {
        GetWindow<SpriteSheetCreatorWindow>("Sprite Sheet Creator");
    }

    private void OnGUI()
    {
        GUILayout.Label("Sprite Sheet Creator", EditorStyles.boldLabel);

        // Sprite size field
        sheetSize = EditorGUILayout.Vector2Field("Sprite Sheet Size (px)", sheetSize);

        // Save path
        savePath = EditorGUILayout.TextField("Save Path", savePath);

        // Scroll view for sprite list
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
        if (sprites != null)
        {
            for (int i = 0; i < sprites.Count; i++)
            {
                sprites[i] = (Sprite)EditorGUILayout.ObjectField("Sprite " + (i + 1), sprites[i], typeof(Sprite), false);
            }
        }
        EditorGUILayout.EndScrollView();

        // Add/Remove buttons for sprite list
        if (GUILayout.Button("Add Sprite"))
        {
            sprites.Add(null);
        }

        if (GUILayout.Button("Remove Last Sprite"))
        {
            if (sprites.Count > 0)
                sprites.RemoveAt(sprites.Count - 1);
        }

        // Button to make all textures readable
        if (GUILayout.Button("Make Textures Readable"))
        {
            MakeAllTexturesReadable();
        }

        // Generate sprite sheet button
        if (GUILayout.Button("Create Sprite Sheet"))
        {
            if (sprites.Count > 0 && sheetSize.x > 0 && sheetSize.y > 0 && !string.IsNullOrEmpty(savePath))
            {
                MakeAllTexturesReadable(); // Ensure textures are readable before creating the sprite sheet
                CreateTransparentTexture(); // Generate a transparent texture
                CreateSpriteSheet(sprites, sheetSize, savePath);
            }
            else
            {
                Debug.LogError("Ensure all fields are filled and sprites are added.");
            }
        }
    }

    // Function to create a 1x1 transparent texture
    private void CreateTransparentTexture()
    {
        if(transparentTexture!=null)
            return;
        transparentTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        transparentTexture.SetPixel(0, 0, new Color(0, 0, 0, 0)); // Fully transparent
        transparentTexture.Apply();
    }

    // Function to create sprite sheet
    private void CreateSpriteSheet(List<Sprite> sprites, Vector2 sheetSize, string savePath)
    {
        int spriteCount = sprites.Count;
        if (spriteCount == 0)
        {
            Debug.LogError("No sprites provided.");
            return;
        }

        // Calculate grid size (e.g., 3x3 for 9 sprites, 4x4 for 16 sprites)
        int gridSize = Mathf.CeilToInt(Mathf.Sqrt(spriteCount));

        // Calculate the size of each sprite in the sheet
        int spriteWidth = Mathf.RoundToInt(sheetSize.x / gridSize);
        int spriteHeight = Mathf.RoundToInt(sheetSize.y / gridSize);

        // Create a RenderTexture for the sprite sheet with alpha support
        RenderTexture renderTexture = new RenderTexture((int)sheetSize.x, (int)sheetSize.y, 0, RenderTextureFormat.ARGB32);
        RenderTexture.active = renderTexture;

        // Create a temporary texture to hold the final sprite sheet with alpha support
        Texture2D spriteSheet = new Texture2D((int)sheetSize.x, (int)sheetSize.y, TextureFormat.RGBA32, false);

        // Clear the render texture with transparency
        GL.Clear(true, true, Color.clear);

        // Prepare for blitting with alpha support
        Material blitMaterial = new Material(Shader.Find("Unlit/Transparent"));

        // Loop through the sprites and blit them to the sprite sheet
        for (int i = 0; i < spriteCount; i++)
        {
            // Get the current sprite
            Sprite sprite = sprites[i];

            // Calculate the position of the sprite in the sheet
            int row = i / gridSize;
            int col = i % gridSize;

            // Calculate the position and size for the current sprite
            Rect spriteRect = new Rect(col * spriteWidth, row * spriteHeight, spriteWidth, spriteHeight);
            // Get the texture of the sprite, including its alpha channel
            Texture2D spriteTexture = GetTextureFromSprite(sprite);

            // Blit the sprite to the render texture
            RenderTexture tempRT = RenderTexture.GetTemporary(spriteTexture.width, spriteTexture.height, 0, RenderTextureFormat.ARGB32);
            // Set the temporary RenderTexture as the target for drawing
            Graphics.SetRenderTarget(tempRT);

            // Clear the temporary RenderTexture to ensure no old data remains
            GL.Clear(true, true, Color.clear);
            
            Graphics.Blit(spriteTexture, tempRT, blitMaterial);

            // Copy the scaled sprite into the sheet
            Graphics.SetRenderTarget(renderTexture);
            
            // Clear the individual cell with the transparent texture
            GL.PushMatrix();
            GL.LoadPixelMatrix(0, renderTexture.width, renderTexture.height, 0);
            Graphics.DrawTexture(spriteRect, tempRT); // Clear the region with the 1x1 transparent texture
            GL.PopMatrix();

         
           GL.PushMatrix();
           GL.LoadPixelMatrix(0, renderTexture.width, renderTexture.height, 0);
           Graphics.DrawTexture(spriteRect, tempRT);
           GL.PopMatrix();

            RenderTexture.ReleaseTemporary(tempRT);
        }

        // Read pixels from the render texture and save them into the texture2D
        spriteSheet.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
        spriteSheet.Apply();

        // Save the sprite sheet as a PNG file
        byte[] bytes = spriteSheet.EncodeToPNG();
        File.WriteAllBytes(savePath, bytes);
        Debug.Log("Sprite sheet saved to: " + savePath);

        // Clean up
        RenderTexture.active = null;
        DestroyImmediate(renderTexture);
    }

    // Function to extract texture from sprite, including the alpha channel
    private Texture2D GetTextureFromSprite(Sprite sprite)
    {
        // Create a new texture with the same dimensions as the sprite
        Texture2D newTexture = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height, TextureFormat.RGBA32, false);

        // Copy pixels from the sprite's texture, including the alpha channel
        Color[] pixels = sprite.texture.GetPixels((int)sprite.rect.x, (int)sprite.rect.y, (int)sprite.rect.width, (int)sprite.rect.height);
        newTexture.SetPixels(pixels);
        newTexture.Apply();

        return newTexture;
    }

    // Function to make all sprite textures readable
    private void MakeAllTexturesReadable()
    {
        foreach (var sprite in sprites)
        {
            if (sprite != null)
            {
                string path = AssetDatabase.GetAssetPath(sprite.texture);
                TextureImporter textureImporter = (TextureImporter)AssetImporter.GetAtPath(path);

                if (textureImporter != null && !textureImporter.isReadable)
                {
                    textureImporter.isReadable = true;
                    AssetDatabase.ImportAsset(path);
                    Debug.Log("Set texture as readable: " + path);
                }
            }
        }
        Debug.Log("All textures are now readable.");
    }
}
