// The MIT License (MIT) - https://gist.github.com/bcatcho/1926794b7fd6491159e7
// Copyright (c) 2015 Brandon Catcho
using UnityEngine;
using UnityEditor;
using System.Reflection;

// Place this file in any folder that is or is a descendant of a folder named "Editor"
using System.Collections.Generic;
using System.Linq;
using System.Collections;


namespace ToivonRandomUtils
{
	[CanEditMultipleObjects] // Don't ruin everyone's day
	[CustomEditor (typeof(MonoBehaviour), true)] // Target all MonoBehaviours and descendants
   	public class MonoBehaviourCustomEditor : UnityEditor.Editor
	{

		void OnEnable(){
            try
            {
                foreach (var item in targets)
                {
                    var onValidateSafe = (item as IOnValidateSafe);
                    if (onValidateSafe != null) onValidateSafe.OnValidateSafe();
                }
            }
            catch (System.Exception ex)
            {
	            Debug.LogException(ex);
                Debug.LogError("MonoBehaviourCustomEditor OnValidateSafe crash\n"+ex);
            }
			
		}

		public override void OnInspectorGUI ()
		{
			InspectorButtonGUI (this);
		}

//		void Update() {
//			if(ShouldConstantlyRepaint(target))Repaint();
//		}

		public override bool RequiresConstantRepaint ()
		{
			if(base.RequiresConstantRepaint ())return true;

			if(target.GetType().ToString() == "ToTMonoBehavioursManager")return true;

			return false;
		}

        //		public static string ByteLenghtToHumanReadableTEMPCOPYPASTEFROMMAXRANDOMUTILS(long byteLenght, bool useBits = false) {
        //			string suffix = "";
        //			long lenght;
        //			if (useBits) lenght = byteLenght * 8;
        //			else lenght = byteLenght;
        //
        //			if (lenght < 1024) {
        //				if (useBits) suffix = " bits";
        //				else suffix = " B";
        //				return lenght + suffix;
        //			}
        //			else if (lenght < 1048576) {
        //				if (useBits) suffix = " kbits";
        //				else suffix = " kB";
        //				return (lenght / 1024) + suffix;
        //			}
        //			else {
        //				if (useBits) suffix = " Mbits";
        //				else suffix = " MB";
        //				return System.Math.Round(((float)lenght / (float)1048576), 2) + suffix;
        //			}
        //		}

        static System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        public static void InspectorButtonGUI (Editor targetEditor, bool drawDefaultInspector = true)
        {
            try // it seems some unhandled exceptions can crash unity
            {
                sw.Reset();
				sw.Start();
                m_InspectorButtonGUI(targetEditor,drawDefaultInspector);
                if(sw.ElapsedMilliseconds> 25)
                {
                    Debug.LogWarning($"Editor is slow for {targetEditor.target}: {sw.ElapsedMilliseconds}", targetEditor.target);
                }
            }
            catch (System.Exception ex)
            {
                Debug.LogException(ex);
            }
        }
		static void m_InspectorButtonGUI(Editor targetEditor, bool drawDefaultInspector = true)
		{
//			var sw = System.Diagnostics.Stopwatch.StartNew ();
//			var ogAlloc = UnityEngine.Profiling.Profiler.GetTotalAllocatedMemoryLong ();
//			Debug.Log (string.Format ("memory alloc {0} START", ByteLenghtToHumanReadableTEMPCOPYPASTEFROMMAXRANDOMUTILS (ogAlloc)));
			EditorGUI.BeginChangeCheck ();
			if (drawDefaultInspector) {
				targetEditor.DrawDefaultInspector ();
			}

			if (targetEditor.target is IOnInspectorGUI iOnInspectorGUIImplementor) {
				foreach (var item in targetEditor.targets) {
					iOnInspectorGUIImplementor.OnInspectorGUI ();
				}
			}
			UpdateDragReceivers(targetEditor);

			var target = targetEditor.target;
			// Draw the normal inspector
			// Currently this will only work in the Play mode. You'll see why
			// Get the type descriptor for the MonoBehaviour we are drawing
			var type = target.GetType ();

			// Iterate over each private or public instance method (no static methods atm)
			var bindingFlags = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;
			foreach (var method in type.GetMethods (bindingFlags)) {
				// make sure it is decorated by our custom attribute

				//var attributes = method.GetCustomAttributes (typeof(InspectorButton), true);
				//if (attributes.Length > 0) {

				//var hasInspecturButtonAttribute = GetMemberAttributes(method).Any(x => x is typeof(InspectorButton) );
				//if(hasInspecturButtonAttribute) {

				var inspectorButtonAttributes = GetMemberAttributes(method).Where(x => x is InspectorButton );
				if (inspectorButtonAttributes.Any()) {

					var btn = inspectorButtonAttributes.First() as InspectorButton;
					//if (btn.spaceBefore > 0) GUILayout.Button ("------");
					if (btn.spaceBefore > 0) GUILayout.Space (btn.spaceBefore);

					if (GUILayout.Button (method.Name)) {
						foreach (var item in targetEditor.targets) {
							if (item != null) {
								Undo.RecordObject (item, "Inspector button press");
								method.Invoke (item, null);
							} else {
								Debug.Log ("null Object at inspector button target. sometimes multiediting this might occur. don't know what this causes so i just leave this debuglog here");
							}
						}
					}
				}
			}
			foreach (var field in type.GetFields(bindingFlags)) {
				//var fieldAttrs = field.GetCustomAttributes (typeof(DrawnNonSerialized), true);
				var fieldAttrs = GetMemberAttributes (field).OfType<DrawnNonSerialized> ();
				DrawDrawnNonSerialized (targetEditor, field.Name, field.GetValue, fieldAttrs.ToArray(), field.FieldType);
			}
			foreach (var propInfo in type.GetProperties(bindingFlags)) {
				//var fieldAttrs = propInfo.GetCustomAttributes (typeof(DrawnNonSerialized), true);
				var propAttrs = GetMemberAttributes (propInfo).OfType<DrawnNonSerialized> ();
				DrawDrawnNonSerialized (targetEditor, propInfo.Name,x=>propInfo.GetValue(x,null), propAttrs.ToArray(),propInfo.PropertyType);
			}
			var target2 = targetEditor.target;
			if (EditorGUI.EndChangeCheck () && target2 as IOnValidateSafe != null) {
				foreach (var item in targetEditor.targets) {
                    try
                    {
					    (item as IOnValidateSafe).OnValidateSafe ();

                    }
                    catch (System.Exception ex)
                    {
                        Debug.LogException(ex,item);
                    }
				}
			}
//			var allocMemory = UnityEngine.Profiling.Profiler.GetTotalAllocatedMemoryLong ();
//			var secondConversionMul = 1000 / sw.ElapsedMilliseconds;
//			Debug.Log (string.Format (secondConversionMul+"memory alloc increase {0} per second", ByteLenghtToHumanReadableTEMPCOPYPASTEFROMMAXRANDOMUTILS (((allocMemory - ogAlloc) * secondConversionMul)) ) );
					
		}

		private static void UpdateDragReceivers(Editor targetEditor)
		{

			Event currentEvent = Event.current;
			EventType currentEventType = currentEvent.type;


			if (targetEditor.target is IOnInspectorGUIDragReceiver iOnInspectorGUIImplementor) {
				foreach (var item in targetEditor.targets) {
					switch (currentEventType)
					{
						case EventType.MouseDown:
							// drag started here - not implemente
							break;
						case EventType.MouseDrag:
							// If drag was started here:
							break;
						case EventType.Layout:
						case EventType.Repaint: 
							EditorGUILayout.HelpBox(iOnInspectorGUIImplementor.GetDragMessage(), MessageType.Info);
							break;
						case EventType.DragUpdated:
							if (iOnInspectorGUIImplementor.ContinueInspectorDrag(DragAndDrop.objectReferences))
							{
								DragAndDrop.visualMode = DragAndDropVisualMode.Link;
							}
							else
							{
								DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;
							}
							break;
						case EventType.DragPerform:
							DragAndDrop.AcceptDrag();
							iOnInspectorGUIImplementor.InspectorDragPerformed(DragAndDrop.objectReferences); 
							currentEvent.Use();
							break;
						case EventType.MouseUp:
							break;
					}
				}
			}

		
		}

		static Dictionary<MemberInfo,object[]> MmmberAttributesCached = new Dictionary<MemberInfo, object[]>();

		static object[] GetMemberAttributes (MemberInfo memberInfo)
		{
			if (!MmmberAttributesCached.ContainsKey (memberInfo)) {
				var attrs = memberInfo.GetCustomAttributes (true);
				MmmberAttributesCached.Add (memberInfo, attrs);
			}

			return MmmberAttributesCached [memberInfo];
		}

		static void DrawDrawnNonSerialized (Editor targetEditor, string fieldName, System.Func<object,object> getVal, object[] fieldAttrs, System.Type type)
		{
            if (fieldAttrs.Length > 0) {
                UnityEditor.EditorGUI.BeginDisabledGroup(true);
				foreach (var item in targetEditor.targets) {
					var fieldVal = getVal (item);
					var isIList = type.GetInterfaces ().Contains (typeof(IList)) && fieldVal != null;
					string extra = "";
					if (isIList) {
						extra = "size: " + (fieldVal as IList).Count;
					}
                    //GUILayout.TextField (string.Format ("{0}: {1} {2}", fieldName, fieldVal, extra));
                    UnityEditor.EditorGUILayout.TextField (fieldName, fieldVal + " " + extra);
                }
                UnityEditor.EditorGUI.EndDisabledGroup();
            }

        }

	}
}