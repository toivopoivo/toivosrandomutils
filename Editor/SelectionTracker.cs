﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using ToivonRandomUtils;
using System.Linq;

[UnityEditor.InitializeOnLoad]
public static class SelectionTracker
{
	
	public static List<Object[]> previousSelections = new List<Object[]> ();
	public static List<Object[]> nextSelections = new List<Object[]> ();
	public static Object[] currentSelection;

	static SelectionTracker ()
	{
//		AssetDatabase.LoadAssetAtPath ("Assets/selectionTrackerData.asset");
		Selection.selectionChanged += OnSelectionChange; 
	}

	static void DebugSpam ()
	{
		
		/*	var active = previousSelectionsAndCurrent.FirstOrDefault ();
		foreach (var item in previousSelectionsAndCurrent) {
			Debug.Log (active == item ? item.First ().name + " <Active" : item.First ().name);
		}*/
	}

	static void OnSelectionChange ()
	{
//		Debug.Log ("dont add: "+dontAdd);
		if (dontAdd) {
			dontAdd = false;
			return;
		}

		nextSelections.Clear ();
		if (currentSelection == null) {
			currentSelection = Selection.objects;
		} else {
			previousSelections.Add (currentSelection);
			currentSelection = Selection.objects;
		}

		if (previousSelections.Count > 20) previousSelections.RemoveAt (0);

	}

	static	bool dontAdd = false;

	[MenuItem ("Edit/Select Previous _%&Q")]
	public static void PreviousSelection ()
	{
		if (previousSelections.Count == 0) {
			Debug.Log ("is last selection");
			return;
		}
		dontAdd = true;
		var selection = previousSelections.Last ();
		nextSelections.Insert (0, currentSelection);
		previousSelections.Remove (selection);
		Selection.objects = currentSelection = selection;
		DebugSpam ();
	}

	[MenuItem ("Edit/Select Next _%&W")]
	public static void NextSelection ()
	{
		if (nextSelections.Count == 0) {
			Debug.Log ("is nextest selection");
			return;
		}
		dontAdd = true;
		var selection = nextSelections.First ();
		previousSelections.Add (currentSelection);
		Selection.objects = currentSelection = selection;
		nextSelections.Remove (selection);
		DebugSpam ();

	}

	[MenuItem ("Edit/Ping Select Next _%#&W")]
	public static void PingNextSelection ()
	{
	
		var selection = nextSelections.FirstOrDefault ().GetFromNonNullOrDefault (x => x.FirstOrDefault ());
		if (selection != null) {
			EditorGUIUtility.PingObject (selection);
		}

	}
	[MenuItem ("Edit/Ping Select Prev _%#&Q")]
	public static void PingPrevSelection ()
	{

		var selection = previousSelections.LastOrDefault ().GetFromNonNullOrDefault (x => x.LastOrDefault ());
		if (selection != null) {
			EditorGUIUtility.PingObject (selection);
		}

	}






}
