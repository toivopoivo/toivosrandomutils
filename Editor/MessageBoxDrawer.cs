using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using NUnit.Framework;
using ToivonRandomUtils;
using UnityEditor.Compilation;
using Assembly = System.Reflection.Assembly;

[CustomPropertyDrawer(typeof(HelpBox))]
public class HelpBoxDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        // toivo change 25.09.2020 why does this work perfectly? its a mystery
        //return  UnityEditor.EditorGUI.GetPropertyHeight(property, true) + GetRekt().height;
        // toivo change 23.10.2020 it only works perfectly if it is not a list item
        //return UnityEditor.EditorGUI.GetPropertyHeight(property, true) ;
        if (EditorGUI.indentLevel == 0) return UnityEditor.EditorGUI.GetPropertyHeight(property, true);
        else return UnityEditor.EditorGUI.GetPropertyHeight(property, true) + GetRekt().height;
    }
    static GUIStyle style;
    
    Rect GetRekt()
    {
        if (style == null)
        {
            style = GUI.skin.GetStyle("HelpBox");
            style.richText = true;


        }
        return GUILayoutUtility.GetRect(new GUIContent((attribute as HelpBox).message), style);
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        //		EditorGUILayout.GetControlRect (;
        var atrrs = (attribute as HelpBox);
        Rect goodSize = GetRekt();
        goodSize.y = position.y;
        goodSize.x += 15 * EditorGUI.indentLevel;
        goodSize.width -= 15 * EditorGUI.indentLevel;
        
        GUI.Box(goodSize, atrrs.message, style);
        position.y += goodSize.height;
        UnityEditor.EditorGUI.PropertyField(position, property, true);
    }


}

[CustomPropertyDrawer(typeof(CreateButton))]
public class CreateButtonDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUI.GetPropertyHeight(property, label, true);
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.objectReferenceValue != null)
        {
            EditorGUI.PropertyField(position, property, label);
            return;
        }
        position.width -= 20;
        EditorGUI.PropertyField(position, property, label);
        position.x += position.width;
        position.width = 20;
        if (GUI.Button(position, "+"))
        {
            if (property.serializedObject.targetObject is MonoBehaviour mb )
            {
                var go = new GameObject(fieldInfo.Name);
                UnityEditor.Undo.RegisterCreatedObjectUndo(go, "created "+fieldInfo.Name );
                UnityEditor.Undo.RecordObject(mb,"asd");
                if (fieldInfo.FieldType == typeof(GameObject))
                {
                    property.objectReferenceValue = go;
                }
                else
                {
                    property.objectReferenceValue = go.AddComponent(fieldInfo.FieldType);
                }
                go .gameObject.transform.SetParent(mb.transform);
                UnityEditor.PrefabUtility.RecordPrefabInstancePropertyModifications(mb);
            }
        }
    }
}

[CustomPropertyDrawer(typeof(ToivoToolTip))]
public class ToivoToolTipDrawer : PropertyDrawer, IPimpMyProperty
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return Mathf.Min(UnityEditor.EditorGUI.GetPropertyHeight(property, true), 20);
    }
   

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var ogGuiColor = GUI.color;
        ToivoToolTip toivoToolTip =(attribute as ToivoToolTip);
        
        PimpMyProperty(position,property,label, null);
        position.x += 20;
        position.width -= 20f;
        
        
        if (toivoToolTip.differentColorOnTheFieldWhyNot != null) GUI.color = ColorTypeUtility.ColorType2Color(toivoToolTip.differentColorOnTheFieldWhyNot.Value);
        UnityEditor.EditorGUI.PropertyField(position, property,label, true);     
        GUI.color = ogGuiColor;
    }



    public void PimpMyProperty(Rect position, SerializedProperty property, GUIContent label, BaseDrawingSettings baseDrawingSettings)
    {
        ToivoToolTip toivoToolTip =(attribute as ToivoToolTip);
        var msg = toivoToolTip.message;
        Rect tipBoxRect = new Rect(position.x, position.y, 20, position.height + 5f);

        var ogGuiColor = GUI.color;
        if (tipBoxRect.Contains(Event.current.mousePosition))
        {
            GUI.color = Color.yellow;
        }

        GUI.Box(tipBoxRect, new GUIContent("tip", msg));
        GUI.color = ogGuiColor;

     

    }
}

[CustomPropertyDrawer(typeof(GameObjectStringReference))]
public class GameObjectStringReferenceDrawer : PropertyDrawer
{
    private bool foldedOut;
    private List<GameObject> _gameObjects;
    private double lazy;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return 17f;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        
            DrawOne(position, property);
    }

    private void DrawOne(Rect position, SerializedProperty property)
    {
        position.width *= 0.5f;
        int index = 0;
        bool dirty = false;
        if (property.isArray)
        {
            var stringIndex = new System.Text.RegularExpressions.Regex(@"\[(\d+)\]").Match(property.propertyPath).Groups[1].Value;
            index = int.Parse( stringIndex);
            dirty = _gameObjects==null || _gameObjects.Count <= index ;
        }
        if (ToivonRandomUtils.RandomUtils.LazyEditorCheck(ref lazy) || dirty)
        {
            
            if (_gameObjects == null) _gameObjects = new List<GameObject>();
            if (property.isArray)
            {
             
                if (_gameObjects == null) _gameObjects = new List<GameObject>();
                if(_gameObjects.Count<=index) _gameObjects.Add(null);
                _gameObjects[index] = GameObject.Find(property.stringValue);
            }
            else
            {
                _gameObjects = new List<GameObject>{GameObject.Find(property.stringValue)};
            }
        }
        GameObject gameObject = _gameObjects[index];
        EditorGUI.BeginChangeCheck();
        gameObject = EditorGUI.ObjectField(position, gameObject, typeof(GameObject), true) as GameObject;
        if (EditorGUI.EndChangeCheck())
        {
            if (gameObject == null) property.stringValue = "";
            else property.stringValue = gameObject.name;
        }

        position.x += position.width;
        if(!property.isArray)property.stringValue = EditorGUI.TextField(position,  property.name,property.stringValue);
        else property.stringValue = EditorGUI.TextField(position,  property.stringValue);
    }
}

[CustomPropertyDrawer(typeof(DrawAllPropertyDrawers))]
public class DrawAllPropertyDrawersDrawer : PropertyDrawer
{

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return Mathf.Min(UnityEditor.EditorGUI.GetPropertyHeight(property, true), 20);
    }
    

    void Initialize()
    {
        Debug.Log("DrawAllPropertyDrawersDrawer.OnEnable - analysing all property drawers" );
        var sw = System.Diagnostics.Stopwatch.StartNew();
          if(attributeTypeToPropertyDrawer==null) attributeTypeToPropertyDrawer =  new Dictionary<Type, List<PropertyDrawer>>();
          if (propertyDrawersForType == null) propertyDrawersForType = new Dictionary<Type, List<Type>>();
          
        var allPropertyDrawers = CompilationPipeline.GetAssemblies().SelectMany( assembly => AppDomain.CurrentDomain.GetAssemblies().Single(x => x.GetName().Name == assembly.name).GetTypes().Where(x => x.IsSubclassOf(typeof(PropertyDrawer))));

        var typeField = typeof(CustomPropertyDrawer).GetField("m_Type", BindingFlags.Instance | BindingFlags.NonPublic);
        foreach (var type in allPropertyDrawers)
        {
            var customAttributes = type.GetCustomAttributes(typeof(CustomPropertyDrawer), false);
            foreach (var ca in customAttributes)
            {
                propertyDrawersForType.CreateNewListOrAddToOld(typeField.GetValue(ca) as System.Type, type);
            }
            
        }

         attributeField = typeof(PropertyDrawer).GetField("m_Attribute", BindingFlags.Instance | BindingFlags.NonPublic);
        Debug.Log("DrawAllPropertyDrawersDrawer.OnEnable - analysing all property drawers - took " + sw.ElapsedMilliseconds);
    }

    private static Dictionary<Type, List<Type>> propertyDrawersForType;

  private static Dictionary<System.Type, List<PropertyDrawer>> attributeTypeToPropertyDrawer;
  private static FieldInfo attributeField;

  public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (propertyDrawersForType == null)
        {
            Initialize();
        }
        var customAttrs = fieldInfo.GetCustomAttributes(true);


        List<KeyValuePair<object, PropertyDrawer>> attributeAndPropertyDrawers = new List<KeyValuePair<object, PropertyDrawer>>();
        

        
        
        foreach (var attr in customAttrs)
        {
            if(attr.GetType() == typeof(DrawAllPropertyDrawers)) continue;

            var type = attr.GetType();
            Assembly assembly = Assembly.GetAssembly(type);

           // if (!propertyDrawersInAssembly.TryGetValue(assembly, out var assemblysPropertyDrawers))
           // {
           //     assemblysPropertyDrawers = ;
           //     propertyDrawersInAssembly.Add(assembly,assemblysPropertyDrawers );
           // }
            
            

            if (!attributeTypeToPropertyDrawer.TryGetValue(type, out var propDrawers) || propDrawers == null  )
            {
                var drawerTypes = propertyDrawersForType[type];

                
                foreach (var drawerType in drawerTypes)
                {
                    
                    var attrFields = drawerType.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
                    var activated = Activator.CreateInstance(drawerType);
               //     attrFields.ForEach(x=>x.SetValue(activated,x.GetValue(attr)));
                    attributeTypeToPropertyDrawer.CreateNewListOrAddToOld(type,(PropertyDrawer) activated);
                }
                
            }
            foreach (var pd in propDrawers)
            {
                attributeAndPropertyDrawers.Add(new KeyValuePair<object, PropertyDrawer>(attr, pd));
            }
        }
        
     

        var pimpPos = position;
        pimpPos.width = 20;
        var baseDrawingSettings = new BaseDrawingSettings();
        foreach (var p in attributeAndPropertyDrawers)
        {
            var pimpMyProperty = p.Value as IPimpMyProperty;
            if (pimpMyProperty == null || p.GetType() == GetType()) continue;

            attributeField.SetValue(p.Value, p.Key);
            pimpMyProperty.PimpMyProperty(pimpPos, property, label, baseDrawingSettings);
            pimpPos.x += pimpPos.width + 1;
        }

        position.x += pimpPos.x;
        bool baseDrawerExists = false;

        if (baseDrawingSettings.disableField)
        {
            EditorGUI.BeginDisabledGroup(true);
        }
        
        foreach (var p in attributeAndPropertyDrawers)
        {
            if (p.Value.GetType() == GetType()) continue;

            var pimpMyProperty = p.Value as IPimpMyProperty;
            if (pimpMyProperty != null) continue;

            baseDrawerExists = true;
            attributeField.SetValue(p.Value, p.Key);
            p.Value.OnGUI(position, property, label);
        }

        if (!baseDrawerExists)
        {
            UnityEditor.EditorGUI.PropertyField(position,property, label);
        }

        if (baseDrawingSettings.disableField)
        {
            EditorGUI.EndDisabledGroup();
        }
    }

   
}

[CustomPropertyDrawer(typeof(EnumConditionalInspectorField))]
public class EnumConditionalInspectorFieldDrawer : PropertyDrawer
{
    private EnumConditionalInspectorField ecif;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
         if (GetIsVisible(property))
         {
            return  UnityEditor.EditorGUI.GetPropertyHeight(property, true);
         }
         else
         {
             return 0;
         }
    }

    
    private bool GetIsVisible(SerializedProperty property)
    {
        if(ecif==null) ecif= ((EnumConditionalInspectorField)attribute);
        var path = property.propertyPath.Split('.');
        object curOb = property.serializedObject.targetObject;

        IList array= null;
        for (int i = 0; i < path.Length-1; i++)
        {

            if (array != null)
            {
              //  parse "data[x]" format
               int index = int.Parse( path[i].Replace("data[", "").Replace("]", "") );
               curOb = array[index];
            }
            else
            {
                
                //parse is array   
                if (path[i] == "Array")
                {
                    array = curOb as IList;
                }
                else
                {
                    var curField = curOb.GetType().GetField(path[i]);
                    curOb = curField.GetValue(curOb);
                }
               
            }
        
        }

        var reflectedValue = (int)curOb.GetType().GetField(ecif.governingEnumFieldName, BindingFlags.Instance |BindingFlags.Public |BindingFlags.NonPublic).GetValue(curOb);
        return  ecif.requireValues.Contains(reflectedValue);
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (GetIsVisible(property))
        {
            UnityEditor.EditorGUI.PropertyField(position, property, label, true);
        }
    }

   
}
[CustomPropertyDrawer(typeof(DrawAsset))]
public class DrawAssetDrawer : PropertyDrawer
{

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return  UnityEditor.EditorGUI.GetPropertyHeight(property, true) + 100;
    }


    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        if (property.objectReferenceValue == null)
        {
            UnityEditor.EditorGUI.PropertyField(position, property, label, true);
            return;
        }

        var propHeight = UnityEditor.EditorGUI.GetPropertyHeight(property, true);
        position.height -= propHeight;

        var picture = AssetPreview.GetAssetPreview(property.objectReferenceValue);
        if (picture != null)
        {
            var picpos = position;
            picpos.width = 100 * (picture.height / (float) picture.width);
            UnityEditor.EditorGUI.DrawPreviewTexture(picpos, picture);
            position.height += propHeight;
            position.y = position.y + 83f;
        }

        UnityEditor.EditorGUI.PropertyField(position, property, label, true);
    }
}