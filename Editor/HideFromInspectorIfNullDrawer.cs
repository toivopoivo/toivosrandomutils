﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ToivonRandomUtils;
using UnityEditor;
[CustomPropertyDrawer(typeof(HideFromInspectorIf))]
public class HideFromInspectorIfNullDrawer : PropertyDrawer {

	public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
	{
		if (property.FindPropertyRelative ((attribute as HideFromInspectorIf).hidingFieldName).boolValue)
			return 0;
		return EditorGUI.GetPropertyHeight (property, label, true);
	}

	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
		if (property.FindPropertyRelative ((attribute as HideFromInspectorIf).hidingFieldName).boolValue)
			return;
		else
			EditorGUI.PropertyField (position, property, true);
	}
}
