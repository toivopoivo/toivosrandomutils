﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Reflection;
using UnityEditor;

/// <summary>
/// DRY! REFACTOR!
/// </summary>
/// 
[CustomPropertyDrawer (typeof(NonEditableField))]
public class NonEditableFieldDrawer : PropertyDrawer
{
	public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
	{

		var attr = attribute as NonEditableField;
		if (attr.height < 0) {
            //return base.GetPropertyHeight (property, label);
            return EditorGUI.GetPropertyHeight(property, label);
		} else {
			return attr.height;
		}
	}
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{
		NonEditableField attr = attribute as NonEditableField;
		string name = (attr.description != "NaN") ? attr.description : property.name;
		// lol
		System.Type type;
//		if (attr.type == null) {
//			type = property.serializedObject.targetObject.GetType ().GetField (property.name).FieldType;
//		} else {
		type = attr.type;
//		}

//		Debug.Log (property.stringValue);

		if (type.IsSubclassOf (typeof(UnityEngine.Object))) {
            EditorGUI.BeginDisabledGroup(true);
			UnityEditor.EditorGUI.ObjectField (position, property.displayName, property.objectReferenceValue, type, true);
            EditorGUI.EndDisabledGroup();
		} else {
			if (type.IsValueType || type == typeof(string)) {
				if (type == typeof(Vector3))
					LabelField (position, property, name, string.Format ("\tx {0} \ty {1} \tz {2}", property.vector3Value.x, property.vector3Value.y, property.vector3Value.z), attr);
				else if (type == typeof(Vector2))
					LabelField (position, property, name, string.Format ("\tx {0} \ty {1}", property.vector2Value.x, property.vector2Value.y), attr);
				else if (type == typeof(string) && attr.height > 0) {
					CreateNecessaryLabelFields (property.stringValue, new Rect (position.x, position.y, position.width, attr.height));
				} else if (type == typeof(string))
					LabelField (position, property, name, property.stringValue, attr);
				else if (type == typeof(float))
					LabelField (position, property, name, property.floatValue, attr);
				else if (type == typeof(int))
					LabelField (position, property, name, property.intValue, attr);
				else if (type == typeof(bool))
					LabelField (position, property, name, property.boolValue, attr);
				else if (type == typeof(Bounds))
					LabelField (position, property, name, property.boundsValue, attr);
				else if (type.IsEnum)
					LabelField (position, property, name, System.Enum.GetNames (attr.type) [property.enumValueIndex], attr);
			} else {
                //    Debug.LogError("NonEditableFieldDrawer - unable to display "+property.name +" properly");
                //LabelField (position, property, name, property.name, attr);
                EditorGUI.BeginDisabledGroup(true);
                //  UnityEditor.EditorGUI.ObjectField(position, property.displayName, property.objectReferenceValue, type, true);
                EditorGUI.PropertyField(position, property, label, true);
                EditorGUI.EndDisabledGroup();
            }
		}

	}
	void CreateNecessaryLabelFields(string str, Rect position){
		string[] splits = str.Split(new[]{"<N>"}, System.StringSplitOptions.RemoveEmptyEntries);
		float height = 0;
		foreach (var item in splits) {
			EditorGUI.LabelField (new Rect (position.x, position.y+height, position.width, 17f), item);
			height+=17f;
		}
	}

	static void LabelField (Rect position, SerializedProperty property, string name, object data, NonEditableField nonEditableField)
	{
        float height = nonEditableField.height < 0 ? 17f : nonEditableField.height;
        EditorGUI.BeginDisabledGroup(true);
        bool displayResetButton = false;
        if (nonEditableField.resetValue != null)
        {
	        displayResetButton = !System.Object.Equals(data, nonEditableField.resetValue);
	        if(displayResetButton) position.width *= 0.5F;
        }
        EditorGUI.TextField(new Rect(position.x, position.y, position.width, height), name, data.ToString());
       
        EditorGUI.EndDisabledGroup();
        
        if (nonEditableField.resetValue != null && displayResetButton)
        {
	        position.x += position.width;
	        if (GUI.Button(position, "Reset"))
	        {
		        UnityEditor.Undo.RecordObject(property.serializedObject.targetObject,"reset");
		        var serializedObjectTargetObject = property.serializedObject.targetObject;
		        var field = serializedObjectTargetObject.GetType().GetFields(BindingFlags.Instance| BindingFlags.Public |BindingFlags.NonPublic).Single(x=>x.Name == property.name);
		        field.SetValue(serializedObjectTargetObject, nonEditableField.resetValue);
		        property.serializedObject.Update();
		        UnityEditor.PrefabUtility.RecordPrefabInstancePropertyModifications(property.serializedObject.targetObject);

	        }
        }
    }
}

