﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class BestOfBestestAlgoTest
{


#if UNITY_EDITOR
    [UnityEditor.MenuItem("toivos best windows/experimental/test  best of bestest")]
#endif
    public static void Test()
    {
        var restaurants = new List<Restaurant>();
        restaurants.Add(new Restaurant { name = "kebab", distanceMeters = 1000, priceEuro = 5 });
        restaurants.Add(new Restaurant { name = "subway", distanceMeters = 2000, priceEuro = 10 });
        restaurants.Add(new Restaurant { name = "fancy ala carte place", distanceMeters = 500, priceEuro = 25 });
        restaurants.Add(new Restaurant { name = "mcdonalds", distanceMeters = 500, priceEuro = 7 });

        ToivoLinq.ToivoLinq.BestOfBestest(restaurants, x => x.distanceMeters, x => x.priceEuro);
        Debug.Log("distance first: "+restaurants.Aggregate("",(x,y)=>y.name+ " ," +x));


        ToivoLinq.ToivoLinq.BestOfBestest(restaurants, x => x.priceEuro, x => x.distanceMeters);
        Debug.Log("price first:" + restaurants.Aggregate("", (x, y) => y.name + " ," + x));
    }

    [System.Serializable]
    public class Restaurant
    {
        public string name;
        public int distanceMeters;
        public int priceEuro;
    }


}

