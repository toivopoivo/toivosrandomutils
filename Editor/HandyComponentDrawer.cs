﻿using UnityEngine;
using System.Collections;
using UnityEditor;
[CustomPropertyDrawer(typeof(HandyComponentField))]
public class HandyComponentDrawer : PropertyDrawer {

	public override float GetPropertyHeight (SerializedProperty property, GUIContent label)
	{
		if (property.FindPropertyRelative ("enabled").boolValue)
			return 17f;
		else
			return 1;
	}

	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
	{

		if (!property.FindPropertyRelative ("enabled").boolValue)
			return;
		
		var compProp = property.FindPropertyRelative ("component");
		SerializedProperty gameObjProp = property.FindPropertyRelative ("gameObj");
		if (compProp.objectReferenceValue!=null) {
			var ogComp = compProp.objectReferenceValue as Component;
			EditorGUI.PropertyField (new Rect(position.x,position.y,position.width,17f), compProp);
			var newComp = (compProp.objectReferenceValue as Component);
			if (ogComp != null && newComp != null && newComp.gameObject != gameObjProp.objectReferenceValue) {
				Debug.Log ("you swapped the components! you sneak! trying to getcomponent");
				compProp.objectReferenceValue = newComp.gameObject.GetComponent (ogComp.GetType ());
				gameObjProp.objectReferenceValue = newComp.gameObject;
			}
			return;
		}

		EditorGUI.PropertyField (new Rect(5,position.y,100,17f) ,gameObjProp, GUIContent.none);
		if (gameObjProp.objectReferenceValue != null) {
			var cast = gameObjProp.objectReferenceValue as GameObject;
			if (cast!=null) {
				var getComps = cast.GetComponents<Component>();
				string[] names = new string[getComps.Length];
				for (int i = 0; i < getComps.Length; i++) {
					names [i] = getComps[i].GetType ().Name;
				}
				var index=EditorGUI.Popup (new Rect (105, position.y, 100, 20), -1, names);
				if (index != -1) {
					compProp.objectReferenceValue = getComps[index];
				}

			}
		}
	}
}
