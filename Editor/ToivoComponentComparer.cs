﻿using NUnit.Framework.Internal;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.TestTools;

public class ToivoComponentComparer : EditorWindow
{
    private SerializedObject serializedObject;
    private SerializedProperty comparerSettingsProp;
    public ComparerSettings comparerSettings = new ComparerSettings();
    private void OnEnable()
    {
        serializedObject = new SerializedObject(this);
        comparerSettingsProp = serializedObject.FindProperty("comparerSettings");
    }
    [MenuItem("toivos best windows/toivo component comparer")]
    public static void Init()
    {
        EditorWindow.GetWindow<ToivoComponentComparer>().Show();
    }
    
    public void OnGUI()
    {
        EditorGUILayout.HelpBox("compare and edit same types of components and order them by a specific field", MessageType.Info);

        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(comparerSettingsProp);
        if (EditorGUI.EndChangeCheck())
        {
            serializedObject.ApplyModifiedProperties();
            serializedObject.Update();
        }

        if (comparerSettings.target == null) return;
        if (comparerSettings.reflectedType == null) return;

        System.Type type = comparerSettings.reflectedType.GetType();
        var reflectedField = type.GetFields().SingleOrDefault(x => x.Name == comparerSettings.reflectedField);
        if(reflectedField == null)
        {
             EditorGUILayout.HelpBox($"no field <{comparerSettings.reflectedField}> found on type {comparerSettings.reflectedType.GetType().Name}", MessageType.Info);
        }else
        {
            var instances = comparerSettings.target.GetComponentsInChildren(type);
            if(reflectedField.FieldType == typeof(int))
            {
               EditorGUILayout.HelpBox($"note manipulation not yetimplemented ", MessageType.Info);
                var vals = instances.Select(x=>new {comp = x , val = (int)reflectedField.GetValue(x) });

                if (comparerSettings.ordering == Ordering.Descending) vals = vals.OrderBy(x => x.val);

                foreach (var item in vals)
                {
                    EditorGUILayout.IntField(item.comp.gameObject.name,item.val);
                }
            }else
            {
                throw new NotImplementedException();
            }
        }

    }
    public enum Ordering
    {
        NoOrdering,
        Descending
    }

    [System.Serializable]
    public class ComparerSettings
    {
        public GameObject target;
        public Component reflectedType;
        public string reflectedField;
        public Ordering ordering;
    }
}
