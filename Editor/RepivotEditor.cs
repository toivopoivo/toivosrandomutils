﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ToivonRandomUtils;
using UnityEditor;
using UnityEngine;

public class RepivotEditor : EditorWindow
{
    

    [UnityEditor.MenuItem("toivos best windows/Repivot Tool")]
    public static void Init()
    {
        EditorWindow.GetWindow<RepivotEditor>().Show();
    }

    /*
     holy shit all of this handles shit sucks
     
    private void OnEnable()
    {
        SceneView.duringSceneGui += Draw;

    }

    private void Draw(SceneView obj)
    {
        if (Event.current.type != EventType.Repaint)
        {
            return;
        }
        Debug.Log("hello " + Event.current.type);
       Handles.BeginGUI();
      float gizmoRadius = 0.1f;
      
       Handles.SphereHandleCap(GUIUtility.GetControlID(GetHashCode(), FocusType.Passive) , position, Quaternion.identity, gizmoRadius, EventType.Repaint );

     // Handles.Button(position, Quaternion.identity, 1f, 1f, (id, vector3, rotation, size) => { });
       Handles.EndGUI();
      // HandleUtility.Repaint();
       obj.Repaint();
   //   if (EventType.Layout == Event.current.type)
   // {
   //     HandleUtility.AddDefaultControl(GUIUtility.GetControlID(GetHashCode(), FocusType.Passive) );
   // }
    }

    private void OnDisable()
    {
        SceneView.duringSceneGui -= Draw;
    }
*/

    private void OnEnable()
    {
        if (newPivot == null)
        {
            newPivot = new GameObject("new pivot").transform;
            newPivot.hideFlags = HideFlags.DontSave;
            UnityEditor.EditorGUIUtility.PingObject(newPivot.gameObject);
        }
    }


    private Transform newPivot;
    private void OnGUI()
    {
        EditorGUILayout.HelpBox("sets scale to 1", MessageType.Info);
       
        if (GUILayout.Button("new pivot"))
        {
            
            UnityEditor.Selection.activeGameObject = newPivot.gameObject;
        }

        if (GUILayout.Button("collider bounds middle"))
        {
            var cols = UnityEditor.Selection.activeGameObject.GetComponentsInChildren<Collider>();
            var bnouds = cols[0].bounds;
            for (int i = 1; i < cols.Length; i++)
            {
                bnouds.Encapsulate(cols[0].bounds);
            }

            newPivot.transform.position = bnouds.center;
        }

        if (GUILayout.Button("move to current"))
        {
            newPivot.transform.position = UnityEditor.Selection.activeGameObject.transform.position;
        }

        if (GUILayout.Button("rotate to current"))
        {
            newPivot.transform.rotation = UnityEditor.Selection.activeGameObject.transform.rotation;
        }

        if (GUILayout.Button("Repivot"))
        {
            var repivoted = Selection.activeGameObject.transform;
            UnityEditor.Undo.RecordObject(repivoted,"adjusted");

            var children = Enumerable.Range(0, repivoted.childCount).Select(x => repivoted.transform.GetChild(x)).ToList();
            

            foreach (var chil in children)
            {
                UnityEditor.Undo.RecordObject(chil.transform,"adjusted");
                chil.transform.SetParent(newPivot, true);
            }


            repivoted.transform.position = newPivot.transform.position;
            repivoted.transform.rotation = newPivot.transform.rotation;
            repivoted.transform.localScale = Vector3.one ;

            
            
            foreach (var chil in children)
            {
                chil.transform.SetParent(repivoted, true);
            }

        }
    }
}
