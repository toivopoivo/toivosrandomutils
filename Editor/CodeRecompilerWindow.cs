﻿using UnityEditor;
using UnityEngine;

namespace ToivonRandomUtils
{
    public class CodeRecompilerWindow : EditorWindow
    {
        public static bool assemblyPossiblyDirty;
        [MenuItem("toivos best windows/CodeRecompilerWindow")]
        public static void Init()
        {
             EditorWindow.GetWindow<CodeRecompilerWindow>().Show();
        }
  //     private void OnEnable()
  //     {
  //     }

         private void OnDisable()
         {
             EditorApplication.UnlockReloadAssemblies();
         }
        bool constantUpdate;
        bool locked= false;
        private void Update()
        {
            if(constantUpdate) Repaint();
        }
        private void OnGUI()
        {
            throw new System.NotSupportedException("VITT UEI TOIMI");
            EditorApplication.LockReloadAssemblies();

         
            if (assemblyPossiblyDirty)
            {
                constantUpdate = true;
                GUI.color = Color.Lerp(Color.white, Color.yellow, Mathf.Sin((float)EditorApplication.timeSinceStartup * 10f));
            }

            if(GUILayout.Button("UNLOCK RELOAD ASSEMBLIES", GUILayout.Height(200f)))
            {
               Debug.Log("asd testi");
                EditorApplication.UnlockReloadAssemblies();
         //       AssetDatabase.Refresh();
                assemblyPossiblyDirty = false;
            }
        }

    }
}
