﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ToivonRandomUtils;
namespace ToivoDebug
{
    [CreateAssetMenu]
    public class PoorMansDebugger : ScriptableObject
    {
        public List<List<PoorMansDebuggerOp>> operations;

        [System.NonSerialized]
        public int collectedFramesCount = 0;

        [System.NonSerialized]
        public float openedStamp = -999f;
        public string fileName;

        [System.NonSerialized]
        public int frameIndex;
        public bool showEmptyFrames;

        public void Open()
        {
            if (collectedFramesCount == 0) return;


            System.Diagnostics.StackFrame[] frames;
            int index;
            fileName = GetLastFrame(out frames, out index).GetFileName();
            openedStamp =  Time.time;
            frameIndex++;
            if (operations == null)
            {
                operations = new List<List<PoorMansDebuggerOp>>();
                operations.Add(new List<PoorMansDebuggerOp>());
            }

            if (frameIndex == collectedFramesCount)
            {
                frameIndex = 0;
            }

            if (operations.Count == frameIndex)
            {
                operations.Add(new List<PoorMansDebuggerOp>());
            }else
            {
                try
                {
                    operations[frameIndex].Clear();
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
            }


        }
        private System.Diagnostics.StackFrame GetLastFrame(out System.Diagnostics.StackFrame[] frames, out int index)
        {
            System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace(true);
            frames = stackTrace.GetFrames();
            index = -1;
            for (int i = 0; i < frames.Length; i++)
            {
                if (!frames[i].GetFileName().Contains("PoorMansDebugger"))
                {
                    index = i;
                    return frames[i];
                }
            }
            throw new System.NotSupportedException();
        }
        public bool allFrames = false;
        public bool Eval(bool value)
        {
            if (collectedFramesCount == 0) return value;

            CreateOp("Evaluated ", value, value ? Color.green : Color.red);
           
            
            return value;
        }

        private void CreateOp(string op, object value, Color color)
        {
            System.Diagnostics.StackFrame[] frames;
            int index;
            if (allFrames)
            {
                GetLastFrame(out frames, out index);
                for (int i = frames.Length - 1; i >= index + 1; i--)
                {
                    RlyCreateOp("stacktrace", "", frames[i].GetFileLineNumber(), Color.gray);
                }
                RlyCreateOp(op,value,frames[index].GetFileLineNumber(),color);
            }
            else
            {
                RlyCreateOp(op, value, GetLastFrame(out frames, out index).GetFileLineNumber(), color);
            }
        }

        private void RlyCreateOp(string op, object value, int lineNumber, Color color)
        {
            PoorMansDebuggerOp item = new PoorMansDebuggerOp(
                                op,
                                value,
                                lineNumber - 1
                            );
            item.color = color;
            operations[frameIndex].Add(item);
        }

        public T Set<T>(T value)
        {
            if (collectedFramesCount == 0) return value;
            CreateOp("Set to", value,Color.white);
            return value;
        }



        public class PoorMansDebuggerOp
        {
            public string opName;
            public object value;
            public int lineIndex;
            public Color color = Color.white;
            public PoorMansDebuggerOp(string opName, object value, int lineIndex)
            {
                this.opName = opName;
                this.value = value;
                this.lineIndex = lineIndex;
            }
        }
    }

}