﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class HandyComponentField {
	public GameObject gameObj;
	public Component component;
	public bool enabled = true;


	public HandyComponentField Clone(){
		return MemberwiseClone () as HandyComponentField;
	}
}
