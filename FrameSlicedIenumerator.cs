﻿using System.Collections;

namespace ToivonRandomUtils
{
    public class FrameSlicedIenumerator
    {
        public System.Func<IEnumerator> restartFunc;
        public IEnumerator current;

        public void Step()
        {
            if (current == null) current = restartFunc();
            if(!current.MoveNext())
            {
                current = null;
            }
        }

        public FrameSlicedIenumerator(System.Func<IEnumerator> restartFunc)
        {
            this.restartFunc = restartFunc;
        }
    }
}