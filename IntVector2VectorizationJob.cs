﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ToivonRandomUtils
{
    /// <summary>
    /// DouglasPeucker
    /// </summary>
    public class IntVector2VectorizationJob
    {
        public List<IntVector2> keptPoints = new List<IntVector2>();
        public List<ExecParams> subJobs = new List<ExecParams>();

        public float strength;

        ExecParams currentJob;

        public IEnumerable<IntVector2> CurrentSubCurve
        {
            get
            {
                if (currentJob==null) return null;
                var asdf = currentJob.GetSubCurve();
                List<IntVector2> temp = new List<IntVector2>();
                while (asdf.MoveNext())
                {
                    temp.Add(asdf.Current);
                }
                return temp;
            }
        }
        public IEnumerator Executing()
        {
            while (subJobs.Count > 0)
            {
                ExecParams lastJob = subJobs.Last();
                var ienum = VectorizeRecurse(currentJob = lastJob);
                while (ienum.MoveNext())
                {
                    yield return null;
                }
                subJobs.Remove(lastJob);
                yield return null;
            }
        }
        public List<IntVector2> wholeCruve;

        public IntVector2VectorizationJob(List<IntVector2> curve, float strength)
        {
            this.strength = strength;
            this.wholeCruve = curve;
            keptPoints.Add(curve[0]);
            int end = curve.Count-1;
            keptPoints.Add(curve[end]);
            subJobs = new List<ExecParams>();
            subJobs.Add(new ExecParams(0, end,wholeCruve));
        }
        /*these fields are only for debugging visualization*/
        public float currentDMax;
        public float lastDistance;
        public int subJobCount;
        public int currentIndex;
        public int strongestPointIndex;
        IEnumerator VectorizeRecurse(ExecParams execParams)
        {
            subJobCount++;
            // Find the point with the maximum distance
            currentDMax = 0;

//            for (int i = 2; i < end; i++) didn't work, but why there was i = 2 in the original algo? testing wo
            for (int i = execParams.startingIndex; i < execParams.end; i++)
            {
                currentIndex = i;
                Vector2 closest;
                lastDistance = PerpedicularDistance(wholeCruve[i], wholeCruve[execParams.startingIndex], wholeCruve[execParams.end - 1], out closest);
                if (lastDistance > currentDMax)
                {
                    strongestPointIndex = i;
                    currentDMax = lastDistance;
                }
                yield return null;
            }

            // If max distance is greater than strenght then recursively simplify
            if (currentDMax > strength)
            {
                keptPoints.Add(wholeCruve[strongestPointIndex]);
                subJobs.Add(new ExecParams(execParams.startingIndex,strongestPointIndex,wholeCruve));
                subJobs.Add(new ExecParams(strongestPointIndex,execParams.end,wholeCruve));
            }

        }






        /// <summary>
        ///  Calculate the distance between
        // point pt and the line p1 --> p2.
        /// </summary>
        private static float PerpedicularDistance(IntVector2 pt, IntVector2 p1, IntVector2 p2, out Vector2 closest)
        {
            float dx = (float)p2.x - p1.x;
            float dy = (float)p2.y - p1.y;
            if ((dx == 0) && (dy == 0))
            {
                // It's a point not a line segment.
                closest = (Vector2)p1;
                dx = pt.x - p1.x;
                dy = pt.y - p1.y;
                return (float)System.Math.Sqrt(dx * dx + dy * dy);
            }

            // Calculate the t that minimizes the distance.
            float t = ((pt.x - p1.x) * dx + (pt.y - p1.y) * dy) /
                (dx * dx + dy * dy);

            // See if this represents one of the segment's
            // end points or a point in the middle.
            if (t < 0)
            {
                closest = new Vector2(p1.x, p1.y);
                dx = pt.x - p1.x;
                dy = pt.y - p1.y;
            }
            else if (t > 1)
            {
                closest = new Vector2(p2.x, p2.y);
                dx = pt.x - p2.x;
                dy = pt.y - p2.y;
            }
            else
            {
                closest = new Vector2(p1.x + t * dx, p1.y + t * dy);
                dx = pt.x - closest.x;
                dy = pt.y - closest.y;
            }

            return (float)System.Math.Sqrt(dx * dx + dy * dy);
        }


        
        public class ExecParams
        {
            public int end;
            public int startingIndex;
            public List<IntVector2> wholeCurve;
            public ExecParams(int startingIndex, int end, List<IntVector2> wholeCurve)
            {
                this.wholeCurve = wholeCurve;
                this.startingIndex = startingIndex;
                this.end = end;
            }
            public IEnumerator<IntVector2> GetSubCurve()
            {
                for (int i = startingIndex; i < end; i++)
                {
                    IntVector2 intVector2 = default(IntVector2);
                    try
                    {
                        intVector2 = wholeCurve[i];
                    }
                    catch (System.Exception ec)
                    {
                        Debug.LogError("fuc " + i + " / " + end);
                        throw ec;
                    }
                    yield return intVector2;
                }
            }
        }
    }
}