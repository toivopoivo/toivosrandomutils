﻿using UnityEngine;
using System.Collections;


public class NonEditableField : PropertyAttribute
{
	public string description = "NaN";
	public System.Type type;
	public float height = -1;
	public object resetValue;
	public NonEditableField (System.Type type ,float height=-1f,string description = "NaN", object resetValue = null)
	{
		this.type = type;
		this.height = height;
		this.description = description;
		this.resetValue = resetValue;
	}

}
