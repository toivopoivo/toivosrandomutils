﻿//using UnityEngine;
//using UnityEngine.TestTools;
//using NUnit.Framework;
//using System.Collections;
//using ToivonRandomUtils;
//public class Int2PlaymodeTest {
//
//	
//	// A UnityTest behaves like a coroutine in PlayMode
//	// and allows you to yield null to skip a frame in EditMode
//
//	[UnityTest,Timeout(9999999)]  /*note make sensible timeout after fixed*/
//	public IEnumerator Int2PlaymodeTestWithEnumeratorPasses() {
//        LogAssert.ignoreFailingMessages = true;
//        var job = new Int2Vector2Utils.FloodPathFindJob(new IntVector2(50,50),x=>x ==new IntVector2(10,10),x=>!x.IsInsideOfRect(new IntVector2(), new IntVector2(100,100)),true,false);
//        var renderer = GameObject.CreatePrimitive(PrimitiveType.Plane).GetComponent<Renderer>();
//        Light light = new GameObject("dir light").AddComponent<Light>();
//        light.type = LightType.Directional;
//        light.transform.rotation = Quaternion.LookRotation(Vector3.down);
//        Camera camera = new GameObject("camera").AddComponent<Camera>();
//        camera.orthographic = true;
//        camera.transform.position = camera.transform.position.SameButDifferent(y: 10f);
//        camera.transform.rotation = Quaternion.LookRotation(Vector3.down);
//        Texture2D texture2D = new Texture2D(100, 100);
//        renderer.material.mainTexture = texture2D;
//        texture2D.SetPixel(50, 50, Color.black);
//        texture2D.SetPixel(10, 10, Color.magenta);
//        IEnumerator enumerator = job.Pathfinding();
//        while (enumerator.MoveNext())
//        {
//            var colors = job.VisualizeProgress();
//            while (colors.MoveNext())
//            {
//                texture2D.SetPixel(colors.Current.Key.x, colors.Current.Key.y, colors.Current.Value);
//            }
//
//
//            texture2D.Apply();
//            yield return null;
//        }
//        LogAssert.ignoreFailingMessages = false;
//        Assert.Pass("yay");
//	}
//
//}
//