﻿using UnityEngine;
using System.Collections;

public class InspectorDataSheet:PropertyAttribute
{
	public System.Type type;
	public InspectorDataSheet(System.Type type){
		this.type = type;
	}
}

public class InspectorDataCell:PropertyAttribute
{
	public bool showName;
	public int rowIndex;
	public InspectorDataCell (int rowIndex, bool showName = false)
	{
		this.rowIndex = rowIndex;
		this.showName = showName;
	}

}
