﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Example
{
	public class InspectorDataSheetExample : MonoBehaviour
	{
		[InspectorDataSheet (typeof(MyDataClass))]
		public List<MyDataClass> shitloadOfDataClass;

		/// <summary>
		/// inspector data sheel property should be added to the system.serializable class
		/// </summary>
		//		[InspectorDataSheet]
		//		public MyDataClass myDataClass;


		[System.Serializable]
		public class MyDataClass
		{

			public int dontCell1;

			// add inspector data cell to fields & rowindex to fields you want celled

			[InspectorDataCell (0)]
			public int plzCell1;
			[InspectorDataCell (0)]
			public int plzCell2;
			[InspectorDataCell (0)]
			public Vector3 plzCell3;
			[InspectorDataCell (0)]
			public int plzCell4;
			[InspectorDataCell (0)]
			public int plzCell5;


			[InspectorDataCell (1)]
			public int plzCell11;
			[InspectorDataCell (1)]
			public int plzCell21;
			[InspectorDataCell (1)]
			public int plzCell31;
			[InspectorDataCell (1)]
			public int plzCell41;
			[InspectorDataCell (1)]
			public int plzCell51;

			public int dontCell2;
			public int dontCell3;

			// there has to be float field named editorHeight because raptor jesus
			public float editorHeight;

		}
	}
}
