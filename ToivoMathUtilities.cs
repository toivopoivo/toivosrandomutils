﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ToivoMathUtilities 
{
    /*
    static Vector3D IntersectPoint(Vector3D rayVector, Vector3D rayPoint, Vector3D planeNormal, Vector3D planePoint)
    {
        var diff = rayPoint - planePoint;
        var prod1 = diff.Dot(planeNormal);
        var prod2 = rayVector.Dot(planeNormal);
        var prod3 = prod1 / prod2;
        return rayPoint - rayVector * prod3;
    }
    */
    public static Vector3 IntersectPoint(Vector3 rayVector, Vector3 rayPoint, Vector3 planeNormal, Vector3 planePoint )
    {
        var diff =  rayPoint - planePoint;
        var prod1 = Vector3.Dot(diff,planeNormal);
        var prod2 = Vector3.Dot( rayVector,planeNormal);
        var prod3 = prod1 / prod2;
        return rayPoint - rayVector * prod3;
    }
    



}
