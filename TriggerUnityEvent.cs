using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerUnityEvent : MonoBehaviour
{

	public UnityEvent turnOnEvent;
	public UnityEvent turnOffEvent;
	public bool state;
	public TriggerType triggerType;
	[Flags]
	public enum TriggerType
	{
		None = 0,
		KeyUp = 1,
	}
	public KeyCode keyCode;

	private void Update()
	{
		if ((triggerType & TriggerType.KeyUp) != 0 && Input.GetKeyUp(keyCode))
		{
			Trigger();
		}
		
	}

	void Trigger()
	{
		if (!state)
		{
			turnOnEvent.Invoke();
			Debug.Log("TriggerUnityEvent.Trigger "+ turnOnEvent.GetPersistentMethodName(0) + " t: " + Time.time);
		}
		else
		{
			turnOffEvent.Invoke();
			Debug.Log("TriggerUnityEvent.Trigger "+ turnOffEvent.GetPersistentMethodName(0) + " t: " + Time.time);

		}

		state = !state;
	}
}
