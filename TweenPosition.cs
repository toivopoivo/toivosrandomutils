﻿using UnityEngine;
using System.Collections;

public class TweenPosition : MonoBehaviour
{

	public Vector3 from;
	public Vector3 to;
	public float duration;

	public bool doOnStart;
	public bool doOnEnable;

	void Start ()
	{
		transform.position = from;
		if (doOnStart) {
			StartTween ();    
		}
	}

	void OnEnable ()
	{
		if (doOnEnable) {
			StartTween ();
		}

	}

	public void StartTween ()
	{
		Debug.Log ("start tween");
		StartCoroutine (TweenValue.TweenVector3 (from, to, duration, (x) => {
			transform.position = x;
		}));
	}

	[ToivonRandomUtils.InspectorButton]
	void GrabFrom ()
	{
		from = transform.position;
	}

	[ToivonRandomUtils.InspectorButton]
	public void GrabTo ()
	{
		to = transform.position;
	}
}
