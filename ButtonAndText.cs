﻿    using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
    using UnityEngine.EventSystems;

#if UNITY_EDITOR
using UnityEditor.Events;
#endif
public class ButtonAndText : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public Button button;
	public Text text;
	public object someData;
	public Action<object> onSendData;
	public Action noArgsSendData;
	public Action onPointerEnter;
	public Action onPointerExit;

	public void SendData ()
	{
		if (onSendData != null)
			onSendData (someData);
		else if (noArgsSendData != null)
			noArgsSendData ();
	}

	[ContextMenu ("get button and text")]
	public void GetButtonAndText ()
	{
		button = GetComponent<Button> ();
		text = GetComponentInChildren<Text> ();
	}
	#if UNITY_EDITOR
	[ContextMenu ("add on click ref to send data")]
	public void AddOnClickRefernce ()
	{
		var but = GetComponent<Button> ();
		if (but != null) {
			UnityEventTools.AddPersistentListener (but.onClick, SendData);
		}
	}
	#endif
	public void OnPointerEnter(PointerEventData eventData)
	{
		onPointerEnter?.Invoke();
	}

	public void OnPointerExit(PointerEventData eventData)
	{
		onPointerExit?.Invoke();
	}
}
