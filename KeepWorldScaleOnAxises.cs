﻿using UnityEngine;
using System.Collections;
using ToivonRandomUtils;

public class KeepWorldScaleOnAxises: MonoBehaviour, IOnInspectorGUI
{
	public Vector3 cachedLossyScale;
	public Vector3 lossyScale;
	public bool applyOnStart;
	public bool setLocPosToZero=true;
	[InspectorDataSheet (typeof(Locks))]
	public Locks locks;
	void Start(){
		if (applyOnStart)
			Apply ();
	}

	[System.Serializable]
	public class Locks
	{
		[InspectorDataCell (0, true)]
		public bool X, Y, Z;
		[SerializeField, HideInInspector]
#pragma warning disable  
        float editorHeight;
#pragma warning restore 
    }

    [ToivonRandomUtils.InspectorButton]
	void Apply ()
	{
		#if UNITY_EDITOR
		UnityEditor.Undo.RecordObject (transform, "Applied scale");
		#endif
		Vector3 scale = new Vector3 (cachedLossyScale.x / transform.lossyScale.x, cachedLossyScale.y / transform.lossyScale.y, cachedLossyScale.z / transform.lossyScale.z);
		scale.x = locks.X ? scale.x : 1f;
		scale.y = locks.Y ? scale.y : 1f;
		scale.z = locks.Z ? scale.z : 1f;
		transform.localScale = transform.localScale.MultiplyAxises (scale);
		transform.localPosition = Vector3.zero;
	}

	[ToivonRandomUtils.InspectorButton]
	void Get ()
	{
		cachedLossyScale = transform.lossyScale;
	}






	#region IOnInspectorGUI implementation

	public void OnInspectorGUI ()
	{
		lossyScale = transform.lossyScale;
	}

	#endregion
}
