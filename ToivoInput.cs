﻿using System.Collections;
using UnityEngine;
using ToivonRandomUtils;
using System.Collections.Generic;
using System.Linq;

namespace ToivoInputs
{
	public static class ToivoInput
	{
		public static void InitializeInputs <T> (T inputClass, List<ToivoControl> controls) where T: class
		{
			var fields = inputClass.GetType ().GetFields ();
			foreach (var item in fields) {
				if (item.FieldType != typeof(ToivoButton) && item.FieldType != typeof(ToivoAxis))
					continue;
				controls.Add (item.GetValue (inputClass) as ToivoControl);
				controls.Last ().name = item.Name.WithACapitalLetter ();
			}
		}

		public static void Update (List<ToivoControl> controls, string inputPrefix = "", bool debugReleasedButtons = false, float axisDeadZone = 0)
		{
			for (int i = 0; i < controls.Count; i++) {
				var toivoAxis = controls [i] as ToivoAxis;
				if (toivoAxis != null) { 
					var val = Input.GetAxis (inputPrefix + controls [i].name);
					toivoAxis.Update (val, axisDeadZone);
					if (toivoAxis.debugSpam) {
						Debug.Log ("" + inputPrefix + controls [i].name + ": " + toivoAxis.value);
					}
				} else {
					var toivoButton = controls [i] as ToivoButton;
					var getButtonName = inputPrefix + controls [i].name;
					var isReleased = Input.GetButtonUp (getButtonName);
					var isDown = Input.GetButton (getButtonName);
					toivoButton.Update (isReleased, isDown);
					if (toivoButton.isReleased && debugReleasedButtons)
						Debug.Log (getButtonName);
				}
			}
		}

		public static void DebugLogNeededInputs <T> (T inputClass, string prefix) where T: class
		{
			System.Text.StringBuilder sb = new System.Text.StringBuilder ();
			var fields = inputClass.GetType ().GetFields ();
			foreach (var item in fields) {
				if (item.FieldType != typeof(ToivoButton) && item.FieldType != typeof(ToivoAxis))
					continue;
				sb.Append (prefix);
				sb.Append (item.Name.WithACapitalLetter ());
				sb.Append ("\t");
			}
			Debug.Log (sb.ToString ());

		}
	}


	public class ToivoButton:ToivoControl
	{
		public bool isDown;
		public bool isReleased;
		public float chargeTime;
		float chargeStamp = -1;

		public void Update (bool released, bool down)
		{
			if (isReleased) {
				chargeStamp = -1;
			}
			if (!isDown && down)
				chargeStamp = Time.time;
			chargeTime = chargeStamp > 0 ? Time.time - chargeStamp : 0;
			isReleased = released;
			isDown = down;

		}
	}

	public class ToivoAxis:ToivoControl
	{
		public bool isNegative;
		public bool isPositive;
		public float value;

		public void Update (float val, float deadZone)
		{
			isPositive = val > deadZone;
			isNegative = val < -deadZone;
			value = (Mathf.Abs (val) > deadZone ? val : 0);
		}
	}

	public abstract class ToivoControl
	{
		public string name;
		public bool debugSpam;
	}

}