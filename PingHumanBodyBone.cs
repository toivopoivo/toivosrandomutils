﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PingHumanBodyBone : MonoBehaviour, IOnInspectorGUI
{
	public void OnInspectorGUI()
	{

		#if UNITY_EDITOR
		
		EditorGUI.BeginChangeCheck();
		HumanBodyBones hb = HumanBodyBones.Chest;
		hb = (HumanBodyBones) EditorGUILayout.EnumPopup(hb);
		if (EditorGUI.EndChangeCheck())
		{
			UnityEditor.EditorGUIUtility.PingObject(GetComponent<Animator>().GetBoneTransform(hb));
		}
		#endif
	}
}
