﻿#define GO_FAST
using System.Collections;
using System.Collections.Generic;
using ToivonRandomUtils;
using UnityEngine;

public class DistanceSortertStressTest : MonoBehaviour
{
    public int amount = 100;

    public bool running;

    DistanceSorterList<Transform> distanceSorterList;
    public List<Transform> createdTransses = new List<Transform>();
    private Transform[] ads;

    [ToivonRandomUtils.InspectorButton]
    public void StartTest()
    {
        createdTransses.DestroyGameObjectsAndClear();
        distanceSorterList = new DistanceSorterList<Transform>(x => x.position);
        for (int i = 0; i < amount; i++)
        {
            Transform trans = new GameObject("" + i).transform;
            createdTransses.Add(trans);
            createdTransses[createdTransses.Count - 1].transform.position = Random.insideUnitSphere * 100f;
            distanceSorterList.Add(trans);
        }
        ads = new Transform[256];
        running = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (running)
        {
            distanceSorterList.ReEvaluateOrder();

            for (int i = 0; i < createdTransses.Count; i++)
            {
                int resultsCount;
                distanceSorterList.GetOthersWithinRadius(createdTransses[Random.Range(0,createdTransses.Count)], 100 * Random.value, ads, out resultsCount) ;
            }
        }
    }
}
