﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ToivoAlgo
{
    [CreateAssetMenu(menuName = "MPSAR/MultiProcessAlgoTestRecoding")]
    public class MultiProcessAlgoTestRecording : ScriptableObject
    {
        public enum MPSARActionType
        {
            VisualizeCurrent,
            StepBackward,
            StepForward
        }

        public List<MPSARActionType> actionsTaken = new List<MPSARActionType>();
        public int runToIndex = -1;
    }

}
