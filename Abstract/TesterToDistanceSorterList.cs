﻿#define GO_FAST
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ToivonRandomUtils;
using Random = UnityEngine.Random;

public class TesterToDistanceSorterList : MonoBehaviour
{
    public DistanceSorterList<Transform> distanceSorter;
    public Transform testIsWithinRadius;
    private IEnumerator<Transform> evaluating;

 
    [ToivonRandomUtils.InspectorButton]
    void Restart()
    {
        distanceSorter = new DistanceSorterList<Transform> ( x => x.position );
        for (int i = 0; i < transform.childCount; i++)
        {
            distanceSorter.Add(transform.GetChild(i));
        }
         finished = false;
    }
    public List<GameObject> cubs = new List<GameObject>();
    [ToivonRandomUtils.InspectorButton]
    public void VizHeights()
    {
        cubs.DestroyGameObjectsAndClear();
        GameObject createdcube = null;
        for (int i = 0; i < distanceSorter.theList.Count; i++)
        {

            try
            {
                createdcube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                float height = distanceSorter.theList[i].height;
                createdcube.transform.localScale = new Vector3(0.8f, height, 1);
                createdcube.transform.position = new Vector3(i + 0.5f, createdcube.transform.localScale.y * 0.5f, 0);
                createdcube.name = distanceSorter.theList[i].item.name + " height:" + height;
                cubs.Add(createdcube);
            }
            catch (System.Exception ex)
            {
                Debug.Log("failed to viz "+ex);
                Destroy(createdcube.gameObject);
            }
        }

    }
    public List<Transform> resultsTransforms = new List<Transform>();
    public Transform[] evaluatedList = new Transform[256];
    int resultsCount;

    [ToivonRandomUtils.InspectorButton]
    public void Evaluate()
    {
        distanceSorter.EvaluatinOthersWithinRadius(testIsWithinRadius, 0.5f, evaluatedList,out resultsCount, DistanceSorterList<Transform>.ResultsArrayLenghtLimitHandlingType.Error);
    }
    public int iterations = 0;
    bool finished = false;
    [ToivonRandomUtils.InspectorButton]
    void Step()
    {
        if (!evaluating.MoveNext() && evaluating.Current != null) {
            Debug.Log("finished");
            finished = true;
            CheckAnswer();

        }else
        {
            iterations++;
            Debug.Log(iterations);
        }
        VizHeights();
    }
    [ToivonRandomUtils.InspectorButton]
    public void CheckAnswer()
    {
        Transform answer = Enumerable.Range(0, transform.childCount).Select(x => transform.GetChild(x)).FirstOrDefault(x => Vector3.Distance(x.position, testIsWithinRadius.position) < 0.5);
        Transform algoAnswer = resultsTransforms[0];
        Debug.Log("answer: " + answer);
        Debug.Log("algo answer: " + algoAnswer);
        Debug.Assert(answer == algoAnswer, "wrong");
    }
    HashSet<Transform> tempRemoved = new HashSet<Transform>();
    public bool runContinously;
    private void OnGUI()
     {
        if (!finished && !runContinously)
        {
            return;
        }
        Transform current;
#if GO_FAST
        if (resultsCount==0) current = null;
        else current = evaluatedList[0];
#endif
        if (runContinously)
        {
                
            if (distanceSorter == null) Restart();

            distanceSorter.ReEvaluateOrder();
            List<Transform> all = new List<Transform>();
#if GO_FAST
            Evaluate();
#else
            var getting = distanceSorter.EvaluatinOthersWithinRadius(testIsWithinRadius, 0.5f);
            int cycles = 0;
            while (getting.MoveNext() && getting.Current == null)
            {
                cycles++;
            }

            Debug.Log(getting.Current + " needed cycles: "+ cycles);
            current = getting.Current;
#endif

           

        }
        else
        {
            current = evaluating.Current;
        }

        foreach (var item in distanceSorter.theList)
        {
            Debug.Assert(distanceSorter.getPosFunc(item.item) == item.item.transform.position);
        }
    
         Vector3 pos = Camera.main.WorldToScreenPoint(testIsWithinRadius.position);
        if (current != null)
         {
             GUI.contentColor = Color.green;
         }
         else
         {
             GUI.contentColor = Color.red;
         }
      //   GUI.Box(new Rect(pos.x, Screen.height - pos.y, 110f, 110f), "");
    
         for (int i = 0; i < transform.childCount; i++)
         {
             Transform item = transform.GetChild(i);
            if (tempRemoved.Contains(item))
            {
                continue;
            }
          //  Vector3 pos = Camera.main.WorldToScreenPoint(Center);
          //  string msg = "\nspeed " + rigidBody.velocity.magnitude.ToString("0.0");
          //  msg += "\nvelocity" + rigidBody.velocity;
          //  msg += "\nis crowded" + aIBase.rvoDensityBehavior.lastJobDensityResult;
          //  MoveToCommand moveToCommand = combatCharacterState.CurrentCommand as MoveToCommand;
          //  if (moveToCommand != null)
          //  {
          //      msg += "\ndist to target: " + Vector3.Distance(moveToCommand.position, transform.position);
          //  }
          //  GUI.Box(new Rect(pos.x, Screen.height - pos.y, 100f, 100f), msg);
             pos = Camera.main.WorldToScreenPoint(item.position);
             string msg ="";
            int index = distanceSorter.listItemToIndex[item];
            msg += "\n i:" + index;
             msg += "\n h:" + distanceSorter.theList[index].height;
             GUI.Box(new Rect(pos.x, Screen.height - pos.y, 100f, 100f), msg);
    
    
    
         }


        if (runContinously)
        {
            Random.InitState(Time.frameCount);
            bool remove = Random.value < 0.5f;

            Transform randomChild = transform.GetChild(Random.Range(0, transform.childCount));
            if (remove && !tempRemoved.Contains(randomChild))
            {
                if(testIsWithinRadius != randomChild)
                {
                    distanceSorter.Remove(randomChild);
                    tempRemoved.Add(randomChild);
                }
            }
            else if (tempRemoved.Contains(randomChild))
            {
                distanceSorter.Add(randomChild);
                tempRemoved.Remove(randomChild);
            }
        }
       
    
     }


    private void OnEnable()
    {
        test = GetFirstMatchingClosestTest();
    }

    private IEnumerator test;

    private void Update()
    {
        test.MoveNext();
    }

    IEnumerator GetFirstMatchingClosestTest()
    {
        DistanceSorterList<Transform> distanceSorterList = new DistanceSorterList<Transform>(x=>x.position);

      //  List<Transform> added = new List<Transform>();
      Transform randomItem = null;
      for (int i = 0; i < 100; i++)
        {
            distanceSorterList.Add(new GameObject("pos"+i).transform);
            randomItem = distanceSorterList.theList.RandomItem().item;
            Transform res;
            var compared = randomItem;
            distanceSorterList.GetFirstMatchingClosest(x => x== compared, distanceSorterList.theList[i].item, out res);
            if (res != randomItem)
            {
                throw new SystemException();
            }
            
        }

        yield return null;
    }
}
