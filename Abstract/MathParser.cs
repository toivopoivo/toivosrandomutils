using System;
using UnityEngine;

public static class MathParser
{
    public static double Parse(string mathExpression)
    {
        MathEvaluator evaluator = new MathEvaluator();
        return evaluator.Evaluate(mathExpression);
    }

    private class MathEvaluator
    {
        private int index;
        private string input;
        private Token currentToken;

        public double Evaluate(string mathExpression)
        {
            input = mathExpression.Replace(" ", "");
            index = 0;
            currentToken = GetNextToken();

            double result = ParseExpression();

            if (currentToken.Type != TokenType.EndOfInput)
            {
                throw new Exception("Invalid syntax.");
            }

            return result;
        }

        private double ParseExpression()
        {
            double result = ParseTerm();

            while (currentToken.Type == TokenType.Plus || currentToken.Type == TokenType.Minus)
            {
                Token op = currentToken;
                if (op.Type == TokenType.Plus)
                {
                    Eat(TokenType.Plus);
                    result += ParseTerm();
                }
                else if (op.Type == TokenType.Minus)
                {
                    Eat(TokenType.Minus);
                    result -= ParseTerm();
                }
            }

            return result;
        }

        private double ParseTerm()
        {
            double result = ParseFactor();

            while (currentToken.Type == TokenType.Multiply || currentToken.Type == TokenType.Divide)
            {
                Token op = currentToken;
                if (op.Type == TokenType.Multiply)
                {
                    Eat(TokenType.Multiply);
                    result *= ParseFactor();
                }
                else if (op.Type == TokenType.Divide)
                {
                    Eat(TokenType.Divide);
                    double denominator = ParseFactor();
                    if (denominator == 0)
                    {
                        throw new Exception("Division by zero.");
                    }

                    result /= denominator;
                }
            }

            return result;
        }

        private double ParseFactor()
        {
            Token token = currentToken;
            if (token.Type == TokenType.Number)
            {
                Eat(TokenType.Number);
                return token.Value;
            }
            else if (token.Type == TokenType.Minus)
            {
                Eat(TokenType.Minus);
                return -ParseFactor();
            }
            else if (token.Type == TokenType.LeftParen)
            {
                Eat(TokenType.LeftParen);
                double result = ParseExpression();
                Eat(TokenType.RightParen);
                return result;
            }
            else
            {
                throw new Exception("Invalid syntax.");
            }
        }

        private Token GetNextToken()
        {
            if (index >= input.Length)
            {
                return new Token {Type = TokenType.EndOfInput};
            }

            char currentChar = input[index];

            if (char.IsDigit(currentChar))
            {
                double value = 0;
                while (index < input.Length && char.IsDigit(input[index]))
                {
                    value = value * 10 + (input[index] - '0');
                    index++;
                }

                if (index < input.Length && input[index] == '.')
                {
                    index++;

                    double fraction = 0.1;
                    while (index < input.Length && char.IsDigit(input[index]))
                    {
                        value += fraction * (input[index] - '0');
                        fraction *= 0.1;
                        index++;
                    }
                }

                return new Token {Type = TokenType.Number, Value = value};
            }
            else if (currentChar == '+')
            {
                index++;
                return new Token {Type = TokenType.Plus};
            }
            else if (currentChar == '-')
            {
                index++;

                if (index < input.Length && char.IsDigit(input[index]))
                {
                    double value = 0;
                    while (index < input.Length && char.IsDigit(input[index]))
                    {
                        value = value * 10 + (input[index] - '0');
                        index++;
                    }

                    if (index < input.Length && input[index] == '.')
                    {
                        index++;

                        double fraction = 0.1;
                        while (index < input.Length && char.IsDigit(input[index]))
                        {
                            value += fraction * (input[index] - '0');
                            fraction *= 0.1;
                            index++;
                        }
                    }

                    return new Token {Type = TokenType.Number, Value = -value};
                }
                else
                {
                    return new Token {Type = TokenType.Minus};
                }
            }
            else if (currentChar == '*')
            {
                index++;
                return new Token {Type = TokenType.Multiply};
            }
            else if (currentChar == '/')
            {
                index++;
                return new Token {Type = TokenType.Divide};
            }
            else if (currentChar == '(')
            {
                index++;
                return new Token {Type = TokenType.LeftParen};
            }
            else if (currentChar == ')')
            {
                index++;
                return new Token {Type = TokenType.RightParen};
            }
            else
            {
                throw new Exception("Invalid character: " + currentChar);
            }
        }

        private void Eat(TokenType type)
        {
            if (currentToken.Type == type)
            {
                currentToken = GetNextToken();
            }
            else
            {
                throw new Exception("Unexpected token: " + currentToken.Type);
            }
        }

        private enum TokenType
        {
            Number,
            Plus,
            Minus,
            Multiply,
            Divide,
            LeftParen,
            RightParen,
            EndOfInput
        }

        private struct Token
        {
            public TokenType Type { get; set; }
            public double Value { get; set; }
        }
    }
}
