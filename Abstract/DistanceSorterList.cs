﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ToivonRandomUtils;
using System;
using System.Linq;

/// <summary>
/// original idea do no not steal :DDD - toivo järvi - 17.11.2019
/// 
/// 
/// lets say you have crowd of characters
/// if we organise the list by the pair of characters that are the furthest away each other.
/// and after that we just populate characters in between with their distance from the first character
/// then characaters would be close to each other in that list with great probability
/// and they definately are close to each other if the angle from first charactare to the second character is similiar 
/// 
/// the more I thought about it i realised that this way of structuring the data will actually make it possible to prune the amount of needed distance calculations.
/// 
/// not sure if works with multiple dimensions
/// </summary>
/// <typeparam name="T"></typeparam>
public class DistanceSorterList<T> where T: class
{
    public System.Func<T, Vector3> getPosFunc;
    public List<SortedItem> theList = new List<SortedItem>();

    public class SortedItem
    {
        public Vector3 cachedPosition;
        public float height =-1f;
        public T item;
    }

    public DistanceSorterList(Func<T, Vector3> getPosFunc)
    {
        this.getPosFunc = getPosFunc;
    }



    /// <summary>
    /// see this nice drawing. 
    /// the height for any other points is the distance from point A:s local X plane to the given point
    /// distance from the first and last in the list is AB line is the longest height, after ordering 
    /// this is used to prune the distance calculations
    ///          B 
    ///          |   
    ///          |  C
    ///          |  |
    /// ---------A---------
    /// 
    /// </summary>
   // public Dictionary<T, float> listItemToHeight = new Dictionary<T, float>();
    public Dictionary<T, int> listItemToIndex = new Dictionary<T, int>();
    bool dirty = false;


    public void Add(T item)
    {
    
        theList.Add(new SortedItem {item= item });
        dirty = true;
    }
    public void Remove(T item)
    {
        //theList.Remove(item);
        for (int i = 0; i < theList.Count; i++)
        {
            if(theList[i].item == item)
            {
                theList.RemoveAt(i);
                dirty = true;
                return;
            }
        }
    }

 

    /// <summary>
    /// basically call this every update
    /// 
    /// NOTE: automaticallty removes nulls
    /// </summary>
    public void ReEvaluateOrder()
    {
        var ordering = Ordering();
        while (ordering.MoveNext()) ;
    }
    SortedItem greatestDistanceStart;
    private Quaternion forward = Quaternion.LookRotation(Vector3.forward);

    public IEnumerator Ordering()
    {
        if(theList.Count==0)
        {
            listItemToIndex.Clear();
            yield break;
        }
        if(theList.Count == 1)
        {
            listItemToIndex.Clear();
            listItemToIndex.Add(theList[0].item, 0);
            yield break;
        }


        dirty =  EvaluateOrderIntergrityAndCachePosition();
        if (!dirty && theList.Count == listItemToIndex.Count) yield break;

        greatestDistanceStart = default;
        SortedItem greatestDistanceEnd = default;
        float greatestDistance = -1;

       

        for (int i = 0; i < theList.Count; i++)
        {
            for (int j = theList.Count - 1; j >= 0; j--)
            {
                if (theList[j].item == null) {
                    theList.RemoveAll(x => x.item == null);
                    ReEvaluateOrder();
                    yield break;
                }
                // no need to go through the same pairs again
                if (i == j) break;
                float dist = (theList[i].cachedPosition - theList[j].cachedPosition).sqrMagnitude;
                
                if (greatestDistance < dist)
                {
                    greatestDistanceStart = theList[i];
                    greatestDistanceEnd = theList[j];
                    greatestDistance= dist;
                }
                yield return null;
            }

        }

        listItemToIndex.Clear();
       //listItemToHeight.Clear();
       forward = GetClosestToFurhestRotation(greatestDistanceStart.cachedPosition,greatestDistanceEnd.cachedPosition);

        // perf: reinvent quicksort algo; using delegate comparer is somewhat costly
        theList.Quicksort(x => GetOrEvaluateHeight(forward,x) );


       for (int i = 0; i < theList.Count; i++)
        {
            listItemToIndex.Add(theList[i].item, i);
            yield return null;
        }
        dirty = false;

    }

    int prevListCount;
    private bool EvaluateOrderIntergrityAndCachePosition()
    {

        /* toivo 3.10.2020 wtf this needs to run everyframe, otherwise the positions wont be up to date
        if (prevListCount != theList.Count) return true;
        prevListCount = theList.Count;
        */

        /*20.7.2020 toivo 
         * I got an idea
         * 
         * I don't need to fully re order every frame i could just run an order integrity check
         * 
         * picture 1
         *  z9--------------
         *  |
         *  |            x0
         *  |
         *  |  x1
         *  |
         *  |       x2
         *  |
         *  |    x3
         *  z0--------------  
         *  
         *  if x2 moves here
         
         * picture 2
         *  z9--------------
         *  |
         *  |            x0
         *  |       x2
         *  |  x1
         *  |
         *  |       
         *  |
         *  |    x3
         *  z0--------------  * 
         *  
         *  then order is filthy because x2 order on Z axis is dfferent compared to x1 and x3 position and we should do a full reordering
         */


        bool orderDirty = false;
        float prevHeight = GetOrEvaluateHeight(forward,theList[0]);
        for (int i = 0; i < theList.Count; i++)
        {

            orderDirty |= GetOrEvaluateHeight(forward, theList[i], true) > prevHeight;
            // anon funcs are heavy to call, so caching
            theList[i].cachedPosition = getPosFunc(theList[i].item);
        }
        return orderDirty;
    }

    private float GetOrEvaluateHeight(Quaternion forward, SortedItem item, bool forceEval = false)
    {
        float val;
        //if (listItemToHeight.TryGetValue(item.item, out val))
        if (item.height>=0 && !forceEval)
        {
            return item.height;
        }
        else
        {
            item.height = (forward * item.cachedPosition).z;
            return item.height;
        }
    }

    /// <summary>
    /// note: order is kind of arbitrary
    /// ALSO NOTE, will not clear the array so use the results count NOT array lenght
    /// </summary>
    /// 
    public void GetOthersWithinRadius(T center, float radius, T[] resultsNonAlloc, out int resultsCount, ResultsArrayLenghtLimitHandlingType resultsArrayLenghtLimitHandlingType = ResultsArrayLenghtLimitHandlingType.Ignore)
    {
        EvaluatinOthersWithinRadius(center, radius, resultsNonAlloc, out resultsCount, resultsArrayLenghtLimitHandlingType);
    }

    public enum ResultsArrayLenghtLimitHandlingType { 
        Error,
        Ignore
    }


    public long MemoryUsage {
        get {
            return System.GC.GetTotalMemory(true);
        }
    }
    /// <summary>
    /// toivo 25.8.2020 - i wrote this code all exited but realized I didn't h
    /// ave the need for it i thought I had
    /// it should work in theory, but it needs to be tested properly
    ///
    /// toivo 16/2/2020 - yeah definately didnt work :D but now it should
    /// </summary>
    public bool GetFirstMatchingClosest(System.Func<T, bool> predicate, T center, out T result, float maxRange = -1)
    {
        //implementation idea -> 
        //this could be easily done by just starting from center index and then offsetting -1 and 1 and -2 2... 
        //it is actually very similiar to the eval others within radius but i should use leave that alone, because its more performant without anonymous methods
        result = default;
        if (theList.Count == 0) return false;
        if (theList.Count == 1)
        {
            result = theList[0].item;
            return theList[0] != center && predicate(theList[0].item) ;
        }

        int centerIndex = GetIndexOrReEvaluate(center);
        int upOffset = 0;
        int downOffset = 0;
        bool upLimitHit = false;
        bool lowerLimitHit = false;
        ListDirection dir = ListDirection.Up;
        float foundGoodOneDist = -1f;

        while (true)
        {
            int newindex;
            if (dir == ListDirection.Up)
            {
                newindex = centerIndex + upOffset;
                upLimitHit = newindex >= theList.Count;
            }
            else
            {
                 newindex = centerIndex - downOffset;
                 lowerLimitHit = newindex < 0;
            }
            if (upLimitHit && lowerLimitHit) {
                return foundGoodOneDist >= 0;
            }

            if ((dir == ListDirection.Up && !upLimitHit) || (dir == ListDirection.Down && !lowerLimitHit))
            {

                float newDist = Vector3.Magnitude(theList[newindex].cachedPosition- theList[centerIndex].cachedPosition);
                float heightDif = Mathf.Abs(theList[newindex].height - theList[centerIndex].height);
                if (foundGoodOneDist >= 0 &&  heightDif > newDist || (maxRange >= 0 && newDist >= maxRange ) )
                {
                    lowerLimitHit = dir == ListDirection.Down;
                    upLimitHit = dir == ListDirection.Up;
                    dir = GFMCNext(dir, upLimitHit, lowerLimitHit, ref upOffset, ref downOffset);
                    continue;
                }

                if (predicate(theList[newindex].item))
                {
                    if(foundGoodOneDist < 0 || newDist <= foundGoodOneDist)
                    {
                        result = theList[newindex].item;
                        foundGoodOneDist = newDist;
                    }
                }
            }

            dir = GFMCNext(dir, upLimitHit, lowerLimitHit, ref upOffset, ref downOffset);
        }
    }

    private static ListDirection GFMCNext(ListDirection dir, bool upLimitHit, bool lowerLimitHit, ref int upOffset, ref int downOffset)
    {
        bool flip = true;
        if (dir == ListDirection.Up) upOffset++;
        if (dir == ListDirection.Down) downOffset++;

        // dont flip when limits are hit
        switch (dir)
        {
            case ListDirection.Down when upLimitHit:
            case ListDirection.Up when lowerLimitHit:
                flip = false;
                break;
        }

        if (flip) dir = dir == ListDirection.Down ? ListDirection.Up : ListDirection.Down;
        return dir;
    }

    public enum ListDirection { Up, Down}
    public void EvaluatinOthersWithinRadius(T center,float radius, T[] resultsNonAlloc, out int resultsCount, ResultsArrayLenghtLimitHandlingType resultsArrayLenghtLimitHandlingType)
    {
        resultsCount = 0;
        int startingIndex;

        if (theList.Count == 0)
        {
            Debug.LogError("use add method first");
        }
        
        // no others
        if (theList.Count < 2)
        {
            if (theList.Count == 1)
            {
                return;
            }
            int otherIndex = -1;
            for (int i = 0; i < 2; i++)
            {
                if (theList[i].item != center)
                {
                    otherIndex = i;
                    break;
                }
            }

            if (Vector3.Distance(getPosFunc(center), getPosFunc(theList[otherIndex].item)) <= radius)
            {
                //resultsNonAlloc.Add( theList[otherIndex].item );
                resultsNonAlloc[resultsCount] = theList[otherIndex].item;
                resultsCount++;
            }

            return;

        }
        startingIndex = GetIndexOrReEvaluate(center);

        T result = null;
        var centerPos = theList[startingIndex].cachedPosition;
        int offset = 1;
        int dir = -1;
        //  bool lastMinIndexOnSameSideOfTheDividerWasOutOfRadius = false;
        // bool lastMaxIndexOnSameSideOfTheDividerWasOutOfRadius = false;
        bool hitMinLimit = false;
        bool hitMaxLimit = false;
        float centerHeight = theList[startingIndex].height;
        while (true)
        {

            if (hitMinLimit && hitMaxLimit)
            {
                break;
            }

            int index = (startingIndex + (offset * dir));
            Vector3 currentPos;
            if (dir < 0)
            {
                hitMinLimit |= index < 0;
                if (hitMinLimit)
                {
                    dir = 1;

                    continue;
                }
                hitMinLimit |= Mathf.Abs(theList[index].height - centerHeight) > radius;
            }
            else
            {
                hitMaxLimit |= index >= theList.Count;

                if (hitMaxLimit)
                {
                    offset++;
                    dir = -1;
                    continue;
                }

                hitMaxLimit |= Mathf.Abs(theList[index].height - centerHeight) > radius;
            }



            currentPos = theList[index].cachedPosition;
            float dist = Vector3.Distance(centerPos, currentPos);
            if (dist < radius)
            {
                result = theList[index].item;
                // results.Add(result);
                if(resultsCount >= resultsNonAlloc.Length)
                {
                    if (resultsArrayLenghtLimitHandlingType == ResultsArrayLenghtLimitHandlingType.Error) throw new System.Exception("results array exceeded");
                    else break;
                            
                }
                resultsNonAlloc[resultsCount] = result;
                resultsCount++;
            }

            if (dir == 1) offset++;
            dir *= -1;
        }

    }

    private int GetIndexOrReEvaluate(T center)
    {
        int startingIndex;
        dirty |= !listItemToIndex.TryGetValue(center, out startingIndex);
        dirty |= listItemToIndex.Count != theList.Count;
        if (!dirty)
        {

            // i wonder if c# can auto skip these lines if above is true
            //dirty |=  startingIndex> theList.Count;
            //dirty |= theList[startingIndex] != center;

            // it wasnt
            if (startingIndex >= theList.Count)
            {
                dirty = true;
            }
            else if (theList[startingIndex] != center)
            {
                dirty = true;
            }
        }

        if (dirty)
        {
            var reEvaling = Ordering();
            while (reEvaling.MoveNext())
            {
            }
            // 20.8.2020 holy shit lack of this line used to cause invalid results, im sure
            startingIndex = listItemToIndex[center];
        }

        return startingIndex;
    }

    private Quaternion GetClosestToFurhestRotation(Vector3 closest, Vector3 furthest)
    {
        //Vector3 closestPosition = getPosFunc(theList[0]);
        //Vector3 furthestPosition = getPosFunc(theList[theList.Count - 1]);
        Vector3 fromClosestToFurthest = closest - furthest;
        Quaternion closestToFurhtestRot;
        if (closest == furthest)
        {
            closestToFurhtestRot =  Quaternion.identity;
        }
        else
        {
            closestToFurhtestRot = Quaternion.LookRotation(fromClosestToFurthest);
        }
        return closestToFurhtestRot;
    }
}
