using System;
using System.Collections;
using System.Collections.Generic;
using ToivonRandomUtils;
using UnityEngine;

public class PooledGameObject : MonoBehaviour
{
	public GameObjectPooler pooler;
	public GameObject instanceOf;
	private IPoolingEventsReceiver[] _poolingEventsReceivers;
	/// <summary>
	/// NULLED AFTER ran once
	/// </summary>
	public System.Action onPooledFireOnce;
	[DrawnNonSerialized]
	[System.NonSerialized]
	public float enabledStamp = -1;

	public PoolingTacticType poolingTacticType;
	
	
	//public ParticleSystem[] particleSystems;
	public void OnDisable()
	{
		if(poolingTacticType== PoolingTacticType.GameObjectActive) PutBackToPool();
	}

	/// <summary>
	/// also set the gameobject active false when you call this manually
	/// </summary>
	public void PutBackToPool()
	{
		if (poolingTacticType == PoolingTacticType.Manual)
		{
			gameObject.SetActive(false);
		}
		if (!init) return;
		if (pooler == null) return;
		pooler.unpooled.Add(this);
		pooler.dirty = true;
		if (onPooledFireOnce != null)
		{
			onPooledFireOnce.Invoke();
			onPooledFireOnce = null;
		}
		if (_poolingEventsReceivers != null)
		{
			foreach (var item in _poolingEventsReceivers)
			{
				if (item != null)
				{
					try
					{
						item.OnDeallocated(instanceOf);
					}
					catch (Exception e)
					{
						Debug.LogException(e);
					}
				}
			}
		}
	}

	private void OnDestroy()
	{
		if(pooler != null && pooler.debugLog)	Debug.Log("PooledGameObject.OnDestroy " + gameObject.name + " t: " + Time.time, gameObject);
	}

	private void OnEnable()
	{
		if (!init) return;
		// removed because performance overhead in unity editor (cuz of the Object overload) is significant and the pooler is about optimisaion
		//if (pooler == null) return;
		enabledStamp = Time.time;
		try
		{
			// can be null in case of recompile (i like hot reloading)
			if (_poolingEventsReceivers != null)
			{
				foreach (var item in _poolingEventsReceivers)
				{
					item.OnAllocated();
				}
			}
		}
		catch (Exception e)
		{
			Debug.LogError(gameObject.name + " " + e, this);
		}
	}


	private bool init = false;

	public void InitializeInstance(GameObjectPooler gameObjectPooler, GameObject prefab)
	{
		pooler = gameObjectPooler;
		instanceOf = prefab;

		_poolingEventsReceivers = GetComponentsInChildren<IPoolingEventsReceiver>(true);
		//		 prefab.GetComponentsInChildren<ParticleSystem>(true);
		init = true;
		OnEnable();
	}

	public  void DisableAfterTime(float time)
	{
		StartCoroutine(Disabling(time));
	}

	IEnumerator Disabling(float time)
	{
		yield return new WaitForSeconds(time);
		gameObject.SetActive(false);
		
	}
	
	
	
}

public enum PoolingTacticType
{
	GameObjectActive,
	Manual
}