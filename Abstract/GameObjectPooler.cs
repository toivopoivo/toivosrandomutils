﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ToivoLinq;
using ToivonRandomUtils;
using UnityEngine;
using UnityEngine.Profiling;
/*
 *  if you want to manually control the pooling you can add the pooled gameobject to the prefab with manual strategy.
 * this is better, for example, characters because then you can disable sometimes without causing mayhem  
 * 
 */
public class GameObjectPooler : MonoBehaviour, IOnInspectorGUI
{
    public Dictionary<GameObject, List<PooledGameObject>> pooledGameObjects = new Dictionary<GameObject, List<PooledGameObject>>();
    public List<PooledGameObject> unpooled = new List<PooledGameObject>();
    
    public bool debugLog = false;
    [DrawnNonSerialized]
    int objectsPooled;
    [DrawnNonSerialized]
    int objectsCreated;

    public List<PrewarmedPrefab> prewarmedPrefabs = new List<PrewarmedPrefab>();

    public void Prewarm()
    {
        foreach (var prewarmedPrefab in prewarmedPrefabs)
        {
            for (int i = 0; i < prewarmedPrefab.prewarmedCount; i++)
            {
                FetchInstance(prewarmedPrefab.prefab, true).gameObject.SetActive(false);
            }
        }
    }
    
    public PooledGameObject FetchInstance(GameObject prefab, Transform newParent)
    {
        var instance = FetchInstance(prefab);
        instance.transform.SetParent(newParent,false);
        return instance;
    }


#if UNITY_EDITOR
    public static UnityEngine.Profiling.CustomSampler poolerSampler   ;
#endif
    public PooledGameObject FetchInstance(GameObject prefab, bool forceInstantiate = false)
    {
#if UNITY_EDITOR
        if (poolerSampler == null) poolerSampler = UnityEngine.Profiling.CustomSampler.Create("GameObjectPooler.Fetch");
        poolerSampler.Begin();
#endif
        var instance = _Fetch(prefab, forceInstantiate);
#if UNITY_EDITOR
        poolerSampler.End();
#endif
        return instance;
    }
    
    PooledGameObject _Fetch(GameObject prefab, bool forceInstantiate = false)
    {
        pooledGameObjects.TryGetValue(prefab, out var freeInstances);
        
        
        
        if (!forceInstantiate)
        {
            if (freeInstances != null && freeInstances.Count > 0)
            {
                PooledGameObject found = null;

                for (int i = freeInstances.Count - 1; i >= 0; i--)
                {
                    if (freeInstances[i] == null)
                    {
                        freeInstances.RemoveAt(i);
                    }
                    else
                    {
                        found = freeInstances[i];
                        freeInstances.RemoveAt(i);
                        found.gameObject.SetActive(true);
                        break;
                    }
                }


                if (found)
                {
                    found.gameObject.SetActive(true);
                    found.transform.SetParent(null);
                    objectsPooled++;
                    return found;
                }
            }
        }

        // no instance found
        PooledGameObject pgo;
        var createdInstance = Instantiate(prefab);
        
        pgo = createdInstance.GetComponent<PooledGameObject>();
        if(pgo==null) pgo = createdInstance.AddComponent<PooledGameObject>();
        
        objectsCreated++;
         if(debugLog) Debug.Log($"GameObjectPooler Instantiating  {prefab} t: {Time.time}");
        pgo.InitializeInstance(this, prefab);
        return pgo;
    }

    public bool dirty;

    private void Update()
    {
        if (dirty)
        {
            for (var index = unpooled.Count - 1; index >= 0; index--)
            {
                var pgo = unpooled[index];
                if (pgo == null) continue;
                if (pgo.instanceOf == null) continue;
                
                //???
                if( pgo.gameObject.activeInHierarchy) continue;
                
                if (!pooledGameObjects.TryGetValue(pgo.instanceOf, out var free))
                {
                    pooledGameObjects.Add(pgo.instanceOf, free = new List<PooledGameObject>());
                }
            
                pgo.gameObject.SetActive(false);
                pgo.transform.SetParent(transform);
                var prefabTrans = pgo.instanceOf.transform;
                var pgoTransform = pgo.transform;
                pgoTransform.localScale = prefabTrans.localScale;
                pgoTransform.localPosition = prefabTrans.localPosition;
                pgoTransform.localRotation = prefabTrans.localRotation;
                free.Add(pgo);
            }
            unpooled.Clear();
            dirty = false;
            
        }
    }
    
   
    public GameObject FetchInstance(GameObject prefab, Vector3 pos, Quaternion rotation)
    {
        
        var instance = FetchInstance(prefab);
        var instanceTransform = instance.transform;
        instanceTransform.position = pos;
        instanceTransform.rotation = rotation;
        return instance.gameObject;
    }

    public void OnInspectorGUI()
    {
#if  UNITY_EDITOR
        foreach (var item in pooledGameObjects)
        {
            UnityEditor.EditorGUILayout.ObjectField("" + item.Value.Count, item.Key, typeof(GameObject), true);
        }
#endif
    }
    
    [System.Serializable]
    public class PrewarmedPrefab
    {
        public GameObject prefab;
        public int prewarmedCount;
    }
}

public interface IPoolingEventsReceiver
{
    void OnAllocated();
    void OnDeallocated(GameObject prefab);
}
