﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
/*
 * 
 * 
 * as of unity 2019 lts json utility seems to have been actually imporved a little bit so use that
 * 
 * 
public class ValueTypeStorage 
{
    public Dictionary<string, object> valuesDic = new Dictionary<string, object>();
    /// <summary>
    /// will currently disregard inherited fields
    /// </summary>
    public static ValueTypeStorage Create<T>(T storaged)
    {
        ValueTypeStorage valueTypeStorage = new ValueTypeStorage();
        var gfd = GetFieldsDic(storaged);
        foreach (var item in gfd)
        {
            valueTypeStorage.valuesDic.Add(item.Key, item.Value.GetValue(storaged));
        }
        return valueTypeStorage;
    }

    private static Dictionary<string,FieldInfo> GetFieldsDic<T>(T storaged)
    {
        return storaged.GetType().GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance).Where(x => x.FieldType.IsValueType).ToDictionary(x => x.Name, x => x);
    }


    public void ApplyStorage<T>(T target)
    {
        var fields = GetFieldsDic(target);
        foreach (var item in fields)
        {
            fields[item.Key].SetValue(target, valuesDic[item.Key]);
        }
    }
}
*/