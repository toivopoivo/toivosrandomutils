﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using ToivoLinq;
using UnityEditor;
using UnityEngine;
[CreateAssetMenu]
public class PersistenceReflector : ScriptableObject, IOnInspectorGUI
{
    public TextAsset codeAsset;
    public int resetLine = 0;
        
    const string RESET_METHOD_START = "// RESET";
    const string RESET_METHOD_END = "// RESET END";

    public  List<string> StaticReflect(System.Text.StringBuilder log)
    {
#if UNITY_EDITOR
	    var path = UnityEditor.AssetDatabase.GetAssetPath(codeAsset);
        log.AppendLine(path);
        var fileInfo = new System.IO.FileInfo(path);
        var fileName = fileInfo.Name;
        fileName = fileName.Replace(".cs","");
        log.AppendLine(fileName);

        var type = AppDomain.CurrentDomain.GetAssemblies().SelectMany(x=>x.GetTypes()).Single(x=>x.Name == fileName);

        
        var bindingFlags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic;
        var fields = type.GetFields(bindingFlags);
        var monofields = typeof(MonoBehaviour).GetFields(bindingFlags).Select(x=>x.Name).ToHashSet();
        var monoprops = typeof(MonoBehaviour).GetProperties(bindingFlags).Select(x=>x.Name).ToHashSet();

       var  problems = new List<string>();
        problems.AddRange(fields.Where(X=>!monofields.Contains(X.Name)).Select(x=>x.Name));
        problems.AddRange(type.GetProperties(bindingFlags).Where(X=>!monoprops.Contains(X.Name)).Select(x=>x.Name));

        var codeLines = codeAsset.text.Split('\n');

        if (resetLine== -1 || !codeLines[resetLine].Contains(RESET_METHOD_START))
        {
            FindResetMethod(codeLines);
        }

        // this galaxy brains line of code makes sure that if theres like field name like "text" and "textUI" then first try match the longer string 
        problems = problems.OrderBy(x => x.Length).ToList();
        
        for (int i = resetLine + 1; i < codeLines.Length; i++)
        {
            for (var j = problems.Count - 1; j >= 0; j--)
            {
                var prob = problems[j];
                if (codeLines[i].Contains(prob))
                {
                    problems.RemoveAt(j);
                    break;
                }
            }
            if (codeLines[i].Contains(RESET_METHOD_END)) break;
        }

        return problems;
#else
        return new List<string>();
#endif
    }

    private void FindResetMethod(string[] codeLines)
    {
        bool found = false;
        for (int i = 0; i < codeLines.Length; i++)
        {
            if (codeLines[i].Contains(RESET_METHOD_START))
            {
                resetLine = i;
#if UNITY_EDITOR
                UnityEditor.EditorUtility.SetDirty(this);
#endif
                found = true;
                break;
            }
        }

        if (!found)
        {
            throw new SystemException($"RESET METHOD NOT FOUND ?!?!? <{RESET_METHOD_START}> <{RESET_METHOD_END}> ");
        }
    }

    private System.Text.StringBuilder sb = new StringBuilder();
    public void OnInspectorGUI()
    {
#if UNITY_EDITOR
        sb.Clear();
        var problems = StaticReflect(sb);
        UnityEditor.EditorGUILayout.HelpBox( sb.ToString(), MessageType.None );
        if (problems.Count > 0)
        {
            UnityEditor.EditorGUILayout.HelpBox( "reset method not properly implemented !", MessageType.Error);
            if (GUILayout.Button("log fields that need resetting"))
            {
                Debug.LogError(string.Join("\n", problems) );
            }
        }
#endif
         
    }
}
