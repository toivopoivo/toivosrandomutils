﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ToivonRandomUtils
{
    public class ControlledList<T> : IEnumerable<T>
    {
        List<T> list = new List<T>();

        public System.Action<T> onAdd;

        public int Count
        {
            get => list.Count;
        }

        public void Add(T item)
        {
            onAdd?.Invoke(item);
            list.Add(item);
        }

        public void AddRange(IEnumerable<T> list)
        {
            foreach (var items in list)
            {
                Add(items);
            }
        }


        public T this[int key]
        {
            get => list[key];
            set => list[key]= value;
        }

        public void RemoveAt(int cSpawnI)
        {
            list.RemoveAt(cSpawnI);
        }

        public void Clear()
        {
            list.Clear();
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var item in list)
            {
                yield return item;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (var item in list)
            {
                yield return item;
            }
        }
    }
}
