﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ToivonRandomUtils;
using UnityEngine;

namespace ToivoAlgo
{
    public class MultiProcessSearchAlgorithmRunner : MonoBehaviour
    {
        public Mode mode;
        [DrawnNonSerialized]
        public List<IMultiProcessSearchAlgorithm> allProcesses = new List<IMultiProcessSearchAlgorithm>();
        [DrawnNonSerialized]
        public IMultiProcessSearchAlgorithm finalState;
        [DrawnNonSerialized]
        public IMultiProcessSearchAlgorithm lastCompletedSubJob;

        public MultiProcessAlgoTestRecording actionsTakenRecord;
        public MultiProcessAlgoTestRecording runActionsTaken;
        /// <summary>
        /// lets think about the branching timelines with this nice diagram
        ///  (p) means timeline paused
        ///  (r) means its resumed  
        /// 
        ///       /------>\
        /// ---->(p)      (r)->
        /// 
        /// there is no sense storing paused timelines
        /// 
        /// </summary>
        public ReadOnlyCollection<HistoryState> historyStates;
        public List<HistoryState> m_historyStates = new List<HistoryState>();
        public int stepsTravelledBackwardInTime = 0;
        private void Update()
        {
            if (allProcesses.Count == 0) return;
            if (mode == Mode.DebugRunOnUpdate)
            {
                VisualiseCurrentAndStep();
            }
        }

        [ToivonRandomUtils.InspectorButton]
        private void VisualiseCurrentAndStep()
        {
            
            if (allProcesses.Count == 0)
            {
                Debug.Log("finished");
                return;
            }
            VisualiseCurrent();
            TryStepForward();
        }

        [ToivonRandomUtils.InspectorButton]
        private void VisualiseCurrent()
        {
            if (actionsTakenRecord != null)
            {
                actionsTakenRecord.actionsTaken.Add(MultiProcessAlgoTestRecording.MPSARActionType.VisualizeCurrent);
            }
            allProcesses.ForEach(x => x.VisualizeProgress());
        }

        public bool historyStateHierarchyVisualisation;
        GameObject multiProcessSearchAlgorithmRunnerVizGo;
        public GameObject MultiProcessSearchAlgorithmRunnerVizGo
        {
            get
            {   
                if(!multiProcessSearchAlgorithmRunnerVizGo)multiProcessSearchAlgorithmRunnerVizGo = new GameObject("MPSAR ALGO HISTORY created: "+System.DateTime.Now);
                return multiProcessSearchAlgorithmRunnerVizGo;
            }
        }

        Transform GetActiveStateHierachyVizTransform()
        {
            Transform current;
            current = MultiProcessSearchAlgorithmRunnerVizGo.transform;
            while (true)
            {
                current = current.GetChild(current.childCount - 1);
                if(current.childCount == 0)
                {
                    return current;
                }
            }
        }

       // public void AddHistoryState(HistoryState added)
       // {
       //     TryVisualizeHistory();
       // }
       //
   

        public void ClearHistoryStates()
        {
            if (historyStateHierarchyVisualisation) DestroyImmediate(MultiProcessSearchAlgorithmRunnerVizGo);
            m_historyStates.Clear();
        }


        void TryVisualizeHistory()
        {
            if (!historyStateHierarchyVisualisation)
            {
                return;
            }
            DestroyImmediate(multiProcessSearchAlgorithmRunnerVizGo);
            int lastSubProcessCount  = 1 ;
            Transform historyStateGo = null;
            Transform parent = MultiProcessSearchAlgorithmRunnerVizGo.transform;
            foreach (var item in historyStates)
            {
                if (lastSubProcessCount < item.currentSubProcessCount)
                {
                    parent = historyStateGo.transform;
                }
                else if(lastSubProcessCount > item.currentSubProcessCount)
                {
                    parent = historyStateGo.transform.parent.parent;
                }
                lastSubProcessCount = item.currentSubProcessCount;
                string isCurrentStr = ActiveJob == item.state ? " current" : "";
                historyStateGo = new GameObject("state:" + (item.state.OpenCount == 0 ? " finished" : " " + item.state.GetCurrent()) + isCurrentStr).transform;
                historyStateGo.transform.SetParent(parent);
            }
#if UNITY_EDITOR
            UnityEditor.EditorGUIUtility.PingObject(GetActiveStateHierachyVizTransform());
#endif 
        }
        /// <summary>
        /// recovering from time travel takes one step - so in that sense forward stepping can "stumble"
        /// </summary>
        /// <returns>false IF job is COMPLETED</returns>
        public bool TryStepForward()
        {
            if (actionsTakenRecord != null)
            {
                actionsTakenRecord.actionsTaken.Add(MultiProcessAlgoTestRecording.MPSARActionType.StepForward);
            }
            if (finalState != null)
            {
                Debug.Log("finished");
            }

            if (stepsTravelledBackwardInTime > 0)
            {
                // if travelled backward in history make history the present by clearing the future
                for (int i = 0; i < stepsTravelledBackwardInTime; i++)
                {
                    m_historyStates.RemoveAt(historyStates.Count - 1);
                }
                stepsTravelledBackwardInTime = 0;
            }

            
            // if this is done before stepping it ensures that current progress is never in history
            TryAddHistory();
            if (TopMostProcess().subProcessStateType == SubProcessStateType.CloseCurrentBeforeContinuing)
            {
                TopMostProcess().CloseCurrent();
                TopMostProcess().subProcessStateType = SubProcessStateType.NoSubProcess;
            }

            if (TopMostProcess().subProcessStateType == SubProcessStateType.NoSubProcess)
            {
                StepResult stepResult = TopMostProcess().Step(null);
                if (stepResult.searchStepResultType == SearchStepResultType.StopSearch || stepResult.searchStepResultType == SearchStepResultType.FoundIt)
                {
                    if (allProcesses.Count == 1)
                    {
                        finalState = TopMostProcess();
                    }
                    lastCompletedSubJob = TopMostProcess();
                    allProcesses.RemoveAt(allProcesses.Count - 1);
                    if (allProcesses.Count > 0)
                    {
                        TopMostProcess().subProcessStateType = SubProcessStateType.PendingPostProcessing;
                    }

                }
                else if (stepResult.searchStepResultType == SearchStepResultType.NewSubProcess)
                {
                    TopMostProcess().subProcessStateType = SubProcessStateType.SubProcessInProgress;
                    allProcesses.Add(stepResult.newChildProcess);
                }
            } else
            {
                if (TopMostProcess().subProcessStateType == SubProcessStateType.SubProcessInProgress) throw new System.NotSupportedException();
                TopMostProcess().Step(lastCompletedSubJob);
                TopMostProcess().subProcessStateType = SubProcessStateType.CloseCurrentBeforeContinuing;
            }
            TryVisualizeHistory();
            return allProcesses.Count > 0;
        }

        private IMultiProcessSearchAlgorithm TopMostProcess()
        {
            return allProcesses[allProcesses.Count - 1];
        }

        private void TryAddHistory()
        {
            if (mode != Mode.Production)
            {
                var progressClone = allProcesses[allProcesses.Count - 1].SemiDeepCopy();
                m_historyStates.Add(new HistoryState(allProcesses.Count, progressClone));
            }
        }

        [ToivonRandomUtils.InspectorButton]
        public void VisualizeCurrentProgress()
        {
            allProcesses.ForEach(x => x.VisualizeProgress());
        }


        [ToivonRandomUtils.InspectorButton]
        public bool StepBackwards()
        {
            if (actionsTakenRecord)
            {
                actionsTakenRecord.actionsTaken.Add(MultiProcessAlgoTestRecording.MPSARActionType.StepBackward);
            }
            if (historyStates.Count - stepsTravelledBackwardInTime == 0)
            {
                Debug.Log("is last history state");
                return false;
            }
            stepsTravelledBackwardInTime++;
            var newCurrentState = historyStates[CurrentHistoryStateIndex];
            allProcesses.Clear();
            // populate all processes list with newest items in each timeline
            // note special for loop
            for (int i = 1; i <= newCurrentState.currentSubProcessCount; i++)
            {
                
                // reverse for because that way you can be sure its the latest
                for (int j = CurrentHistoryStateIndex; j >= 0; j--)
                {
                    if (historyStates[j].currentSubProcessCount == i)
                    {
                        allProcesses.Add(historyStates[j].state);
                        break;
                    }
                }
            }
            if (allProcesses.Count == 0) throw new System.NotSupportedException();
            TryVisualizeHistory();
            return true;
        }

        private int CurrentHistoryStateIndex
        {
            get
            {
                if (stepsTravelledBackwardInTime == 0) throw new System.NotSupportedException();
                return historyStates.Count - stepsTravelledBackwardInTime;
            }
        }

        public enum Mode{
               Production,
               DebugRunOnUpdate,
               DebugRunManually
        }

        
        public void StartNewRootProcess(IMultiProcessSearchAlgorithm algo)
        {
            historyStates = m_historyStates.AsReadOnly();
            finalState = null;
            stepsTravelledBackwardInTime = 0;
            allProcesses.Clear();
            ClearHistoryStates();
            if (actionsTakenRecord != null) actionsTakenRecord.actionsTaken.Clear();
            allProcesses.Add(algo);
        }

        public void StartNewProcessAndRunImmediatly(IMultiProcessSearchAlgorithm algo)
        {
            StartNewRootProcess(algo);
            while (TryStepForward())
            {
            }
        }

        [InspectorButton]
        public void RunRecordedActions()
        {
            gameObject.AddComponent<EditorJobRunner>().job =  RunActionsTakening(runActionsTaken);
        }

        public IEnumerator RunActionsTakening(MultiProcessAlgoTestRecording actions)
        {
            for (int i = 0; i < actions.actionsTaken.Count; i++)
            {
                if (actions.runToIndex == i) break;
                var item = actions.actionsTaken[i];
                switch (item)
                {
                    case MultiProcessAlgoTestRecording.MPSARActionType.VisualizeCurrent:
                        VisualiseCurrent();
                        break;
                    case MultiProcessAlgoTestRecording.MPSARActionType.StepBackward:
                        StepBackwards();
                        break;
                    case MultiProcessAlgoTestRecording.MPSARActionType.StepForward:
                        TryStepForward();
                        break;
                }
                yield return null;
            }
        }

        public bool Running
        {
            get { return allProcesses.Count > 0; }
        }

        public IMultiProcessSearchAlgorithm ActiveJob
        {
            get
            {

                if (stepsTravelledBackwardInTime == 0)
                {
                    if (finalState != null)  // running is finished, hence active job is null
                    {
                        return null;
                    }
                    return allProcesses[allProcesses.Count - 1];
                }
                return historyStates[CurrentHistoryStateIndex].state;
            }
        }


        public class HistoryState
        {
            public int currentSubProcessCount;
            public IMultiProcessSearchAlgorithm state;

            public HistoryState(int currentSubProcessCount, IMultiProcessSearchAlgorithm state)
            {
                this.currentSubProcessCount = currentSubProcessCount;
                this.state = state;
            }
        }

    }
}