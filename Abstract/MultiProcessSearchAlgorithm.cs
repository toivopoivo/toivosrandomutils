﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ToivonRandomUtils;
using UnityEngine;
namespace ToivoAlgo
{
    public enum SubProcessStateType
    {
        NoSubProcess,
        SubProcessInProgress,
        PendingPostProcessing,
        CloseCurrentBeforeContinuing
    }
    public abstract class MultiProcessSearchAlgorithm<T>: IMultiProcessSearchAlgorithm
    {
        public SubProcessStateType subProcessStateType{ get; set; }
        public StepResult lastStepResult;

        protected abstract void VizCurrentState();

        public StepResult Step(IMultiProcessSearchAlgorithm completedSubJobs)
        {
            if (completedSubJobs != null)
            {
                lastStepResult = PostProcessSubJob(completedSubJobs);
            }
            else
            {
                if (OpenCount == 0)
                {
                    Debug.Log(GetType().Name + " OpenCount is 0 - giving up");
                    return new StepResult(SearchStepResultType.StopSearch);
                }
                lastStepResult = EvaluateCurrentResult();
                // toivo 24.8.2018 trying not closing current if post processsing subjob. theory behind this is that because how runner collects history this can cause mutilation of history
                if (lastStepResult.searchStepResultType == SearchStepResultType.Continue) CloseCurrent();
            }
           //if (lastStepResult.searchStepResultType == SearchStepResultType.Continue) CloseCurrent();
            return lastStepResult;
        }
        public abstract StepResult EvaluateCurrentResult();
        public abstract StepResult PostProcessSubJob(IMultiProcessSearchAlgorithm job);

        public abstract int OpenCount
        {
            get;
        }

        public abstract T Current
        {
            get;
        }
        public abstract void CloseCurrent();

        public void VisualizeProgress()
        {
            if(OpenCount == 0)
            {
                return;
            }
            Debug.Log(GetType().Name + " "+Current +" " + lastStepResult.searchStepResultType + " OpenCount " + OpenCount);
            VizCurrentState();
        }


        private List<FieldInfo> importantFields;
        public IMultiProcessSearchAlgorithm SemiDeepCopy()
        {
            var clone = MemberwiseClone();
            if (importantFields == null)
            {
                var bindingFlags = System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Public;
                importantFields = GetType().GetFields(bindingFlags).ToList();
                importantFields.RemoveAll(x => x.GetCustomAttributes(typeof(ImportantAlgoProgress), false).Length == 0);
                Debug.Assert(importantFields.Count != 0, "failed to find any open items list on type - did you put OpenItemsList attribute in your open items list?");
            }

            foreach (var item in importantFields)
            {
                var val = item.GetValue(this);
                if (val == null)
                {
                    continue;
                }
                var iList = val as IList;
                item.SetValue(clone, iList.CreateCopyList() );
                
            }
            return clone as IMultiProcessSearchAlgorithm;
        }

        object IMultiProcessSearchAlgorithm.GetCurrent()
        {
            if (OpenCount == 0) return null;
            return this.Current;
        }
    }

    public struct StepResult
    {
        public SearchStepResultType searchStepResultType;
        public IMultiProcessSearchAlgorithm newChildProcess;

        public StepResult(SearchStepResultType searchStepResult)
        {
            this.searchStepResultType = searchStepResult;
            this.newChildProcess = null;
        }
        public StepResult(IMultiProcessSearchAlgorithm newChildProcess)
        {
            this.newChildProcess = newChildProcess;
            this.searchStepResultType = SearchStepResultType.NewSubProcess;

        }
    }

    public enum SearchStepResultType
    {
        Continue,
        NewSubProcess,
        FoundIt,
        StopSearch
    }

    public class ImportantAlgoProgress : System.Attribute
    {

    }

    public interface IMultiProcessSearchAlgorithm {
        int OpenCount { get; }
        SubProcessStateType subProcessStateType { get; set; }
        IMultiProcessSearchAlgorithm SemiDeepCopy();
        StepResult Step(IMultiProcessSearchAlgorithm completedSubJob);
        void VisualizeProgress();
        object GetCurrent();
        void CloseCurrent();
    }
}