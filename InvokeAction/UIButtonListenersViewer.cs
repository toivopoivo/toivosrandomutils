﻿using UnityEngine;
using UnityEditor;
using UnityEngine.Events;
using System.Collections.Generic;
using System;

namespace ToivonRandomUtils
{
    public class UIButtonListenersViewer : MonoBehaviour, IOnInspectorGUI
    {
#if UNITY_EDITOR
        int maxReflectionDepth = 3;
        public void OnInspectorGUI()
        {
            EditorGUILayout.HelpBox("isnt it annoying that you cant see what functions are registered to a certain button? well this component tries to fix it. just add this to a button game object and see what happens.", MessageType.Info);
            //var type = GetComponent<UnityEngine.UI.Button>().onClick.GetType();
            var type = typeof(UnityEventBase);
            ReflectAndDisplay(type, GetComponent<UnityEngine.UI.Button>().onClick);
            //   foreach (var item in (GetComponent<UnityEngine.UI.Button>().onClick.AddListener)
            // {
            //     //EditorGUILayout.TextField(item.Name, item.GetValue(GetComponent<UnityEngine.UI.Button>().onClick).NullSafeToString() );
            //     EditorGUILayout.HelpBox(item.FieldType + " " + item.Name, MessageType.None);
            //  }
        }

        private void ReflectAndDisplay(Type type, object target)
        {
            if (EditorGUI.indentLevel == maxReflectionDepth) return;
            EditorGUI.indentLevel++;
            var fields = type.GetFields(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.FlattenHierarchy);
            //   var props = type.GetProperties(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.FlattenHierarchy);
            //EditorGUILayout.LabelField("fields");
            EditorGUI.BeginDisabledGroup(true);
            foreach (var item in fields)
            {
                //EditorGUILayout.TextField(item.Name, item.GetValue(GetComponent<UnityEngine.UI.Button>().onClick).NullSafeToString() );
                object val = item.GetValue(target);
                if (item.Name.Contains("alls") || item.Name.Contains("ize"))
                {
                    if (val is int) EditorGUILayout.IntField(item.Name, (int)val);
                    else EditorGUILayout.LabelField(item.Name);

                }
                if(val != null) ReflectAndDisplay(val.GetType(), val);
            }
            EditorGUI.EndDisabledGroup();
          //    EditorGUILayout.LabelField("properties");
        //   foreach (var item in props)
        //   {
        //       try
        //       {
        //           //EditorGUILayout.TextField(item.Name, item.GetValue(GetComponent<UnityEngine.UI.Button>().onClick).NullSafeToString() );
        //           object val = item.GetValue(target);
        //           if (item.Name.Contains("alls") || item.Name.Contains("ize") ) EditorGUILayout.HelpBox(item.PropertyType + " " + item.Name + " " + val, MessageType.None);
        //           if (val != null) ReflectAndDisplay(val.GetType(), val);
        //       }
        //       catch (Exception ex) 
        //       {
        //       //    EditorGUILayout.HelpBox(item.PropertyType + " " + item.Name + " FAILED GETTING VALUE" + ex, MessageType.Error);
        //       }
        //       
        //   }
            EditorGUI.indentLevel--;
        }
#endif
    }

}