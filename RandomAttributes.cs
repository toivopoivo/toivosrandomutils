﻿using UnityEngine;
using System.Collections;

public class EnumFilter : PropertyAttribute
{
	public System.Type type;

	public EnumFilter (System.Type type)
	{
		this.type = type;
	}
}

public class HelpBox : PropertyAttribute
{
	public string message;
	
	public HelpBox (string message)
	{
		this.message = message;
	}
}
public class EnumConditionalInspectorField:PropertyAttribute
{
	public string governingEnumFieldName;
	public int[] requireValues;
	public EnumConditionalInspectorField(string governingEnumFieldName, params int[] requireValues)
	{
		this.governingEnumFieldName = governingEnumFieldName;
		this.requireValues = requireValues;
	}
}

public class DrawAsset : PropertyAttribute
{
	
}
public class ReflectedDebugData:System.Attribute
{

}

public class DebugDataReflectorInvokedMethod:System.Attribute
{

}


public class DateLong : PropertyAttribute
{

}

public class CreateButton : PropertyAttribute
{
	
}

/// <summary>
/// has nice icon
/// </summary>
public class ToivoToolTip : PropertyAttribute
{
    public string message;
    public ColorType? differentColorOnTheFieldWhyNot;

    public ToivoToolTip(string message)
    {
	    this.message = message;
    }
    
    public ToivoToolTip(string message, ColorType colorType )
    {
	    this.differentColorOnTheFieldWhyNot=  colorType;
        this.message = message;
    }
}


/// <summary>
/// see IPimpMyProperty below
/// </summary>
public class DrawAllPropertyDrawers : PropertyAttribute
{
	
}
/// <summary>
/// implement this is in your property drawers so to allow mix and matching different property drawers (for DrawAllPropertyDrawers) 
/// </summary>
public interface IPimpMyProperty
{
#if UNITY_EDITOR
	void PimpMyProperty(Rect position, UnityEditor.SerializedProperty property, GUIContent label,BaseDrawingSettings baseDrawingSettings);
#endif
}

public class BaseDrawingSettings
{
	public bool disableField;
}

public class GameObjectStringReference : PropertyAttribute
{
	
}
