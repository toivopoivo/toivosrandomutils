﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ToivonRandomUtils;
using System.Diagnostics;
using System.Linq;

namespace ToivoDebug
{
	public class StopwatchStats
	{
		public Stopwatch stopWatch = Stopwatch.StartNew ();
		public List<DiagnosticsInterval> intervals = new List<DiagnosticsInterval> ();
		public static StopwatchStats globalStats = new StopwatchStats ();

		public static void TryBeginInterval (string name, bool callStack)
		{
			globalStats.BeginInterval (name, callStack);
		}

		public static void TryEndInterval ()
		{
			globalStats.BeginInterval ("UNMAPPED", false);

#if UNITY_EDITOR
			if (!logging)
                UnityEditor.EditorApplication.delayCall += LogDelayed;

#else
            if (!logging)
                InvokeAction.Invoke(LogDelayed, 0); 
#endif

            logging = true;

		}

		static void LogDelayed ()
		{
			globalStats.LogResults ();
			globalStats = new StopwatchStats ();
		}

		static bool logging = false;




		public void BeginInterval (string name, bool callStack)
		{
			long time = stopWatch.ElapsedMilliseconds;
			if (intervals.Any ()) {
				intervals.Last ().endStampMs = time;
			}
			var diagnosticsInterval = new DiagnosticsInterval ();
			intervals.Add (diagnosticsInterval);
			diagnosticsInterval.name = name;


			if (callStack)
				diagnosticsInterval.stackTrace = new StackTrace ();


//			UnityEngine.Debug.Log (string.Format ("dont draw conclusion from here yet --- {0} time (ms){1}", name, stopWatch.ElapsedMilliseconds));
			diagnosticsInterval.startedStampMs = stopWatch.ElapsedMilliseconds;
		}


		public void LogResults ()
		{
			logging = false;
			intervals.Last ().endStampMs = stopWatch.ElapsedMilliseconds;

			var data = intervals.GroupBy (x => x.GetLongName ()).Select (x => new {interval = x, timeTaken = x.Sum (y => y.EllapsedMillis)}).OrderBy (x => x.timeTaken);
			foreach (var item in data) {
				UnityEngine.Debug.Log (string.Format ("{1}ms, calls during frame:{2} {3} {0}", item.interval.Key, item.timeTaken, item.interval.Count (),System.DateTime.Now.ToString("HH:mm:ss")));

            }
		}
	}

	public class DiagnosticsInterval
	{
		public StackTrace stackTrace {
			get;
			set;
		}

		public string name;

		public long endStampMs {
			get;
			set;
		}

		public long startedStampMs {
			get;
			set;
		}

		public long EllapsedMillis {
			get {
				return endStampMs - startedStampMs;
			} 
		}
		public string GetLongName ()
		{
			if (stackTrace != null) {
				return name + " " + stackTrace.ToStringLong();
			} else {
				return name;
			}
		}
	}

}