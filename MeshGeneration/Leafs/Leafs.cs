﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ToivonRandomUtils;
using ToivoMeshGeneration;
[RequireComponent(typeof(MeshFilter))]
public class Leafs : MonoBehaviour {
	public enum LeafGeneratingModeType{ UseExsistingMesh, Spherical }
	public float leafHalfSize=0.08f;
	public float bottomOffSetting=0.06f;
	public float randomness=0.08f;
	public float minDist=0.2f;
	public LeafGeneratingModeType mode;
	public Vector3 upDirection = new Vector3 (0, 1, 0);

	//keyframes for the curve in case lost
	//x 0.143 y 1
	//x 0.8 y 0.5 
	//x 1 y 1 
	public AnimationCurve leafDirectionChange;

	[Header("Spherical mode settings")]
	public Vector3 direction;
	public float diameter=1;
	public int rows=16;
	public int lines=16;
	
	public bool halfSpherical;

	[Header("Debug stuff")]
	public bool randomDistance=true;
	public bool updateRedraw=false;

	[HideInInspector]
	public Mesh oldMesh;
	void Start () {
		ReDraw ();

	}
	void Update(){
		if (updateRedraw)
			ReDraw ();
	}
	[ContextMenu("Redraw")]
	void ReDraw(){
		Vector3[] leafPositions;
		int leafCount=0;
		MeshFilter meshF=GetComponent<MeshFilter>();
		float highestPos=0f;
		if (mode == LeafGeneratingModeType.UseExsistingMesh) {
			if (oldMesh == null) {
				oldMesh = meshF.mesh; 
			}
			leafPositions = oldMesh.vertices;
			leafCount = leafPositions.Length;
			for (int i = 0; i < leafCount; i++) {
				if(leafPositions[i].y>highestPos){
					highestPos=leafPositions[i].y;
				}
			}
		} else {
//			leafPositions = new Vector3[rows*lines];
//			for (int m = 0; m < rows; m++) {
//				for (int n = 0; n < lines; n++) {
//					//				float angle = (float)i / (halfSpherical ? 180f : 360f );
//					//				leafPositions[i]=Vector3 right
//					leafPositions[n*m] = new Vector3(Mathf.Sin(Mathf.PI * m/rows)*Mathf.Cos(2*Mathf.PI * n/lines) ,
//					                              Mathf.Sin(Mathf.PI * m/rows)* Mathf.Sin(2*Mathf.PI * n/lines),
//			                           		      Mathf.Cos(Mathf.PI * m/rows)) *radius;
//					if(halfSpherical){
//						leafPositions[n*m]=new Vector3(Mathf.Abs( leafPositions[n*m].x), Mathf.Abs(leafPositions[n*m].y), Mathf.Abs(leafPositions[n*m].z ));
//					}
//				}
//			}
			leafPositions = new Vector3[lines*rows];
			int index=0;
			for (float i = 0; i < 1f; i+=0.1f) {
				index=DrawCircle (leafPositions, index,Mathf.Sin( i *Mathf.PI)*diameter  , i*diameter*2-diameter, lines, false); 
			}
			highestPos = diameter;
			leafCount = leafPositions.Length;
		}

		MeshGenerationData newMeshData=new MeshGenerationData();
		List<float> magnitudes=new List<float>();
		float furthestMagnitude=0;
		for (int i = 0; i < leafCount; i++) {
			//			Debug.DrawRay(Vector3.zero, oldMesh.vertices[i],Color.blue, 10f);
			Vector3 leafPosition = leafPositions[i]*(minDist+(1.0f-minDist)* (randomDistance ? Random.value : 1) );
			float mag =leafPosition.magnitude;
			magnitudes.Add(mag);
			if(furthestMagnitude<mag){
				furthestMagnitude=mag;
			}
			Leaf(newMeshData, new Vector3(leafPositions[i].x, 0, leafPositions[i].z).normalized , leafPosition, bottomOffSetting , leafHalfSize, upDirection, leafDirectionChange.Evaluate(leafPositions[i].y/highestPos ) ) ;
		}
		for (int i = 0; i < leafCount; i++) {
			newMeshData.AddVertexColor(new Color(magnitudes[i]/furthestMagnitude,0,0,0));
			newMeshData.AddVertexColor(new Color(magnitudes[i]/furthestMagnitude,0,0,0)); 
			newMeshData.AddVertexColor(new Color(magnitudes[i]/furthestMagnitude,0,0,0)); 
			newMeshData.AddVertexColor(new Color(magnitudes[i]/furthestMagnitude,0,0,0)); 
		}
		
		meshF.mesh = newMeshData.Mesh;
	}
	static int DrawCircle(Vector3[] posArray, int index, float diameter, float yPos, int verts, bool debugMode){
		for (int i = 0; i < 2; i++) {
			int zMul=(i==0?-1:1);
			for (int l = 0; l < verts/2; l++) {
				float progress = (float)l/(verts/2);
				float zVal= Mathf.Sin(Mathf.PI*progress) * zMul;
				float xVal= (Mathf.Cos(Mathf.PI*progress))-1f;
				posArray[index] = new Vector3( xVal * diameter, yPos, zVal * diameter ) + new Vector3(diameter,0,0) ;
				if(debugMode){
					posArray[index].DrawDebugPoint(Color.red);
				}
				index++;
			}
		}
		return index;
	}
//
	public static void Leaf (MeshGenerationData meshData, Vector3 direction, Vector3 position, float bottomOffSetting ,float halfQuadSize, Vector3 upDirection, float topMultiplier ){
		Vector3 right = Vector3.Cross (direction, upDirection).normalized;
		int index = meshData.verts.Count;
		meshData.verts.AddMultiple (
			position*topMultiplier + (right*halfQuadSize),
			position*topMultiplier - (right*halfQuadSize),
			position + (right*halfQuadSize)-upDirection*halfQuadSize*2f*topMultiplier+ direction  * bottomOffSetting,
			position - (right*halfQuadSize)-upDirection* halfQuadSize*2f*topMultiplier+ direction * bottomOffSetting
		); 
		meshData.tris.AddMultiple (
			index + 3, index+1 , index + 2,
			index, index + 2,index + 1
		);

		meshData.normals.AddMultiple (direction, direction, direction, direction);

		meshData.uvs.AddMultiple (
			new Vector2 (1f, 1f),
			new Vector2 (0f, 1f),
			new Vector2 (1f, 0f),
			new Vector2 (0f, 0f)
		);
//		
	}
	
}
