﻿using UnityEngine;
using System.Collections;
using ToivonRandomUtils;
using ToivoMeshGeneration;
public class QuadLine : MonoBehaviour {
	void Start(){
		var pointA = Vector3.zero;//.DrawDebugPoint ();
		var pointB = (Vector3.forward * 3 + Vector3.up + Vector3.left);//.DrawDebugPoint ();

		var data = GetQuadLine (pointA, pointB, 0.3f, Vector3.up);
		MeshGenerationUtils.MakeGameObjectWithReadyMeshFilter ().mesh=data.Mesh;
	}

	public static MeshGenerationData GetQuadLine(Vector3 pointA, Vector3 pointB,float width, Vector3 normalDirection ) {
		var cross = Vector3.Normalize( Vector3.Cross (pointA - pointB, normalDirection	) );//.DrawDebugPoint (Color.white);
		var meshData = new MeshGenerationData ();
		meshData.verts.AddMultiple (pointA+cross*width, pointA+(-cross*width));
		meshData.verts.AddMultiple (pointB+cross*width, pointB+(-cross*width));
		meshData.tris.AddMultiple (0, 1, 3);
		meshData.tris.AddMultiple (2, 0, 3);

		for (int i = 0; i < meshData.verts.Count; i++) {
			meshData.normals.Add (normalDirection);
		}
		meshData.uvs.AddMultiple (
			new Vector2 (1f, 1f),
			new Vector2 (0f, 1f),
			new Vector2 (1f, 0f),
			new Vector2 (0f, 0f)
		);

		return  meshData;
	}


	public static MeshGenerationData ContinueQuadLine (MeshGenerationData meshData, Vector3 pointA, Vector3 pointB, float width, Vector3 normalDirection, bool getWeird=false){
		var newData = GetQuadLine (pointA, pointB, width, normalDirection);
		if (getWeird) {
			newData.verts [0] = meshData.verts [meshData.verts.Count - 1]; // nää kaks voi olla toiste päi ehk
			newData.verts [1] = meshData.verts [meshData.verts.Count - 2]; //
		} else {
			newData.verts [1] = meshData.verts [meshData.verts.Count - 1]; 
			newData.verts [0] = meshData.verts [meshData.verts.Count - 2]; 
		}
		meshData.AppendData( newData);
		return meshData;
	}

}
