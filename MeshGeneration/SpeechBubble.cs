﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ToivoMeshGeneration;
using ToivonRandomUtils;

[ExecuteInEditMode]
public class SpeechBubble: MonoBehaviour
{
	//	public Vector3 boundss;
	//	public Vector3 normalDirection;
	public Transform tiiTarget;
	public bool lockTiiStart;
	public int cornerSteps = 3;
	//	public int horizontalSteps = 4;
	//	public int verticalSteps = 2;
	public Vector2 outlineSize = Vector2.one * 0.2f;
	public float tiiOutlineBonus = 0.2f;
	//	public Transform tiiTiiTarget;
	public Vector3 size = new Vector3 (1f, 1f, 1f);
	[RangeAttribute (0f, 1f)]
	public float cornerSize;
	[UnityEngine.Serialization.FormerlySerializedAs ("mesh")]
	public MeshFilter meshFilter;
	int tiiStartIndex = 0;
	public Color insideColor = Color.white;
	public Color outlineColor = Color.black;
	public bool drawOutline = true;
	//	public Vector3 tiiPos = Vector3.up + Vector3.left;
	//	List<KeyValuePair<int, int>> midSectionStartEndPoints = new List<KeyValuePair<int,int>> ();
	[ToivonRandomUtils.InspectorButton]

	Vector3 lastTiiPos = new Vector3 (-100, -100, -100);
	Color lastInsideColor;
	Color lastOutlineColor;

	void Update ()
	{
		var tii = tiiTarget.transform.localPosition.MultiplyAxises (1, 1, 0);
		if (Application.isPlaying && lastTiiPos == tii && IsColorSame (insideColor, lastInsideColor) && IsColorSame (outlineColor, lastOutlineColor)) {
			return;
		} else {
			lastTiiPos = tii;
			lastInsideColor = insideColor;
			lastOutlineColor = outlineColor;
		}
		var meshGenData = new MeshGenerationData ();
		meshGenData.AppendData (GetBubble (insideColor, size, tii));
		for (int i = 0; i < meshGenData.verts.Count; i++) {
			meshGenData.verts [i] = meshGenData.verts [i] + Vector3.forward * size.z;
		}
		if (drawOutline)
			meshGenData.AppendData (GetBubble (outlineColor, size + (Vector3)outlineSize, tii + tii * tiiOutlineBonus));
//		while (true) {

		meshFilter.mesh = meshGenData.Mesh;

//			DrawVertices (vertices);
//		}
	}

	bool IsColorSame (Color colA, Color colB)
	{
		if (colA.a == colB.a && colA.r == colB.r && colA.b == colB.b && colA.g == colB.g)
			return true;
		return false;
	}

	MeshGenerationData  GetBubble (Color col, Vector3 size, Vector3 tiiPos)
	{
		var meshGenData = new MeshGenerationData ();
		List<Vector3> vertices;
		meshGenData.verts = vertices = new List<Vector3> ();
		vertices.Add (Vector3.zero);
		var scaledUp = Vector3.up * size.y;
		var scaledDown = Vector3.down * size.y;
		var scaledLeft = Vector3.left * size.x;
		var scaledRight = Vector3.right * size.x; 
		vertices.Add (scaledUp);

		//		4	|	0
		//		-		-	
		//		3	|	1
		vertices.AddRange (GetCornerPolys (scaledUp + scaledRight,
			scaledUp + scaledRight * cornerSize,
			scaledRight + scaledUp * cornerSize)
		);
		//			yield return DrawVertices (vertices, 1);
		//			yield return new WaitForSeconds (0.5f);
		//			
		vertices.Add (scaledRight);

		vertices.AddRange (GetCornerPolys (scaledDown + scaledRight,
			scaledRight + scaledDown * cornerSize,
			scaledDown + scaledRight * cornerSize)
		);

		//			yield return  DrawVertices (vertices, 1);
		//			yield return new WaitForSeconds (0.5f);

		vertices.Add (scaledDown);

		vertices.AddRange (GetCornerPolys (scaledDown + scaledLeft,
			scaledDown + scaledLeft * cornerSize,
			scaledLeft + scaledDown * cornerSize)
		);
		//			yield return DrawVertices (vertices, 1);
		//			yield return new WaitForSeconds (0.5f);

		vertices.Add (scaledLeft);

		vertices.AddRange (GetCornerPolys (scaledLeft + scaledUp,
			scaledLeft + scaledUp * cornerSize,
			scaledUp + scaledLeft * cornerSize)
		);
		//			yield return DrawVertices (vertices, 1);
		//			yield return new WaitForSeconds (0.5f);

		//			vertices.Add (Vector3.up * size.x);

		//			Vector3 tiiPos = transform.InverseTransformPoint (tiiTarget.transform.position);
		if (!lockTiiStart) {
			float currentClosestDistance;
			currentClosestDistance = Vector3.Distance (vertices [tiiStartIndex], tiiPos);  
			for (int i = 1; i < vertices.Count; i++) {

				var magnitude = (vertices [i] - tiiPos).magnitude;
				if (magnitude < currentClosestDistance) {
					tiiStartIndex = i;
					currentClosestDistance = magnitude;
				}

			}
		}
		vertices [tiiStartIndex] = tiiPos;

		int innerVerticesEnd = vertices.Count;
		// create outline
		//		List<Vector3> newVerts = new List<Vector3> ();
		//		for (int i = 1; i < vertices.Count - 1; i++) {
		////			Vector3 cross;
		//			Vector3 cross = vertices [i].normalized;
		//			;
		//			if (i != tiiStartIndex) {
		//				//				if (vertices.Count < i + 2) {
		//				//					cross = Vector3.Cross (Vector3.Lerp ((vertices [i] - vertices [i + 1]), vertices [i] - vertices [i + 2], 0.5f).normalized, Vector3.forward);
		//				//				} else {
		//				//					cross = Vector3.Cross (Vector3.Lerp ((vertices [i] - vertices [i + 1]), vertices [i] - vertices [1], 0.5f).normalized, Vector3.forward);
		//				//				}
		////				cross = Vector3.Cross (Vector3.Lerp (cross, (vertices [i] - vertices [i + 1]).normalized, 0.5f), Vector3.forward);
		//				cross = Vector3.Cross (vertices [i] - vertices [i + 1].normalized, Vector3.forward);
		//
		//			}
		////			else {
		////				cross = vertices [tiiStartIndex].normalized;
		////			}
		//			newVerts.Add (vertices [i] + cross * outlineSize);
		//		}
		//		vertices.AddRange (newVerts);

		//  triagles for inner verts

		meshGenData.tris = new List<int> (vertices.Count * 3);
		for (int i = 1; i < innerVerticesEnd - 1; i++) {
			meshGenData.tris.AddMultiple (i, i + 1, 0);
		}
		meshGenData.tris.AddMultiple (1, 0, innerVerticesEnd - 1);

		// tris for outline verts
		//		int newVertsCount = newVerts.Count;
		//		for (int i = innerVerticesEnd; i < vertices.Count - 1; i++) {
		//			meshGenData.tris.AddMultiple (i, i + 1, i - newVertsCount);
		//			meshGenData.tris.AddMultiple (i + 1, (i + 1) - newVertsCount, i - newVertsCount);
		//			meshGenData.colors.Add (Color.black);
		//		}
		//		for (int i = meshGenData.colors.Count; i < vertices.Count; i++) {
		//			meshGenData.colors.Add (Color.white);
		//		}
		meshGenData.colors = new List<Color> ();
		meshGenData.normals = new List<Vector3> (vertices.Count);
		for (int i = 0; i < vertices.Count; i++) {
			meshGenData.normals.Add (Vector3.forward);
			meshGenData.colors.Add (col);
		}
		return meshGenData;
		//
	}

	void DrawVertices (List<Vector3> vertices, float time = -1f)
	{
		
		for (int i = 0; i < vertices.Count - 1; i++) {
			if (time == -1f)
				Debug.DrawLine (vertices [i], vertices [i + 1], Color.blue);
			else
				Debug.DrawLine (vertices [i], vertices [i + 1], Color.blue, time);
//			yield return new WaitForSeconds (0.1f);
		}
	}

	public enum Axises
	{
		X,
		Y,
		XY

	}

	//	List<Vector3> GetCornerPolys (int x1, int y1, int x2, int y2, int x3, int y3)
	//	{
	//		return GetCornerPolys (new Vector3 (one.x * x1, one.y * y1),
	//			new Vector3 (one.x * x2, one.y * y2),
	//			new Vector3 (one.x * x3, one.y * y3)
	//		);
	//	}


	Vector3 MulSize (int xMul, int yMul)
	{
		var halfSize = size / 2f;
		return Mul (new Vector3 (halfSize.x, halfSize.y, 0), (float)xMul, (float)yMul);
	}
	//
	Vector3 Mul (Vector3 input, float xMul, float yMul)
	{
		return new Vector3 (input.x * xMul, input.y * yMul);
	}

	List<Vector3> GetCornerPolys (Vector3 corner, Vector3 start, Vector3 end)
	{
		var rList = new List<Vector3> ();
//		var cornerVector = new Vector3 (x, y, 0);

//		for (float i = 0; i < cornerSteps; i++) {
//			rList.Add (new Vector3 (
//				Mathf.Lerp (0, x, ((float)cornerSteps - i) / (float)cornerSteps),
//				Mathf.Lerp (0, y, ((float)i / (float)cornerSteps)),
//				0)
//			);

//		Debug.DrawLine (start, corner, Color.red, 1f); 
//		Debug.DrawLine (corner, end, Color.red, 1f); 
//		}
		for (float i = 0; i < cornerSteps; i++) {
			var time = i / cornerSteps;
			rList.Add (Vector3.Lerp (Vector3.Lerp (start, corner, time), Vector3.Lerp (corner, end, time), time));
		}
		rList.Add (end);

		return rList;
	}
	//
	//	Vector3 GetBoundsOfTwoVectors (Vector3 a, Vector3 b)
	//	{
	//		float[] values = new float[3];
	//
	//		for (int i = 0; i < 3; i++) {
	//			if (Mathf.Abs (a [i]) > Mathf.Abs (b [i])) {
	//				values [i] = (a [i]);
	//			} else {
	//				values [i] = (b [i]);
	//			}
	//		}
	//
	//		return new Vector3 (values [0], values [1], values [2]);
	//
	//	}


}
