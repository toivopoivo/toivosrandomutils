﻿using UnityEngine;
using System.Collections;
using ToivonRandomUtils;

public class CanvasRendererArrowDrawer : MonoBehaviour
{
	public Material material;
	public float alpha = 1;
	public CanvasRenderer canvasRenderer;
	Vector3 a ;
	Vector3 b ;
	public RectTransform rectTransform;
	/*RectTransform myRectTransform;*/
	public	CanvasRendererArrowDrawer mirrorWidthFrom;
	public float scaleMultiplier = 1;
	public float width;

	public ArrowShadowProperties shadowProperties;
	void Reset(){
		rectTransform = GetComponent<RectTransform> ();
	}
	Vector3[] fourCorners = new Vector3[4];
	void Start(){
	}
	void Update ()
	{
		/*if (myRectTransform != rectTransform) {*/
//			if(myRectTransform ==null)
//				myRectTransform= GetComponent<RectTransform> ();
//			myRectTransform.anchorMax = rectTransform.anchorMax;
//			myRectTransform.anchorMax = rectTransform.anchorMin;
//			myRectTransform.pivot = rectTransform.pivot;
//			myRectTransform.anchoredPosition = rectTransform.anchoredPosition;
//			myRectTransform.sizeDelta = rectTransform.sizeDelta;
		/*}*/
		if(mirrorWidthFrom!=null)
			width = mirrorWidthFrom.width;
		Mesh mesh = new Mesh ();
		 rectTransform.GetLocalCorners(fourCorners);
		a = fourCorners[1];
		b = fourCorners [3] * scaleMultiplier;
		var cross = Vector3.Normalize( Vector3.Cross( a- b, Vector3.back) );
		mesh.vertices = new Vector3[]{a+(-cross*width), a+b, a+ (cross*width) };
		mesh.triangles = new int[]{ 1,0,2};
		material.color = material.color.WithTransparencyOf(alpha);
		canvasRenderer.materialCount = 1;
		canvasRenderer.SetMaterial	(material, 0);
		canvasRenderer.SetMesh (mesh);
	
	}
	[ToivonRandomUtils.InspectorButton]
	void GetComponentCanvasRenderer(){
		canvasRenderer = GetComponent<CanvasRenderer> ();
	}

	[ContextMenu("Create shadow")]
	void CreateShadow(){
		var go = Instantiate (gameObject) as GameObject;
		go.transform.SetParent (transform.parent, true);
		GetComponent<RectTransform>().CopyRectTransformTo( go.GetComponent<RectTransform>() );
		go.GetComponent<CanvasRendererArrowDrawer> ().material = shadowProperties.shadowMaterial;
		go.GetComponent<CanvasRendererArrowDrawer> ().width= shadowProperties.width;
		go.GetComponent<CanvasRendererArrowDrawer> ().scaleMultiplier= shadowProperties.scaleMul;
		go.GetComponent<Animator> ().runtimeAnimatorController = shadowProperties.shadowAnimator;
		go.name = name.Replace ("white", "black");
	}

}
[System.Serializable]
public class ArrowShadowProperties{
	[Header("hack. you probly don't need to care about this stuff")]
	public float width=10f;
	public float scaleMul=1.2f;
	public Material shadowMaterial;
	public RuntimeAnimatorController shadowAnimator;
}