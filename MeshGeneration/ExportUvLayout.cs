﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ToivonRandomUtils;

public class ExportUvLayout : MonoBehaviour
{
	public MeshFilter meshFilter;
	public bool debugVisualize;

	[ToivonRandomUtils.InspectorButton]
	public void Export ()
	{
		if (meshFilter == null) meshFilter = GetComponent<MeshFilter> ();
		var resolution = new ToivonRandomUtils.IntVector2 (512, 512);
		Texture2D txr = new Texture2D (resolution.x, resolution.y);

		var mesh = meshFilter.sharedMesh;
		for (int i = 0; i < mesh.triangles.Length / 3; i++) {
			int index = i * 3;
			DrawLine (txr, GetPixelCoordWithTriangleIndex (mesh, resolution, index), GetPixelCoordWithTriangleIndex (mesh, resolution, index + 1), Color.red);
			if (debugVisualize) Export (txr, "" + (index));
			DrawLine (txr, GetPixelCoordWithTriangleIndex (mesh, resolution, index), GetPixelCoordWithTriangleIndex (mesh, resolution, index + 2), Color.red);
			if (debugVisualize) Export (txr, "" + (index + 1));
			DrawLine (txr, GetPixelCoordWithTriangleIndex (mesh, resolution, index + 1), GetPixelCoordWithTriangleIndex (mesh, resolution, index + 2), Color.red);
			if (debugVisualize) Export (txr, "" + (index + 2));

		}
		#if UNITY_EDITOR
		UnityEditor.EditorUtility.RevealInFinder (Export (txr));
		#endif
	}

	static string Export (Texture2D txr, string suffix = "")
	{
		txr.Apply ();
		byte[] bytes = txr.EncodeToPNG ();
		var path = Application.dataPath + "/../savedUvLayout" + suffix + ".png";
		Debug.Log (path);
		System.IO.File.WriteAllBytes (path, bytes);
		return path;
	}


	Vector2 GetPixelCoordWithTriangleIndex (Mesh mesh, IntVector2 resolution, int triangleIndex)
	{
		Vector2 vertUv;
		try {
			vertUv = mesh.uv [mesh.triangles [triangleIndex]];
		} catch (System.Exception ex) {
			throw ex;
		}
		return new Vector2 (resolution.x * vertUv.x, resolution.y * vertUv.y);
	}


	public static void DrawLine (Texture2D tex, Vector2 p1, Vector2 p2, Color col)
	{
		Vector2 t = p1;
		float frac = 1 / Mathf.Sqrt (Mathf.Pow (p2.x - p1.x, 2) + Mathf.Pow (p2.y - p1.y, 2));
		float ctr = 0;

		while ((int)t.x != (int)p2.x || (int)t.y != (int)p2.y) {
			t = Vector2.Lerp (p1, p2, ctr);
			ctr += frac;
			tex.SetPixel ((int)t.x, (int)t.y, col);
		}
	}

}
