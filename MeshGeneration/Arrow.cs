﻿using UnityEngine;
using System.Collections;
using ToivonRandomUtils;
using ToivoMeshGeneration;
[RequireComponent(typeof( MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
public class Arrow : MonoBehaviour {
	public MeshFilter meshFilter;
	public ArrowData arrowData;
	public Transform endPoint;
	void Reset(){
		meshFilter = GetComponent<MeshFilter> ();
	}
	void Update () {
		if (endPoint!= null) {
			arrowData.endingPoint = endPoint.transform.localPosition;
		}
		meshFilter.mesh = DrawArrowBase (arrowData).Mesh;
	}


	public static MeshGenerationData DrawArrowBase(ArrowData data){
		var cross = Vector3.Normalize (Vector3.Cross ( data.endingPoint- data.startingPoint, data.normalDirection));
		var meshData = new MeshGenerationData ();
		meshData.verts.AddMultiple (data.startingPoint + cross * data.arrowWidth,	 data.endingPoint,	 data.startingPoint,	 data.startingPoint - cross * data.arrowWidth);
		meshData.tris.AddMultiple (0, 1, 2);
		meshData.tris.AddMultiple (1, 3, 2);
		for (int i = 0; i < meshData.verts.Count; i++) {
			meshData.normals.Add( data.normalDirection );
		}

		return meshData;
	}
}
[System.Serializable]
public class ArrowData{
	public Vector3 startingPoint;
	public Vector3 endingPoint;
	public float arrowWidth=5;
	public Vector3 normalDirection = Vector3.up;

}