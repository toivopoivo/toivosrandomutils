﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ToivoAlgo;
using ToivoLinq;
using ToivoMeshGeneration;
using ToivonRandomUtils;
using UnityEngine;
namespace ToivoMeshGeneration
{
    public class FillEdgeRingJob : ToivoAlgo.MultiProcessSearchAlgorithm<KeyValuePair<Line, bool>>
    {

        public int debuggedIndex = -1;

        public MeshGenerationData meshGenerationData;
        public MeshGenSelection meshGenSelection;

        public MeshGenSelection newMeshGenSelection = new MeshGenSelection();


        public List<Line> triLines = new List<Line>();
        public List<Line> processedLines = new List<Line>();
        /*if bool is true it means its an outline*/
        [ImportantAlgoProgress]
        public List<KeyValuePair<Line, bool>> nonProcessedLiness;
        public int debugUnprocessedIndex = 0;
        public List<Line> outLines;
        MeshFilter applyMeshTo;

        [ImportantAlgoProgress]
        public List<int> triangleDefinitions;
        public List<Vector3> verts
        {
            get
            {
                return meshGenerationData.verts;
            }
        }

        /// <summary>
        /// excpects the vertices to be in order
        ///  0 1 2
        ///  7   3
        ///  6 5 4
        /// supports even complex concave rings
        /// </summary>
        /// <param name="meshGenSelection"></param>
        /// <returns></returns>
        public FillEdgeRingJob(MeshGenerationData meshGenerationData, MeshGenSelection meshGenSelection, MeshFilter applyMeshTo)
        {
            this.meshGenSelection = meshGenSelection;
            this.meshGenerationData = meshGenerationData;
            outLines = Enumerable.Range(0, meshGenSelection.selectedVertIndexes.Count - 1).Select(i => new Line(i, i + 1, meshGenerationData.verts)).ToList();
            outLines.Add(new Line(meshGenSelection.selectedVertIndexes.Count - 1, 0, meshGenerationData.verts));
            nonProcessedLiness = outLines.Select(x => new KeyValuePair<Line, bool>(x, true)).ToList();
            this.applyMeshTo = applyMeshTo;
        }

        private FillEdgeRingJob()
        {
        }

        public override int OpenCount { get { return nonProcessedLiness.Count; } }

        public override KeyValuePair<Line, bool> Current { get { return nonProcessedLiness[nonProcessedLiness.Count - 1]; } }

        public override void CloseCurrent()
        {
            nonProcessedLiness.RemoveAt(nonProcessedLiness.Count - 1);
        }

        FillEdgeRingJobThirdCornerJob thirdCornerJob;
        Quaternion rightDirection = Quaternion.LookRotation(Vector3.right);

        Line[] twoPossiblyNonProcessedLines;
        bool[] debugLastAddedIndicesWereAddedToUnprocessedLines = new bool[2];

        public override StepResult EvaluateCurrentResult()
        {
            if (thirdCornerJob == null)
            {
                twoPossiblyNonProcessedLines= null;
                var verts = meshGenerationData.verts;
                /*        while (nonProcessedLiness.Count > 0)
                        {
                        */
                debugUnprocessedIndex++;
                //DebugDraw.Cylinder(currentLine.pointA + Vector3.up, currentLine.pointB + Vector3.up, Color.white, "current debugUnprocessedIndex: " + debugUnprocessedIndex, width: 1f);


                processedLines.Add(Current.Key);
                Vector3 innerDir = GetInnerDirection();


                // best index in the inner dir
                // 23.8.2018 (addition) toivo, the thing is you want triangles that form to the right when current processed line is on the outline.
                // however if it is a inner indice then, it can be in any direction
                List<int> bestest;
                if (Current.Value)
                {
                    bestest = meshGenSelection.selectedVertIndexes.Where(x => x != Current.Key.pointAIndex && x != Current.Key.pointBIndex).Where(x => Vector3.Angle(verts[x] - Current.Key.pointA, innerDir) < 45f).OrderBy(x => Vector3.Distance(Current.Key.pointA, verts[x])).ToList();
                }
                else
                {
                    bestest = meshGenSelection.selectedVertIndexes;
                }


                if (bestest.Count == 0)
                {
                    Debug.Log("no bestest found !!! ");
                    return new StepResult(SearchStepResultType.Continue);
                }

                thirdCornerJob = new FillEdgeRingJobThirdCornerJob(bestest, outLines, triLines, verts, Current);
                return new StepResult(thirdCornerJob);
            }
            int thirdCorner = thirdCornerJob.result;
            thirdCornerJob = null;
            if (thirdCorner == -1)
            {
                Debug.Log("all possible third corners overlapped shape lines");
                return new StepResult(SearchStepResultType.Continue);
            }
            //DebugDraw.Sphere(verts[thirdCorner], 2f, Color.white, "found good third corner, debug i" + debugUnprocessedIndex + " processing outline edge " + Current.Value + " thirdcorner index: " + thirdCorner);

            newMeshGenSelection.BeginHot();
            if (Current.Value)
            {
                AddTriangle(Current.Key.pointAIndex, Current.Key.pointBIndex, thirdCorner);
            }
            else
            { /*29.7.2018 toivo it seems triangles get flipped if they generated from outside of outline. just trying my luck if just reversing the triangle indexes fix it*/
                AddTriangle(thirdCorner, Current.Key.pointBIndex, Current.Key.pointAIndex);

            }
                (((Current.Key.pointA + Current.Key.pointB + verts[thirdCorner]) * 0.333f) + Vector3.up * 5f).ShowNotification("debug i: " + debugUnprocessedIndex, 1f, Color.white, 0.006f);
            triLines.Add(new Line(Current.Key.pointAIndex, Current.Key.pointBIndex, verts));

            twoPossiblyNonProcessedLines = new Line[]{
                new Line(Current.Key.pointBIndex, thirdCorner, verts),
                new Line(thirdCorner, Current.Key.pointAIndex, verts)
            };
            bool newUnprocessedLine = false;
            for (int i = 0; i < 2; i++)
            {
                 triLines.Add(twoPossiblyNonProcessedLines[i]);
                 newUnprocessedLine |= debugLastAddedIndicesWereAddedToUnprocessedLines[i] = TryAddToNonProcessedLines(twoPossiblyNonProcessedLines[i]);
            }
            if (newUnprocessedLine)
            {
                var distinctComparer = new DelegateEqualityComparer<KeyValuePair<Line, bool>>((x, y) => x.Key.BothIndexesMatchAnyDirection(y.Key));
                nonProcessedLiness = nonProcessedLiness.Distinct(distinctComparer).ToList();
            }

            return new StepResult(SearchStepResultType.Continue);

        }

        void AddTriangle(int a, int b, int c)
        {
            triangleDefinitions.AddMultiple(a, b, c);
        }

        private Vector3 GetInnerDirection()
        {
            var innerDir = rightDirection * (Current.Key.pointB - Current.Key.pointA).normalized;
            return innerDir;
        }

        protected override void VizCurrentState()
        {
            if (twoPossiblyNonProcessedLines == null)
            {
                DebugDraw.Cylinder(Current.Key.pointA + Vector3.up, Current.Key.pointB + Vector3.up, Color.white, "current debugUnprocessedIndex: " + debugUnprocessedIndex, width: 1f);
                DebugDraw.Cylinder(Current.Key.Middle, Current.Key.Middle + GetInnerDirection(), Color.red, "inner direction");
            } else
            {
                for (int i = 0; i < 2; i++)
                {
                    var possiblyNonProcessedLine = twoPossiblyNonProcessedLines[i];
                    DebugDraw.Cylinder(possiblyNonProcessedLine.pointA, possiblyNonProcessedLine.pointB, debugLastAddedIndicesWereAddedToUnprocessedLines[i] ? Color.yellow : Color.gray, "new unprocessed IF yellow", width: 2f);
                }
                meshGenerationData.tris = triangleDefinitions;
                Mesh mesh = meshGenerationData.Mesh;
                mesh.RecalculateNormals();
                applyMeshTo.mesh = mesh;

            }
        }




        //Debug.Log("setting normals to up");

        /*     Debug.Break();
             List<Line> outputtedUniqueLines = new List<Line>();
             var findingHoles = DetectInnerHoleEdges(outLines, triLines, outputtedUniqueLines);
             while (findingHoles.MoveNext())
             {
                 yield return null;
             }
             yield return null;
             Debug.Break();
             Debug.Log("order into hole grups");
             var holeGroups = OrderIntoHoleGroups(outputtedUniqueLines);

             foreach (var hole in holeGroups)
             {
                 for (int i = 0; i < hole.Count-1; i++)
                 {
                     DebugDraw.Cylinder(hole[i].pointA, hole[i + 1].pointB, Color.red, "hole " + i, 0f, 2.2f);
                 }
                 yield return null;
             }
             */


        /*
        List<IEnumerator<MeshGenSelection>> recurseJobs = new List<IEnumerator<MeshGenSelection>>();

        foreach (var hole in holes)
        {
           //  "todo will not probalby work since i havent yet sorted lines to nice rings"
           //  "also ill have to do a visualization of all these holes"       
            MeshGenSelection recurseSel = new MeshGenSelection();
            recurseSel.selectedVertIndexes = hole.SelectMany(x => new int[] { x.pointAIndex, x.pointBIndex }).Distinct().ToList();
            recurseJobs.Add(FillEdgeRing(recurseSel));
        }

        foreach (var item in recurseJobs)
        {
            Debug.Log("New recursion level");
            Debug.Break();
            yield return null;
            while (item.MoveNext())
            {
                yield return item.Current;
            }
        }
        */

        bool TryAddToNonProcessedLines(Line possiblyNonProcessedLine)
        {
            bool newUnprocessedLine = false;
            if (outLines.Any(x => x.BothIndexesMatchAnyDirection(possiblyNonProcessedLine)))
            {
                return false;
            }
            if (!processedLines.FastAny(x => x.BothIndexesMatchAnyDirection(possiblyNonProcessedLine)))
            {
                nonProcessedLiness.Add(new KeyValuePair<Line, bool>(possiblyNonProcessedLine, false));
                newUnprocessedLine = true;
            }
            else
            {
             //   DebugDraw.Cylinder(possiblyNonProcessedLine.pointA, possiblyNonProcessedLine.pointB, Color.gray, "already processed", width: 2f);
            }

            return newUnprocessedLine;
        }

        public override StepResult PostProcessSubJob(IMultiProcessSearchAlgorithm job)
        {
            throw new System.NotImplementedException();
        }
    }





    public class FillEdgeRingJobThirdCornerJob : MultiProcessSearchAlgorithm<int>
    {
        [ImportantAlgoProgress]
        public List<int> possibleVertIndexes;
        List<Line> outLines = new List<Line>();
        List<Line> triLines = new List<Line>();
        public List<Vector3> verts = new List<Vector3>();
        KeyValuePair<Line, bool> lastNonProcessedPair;

        public FillEdgeRingJobThirdCornerJob(List<int> possibleVertIndexes, List<Line> outLines, List<Line> triLines, List<Vector3> verts, KeyValuePair<Line, bool> lastNonProcessedPair)
        {
            this.possibleVertIndexes = possibleVertIndexes;
            this.outLines = outLines;
            this.triLines = triLines;
            this.verts = verts;
            this.lastNonProcessedPair = lastNonProcessedPair;
        }

        public override int OpenCount
        {
            get
            {
                return possibleVertIndexes.Count;
            }
        }

        public override int Current
        {
            get
            {
                return possibleVertIndexes[possibleVertIndexes.Count - 1];
            }
        }

        protected override void VizCurrentState()
        {
            DebugDraw.Cylinder(verts[Current], verts[Current] + (Vector3.up * 3f), Color.blue, "is this a good third corner?", width: 2f);
        }

        public override void CloseCurrent()
        {
            possibleVertIndexes.RemoveAt(possibleVertIndexes.Count - 1);
        }

        IntersectWithListOfLinesJob findIntersectingOutlineJob;
        IntersectWithListOfLinesJob findIntersectingIndiceJob;
        public int result = -1;
        int outlineIndex = -1;
        int indiceIndex = -1;
        public override StepResult EvaluateCurrentResult()
        {
            if (!lastNonProcessedPair.Value)
            {
                var middleOfIndice = (lastNonProcessedPair.Key.pointA + lastNonProcessedPair.Key.pointB) * 0.5f;
                if (findIntersectingOutlineJob == null)
                {
                    findIntersectingOutlineJob = new IntersectWithListOfLinesJob(outLines, verts[Current], middleOfIndice);
                    return new StepResult(findIntersectingOutlineJob);
                }
                else
                {
                    if (findIntersectingOutlineJob.lastStepResult.searchStepResultType == SearchStepResultType.FoundIt) return new StepResult(SearchStepResultType.StopSearch);
                    if (findIntersectingIndiceJob == null)
                    {
                        findIntersectingIndiceJob = new IntersectWithListOfLinesJob(triLines, verts[Current], middleOfIndice);
                        return new StepResult(findIntersectingIndiceJob);
                    }
                    else
                    {
                        if (findIntersectingIndiceJob.lastStepResult.searchStepResultType == SearchStepResultType.FoundIt) return new StepResult(SearchStepResultType.StopSearch);
                    }
                }
            }

            var newLine1 = new Line(lastNonProcessedPair.Key.pointAIndex, Current, verts);
            var newLine2 = new Line(lastNonProcessedPair.Key.pointBIndex, Current, verts);

            // check does not break shapes outline
            while (outlineIndex < outLines.Count - 1)
            {
                outlineIndex++;
                if (outLines[outlineIndex].IntersectsWith(newLine1, true, Line.Vizmode.OnlyIntersecting) || outLines[outlineIndex].IntersectsWith(newLine2, true, Line.Vizmode.OnlyIntersecting))
                {
                    return new StepResult(SearchStepResultType.Continue);
                }
            }
            int sharedLinesWithOtherIndices = 0;
            while (indiceIndex < triLines.Count - 1)
            {
                indiceIndex++;
                if (triLines[indiceIndex].BothIndexesMatchAnyDirection(newLine1) || triLines[indiceIndex].BothIndexesMatchAnyDirection(newLine2) || triLines[indiceIndex].BothIndexesMatchAnyDirection(lastNonProcessedPair.Key))
                {
                    Debug.Log("is first literally same line so cant break it");
                    if (sharedLinesWithOtherIndices == 1)
                    {
                        Debug.Log("both lines would be shared. triangle would be identical");
                        return new StepResult(SearchStepResultType.Continue);
                    }
                    sharedLinesWithOtherIndices++;
                }
                if (triLines[indiceIndex].IntersectsWith(newLine1, true, Line.Vizmode.OnlyIntersecting) || triLines[indiceIndex].IntersectsWith(newLine2, true, Line.Vizmode.OnlyIntersecting))
                {
                    DebugDraw.Cylinder(triLines[indiceIndex].pointA, triLines[indiceIndex].pointB, Color.red, "breaks this indice", width: 2.2f, debugLog: true);
                    return new StepResult(SearchStepResultType.Continue);
                }
            }
            result = Current;
            return new StepResult(SearchStepResultType.FoundIt);
        }

        public override StepResult PostProcessSubJob(IMultiProcessSearchAlgorithm job)
        {
            throw new System.NotImplementedException();
        }

        //public override StepResult EvaluateCurrentResult()
        //{

        //    bool bad = false;
        //    /*is a indice. indices don't follow the same rules outlines do*/
        //    if (!lastNonProcessedPair.Value)
        //    {
        //        var middleOfIndice = (item.pointA + item.pointB) * 0.5f;
        //        IntersectWithListOfLinesJob outlineCheckJob = new IntersectWithListOfLinesJob(outLines, verts[Current], middleOfIndice);
        //        yield return new StepResult(outlineCheckJob);
        //        bad = outlineCheckJob.lastStepResult.searchStepResultType == SearchStepResultType.FoundIt;
        //        if (!bad)
        //        {
        //            IntersectWithListOfLinesJob indiceCheckJob = new IntersectWithListOfLinesJob(triLines, verts[Current], middleOfIndice);
        //            yield
        //            bad = indiceCheckJob;
        //        }

        //        var newLine1 = new Line(item.pointAIndex, Current, verts);
        //        var newLine2 = new Line(item.pointBIndex, Current, verts);

        //        if (!bad)
        //        {
        //            // check does not break shapes outline
        //            for (int k = 0; k < outLines.Count; k++)
        //            {

        //                if (outLines[k].IntersectsWith(newLine1, true, Line.Vizmode.OnlyIntersecting) || outLines[k].IntersectsWith(newLine2, true, Line.Vizmode.OnlyIntersecting))
        //                {


        //                    Debug.Log("breaks outline");
        //                                        bad = true;
        //                    break;
        //                }
        //            }
        //        }
        //        if (!bad)
        //        {
        //            int sharedLinesWithOtherIndices = 0;
        //            for (int k = 0; k < triLines.Count; k++)
        //            {
        //                if (triLines[k].BothIndexesMatchAnyDirection(newLine1) || triLines[k].BothIndexesMatchAnyDirection(newLine2) || triLines[k].BothIndexesMatchAnyDirection(lastNonProcessedPair.Key))
        //                {
        //                    Debug.Log("is first literally same line so cant break it");
        //                    if (sharedLinesWithOtherIndices == 1)
        //                    {
        //                        Debug.Log("both lines would be shared. triangle would be identical");
        //                        bad = true;
        //                        break;
        //                    }
        //                    sharedLinesWithOtherIndices++;
        //                    continue;
        //                }
        //                if (triLines[k].IntersectsWith(newLine1, true, Line.Vizmode.OnlyIntersecting) || triLines[k].IntersectsWith(newLine2, true, Line.Vizmode.OnlyIntersecting))
        //                {
        //                    DebugDraw.Cylinder(triLines[k].pointA, triLines[k].pointB, Color.red, "breaks this indice", width: 2.2f, debugLog: true);
        //                    bad = true;
        //                    break;
        //                }
        //            }

        //        }
        //    }
        //    if (!bad)
        //    {
        //        yield return new StepResult(SearchStepResultType.FoundIt);
        //    }
        //    if (!bad)
        //    {
        //        thirdCorner = possibleVertIndexes[j];
        //        break;
        //    }
    }



    public class IntersectWithListOfLinesJob : MultiProcessSearchAlgorithm<Line>
    {
        [ImportantAlgoProgress]
        List<Line> listOfLines;
        Vector3 pos;
        Vector3 middleOfIndice;

        public IntersectWithListOfLinesJob(List<Line> listOfLines, Vector3 pos, Vector3 middleOfIndice)
        {
            this.listOfLines = listOfLines;
            this.pos = pos;
            this.middleOfIndice = middleOfIndice;
        }

        public override int OpenCount
        {
            get { return listOfLines.Count; }
        }

        public override Line Current
        {
            get
            {
                return listOfLines[listOfLines.Count - 1];
            }
        }
        public override void CloseCurrent()
        {
            listOfLines.RemoveAt(listOfLines.Count - 1);
        }

        protected override void VizCurrentState()
        {
            DebugDraw.Cylinder(pos, middleOfIndice, Color.yellow, "possible intersector", width: 2f, time: 0f);
            if (lastStepResult.searchStepResultType == SearchStepResultType.FoundIt)
            {
                DebugDraw.Cylinder(Current.pointA, Current.pointB, Color.red, "bad intersection", width: 2f, time: 0f);
            }
            else
            {
                DebugDraw.Cylinder(Current.pointA, Current.pointB, Color.green, "ok", width: 2f, time: 0f);
            }
        }

        public override StepResult EvaluateCurrentResult()
        {
            Vector2 intersection;
            if (Line.LineSegmentsIntersection(Line.V3ToV2OnXZ(middleOfIndice), Line.V3ToV2OnXZ(pos), Line.V3ToV2OnXZ(Current.pointA), Line.V3ToV2OnXZ(Current.pointB), out intersection))
            {
                Vector3 intersectionPoint = new Vector3(intersection.x, 0, intersection.y);
                // toivo 25.7.2018 bad way to do this because it will ignore if there is any lines that would actually be trouble
                if (intersectionPoint.IsApproximitelySame(pos))
                {
                    // intersection point is the target point. means its not actually intesecting but connecting
                    return new StepResult(SearchStepResultType.Continue);
                }
                if (intersectionPoint.IsApproximitelySame(middleOfIndice))
                {
                    return new StepResult(SearchStepResultType.Continue);
                }
                return new StepResult(SearchStepResultType.FoundIt);
            }
            return new StepResult(SearchStepResultType.Continue);
        }

        public override StepResult PostProcessSubJob(IMultiProcessSearchAlgorithm job)
        {
            throw new System.NotImplementedException();
        }
    }


}