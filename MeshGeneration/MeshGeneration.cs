﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ToivonRandomUtils;
using System.Linq;
using System;
using System.ComponentModel;
using ToivoLinq;

namespace ToivoMeshGeneration
{
    public static class MeshGenerationUtils
    {
        public static MeshFilter MakeGameObjectWithReadyMeshFilter()
        {
            var go = new GameObject();
            go.AddComponent<MeshRenderer>();
            return go.AddComponent<MeshFilter>();
        }

    }

    [System.Serializable]
    public class MeshGenerationData
    {
        public List<Vector3> verts = new List<Vector3>();
        public List<int> tris = new List<int>();
        public List<Vector3> normals = new List<Vector3>();
        public List<Vector2> uvs = new List<Vector2>();
        public List<Color> colors;
        private List<Color> vertColors;

        public void AddVertexColor(Color color)
        {
            if (vertColors == null)
            {
                vertColors = new List<Color>();
            }

            vertColors.Add(color);
        }

        public Mesh Mesh
        {
            get
            {
                Mesh mesh = new Mesh();
                mesh.vertices = verts.ToArray();
                mesh.triangles = tris.ToArray();
                mesh.normals = normals.ToArray();
                mesh.uv = uvs.ToArray();
                if (vertColors != null)
                {
                    mesh.colors = vertColors.ToArray();
                }

                return mesh;
            }
        }

        public bool ValidateTriangles()
        {
            bool valid = true;
            foreach (var item in tris.Where(x => x >= verts.Count || x < 0))
            {
                Debug.LogError("invalid triagle vert reference. referecing vert " + item + " at index of " +
                               tris.IndexOf(item) + ". vertex count " + verts.Count + " tris count " + tris.Count);
                valid = false;
            }

            return valid;
        }

        /// <summary>
        /// Appends the data and takes care of triangle indexes
        /// </summary>
        public void AppendData(MeshGenerationData mgd)
        {
            int startingVerts = verts.Count;
            verts.AddRange(mgd.verts);
            normals.AddRange(mgd.normals);
            if (mgd.colors != null)
                if (colors != null)
                    colors.AddRange(mgd.colors);
                else
                    colors = mgd.colors.ToList();
            uvs.AddRange(mgd.uvs);
            tris.AddSubRange(mgd.tris, x => x + startingVerts);
        }

        public void Visualize(Transform transform)
        {
            if (Application.isPlaying)
            {
                for (int i = 0; i < verts.Count; i++)
                {
                    (transform.position + verts[i]).ShowNotification(
                        string.Format("{0} <size=8> uv:{1}</size>", i, uvs[i]), 1f, Color.red, 0.001f);
                }

            }
        }

        /// <summary>
        /// pivot is left up, points up by default
        /// </summary>
        public void AddQuad(Vector3 position, float size, Vector2 uvStart, float uvWidth)
        {
            AddQuad(position, size, uvStart, uvWidth, Quaternion.identity);
        }

        public void AddQuad(Vector3 position, float size, Vector2 uvStart, float uvWidth, Quaternion rotation)
        {
            int indexOffSet = verts.Count;

            verts.Add(position);
            uvs.Add(uvStart);

            verts.Add(position + (rotation * (Vector3.right * size)));
            uvs.Add(uvStart + Vector2.right * uvWidth);

            verts.Add(position + (rotation * (Vector3.back * size)));
            uvs.Add(uvStart + Vector2.up * uvWidth);

            verts.Add(position + (rotation * (Vector3.right * size)) + (rotation * (Vector3.back * size)));
            uvs.Add(uvStart + Vector2.up * uvWidth + Vector2.right * uvWidth);


            tris.AddMultiple(indexOffSet + 0, indexOffSet + 1, indexOffSet + 2);
            tris.AddMultiple(indexOffSet + 1, indexOffSet + 3, indexOffSet + 2);
            var yAxis = Vector3.up;
            normals.Add(yAxis);
            normals.Add(yAxis);
            normals.Add(yAxis);
            normals.Add(yAxis);
        }

        [Flags]
        public enum CubeFaceTypes
        {
            Top = 2,
            Bottom = 4,
            Front = 8,
            Back = 16,
            Left = 32,
            Right = 64
        }

        public void AddCube(Vector3 position, CubeFaceTypes cubeFaceTypes)
        {
            if ((cubeFaceTypes & CubeFaceTypes.Top) != 0)
                AddQuad(position + new Vector3(0, 1, 1), 1f, new Vector2(), 1f, Quaternion.Euler(0, 0, 0));
            if ((cubeFaceTypes & CubeFaceTypes.Bottom) != 0)
                AddQuad(position + new Vector3(0, 0, 0), 1f, new Vector2(), 1f, Quaternion.Euler(180, 0, 0));
            if ((cubeFaceTypes & CubeFaceTypes.Back) != 0)
                AddQuad(position + new Vector3(0, 1, 0), 1f, new Vector2(), 1f, Quaternion.Euler(270, 0, 0));
            if ((cubeFaceTypes & CubeFaceTypes.Front) != 0)
                AddQuad(position + new Vector3(1, 1, 1), 1f, new Vector2(), 1f, Quaternion.Euler(270, 180, 0));
            if ((cubeFaceTypes & CubeFaceTypes.Right) != 0)
                AddQuad(position + new Vector3(1, 1, 0), 1f, new Vector2(), 1f, Quaternion.Euler(270, 180, 90));
            if ((cubeFaceTypes & CubeFaceTypes.Left) != 0)
                AddQuad(position + new Vector3(0, 1, 1), 1f, new Vector2(), 1f, Quaternion.Euler(270, 180, -90));

        }


        public static Vector3Int FaceTypeToDirectionIntVector3(CubeFaceTypes faceTypes)
        {
            switch (faceTypes)
            {
                case CubeFaceTypes.Top:
                    return Vector3Int.up;
                case CubeFaceTypes.Bottom:
                    return Vector3Int.down;
                case CubeFaceTypes.Front:
                    return Vector3Int.forward;
                case CubeFaceTypes.Back:
                    return Vector3Int.back;
                case CubeFaceTypes.Left:
                    return Vector3Int.left;
                case CubeFaceTypes.Right:
                    return Vector3Int.right;
                default:
                    Debug.LogError(faceTypes);
                    throw new ArgumentOutOfRangeException(nameof(faceTypes), faceTypes, null);
            }
        }

        public static CubeFaceTypes DirectionIntVector3ToFaceType(Vector3Int vector3Int)
        {
            if (vector3Int == Vector3Int.up) return CubeFaceTypes.Top;
            if (vector3Int == Vector3Int.down) return CubeFaceTypes.Bottom;
            if (vector3Int == Vector3Int.right) return CubeFaceTypes.Right;
            if (vector3Int == Vector3Int.left) return CubeFaceTypes.Left;
            if (vector3Int == Vector3Int.forward) return CubeFaceTypes.Front;
            if (vector3Int == Vector3Int.back) return CubeFaceTypes.Back;
            throw new InvalidEnumArgumentException();
        }


       




        //		public enum ExpansionDirection
            //		{
            //			Right,
            //			Down
            //		}
            //		Vector3 ExpDir2V3(ExpansionDirection expDir){
            //			switch (expDir) {
            //			case ExpansionDirection.Right:
            //				return Vector3.right;
            //			case ExpansionDirection.Down:
            //				return Vector3.down;
            //			}
            //			throw System.ArgumentException ();
            //		}
            //		Vector2 ExpDir2V2(ExpansionDirection expDir){
            //			switch (expDir) {
            //			case ExpansionDirection.Right:
            //				return Vector2.right;
            //			case ExpansionDirection.Down:
            //				return Vector2.down;
            //			}
            //			throw System.ArgumentException ();
            //		}
            /// <summary>
            /// pivot is left up, points up
            /// usage: youll have to start with addquad and end with addquad. or something
            /// since this method refers previous and future verts.
            /// so after this method the mesh is invalid state
            /// </summary>
            public void ContinueQuadLineToRight(Vector3 position, float size, Vector2 uvStart, float uvWidth,
                bool validationMode = false)
            {
                verts.Add(position + (Vector3.right * size));
                uvs.Add(uvStart + Vector2.right * uvWidth);

                verts.Add(position + (Vector3.right * size) + (Vector3.back * size));
                uvs.Add(uvStart + Vector2.up * uvWidth + Vector2.right * uvWidth);


                // toivo 23.6.2018 i tried using add quad then this method and my triangle refs were off by one
                // so added -1. i have no idea how this affects other clients
                //int indexOffSet = verts.Count;
                int indexOffSet = verts.Count;

                tris.AddMultiple(indexOffSet - 2, indexOffSet + 1, indexOffSet);
                //TryCheckTris(validationMode);
                tris.AddMultiple(indexOffSet, indexOffSet + 1, indexOffSet + 2);
                //TryCheckTris(validationMode);
                ContinueQuad();

            }

            private void TryCheckTris(bool validationMode)
            {
                if (validationMode && !ValidateTriangles())
                {
                    throw new System.Exception("invalid triangle vert refs");
                }
            }

            public void ContinueWithNewQuadRow(Vector3 position, float size, Vector2 uvStart, float uvWidth,
                int prevRowIndex)
            {
                int indexOffSet = verts.Count;

                verts.Add(position + (Vector3.down * size));
                uvs.Add(uvStart + Vector2.up * uvWidth);

                verts.Add(position + (Vector3.right * size) + (Vector3.back * size));
                uvs.Add(uvStart + Vector2.up * uvWidth + Vector2.right * uvWidth);

                tris.AddMultiple(prevRowIndex + 2, indexOffSet + 1, indexOffSet);
                tris.AddMultiple(indexOffSet, indexOffSet + 1, indexOffSet + 2);

                ContinueQuad();

            }

            void ContinueQuad()
            {

                normals.Add(Vector3.up);
                normals.Add(Vector3.up);
            }


            public MeshGenerationData DeepCopy()
            {
                /*       public List<Vector3> verts = new List<Vector3>();
                      public List<int> tris = new List<int>();
                      public List<Vector3> normals = new List<Vector3>();
                   public List<Vector2> uvs = new List<Vector2>();
                      public List<Color> colors;
                   private List<Color> vertColors;*/

                var newData = new MeshGenerationData();
                newData.verts = verts.ToList();
                newData.tris = tris.ToList();
                newData.normals = normals.ToList();
                newData.uvs = uvs.ToList();
                if (colors != null) newData.colors = colors.ToList();
                if (vertColors != null) newData.vertColors = vertColors.ToList();
                return newData;
            }

            /// <summary>
            /// usage: if youre starting with nothing create new meshgenselection and populate it with CaptureAddedVertex
            /// meshgenselection allows this method to be called multiple times
            /// </summary>
            /// <param name="originalEdgeSelection"></param>
            /// <param name="closeLoop"></param>
            /// <param name="yOffset"></param>
            /// <returns></returns>
            public IEnumerator<MeshGenSelection> ExtrudeEdgeFromPoints(MeshGenSelection originalEdgeSelection,
                bool closeLoop, float yOffset = 1f)
            {
                MeshGenSelection newSel = new MeshGenSelection();
                for (int i = 0; i < originalEdgeSelection.selectedVertIndexes.Count - 1; i++)
                {
                    newSel.BeginHot();

                    /*
                     *  toivo 24.6.2018 this party is about to get crazy
                     *  i have to mix old and new indexes 
                     * tris.AddMultiple(verts.Count, verts.Count + 3, verts.Count + 1);
                      tris.AddMultiple(verts.Count + 1, verts.Count + 3, verts.Count +2 );
                      */

                    //        newSel.CaptureAddedTriangleDefinition(this,originalEdgeSelection.selectedVertIndexes[i], verts.Count+1, nextIndex);
                    newSel.CaptureAddedTriangleDefinition(this, originalEdgeSelection.selectedVertIndexes[i],
                        verts.Count + 1, verts.Count);
                    newSel.CaptureAddedTriangleDefinition(this, verts.Count + 1,
                        originalEdgeSelection.selectedVertIndexes[i], originalEdgeSelection.selectedVertIndexes[i + 1]);

                    newSel.CaptureAddedVertex(this,
                        (verts[originalEdgeSelection.selectedVertIndexes[i]] + (Vector3.up * yOffset)));
                    newSel.CaptureAddedVertex(this,
                        verts[originalEdgeSelection.selectedVertIndexes[i + 1]] + (Vector3.up * yOffset));
                    ValidateTriangles();
                    yield return newSel;
                    //                verts.Add(points[nextIndex]);
                    //                verts.Add(points[i]);
                }

                if (closeLoop)
                {
                    newSel.CaptureAddedTriangleDefinition(this, newSel.selectedVertIndexes[0],
                        newSel.selectedVertIndexes.Last(), originalEdgeSelection.selectedVertIndexes.Last());
                    newSel.CaptureAddedTriangleDefinition(this, newSel.selectedVertIndexes[0],
                        originalEdgeSelection.selectedVertIndexes.Last(), originalEdgeSelection.selectedVertIndexes[0]);
                }

                yield return newSel;
            }





            public IEnumerator DetectInnerHoleEdges(List<Line> outLines, List<Line> triangleLines,
                List<Line> outputtedUniqueLines)
            {
                outputtedUniqueLines.Clear();

                /*
                outputtedUniqueLines.AddRange(triangleLines);
                outputtedUniqueLines.RemoveAll(x => outLines.Any(y => y.BothIndexesMatchAnyDirection(x)));
                var groups = outputtedUniqueLines.GroupBy(x => x.pointAIndex + "_" + x.pointBIndex);
                foreach (var group in groups)
                {
                    var list = group.ToList();
                    for(int i = 1; i < list.Count; i++)
                    {
                        outputtedUniqueLines.Remove(list[i]);
                    }
                }
    
                for (int i = 0; i < outputtedUniqueLines.Count; i++)
                {
                    DebugDraw.Cylinder(outputtedUniqueLines[i].pointA, outputtedUniqueLines[i].pointB, Color.blue, "sanity check " + i, width: 2f, time: 0);
                    yield return null;
                }
                */



                for (int i = 0; i < triangleLines.Count; i++)
                {
                    var item = triangleLines[i];
                    DebugDraw.Sphere(item.pointA, 3f, Color.yellow, "current point a");
                    DebugDraw.Sphere(item.pointB, 3f, Color.yellow, "current point b");
                    if (outLines.FastAny(x => x.BothIndexesMatchAnyDirection(item)))
                    {
                        DebugDraw.Cylinder(item.pointA, item.pointB, Color.gray, "part of outline", width: 1f);
                        yield return null;
                        continue;
                    }

                    bool isUnique = true;
                    for (int j = outputtedUniqueLines.Count - 1; j >= 0; j--)
                    {
                        var snowflake = outputtedUniqueLines[j];
                        if (snowflake.BothIndexesMatchAnyDirection(item))
                        {
                            isUnique = false;
                            outputtedUniqueLines.RemoveAt(j);
                            DebugDraw.Cylinder(snowflake.pointA, snowflake.pointB, Color.red, "no longer unique line",
                                width: 2f);
                            break;
                        }
                    }

                    if (isUnique)
                    {
                        outputtedUniqueLines.Add(item);
                        DebugDraw.Cylinder(item.pointA, item.pointB, Color.green, "unique line", width: 1f);
                    }

                    yield return null;
                }

                for (int i = 0; i < outputtedUniqueLines.Count; i++)
                {
                    DebugDraw.Cylinder(outputtedUniqueLines[i].pointA, outputtedUniqueLines[i].pointB, Color.blue,
                        "sanity check " + i, width: 2f, time: 0);
                }

                Debug.Break();
                yield return null;

            }

            List<List<Line>> OrderIntoHoleGroups(List<Line> lines)
            {
                List<List<Line>> holeGroups = new List<List<Line>>();
                if (lines.Count == 0)
                {
                    Debug.Log("zero lines wtf");
                    return holeGroups;
                }

                var remainingLines = lines.ToList();
                bool createNewGroup = true;
                List<Line> currentGroup = null;
                while (true)
                {
                    if (createNewGroup)
                    {
                        currentGroup = new List<Line>();
                        holeGroups.Add(currentGroup);
                        currentGroup.Add(remainingLines[remainingLines.Count - 1]);
                        remainingLines.RemoveAt(remainingLines.Count - 1);
                    }

                    createNewGroup = true;
                    for (int i = 0; i < remainingLines.Count; i++)
                    {
                        if (currentGroup.Any(x => x.AnyIndexesMatchAnyDirection(remainingLines[i])))
                        {
                            currentGroup.Add(remainingLines[i]);
                            createNewGroup = false;
                            remainingLines.RemoveAt(i);
                            break;
                        }
                    }

                    if (remainingLines.Count == 0) break;
                }

                holeGroups.RemoveAll(x => x.Count == 1);

                return holeGroups;
            }

            public static GameObject CreateGameObject(Material mat, Mesh mesh)
            {
                var go = new GameObject("created mesh");
                go.AddComponent<MeshRenderer>().sharedMaterial = mat;
                go.AddComponent<MeshFilter>().mesh = mesh;
                return go;
            }


        }

        public class Line
        {
            public int pointAIndex;

            public Vector3 pointA
            {
                get { return verts[pointAIndex]; }
            }

            public int pointBIndex;

            public Vector3 pointB
            {
                get { return verts[pointBIndex]; }
            }

            public float maxZ;
            public List<Vector3> verts;

            public Vector3 Middle
            {
                get { return (pointA + pointB) * 0.5f; }
            }

            public Line(int pointAIndex, int pointBIndex, List<Vector3> verts)
            {
                this.pointAIndex = pointAIndex;
                this.pointBIndex = pointBIndex;
                this.verts = verts;
                try
                {
                    maxZ = pointA.z > pointB.z ? pointA.z : pointB.z;
                }
                catch (Exception EX)
                {
                    Debug.LogError("invalid line definition");
                    throw EX;
                }

            }

            public bool BothIndexesMatchAnyDirection(Line line)
            {
                return (pointAIndex == line.pointAIndex && pointBIndex == line.pointBIndex) ||
                       (pointAIndex == line.pointBIndex && pointBIndex == line.pointAIndex);
            }

            public bool AnyIndexesMatchAnyDirection(Line line)
            {
                return pointAIndex == line.pointAIndex || pointBIndex == line.pointBIndex ||
                       pointAIndex == line.pointBIndex || pointBIndex == line.pointAIndex;
            }

            IEnumerator<KeyValuePair<Vector3, Vector3>> PossiblePointOrders2d()
            {
                yield return new KeyValuePair<Vector3, Vector3>(V3ToV2OnXZ(pointA), V3ToV2OnXZ(pointB));
                yield return new KeyValuePair<Vector3, Vector3>(V3ToV2OnXZ(pointB), V3ToV2OnXZ(pointA));
            }

            public static Vector2 V3ToV2OnXZ(Vector3 v3)
            {
                return new Vector2(v3.x, v3.z);
            }

            public enum Vizmode
            {
                None,
                Always,
                OnlyIntersecting
            }

            public bool IntersectsWith(Line line, bool workOnXZAxis, Vizmode vizmode = Vizmode.None,
                bool intersectsIfSegmentsShareOnlyOnePoint = false)
            {
                bool canIntersect = true;
                if (!intersectsIfSegmentsShareOnlyOnePoint)
                {
                    var myPointOrders = PossiblePointOrders2d();
                    while (myPointOrders.MoveNext())
                    {
                        var oth = line.PossiblePointOrders2d();
                        while (oth.MoveNext())
                        {
                            if (myPointOrders.Current.Key == oth.Current.Key &&
                                myPointOrders.Current.Value != oth.Current.Value
                                || myPointOrders.Current.Value == oth.Current.Value &&
                                myPointOrders.Current.Key != oth.Current.Key)
                            {
                                if (vizmode == Vizmode.Always)
                                {
                                    canIntersect = false;
                                }
                                else
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }


                if (!workOnXZAxis) throw new System.NotImplementedException();
                Vector2 intersection;
                var intersects = LineSegmentsIntersection(V3ToV2OnXZ(pointA), V3ToV2OnXZ(pointB),
                    V3ToV2OnXZ(line.pointA), V3ToV2OnXZ(line.pointB), out intersection);
                bool returnValue = canIntersect && intersects;
                if (vizmode == Vizmode.Always || returnValue && vizmode == Vizmode.OnlyIntersecting)
                {
                    if (!canIntersect)
                        Debug.Log("since both lines have same origin but different endpoints they cannot intersecs");
                    DebugDraw.Cylinder(line.pointA, line.pointB, Color.white, "lineb");
                    var go = DebugDraw.Cylinder(pointA, pointB, returnValue ? Color.red : Color.green, "linea");
                    if (intersects)
                        DebugDraw.Sphere(new Vector3(intersection.x, 0, intersection.y), 1f, Color.yellow,
                            "intersection point");
                }

                return returnValue;
            }

            //https://github.com/setchi/Unity-LineSegmentsIntersection/blob/master/Assets/LineSegmentIntersection/Scripts/Math2d.cs
            //setchi
            // MIT
            public static bool LineSegmentsIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector3 p4,
                out Vector2 intersection)
            {
                intersection = Vector2.zero;

                var d = (p2.x - p1.x) * (p4.y - p3.y) - (p2.y - p1.y) * (p4.x - p3.x);

                if (d == 0.0f)
                {
                    return false;
                }

                var u = ((p3.x - p1.x) * (p4.y - p3.y) - (p3.y - p1.y) * (p4.x - p3.x)) / d;
                var v = ((p3.x - p1.x) * (p2.y - p1.y) - (p3.y - p1.y) * (p2.x - p1.x)) / d;

                if (u < 0.0f || u > 1.0f || v < 0.0f || v > 1.0f)
                {
                    return false;
                }

                intersection.x = p1.x + u * (p2.x - p1.x);
                intersection.y = p1.y + u * (p2.y - p1.y);

                return true;
            }



        }

        public class MeshGenSelection
        {
            public List<int> selectedVertIndexes = new List<int>();
            public List<int> triangleDefinitions = new List<int>();

            public List<HotModification> hotMods;

            public class HotModification
            {
            }

            public class HotVert : HotModification
            {
                public int addedVertIndex;
            }

            public class HotTriangle : HotModification
            {
                public int[] vertIndexes;
            }

            public void BeginHot()
            {
                hotMods = new List<HotModification>();
            }

            public void CaptureAddedVertex(MeshGenerationData meshGenerationData, Vector3 vertex)
            {
                selectedVertIndexes.Add(meshGenerationData.verts.Count);
                if (hotMods != null) hotMods.Add(new HotVert {addedVertIndex = meshGenerationData.verts.Count});

                meshGenerationData.verts.Add(vertex);
            }

            public void CaptureAddedTriangleDefinition(MeshGenerationData meshGenerationData, params int[] addedIndexes)
            {
                if (hotMods != null) hotMods.Add(new HotTriangle {vertIndexes = addedIndexes});
                triangleDefinitions.AddRange(addedIndexes);
                meshGenerationData.tris.AddRange(addedIndexes);
            }

            public void VisualizeHot(MeshGenerationData genData, float time = 0.16f)
            {

                for (int i = 0; i < hotMods.Count; i++)
                {

                    var hotVert = hotMods[i] as HotVert;
                    var hotTriangle = hotMods[i] as HotTriangle;
                    if (hotVert != null)
                    {
                        DebugDraw.Cube(genData.verts[hotVert.addedVertIndex], time, Color.yellow,
                            "vertex " + hotVert.addedVertIndex + " " + genData.verts[hotVert.addedVertIndex] +
                            " operation index " + i);
                    }

                    if (hotTriangle != null)
                    {
                        int[] indexes = hotTriangle.vertIndexes;
                        indexes.Aggregate(new Vector3(), (x, y) => genData.verts[y] + x)
                            .MultiplyAxises(0.333f, 0.333f, 0.333f).ShowNotification(
                                "op " + i + ", triangle between indexes" + indexes.GivePrintOut(), time, Color.yellow);
                        for (int j = 0; j < indexes.Length; j++)
                        {
                            int next = j + 1;
                            if (next == indexes.Length) next = 0;
                            DebugDraw.Cylinder(genData.verts[indexes[j]], genData.verts[indexes[next]], Color.cyan,
                                "edge from " + indexes[j] + " to " + indexes[next] + " in op " + i, time);
                        }
                    }

                }
            }


        }
    }    
