﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ToivoMeshGeneration;
using ToivonRandomUtils;

public class PropCreator : MonoBehaviour
{
    public float widthLength = 1f;
    public float height = 1f;

    public Vector3 innerQuadScale = new Vector3(0.4f, 0.8f, 0.5f);
    public float smoothMul = 0;
    void CreateMesh(MeshGenerationData meshGenData)
    {

        // frontest quad

        Vector3 rightBackCorner = new Vector3(widthLength, 0, widthLength);
        Vector3 leftBackCorner = new Vector3(0, 0, widthLength);

        meshGenData.verts.Add(new Vector3(0, 0, 0));
        float innerQuadUVHeight;
        if (height > widthLength)
        {
            innerQuadUVHeight = 1f - (widthLength / height * 0.5f);
        }
        else
        {
            innerQuadUVHeight = (height / widthLength);
        }
        Debug.Log(innerQuadUVHeight);
        meshGenData.uvs.Add(new Vector2(.25f, 0));


        meshGenData.verts.Add(new Vector3(widthLength, 0, 0));
        meshGenData.uvs.Add(new Vector2(.75f, 0));


        Vector3 rightFrontUpPos = Vector3.Lerp( new Vector3(widthLength, height, 0), rightBackCorner, smoothMul);
        meshGenData.verts.Add(rightFrontUpPos);
        meshGenData.uvs.Add(new Vector2(0.75f, innerQuadUVHeight));


        Vector3 leftFrontUpPos = Vector3.Lerp(new Vector3(0, height, 0), leftBackCorner, smoothMul);
        meshGenData.verts.Add(leftFrontUpPos);
        meshGenData.uvs.Add(new Vector2(0.25f, innerQuadUVHeight));







        meshGenData.tris.AddMultiple(0, 3, 1);
        meshGenData.tris.AddMultiple(1, 3, 2);




        // upper quad

        meshGenData.verts.Add(new Vector3(0, height, widthLength));
        meshGenData.uvs.Add(Vector2.up);
        meshGenData.verts.Add(new Vector3(widthLength, height, widthLength));
        meshGenData.uvs.Add(Vector2.right + Vector2.up);


        meshGenData.tris.AddMultiple(4, 5, 3);
        meshGenData.tris.AddMultiple(3, 5, 2);






        // right quad

        meshGenData.verts.Add(rightBackCorner);
        meshGenData.uvs.Add(Vector2.right);
        meshGenData.tris.AddMultiple(1, 2, 6);
        meshGenData.tris.AddMultiple(2, 5, 6);

        // left quad

        meshGenData.verts.Add(leftBackCorner);
        meshGenData.uvs.Add(Vector2.zero);
        meshGenData.tris.AddMultiple(4, 3, 7);
        meshGenData.tris.AddMultiple(7, 3, 0);




        for (int i = 0; i < 8; i++)
        {
            var vert = meshGenData.verts[i];
            vert.x -= widthLength * 0.5f;
            vert.z -= widthLength;
            meshGenData.verts[i] = vert;
        }

        for (int i = 0; i < 4; i++)
        {
            var vert = meshGenData.verts[i];
            vert = vert.MultiplyAxises(innerQuadScale);
            meshGenData.verts[i] = vert;
        }


        meshGenData.Visualize(transform);


    }

    

    [ToivonRandomUtils.InspectorButton]
    void Create()
    {
        throw new System.NotImplementedException("5.4.2018 had bad merge conflict");
     /*   MeshGenerationData meshGenData = new MeshGenerationData();
        //		meshGenData.AddQuad (new Vector3 (), 1f, Vector2.zero, 1f);
        CreateMesh(meshGenData);
        //		meshGenData.AddQuad(Vector3.zero, 1f, Vector2.zero,1f);


        Mesh mesh = meshGenData.Mesh;
        mesh.RecalculateNormals();
        gameObject.GetOrAddComponent<MeshRenderer>();
        if (Application.isPlaying)
        {
            gameObject.GetOrAddComponent<MeshFilter>().mesh = mesh;
        }
        else
        {
            var meshFilter = gameObject.GetOrAddComponent<MeshFilter>();
            string oldAssetPath = null;
            if (meshFilter.sharedMesh != null)
            {
#if UNITY_EDITOR
                if (UnityEditor.EditorUtility.DisplayDialog("delete old mesh asset?", "this is probly undoable", "yes"))
                {
                    oldAssetPath = UnityEditor.AssetDatabase.GetAssetPath(meshFilter.sharedMesh);
                    System.IO.File.Delete(oldAssetPath);
                    UnityEditor.AssetDatabase.Refresh();
                }
#endif
            }
            string assetName = "prop_" + height + "m";
            assetName.Replace('.', '_');
#if UNITY_EDITOR
            var mesh2 = ToivonRandomUtils.RandomUtils.CreateAssetObject(mesh, assetName, oldAssetPath) as Mesh;
            meshFilter.mesh = mesh2;
#endif
        }
        */
    }
}
