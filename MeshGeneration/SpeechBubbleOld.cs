﻿//using UnityEngine;
//using System.Collections;
//using ToivonRandomUtils;
//using ToivoMeshGeneration;
//
//namespace ToivoMeshGeneration
//{
//	[RequireComponent(typeof(RectTransform))]
//	[RequireComponent (typeof(CanvasRenderer))]
//	[ExecuteInEditMode]
//	public class SpeechBubble : MonoBehaviour
//	{
//		public Mesh mesh;
//		public Material material;
//		CanvasRenderer canvasRenderer;
//		RectTransform rectTransform;
//		public SmoothCornerData cornerData;
//		public Vector3[] fourCorners = new Vector3[4];
//		void Start ()
//		{
//			rectTransform = GetComponent<RectTransform> ();
//			canvasRenderer = GetComponent<CanvasRenderer> ();
//		}
//
//		void Update ()
//		{
//			rectTransform.GetLocalCorners(fourCorners);
//			canvasRenderer.materialCount = 1;
//			canvasRenderer.SetMaterial (material,0);
//			mesh = DrawCorner (cornerData).Mesh;
//			canvasRenderer.SetMesh (mesh);
//
//		}
//
//		public static MeshGenerationData DrawCorner (SmoothCornerData data)
//		{
//			var meshGenData = new MeshGenerationData ();
//			meshGenData.verts.AddMultiple (Vector3.up, Vector3.right, Vector3.left);
//			meshGenData.tris.AddMultiple (0,1, 2);
//
//			meshGenData.normals.AddMultiple (Vector3.back, Vector3.back, Vector3.back);
//
//			return meshGenData;
//		}
//	}
//
//	public class SmoothCornerData
//	{
//		public Vector3 pointA, pointB, pointC;
//		public int smoothingVertices;
//	}
//}