﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ToivonRandomUtils;

public class DebugLogPhysicsEvents : MonoBehaviour
{
	public string prefix;

	public bool doLog = true;

	public bool returnIfColTargetHasThisComponent = true;
    public bool spamVelocity;


    private void FixedUpdate()
    {
        if (spamVelocity) Log(" spam velocity: " + GetComponent<Rigidbody>().velocity, null);
    }
    [System.Serializable]
	public class HitCollider
	{
		public string key;
		public Collider col;
	}

	public List<HitCollider> lastHitColliders = new List<HitCollider> ();

	void OnCollisionEnter (Collision collision)
	{
		Log (" collision enter"+" impulse:"+collision.impulse, collision.collider);
	}

	void OnCollisionExit (Collision collision)
	{
		Log (" collision exit", collision.collider);
	}

	void OnCollisionStay (Collision collision)
	{
		Log (" collision stay", collision.collider);
	}

	void OnTriggerEnter (Collider col)
	{
		Log (" trigger enter", col);
	}

	void OnTriggerExit (Collider col)
	{
		Log (" trigger exit", col);
	}

	void OnTriggerStay (Collider col)
	{
		Log (" trigger stay", col);

	}

	void OnEnable() {
		Log (" OnEnable", null);
	}

	void OnDisable(){
		//Debug.Log ("DebugLogPhysicsEvents disable", gameObject);
		Log (" OnDisable", null);
	}
	void OnDestroy(){
		//Debug.Log ("DebugLogPhysicsEvents destroy", gameObject);
		Log (" OnDestroy", null);
	}

	void Log (string str, Collider col)
	{
		//if (returnIfColTargetHasThisComponent || col.GetComponent<DebugLogPhysicsEvents> () == null) {

		if (doLog)
			Debug.Log (string.Format ("DebugLogPhysicsEvents {0} {1}{2} {3}", prefix, col.NullSafeToString (), str, gameObject.name), gameObject);

		if (col != null) {
			var lastHitColsListItem = lastHitColliders.SingleOrDefault (x => x.key == str);
			if (lastHitColsListItem == null) {
				lastHitColliders.Add (new HitCollider (){ key = str, col = col });
			} else {
				lastHitColsListItem.col = col;
			}
		}

		//}
	}
}
