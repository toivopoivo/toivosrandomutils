﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace CSVParsing
{
	public static class CSVDataParsing
	{

		public static List<string> GetCSVColumn (this string csvData, string columnKey)
		{
			string[] lines = csvData.Split (new string[] {System.Environment.NewLine }, System.StringSplitOptions.RemoveEmptyEntries);
			List<string> data = new List<string> ();
			var rows = lines [0].Split (new char[]{',', ';'}, System.StringSplitOptions.None);
			int columnIndex = 0;
			bool rowFound = false;
			for (int i = 0; i < rows.Length; i++) {
				if (rows [i] == columnKey) {
					columnIndex = i;
					rowFound = true;
				}
			}
			if (!rowFound)
				Debug.LogError ("Could not find the specified column");
			for (int i = 1; i < lines.Length; i++) {	
				rows = lines [i].Split (new char[]{',', ';'}, System.StringSplitOptions.None);
				data.Add (rows [columnIndex]);
			}
			return data;
		}

		public static Dictionary<string, string> GetCSVRow (this string csvData, int index=1)
		{
			if (index == 0)
				Debug.Log ("index 0 should be reseverved for header values. data rows should start at 1");
			string[] lines = csvData.Split (new string[] {System.Environment.NewLine }, System.StringSplitOptions.RemoveEmptyEntries);
			List<string> keys = new List<string> ();
			var rows = lines [0].Split (new char[]{',', ';'}, System.StringSplitOptions.None);
//			int rowIndex = 0;
//			bool rowFound = false;

			for (int i = 0; i < rows.Length; i++) {
				keys.Add (rows [i]);
			}
			rows = lines [index].Split (new char[]{',', ';'}, System.StringSplitOptions.None);
			Dictionary<string,string> rDic = new Dictionary<string, string> ();
			for (int i = 0; i < rows.Length; i++) {
				rDic.Add (keys [i], rows [i]);
			}
		
			return rDic;
		}
        public class ParsedObject
        {
            protected string strVal; 
            protected int? intVal; 
            protected float? floatVal;

            public ParsedObject(string strVal=null, int? intVal=null, float? floatVal=null){
                this.strVal = strVal;
                this.intVal = intVal;
                this.floatVal = floatVal;
             }
            public static explicit operator float(ParsedObject x){
                return (float)x.floatVal;
            }
            public static explicit operator int(ParsedObject x)
            {
                return (int)x.intVal;
            }
            public static explicit operator string(ParsedObject x)
            {
                return x.strVal.ToString();
            }
			public object GetValue(System.Type type)
			{
				if (type == typeof(System.String))
				{
					return strVal as object;
				}
				if (type == typeof(System.Int32))
				{
					return intVal as object;
				}
				if (type == typeof(float))
				{
					return floatVal as object;
				}
				return false;
			}
        }

        
		public static ParsedObject ParseObject(string headerKey,string dataValue ){
			var prefixAndName= headerKey.Split(new char[]{'_'},System.StringSplitOptions.None);
//			string name=prefixAndName[0];
			string prefix=prefixAndName[1];
			
			if(prefix=="srt"||prefix=="str"){//HAVUMÄKIKALLU: pakko tehä turhii iffei ku oon niin kehari etten osaa kirjottaa
				prefix="string";
			} 
//			classDataFields+="\tpublic "+prefix+" "+name+";\n";

                int intenger=0;
                float floatingPoint=0;
                if(  prefix == "int"){

                    return new ParsedObject(null, int.TryParse(dataValue, out intenger)?intenger:-1, null);
					
                }else if( prefix == "float"){

                    return new ParsedObject(null, null, float.TryParse(dataValue, out floatingPoint)?floatingPoint:floatingPoint);
					
                }else{
                    return new ParsedObject(dataValue, null, null);
                }
			}

        public static ParsedObject ParseObjectAtCell(this string csvData , string header, int row)
        {
            return ParseObject(header ,GetCSVColumn(csvData, header)[row]);
        }
			
		public static int GetTotalRowCount(this string csvData){
			 string[] csv = csvData.Split (new string[] {System.Environment.NewLine }, System.StringSplitOptions.RemoveEmptyEntries);
             return csv.Length;
		}

	}
}
