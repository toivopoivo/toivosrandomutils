﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
//public enum FieldType{String, Int}
using System;


[ExecuteInEditMode]
public class CSVParser : MonoBehaviour {
	public string parsedClassName="CSVParsed";
	public string scriptFolder="/CSVData/";
//	public bool refresh=false;
	public bool create=false;
	public TextAsset csvFile;
	public TextAsset templateFile;

	// Update is called once per frame
#if UNITY_EDITOR
	void Update () {

		if(create){
			CreateUnits();
			create=false;
			enabled=false;
		}
	}
	void CreateUnits(){
		string classDataFields = "";
		string[] lines = csvFile.text.Split(new string[] {System.Environment.NewLine }, StringSplitOptions.None);
		var rows= lines[0].Split(new string[]{";",","},System.StringSplitOptions.None);
		foreach (var item2 in rows) {
			var prefixAndName= item2.Split(new char[]{'_'},System.StringSplitOptions.None);
			string name=prefixAndName[0];
			string prefix=prefixAndName[1];

			if(prefix=="srt"||prefix=="str"){//HAVUMÄKIKALLU: pakko tehä turhii iffei ku oon niin kehari etten osaa kirjottaa
				prefix="string";
			} 
			classDataFields+="\tpublic "+prefix+" "+name+";\n";
		}
		StreamWriter sw= new StreamWriter(Application.dataPath+scriptFolder+parsedClassName+"s.cs",false);
		string templateFileString = templateFile.text;
		templateFileString=templateFileString.Replace ("[ClassName]", parsedClassName);
		templateFileString=templateFileString.Replace ("[ClassDataFields]", classDataFields);
		sw.Write(templateFileString);
		sw.Close();
	}



//	public static string ParseColumnPrefix(string csv, int columnIndex) {
//		string[] lines = csv.Split(new string[] {System.Environment.NewLine }, StringSplitOptions.None);
//		var rows= lines[0].Split(new string[]{";",","},System.StringSplitOptions.None);
//
//		var prefixAndName= rows[columnIndex].Split(new char[]{'_'},System.StringSplitOptions.None);
//		string prefix=prefixAndName[1];
//		
//		if(prefix=="srt"||prefix=="str"){//HAVUMÄKIKALLU: pakko tehä turhii iffei ku oon niin kehari etten osaa kirjottaa
//			prefix="string";
//		} 
//	 	return prefix;
//	}
//	public static string GetColumnTypeCode(string csv, int columnIndex){
//		string prefix = ParseColumnPrefix(csv,columnIndex);
//		string typeCode = ""; 
//		if (prefix == "int") {
//			typeCode = "System.TypeCode.Int32";
//		} else if (prefix == "str") {
//			typeCode = "System.TypeCode.String";
////		} else if(float) {
////			typeCode = System.TypeCode.;
//		}
//		return typeCode;
//	}
#endif
}